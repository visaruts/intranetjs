import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import "./assets/css/main.scss";
import "./assets/css/main.sass";
import "./assets/js/main";
import Parser from "html-react-parser";
// import { CircleArrow as ScrollUpButton}   from "react-scroll-up-button";
import { TinyButton as ScrollUpButton } from "react-scroll-up-button";
import { lang } from "./Lang.jsx";

class Footer extends Component {
  render() {
    var now = new Date();
    const { hiddenFooter } = this.props.configReducer;
    return (
      <div>
        {hiddenFooter ? <div></div> :
          <footer className="footer">
            <div className="columns">
              <div className="column has-text-centered">
                {window.innerWidth <= 500 ? <ScrollUpButton
                  StopPosition={0}
                  ShowAtPosition={150}
                  EasingType='easeOutCubic'
                  AnimationDuration={500}
                  ContainerClassName='ScrollUpButton__Container'
                  TransitionClassName='ScrollUpButton__Toggled'
                  style={{
                    fill: "#d40032",
                    backgroundColor: "#eee",
                    border: "1px solid #d40032",
                    height: "35"
                  }}
                  showUnder={160}
                  ToggledStyle={{}}
                /> : ""}
                © Copyright {now.getFullYear()} Red Planet Hotels Limited, All Rights Reserved.
          {/* <Link to={""} title="Red Planet Hotels">Red Planet Hotels Limited, All Rights Reserved. </Link> */}
              </div>
            </div>
          </footer>
        }

      </div>

    );
  }

}
const mapStateToProps = state => {
  return { landReducer: state.langReducer, loginReducer: state.loginReducer, configReducer: state.configReducer };
};
export default connect(mapStateToProps)(Footer);
