import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";

/* Use */
import usersReducer from "./reducers/users";
import langReducer from "./reducers/lang";
import countryReducer from "./reducers/country";
import loginReducer from "./reducers/login";
import entityReducer from "./reducers/entity";
import propertyReducer from "./reducers/property";
import s3Reducer from "./reducers/s3";
import tnlistReducer from "./reducers/tnlist";
import hrReducer from "./reducers/hr";
import libraryReducer from "./reducers/library";
import contactReducer from "./reducers/contact";
import masterReducer from "./reducers/master";
import configReducer from "./reducers/config";
const reducers = combineReducers({
  usersReducer,
  langReducer,
  loginReducer,
  entityReducer,
  propertyReducer,
  tnlistReducer,
  hrReducer,
  libraryReducer,
  contactReducer,
  s3Reducer,
  countryReducer,
  masterReducer,
  configReducer
});

/*export default createStore(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(thunk)
);*/
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  reducers,
  composeEnhancer(applyMiddleware(thunk)),
);

export default store;
