import { GET_JOBTITLE, GET_JOBDEPT } from "../actions/master";

const initState = {
  code: 401,
  JobDept: {},
  JobTitle: {},
  message: ""
};

const reducer = (state = initState, action) => {
  switch (action.type) {
    case GET_JOBDEPT:
      return {
        ...state,
        ...action.payload
      };
    case GET_JOBTITLE:
      return {
        ...state,
        ...action.payload
      };
    default:
      return state;
  }
};
export default reducer;
