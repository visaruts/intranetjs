import { LOAD_LANG } from "../actions/lang";
const initState = {
  langCode: ""
};
const reducer = (state = initState, action) => {
  switch (action.type) {
    case LOAD_LANG:
      let langCode = "";
      switch (action.payload) {
        case "ja":
          langCode = action.payload;
          break;

        case "th":
          langCode = action.payload;
          break;

        default:
          langCode = "en";
          break;
      }

      return {
        ...state,
        langCode
      };

    default:
      return state;
  }
};

export default reducer;
