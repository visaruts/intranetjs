import { PROPERTY_USER, SAVE_PROPERTY } from "../actions/property";

const initState = {
  code: 401,
  entityCode: "",
  propertyCode: "",
  propertyName: "",
  entityName: "",
  property: [],
  message: ""
};
const reducer = (state = initState, action) => {
  switch (action.type) {
    case PROPERTY_USER:
      return {
        ...state,
        ...action.payload
      };
    case SAVE_PROPERTY:
      return {
        ...state,
        propertyCode: action.payload.PropertyCode,
        entityCode: action.payload.EntityCode,
        propertyName: action.payload.PropertyName,
        entityName: action.payload.EntityName
      };
    default:
      return state;
  }
};
export default reducer;
