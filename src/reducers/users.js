import { AUTH_APPLICATION_START, AUTH_APPLICATION_FINISH, AUTH_APPLICATION_ERROR
,AUTH_USER_START, AUTH_USER_FINISH, AUTH_USER_ERROR } from "../actions/users";
const initState = {
  users:[],
  application:[]
};
const reducer = (state = initState, action) => {
  switch (action.type) {
    case AUTH_APPLICATION_START:
      return state;
    case AUTH_APPLICATION_FINISH:
      return {
        ...state,
        ...action
      };
    case AUTH_APPLICATION_ERROR:
      return state;
      
    case AUTH_USER_START:
      return state;
    case AUTH_USER_FINISH:
      return {
        ...state,
        ...action
      };
    case AUTH_USER_ERROR:
      return state;

    default:
      return state;
  }
  return state;
};

export default reducer;
