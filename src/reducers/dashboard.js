import { LOADDATA,SAVEDATA } from "../actions/dashboard";

const initState = {
  data:[],
  propertyCode:"",
  search: "",
  recordsTotal: 0,
  page_start: 0,
  page_length: 25,
  status: "'Open','On-progress','Re-schedule','Re-assigned'",
  urgent: "'1','0'",
  order:"RequestDate desc",
  from:new Date(new Date().setMonth(new Date().getMonth() - 3)),
  to:new Date()
  /*order: [{ column: 12, dir: "desc" }]*/
};
const reducer = (state = initState, action) => {
  switch (action.type) {
    case LOADDATA:
      //console.log("DASHBOARD_LOADDATA");      
      return state
    case SAVEDATA:
      //console.log("DASHBOARD_SAVEDATA");
      //console.log(action);
      return {
        ...state,
        /*data: action.data,
        propertyCode: action.propertyCode,
        search: action.search,
        recordsTotal: action.recordsTotal,
        page_start: action.page_start,
        page_length: action.page_length,
        draw: action.draw,
        order: action.order*/
        ...action
      };
    default:
      return state;
  }
};
export default reducer;
