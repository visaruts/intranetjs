import { HIDDEN_NAVBAR, HIDDEN_FOOTER } from "../actions/config";
const initState = {
  hiddenNavbar: false,
  hiddenFooter: false,
};
const reducer = (state = initState, action) => {
  let hidden = "";
  switch (action.type) {
    case HIDDEN_NAVBAR:
        return {
          ...state,
          ...{hiddenNavbar:true}
        };
    case HIDDEN_FOOTER:
        return {
          ...state,
          ...{hiddenFooter:true}
        };
    default:
      return   {
        ...state
      };;
  }
};

export default reducer;
