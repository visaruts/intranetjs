import { MYACTION_LOADDATA,MYACTION_SAVEDATA } from "../actions/myaction";

const initState = {
  data:[],
  propertyCode:"",
  search: "",
  recordsTotal: 0,
  page_start: 0,
  page_length: 25,
  status: "'Open','On-progress','Re-schedule','Re-assigned'",
  urgent: "'1','0'",
  order:"RequestDate desc",
  from:new Date(new Date().setMonth(new Date().getMonth() - 3)),
  to:new Date()
};
const reducer = (state = initState, action) => {
  switch (action.type) {
    case MYACTION_LOADDATA:
      //console.log("MYACTION_LOADDATA");      
      return state
    case MYACTION_SAVEDATA:
      //console.log("MYACTION_SAVEDATA");
      //console.log(action);
      return {
        ...state,
        ...action
      };
    default:
      return state;
  }
};
export default reducer;
