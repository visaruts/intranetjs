import { S3_LOADDATA } from "actions/s3/actionS3";

const initState = {
  data:[]
};
const reducer = (state = initState, action) => {
  switch (action.type) {
    case S3_LOADDATA:
      //console.log(state);  
      return  {
        ...state,
        ...action
      }
    default:
      return state;
  }
};
export default reducer;
