import {
  AUTH_LOGIN_START,
  AUTH_LOGIN_FINISH,
  AUTH_FROM_STORAGE,
  AUTH_LOGOUT,
  AUTH_USERINFO,
  ACCESS_TOKEN_STORAGE
} from "../actions/login";

const initState = {
  request_token: "",
  code: 401,
  message: "",
  blockLogin: {},
  loading: false,
  access_token: "",
  access_token_exp: 0,
  userinfo: {}
};
const reducer = (state = initState, action) => {
  switch (action.type) {
    case AUTH_LOGIN_START:
      return {
        ...state,
        loading: true
      };
    case AUTH_LOGIN_FINISH:
      //console.log(action.payload);
      //const { request_token, code, message, blockLogin } = action.payload;
      return {
        ...state,
        loading: false,
        ...action.payload
      };
    case ACCESS_TOKEN_STORAGE:
      const access_token_data = parseJwt(action.payload.access_token);
      //console.log(access_token_data);
      return {
        ...state,
        loading: false,
        ...action.payload,
        access_token_exp: access_token_data.exp * 1000
      };
    case AUTH_FROM_STORAGE:
      return {
        ...state,
        request_token: action.payload
      };
    case AUTH_USERINFO:
      return {
        ...state,
        userinfo: action.payload
      };
    case AUTH_LOGOUT:
      return {
        ...state,
        request_token: ""
      };
    default:
      return state;
  }
};
function parseJwt(token) {
  var base64Url = token.split(".")[1];
  var base64 = base64Url.replace("-", "+").replace("_", "/");
  return JSON.parse(window.atob(base64));
}
export default reducer;
