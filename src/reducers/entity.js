import { ENTITY_USER } from "../actions/entity";

const initState = {
  code: 401,
  entity: {},
  message: ""
};

const reducer = (state = initState, action) => {
  switch (action.type) {
    case ENTITY_USER:
      return {
        ...state,
        ...action.payload.entity
      };
    default:
      return state;
  }
};
export default reducer;
