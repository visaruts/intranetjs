import { CONTACT_LOADDATA,CONTACT_GET_USER} from "actions/contact/actionContact";

const initState = {
  data: [],
};
const reducer = (state = initState, action) => {
  switch (action.type) {
    case CONTACT_LOADDATA:
      //console.log(state);  
      return {
        ...state,
        ...action
      }
    case CONTACT_GET_USER:
      return {
        ...state,
        ...action
      }

    default:
      return state;
  }
};
export default reducer;
