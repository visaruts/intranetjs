import { LOAD_COUNTRY } from "../actions/country";
const initState = {
  countryCode: ""
};
const reducer = (state = initState, action) => {
  switch (action.type) {
    case LOAD_COUNTRY:
      let countryCode = "";
      switch (action.payload) {
        case "jpn":
          countryCode = action.payload;
          break;
        case "idn":
          countryCode = action.payload;
          break;
        case "phi":
          countryCode = action.payload;
          break;
        case "tha":
          countryCode = action.payload;
          break;
        default:
          countryCode = "tha";
          break;
      }
      return {
        ...state,
        countryCode
      };
    default:
      return state;
  }
};

export default reducer;
