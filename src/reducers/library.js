import { LIBRARY_LOADDATA,LIBRARY_GET_USER} from "actions/library/actionLibrary";

const initState = {
  data: [],
  DocumentNo: "",
  Category: "",
  PageNum: "",
  PageSize: "",
  Search: "",
  CountryCode: "",
  UserCode: "",
};
const reducer = (state = initState, action) => {
  switch (action.type) {
    case LIBRARY_LOADDATA:
      //console.log(state);  
      return {
        ...state,
        ...action
      }
    case LIBRARY_GET_USER:
      return {
        ...state,
        ...action
      }

    default:
      return state;
  }
};
export default reducer;
