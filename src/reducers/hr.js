import { HR_LOADDATA,HR_GET_USER, HR_SAVEDATA } from "actions/hr/actionHR";

const initState = {
  data: [],
  AnnouncementID: "",
  Category: "",
  Department: "",
  StartDate: "",
  EndDate: "",
  Country: "",
  PinnedCalendar: "",
  CreateBy: "",
  AnnouncementStatus: "",
  PageNum: "1",
  PageSize: "10",
  Search: ""
};
const reducer = (state = initState, action) => {
  switch (action.type) {
    case HR_LOADDATA:
      //console.log(state);  
      return {
        ...state,
        ...action
      }
    case HR_GET_USER:
      return {
        ...state,
        ...action
      }
    case HR_SAVEDATA:
      //console.log(action);
      return {
        ...state,
        ...action
      };
    default:
      return state;
  }
};
export default reducer;
