
const checkPermissionApplication = (applicationList, moduleCode) => {
  
  if(applicationList.length>0){
    let permission=getPermissionApplication(applicationList, moduleCode);

    if(!permission){
      window.location.href = "/error_permission";
    }
  }
  else
  {
    window.location.href = "/error_permission";
  }
  
}
const getPermissionApplication = (applicationList, moduleCode) => {
  
  let permission=false;
  if(applicationList.length>0){
    applicationList.map(each => {
      if(each.ModuleCode===moduleCode)
      {
        permission=true;
      }
    });
  }
  return permission;
  
}

export { checkPermissionApplication, getPermissionApplication };
