import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { lang } from "Lang.jsx";
import ReactImageFallback from "react-image-fallback";
import { base_url_webapp,public_url, uniqid, ENV, s3_url_webapp, s3_url_intranet} from "../../Config_API.jsx";
import "react-toastify/dist/ReactToastify.css";
import { actionLoadUserApplicationData } from "actions/library/actionLibrary";
import { actionGetAccessToken } from "actions/login";
import qs from "query-string";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faPrint, faBook, faUser, faFolderOpen } from "@fortawesome/free-solid-svg-icons";



class Library extends Component {
    state = {
        loading: '0',
        Category:"",
        aURLS3: [],
        page: '1',
        loadIframe:false,
        PageSize: 6,
        firstLoadPage:true,
        PageTotal:1
    }
    render() {
        const { 
            activeClass,
            Category,
            firstLoadPage,
            //data
         } = this.state;
        const {aData} = this.props;
        let arrDisplay = [];
        const query = '';
        let i_tmp = 0;
       // console.log("aData");
       //console.log(aData);
       if(!firstLoadPage){
            if(aData.length > 0 ){
                aData.map((each, key) => {
                    arrDisplay.push(
                        <div style={{marginLeft:'150px'}} className="columns is-multiline" key={"content" + i_tmp}>
                            <div className="column is-1 body-admin">
                                <a className="has-text-danger" href="#" >Delete</a>
                            </div>
                            <div className="column is-2 body-admin">
                                <input className="input " type="text" value={each.UserCode} readOnly />
                            </div>
                            <div className="column is-4 body-admin">
                                <input className="input " type="text" value={each.UserFName} readOnly />
                            </div>
                            <div className="column is-3 body-admin">
                                <input className="input " type="text" value={each.JobTitle} readOnly />
                            </div>
                        </div>
                    );
                i_tmp++;
                });
                arrDisplay.push(
                    <div style={{marginLeft:'150px', marginTop:'10px'}} className="columns is-multiline" key={"content" + i_tmp}>
                        <button className="button is-danger">Save</button>
                    </div>
                );
            } 
        }
        return ( 
            <div>
                {arrDisplay}   
            </div>
        )        
    }   
    async get_UserApplication() {
        //console.log("get_library");    
        const userinfo = JSON.parse(localStorage.getItem("userinfo"));
        const Module = this.props.ModuleCode;
        //console.log(userinfo.usercode);  
        const param = {
          AppCode: 'LIBRARY',
          ModuleCode:Module,
          UserCode:userinfo.usercode
        }
        let { access_token = "" } = this.props.loginReducer;
        if (access_token === "") {
          const request_token = localStorage.getItem("request_token");
          await this.props.dispatch(actionGetAccessToken(request_token));
        }
       // console.log(param);
        await this.props.dispatch(actionLoadUserApplicationData(access_token, 'user_application', param));
       // console.log(this.props.libraryReducer.data[0]['count']);
        if (this.props.libraryReducer.data.length > 0) {
            this.setState({
                loading: 1,
                data: this.props.libraryReducer.data,
                firstLoadPage:false,
        
            });
        } else {
            this.setState({
                loading: 1,
                data: [],
                firstLoadPage:false,
            });
        }
    }
    async componentDidUpdate(prevProps) {
       // console.log('prevProps');
        //To Do....
        // const queryString = qs.parse(this.props.sPram);
        // const page = typeof queryString.p == "undefined" ? 1 : queryString.p;
        // if (this.state.page !== page) {
        //   this.get_library();
        // }
    }
    async componentDidMount() {
      // console.log(this.props);
       //console.log('componentDidMount');
       this.setState({
            loading: 1,
            firstLoadPage:false,
       });
      //console.log(React);
      //this.get_UserApplication();        
    }
}
const mapStateToProps = state => {
    return {
        loginReducer: state.loginReducer,
        usersReducer: state.usersReducer,
        libraryReducer: state.libraryReducer
    };
};
export default connect(mapStateToProps)(Library);