import React, { Component } from "react";
import { Prompt } from "react-router";
import { connect } from "react-redux";
import { lang } from "../Lang.jsx";
/** moment datetime */
import moment from 'moment';
import 'moment/min/locales.min';

/**react-day-picker */
import DayPicker from 'react-day-picker';
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import MomentLocaleUtils, {
  formatDate,
  parseDate,
} from 'react-day-picker/moment';
/**react-day-picker */

const currentYear = new Date().getFullYear();
const fromMonth = new Date(currentYear - 1, 0);
const toMonth = new Date(currentYear + 10, 11);
const currentMonth = new Date();

function YearMonthForm({ date, localeUtils, onChange }) {
  const months = localeUtils.getMonths();

  const years = [];
  for (let i = fromMonth.getFullYear(); i <= toMonth.getFullYear(); i += 1) {
    years.push(i);
  }

  const handleChange = function handleChange(e) {
    const { year, month } = e.target.form;
    onChange(new Date(year.value, month.value));
  };

  return (
    <form className="DayPicker-Caption">
      <select name="month" onChange={handleChange} value={date.getMonth()}>
        {months.map((month, i) => (
          <option key={month} value={i}>
            {month}
          </option>
        ))}
      </select>
      <select name="year" onChange={handleChange} value={date.getFullYear()}>
        {years.map(year => (
          <option key={year} value={year}>
            {year}
          </option>
        ))}
      </select>
    </form>
  );
}

export default class DatePicker extends React.Component {
  constructor(props) {
    super(props);
    this.handleYearMonthChange = this.handleYearMonthChange.bind(this);
    this.handleDayChange = this.handleDayChange.bind(this);
    this.state = {
      month: currentMonth,
      selectedDate: undefined
    };
  }
  handleYearMonthChange(month) {
    this.setState({ month });
  }
  handleDayChange(selectedDate, modifiers, dayPickerInput) {
    //const input = dayPickerInput.getInput();
    //console.log(selectedDate);
    this.props.handleChange(moment(selectedDate).format('YYYY-MM-DD'));
    this.setState({
      selectedDate: moment(selectedDate).format('YYYY-MM-DD')        
    });
  }
  render() {
    const {selectedDate}=this.props;
    return (
      <div className="YearNavigation">
        <DayPickerInput className="input is-info"
          format={"YYYY-MM-DD"}
          value={selectedDate}
          formatDate={formatDate}
          parseDate={parseDate}
          onDayChange={this.handleDayChange}
          dayPickerProps={{
            selectedDays: [selectedDate],
            locale: lang("code"),
            localeUtils: MomentLocaleUtils,
            numberOfMonths: 1,
          }}
          dayPickerProps={
            {
              month: this.state.month,
              fromMonth,
              toMonth,
              captionElement: <YearMonthForm
                                onChange={this.handleYearMonthChange} />,
            }
          }
        />
      </div>
    );
  }
  componentDidMount(){
    //console.log(this.props);
    const {selectedDate}=this.props;
    this.setState({
      selectedDate
    });
  }
  /*componentDidUpdate(p){
    //console.log(this.props);
    //console.log(p);
    if(p.selectedDate!==this.state.selectedDate)
    {
      const {selectedDate}=this.props;
      this.setState({
        selectedDate
      });
    }
  }*/
}