import React, { Component } from "react";
import { connect } from "react-redux";
import { ENV } from "../Config_API.jsx";
class TitleMenu extends Component {

  render() {
    // console.log(this.props);
    return (
        <div className="has-text-centered" style={{marginBottom:"2rem"}}>
          <h2 className="title">
           {this.props.title}
          </h2>
        </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    propertyReducer: state.propertyReducer,
    usersReducer: state.usersReducer
  };
};
export default connect(mapStateToProps)(TitleMenu);
