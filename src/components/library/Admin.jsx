import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { lang } from "Lang.jsx";
import ReactImageFallback from "react-image-fallback";
import { base_url_webapp,public_url, uniqid, ENV, s3_url_webapp, s3_url_intranet} from "../../Config_API.jsx";
import "react-toastify/dist/ReactToastify.css";
import { actionLoadLibraryData } from "actions/library/actionLibrary";
import { actionGetAccessToken } from "actions/login";
import qs from "query-string";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faPrint, faBook, faUser, faFolderOpen } from "@fortawesome/free-solid-svg-icons";
class Admin extends Component {
    state = {
        documentNo:"",
        category:"",
        CurrentURL:"",
        firstLoadPage: true,
        display: {},
        data: "",
        loading:"1",
        Search:""
      };
    render() {
        const { aData, CurrentURL, PageNum, PageSize, Category} = this.props;
        const { category,firstLoadPage,data , Search} = this.state;
        const langCode = localStorage.getItem("langCode");
        const path = CurrentURL.split("/")[1];
        let activeClass= path.replace(" ", "_");
        const query = Search;
        //let activeClass= 'training_courses';
       //console.log(sPram);
        let arrDisplay = [];
        if(path == 'insurance'){
            arrDisplay.push(
                <div className="columns" key={uniqid()}>
                    <div className="column is-4  content-company ">
                        <Link to={"./insurance_glance/view"} className="is-12 has-text-danger company-title">
                            <figure className="image-insurance">
                                <ReactImageFallback
                                    src={s3_url_intranet+"/file/library/insurance/insurance_glance.jpg"}
                                    fallbackImage={public_url + "/images/no.png"}
                                    initialImage={public_url + "/images/loader.gif"}
                                    alt="image profile"
                                    className="image-ins"
                                    />
                            </figure>
                            {'At a Glance (Summary)'}
                        </Link>
                    </div>
                    <div className="column is-4  content-company ">
                        <Link to={"./insurance_property/view"} className="is-12 has-text-danger company-title">
                            <figure className="image-insurance">
                                <ReactImageFallback
                                    src={s3_url_intranet+"/file/library/insurance/insurance_property.jpg"}
                                    fallbackImage={public_url + "/images/no.png"}
                                    initialImage={public_url + "/images/loader.gif"}
                                    alt="image profile"
                                    className="image-ins"
                                    />
                            </figure>
                            {'Property'}
                        </Link>
                    </div>
                    <div className="column is-4  content-company " >
                        <Link to={"./insurance_people/view"} className="is-12 has-text-danger company-title">
                            <figure className="image-insurance">
                                <ReactImageFallback
                                    src={s3_url_intranet+"/file/library/insurance/insurance_people.jpg"}
                                    fallbackImage={public_url + "/images/no.png"}
                                    initialImage={public_url + "/images/loader.gif"}
                                    alt="image profile"
                                    className="image-ins"
                                    />
                            </figure>
                            {'Directors & Officers Liability'}
                        </Link>
                    </div>
                </div>
            );
        }
        else if (path == 'admin') {
            arrDisplay.push(
                <div className="columns" key={uniqid()}>
                    <div className="column pos-admin">
                        <label style={{ fontSize: "1.5rem", fontWeight: "normal" }}>{'Company Manuals'}</label>
                        <ul className="management-adminnistrator has-text-danger">
                            <li><a className="has-text-danger" href={base_url_webapp + langCode + "/hr/library/index/company_manuals"} target="_blank"> List of company manuals </a></li>
                            <li><a className="has-text-danger" href={base_url_webapp + langCode + "/hr/library/addnew_library/company_manuals"} target="_blank"> Upload company manuals </a></li>
                            <li><a className="has-text-danger" href={"/admin/library/company_manuals"} target="_blank"> Assign permission company manuals </a></li>
                        </ul>
                        <label style={{ fontSize: "1.5rem", fontWeight: "normal" }}>{'Training Courses'}</label>
                        <ul className="management-adminnistrator has-text-danger">
                            <li><a className="has-text-danger" href={base_url_webapp + langCode + "/hr/library/index/training_courses"} target="_blank"> List of training courses </a></li>
                            <li><a className="has-text-danger" href={base_url_webapp + langCode + "/hr/library/addnew_library/training_courses"} target="_blank"> Upload training courses </a></li>
                            <li><a className="has-text-danger" href={"/admin/library/training_courses"} target="_blank"> Assign permission training courses </a></li>
                        </ul>
                        <label style={{ fontSize: "1.5rem", fontWeight: "normal" }}>{'Insurance'}</label>
                        <ul className="management-adminnistrator has-text-danger">
                            <li><a className="has-text-danger" href={base_url_webapp + langCode + "/hr/library/index/insurance_glance"} target="_blank"> List of At a Glance (Summary) </a></li>
                            <li><a className="has-text-danger" href={base_url_webapp + langCode + "/hr/library/addnew_library/insurance_glance"} target="_blank"> Upload At a Glance (Summary) </a></li>
                            <li><a className="has-text-danger" href={base_url_webapp + langCode + "/hr/library/index/insurance_property"} target="_blank"> List of Property </a></li>
                            <li><a className="has-text-danger" href={base_url_webapp + langCode + "/hr/library/addnew_library/insurance_property"} target="_blank"> Upload Property </a></li>
                            <li><a className="has-text-danger" href={base_url_webapp + langCode + "/hr/library/index/insurance_people"} target="_blank"> List of Directors & Officers Liability </a></li>
                            <li><a className="has-text-danger" href={base_url_webapp + langCode + "/hr/library/addnew_library/insurance_people"} target="_blank"> Upload Directors & Officers Liability </a></li>
                            <li><a className="has-text-danger" href={"/admin/library/insurance"} target="_blank"> Assign permission insurance </a></li>
                        </ul>
                        <label style={{ fontSize: "1.5rem", fontWeight: "normal" }}><div><font color='red'>Red</font> <span>Letter</span></div></label>
                        <ul className="management-adminnistrator has-text-danger">
                            <li><a className="has-text-danger" href={base_url_webapp + langCode + "/hr/library/index/red_letter"} target="_blank"> List of Red Letter </a></li>
                            <li><a className="has-text-danger" href={base_url_webapp + langCode + "/hr/library/addnew_library/red_letter"} target="_blank"> Upload Red Letter </a></li>
                        </ul>
                        <label style={{ fontSize: "1.5rem", fontWeight: "normal" }}>{'Public Library'}</label>
                        <ul className="management-adminnistrator has-text-danger">
                            <li><a className="has-text-danger" href={base_url_webapp + langCode + "/hr/library/index/brand_guideline"} target="_blank"> List of Public Library </a></li>
                            <li><a className="has-text-danger" href={base_url_webapp + langCode + "/hr/library/addnew_library/brand_guideline"} target="_blank"> Upload Public Library </a></li>
                        </ul>
                    </div>
                </div>
            );
        }
        return ( 
            <div>     
                {arrDisplay}
            </div>
        )
        
    }    
    handleInputChange = (e, tabs) => {
        //console.log(this.search.value);
        this.setState({ Search: this.search.value });
        this.get_library();
    }
    async get_library() {
        const {
            documentNo
        } = this.state;   
        const {CurrentURL} = this.props;
        const userinfo = JSON.parse(localStorage.getItem("userinfo"));
        const path = CurrentURL.split("/")[1];
        const PageNum = '';
        const PageSize = '';
        let activeClass= path.replace(" ", "_");
        const param = {
            DocumentNo: documentNo,
            Category: activeClass,
            PageNum: PageNum,
            PageSize: PageSize,
            Search:this.search.value,
            CountryCode:localStorage.getItem("countryCode"),
            UserCode:userinfo.usercode
        }
        let { access_token = "" } = this.props.loginReducer;
        if (access_token === "") {
            const request_token = localStorage.getItem("request_token");
            await this.props.dispatch(actionGetAccessToken(request_token));
        }
        console.log(param);
        await this.props.dispatch(actionLoadLibraryData(access_token, 'company_manuals', param));
        // console.log(this.props.libraryReducer.data);
        if (this.props.libraryReducer.data.length > 0) {
            this.setState({
            loading: 1,
            data: this.props.libraryReducer.data,
            firstLoadPage:false,

            });
        } else {
            this.setState({
            loading: 1,
            data: [],
            firstLoadPage:false,
            });
        }
    }
    async componentDidUpdate(prevProps) {
        //To Do....
        // const queryString = qs.parse(this.props.sPram);
        // const page = typeof queryString.p == "undefined" ? 1 : queryString.p;
        // if (this.state.page !== page) {
        //   this.get_library();
        // }
    }
    async componentDidMount() {
      // console.log(this.props);
       let CurrentURL = this.props.CurrentURL;
           CurrentURL =CurrentURL.split("/")[1];
           //console.log(CurrentURL);
       if(CurrentURL !== 'insurance' && CurrentURL !== 'admin'){
          this.get_library(); 
       }
        
    }
}
const mapStateToProps = state => {
    return {
        loginReducer: state.loginReducer,
        usersReducer: state.usersReducer,
        libraryReducer: state.libraryReducer
    };
};
export default connect(mapStateToProps)(Admin);