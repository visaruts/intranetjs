
import React, { Component } from "react";
import { Prompt } from "react-router";
import { connect } from "react-redux";
import moment from 'moment';
import "react-toastify/dist/ReactToastify.css";
import Pagination from "components/Pagination.jsx";
import { Link } from "react-router-dom";
import { lang } from "Lang.jsx";
import { toast } from "react-toastify";
import ReactImageFallback from "react-image-fallback";
import { base_url_webapp,public_url, uniqid, ENV, s3_url_webapp, api } from "../../Config_API.jsx";
import Iframe from 'react-iframe'
import "react-toastify/dist/ReactToastify.css";
import { actionLoadLibraryData } from "actions/library/actionLibrary";
import { actionGetAccessToken } from "actions/login";
import TitleMenu from "components/TitleMenu.jsx";
import qs from "query-string";
import axios from "axios";
import ReactLoading from 'react-loading';
import { faDivide } from "@fortawesome/free-solid-svg-icons";
import { faSearch, faChevronCircleLeft, faPrint,faBackward, faDownload} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
class LibraryView extends Component {
    state = {
      documentNo:"",
      category:"",
      CurrentURL:"",
      firstLoadPage: true,
      display: {},
      data: "",
      loading:"1",
      active:true,
      Search: "",
      PageNum:1,
      PageSize:10,
      PageTotal:1
    }
    notify = (type, message) => {
      const config = {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true
      };
      if (type === "error") {
        toast.error(message, config);
      } else if (type === "success") {
        toast.success(message, config);
      } else if (type === "warn") {
        toast.warn(message, config);
      } else if (type === "info") {
        toast.info(message, config);
      }
    };
    render() {
      const { data, Search, aURLS3,loading , PageSize, loadIframe} = this.state;
      const {documentNo,PageTotal, PageNum,  Category, CurrentURL } = this.props;
      //const query = Search;
      let aData = [];
      const URLGoogleDoc = "https://docs.google.com/viewerng/viewer?embedded=true&url=";
      let arrDisplay = [];
      let i_tmp = 0;
      //console.log(CurrentURL);
      if(data.length > 0 ){
        if(Category !== 'insurance_glance' && Category !== 'insurance_property' && Category !== 'insurance_people'){
          const filename = data[0]['CoverImageFile'];
          const Topic = data[0]['Topic'];
          let Attachtopic = data[0]['AttachPdfTopic'];
              Attachtopic = Attachtopic.split("|");
          let Attachfiles = data[0]['AttachPdfFiles'];
              Attachfiles = Attachfiles.split("|");
          let aAttach = [];
          let i=0;
          for(i=0; i<Attachtopic.length; i++){            
            if(Attachtopic[i] !== '' && Attachfiles[i] !== ''){
              aAttach.push({'PdfTopic':Attachtopic[i], 'PdfFiles':Attachfiles[i]});
            }            
          }
          //console.log(aAttach);
          arrDisplay.push(
            <div key={"column-1"}>
              <div className="has-text-right button-download">
                  <Link to={"/library/" + Category} className="button is-danger" title="Back"> <FontAwesomeIcon icon={faChevronCircleLeft} /> <span className="is-hidden-mobile">Back</span> </Link>
              </div>
              <TitleMenu title={Topic} />
              <div className="columns">
                  <div className="column-5">
                    <figure className={"image-view-"+Category}>
                        <ReactImageFallback
                            src={s3_url_webapp+"/file/library/"+Category+"/covers/"+filename}
                            fallbackImage={public_url + "/images/no.png"}
                            initialImage={public_url + "/images/loader.gif"}
                            alt="image view"
                            />
                    </figure>
                  </div>
                  <div className="column-7">
                  {
                    aAttach.map((each,key) => {
                      if(each !== ""){
                        return <div className="box-list" key={key}>
                          <img src={"/images/pdf.png"} alt="image pdf"/>
                          <a className="has-text-danger view-list" onClick={((e) => this.handleClick(e, each.PdfFiles))} target="_blank">
                            {each.PdfTopic}
                          </a>
                        </div>
                      }
                    })
                  }
                  </div>
              </div>
            </div>
          );
        }
        else if((Category === 'insurance_glance' || Category === 'insurance_property' || Category === 'insurance_people') && (typeof documentNo !== 'undefined')){
         // console.log(data);
          const Topic = data[0]['Topic'];
          let Attachtopic = data[0]['AttachPdfTopic'];
              Attachtopic = Attachtopic.split("|");
          let Attachfiles = data[0]['AttachPdfFiles'];
              Attachfiles = Attachfiles.split("|");
          let aAttach = [];
          let i=0;
          for(i=0; i<Attachtopic.length; i++){            
            if(Attachtopic[i] !== '' && Attachfiles[i] !== ''){
              aAttach.push({'PdfTopic':Attachtopic[i], 'PdfFiles':Attachfiles[i]});
            }            
          }
          //console.log(aAttach);
          arrDisplay.push(
            <div key={"column-1"}>
              <div className="has-text-right button-download">
                  <Link to={"/library/" + Category+'/view'} className="button is-danger" title="Back"> <FontAwesomeIcon icon={faChevronCircleLeft} /> <span className="is-hidden-mobile">Back</span> </Link>
              </div>
              <TitleMenu title={Topic} />
              <div className="columns">
                  <div className="column-7 list-insurance" >
                  {
                    aAttach.map((each,key) => {
                      if(each !== ""){
                        return <div className="box-list" key={key}>
                          <img src={"/images/pdf.png"} alt="image pdf"/>
                          <a className="has-text-danger view-list" onClick={((e) => this.handleClick(e, each.PdfFiles))} target="_blank">
                            {each.PdfTopic}
                          </a>
                        </div>
                      }
                    })
                  }
                  </div>
              </div>
            </div>
          );
        }
        else{
          let Topic = '';
          if(Category === 'insurance_glance'){
            Topic = 'AT A GLANCE (SUMMARY)';
          }
          else if(Category === 'insurance_property'){
            Topic = 'PROPERTY';
          }
          else if(Category === 'insurance_people'){
            Topic = 'DIRECTORS & OFFICERS LIABILITY';
          }
          let currentURL = CurrentURL+"/view";
          
          //const path = CurrentURL;
          //console.log(currentURL);
          arrDisplay.push(
            <div className="column is-12" style={{ padding: "0" }} key={"column-1"}>
                  <TitleMenu title={Topic} />
                  <div className="content-insurance" key={uniqid()}>
                    <div id="policy-search" className="column is-12">                      
                      <div className="column is-2 is-pulled-left control-search" style={{ paddingRight: "0" }}>                        
                        <div className="control has-icons-left has-icons-right is-pulled-left control-search">
                          <input className="input is-danger" type="text" placeholder="Search..." value={Search}
                            ref={input => this.search = input}
                            onChange={((e) => this.handleInputChange(e))}
                            id="search-input"
                          />
                          <span className="icon is-small is-left">
                            <FontAwesomeIcon icon={faSearch} />
                          </span>
                        </div>                        
                      </div>
                      <div className="is-pulled-right">
                        <div className="has-text-right button-download">
                            <Link to={"/library/insurance"} className="button is-danger" title="Back"> <FontAwesomeIcon icon={faChevronCircleLeft} /> <span className="is-hidden-mobile">Back</span> </Link>
                        </div>
                      </div>
                    </div>
                    <div className="column clear" >
                        {data.length > 0 ? <div className="list is-hoverable template">
                          {data.map((each, key) => {
                              let id_tmp = Math.random();
                              let ClassColor = "policy-02";
                              return (
                                <div key={"list-item-" + id_tmp} className={key % 2 == 0 ? "list-item " : "list-item even"}>
                                 <label  htmlFor={"checkbox-" + id_tmp}  ></label>
                                  
                                  <Link to={"./view/" + each.DocumentNo} className={ClassColor} >
                                    {each.Topic}
                                  </Link>
                                </div>
                              )
                            })
                          }
                          </div>
                          : ""
                        }
                      <div>
                      <Pagination recordsTotal={PageTotal} page={PageNum} pageSize={PageSize} link={currentURL}
                        onClick={e => {
                          this.setState({ active: false });
                        }}
                      />
                      </div>
                    </div>
                  
                  </div>
                </div>
          );

        }
      }
      return (
        <div className="notification">
          {arrDisplay}
        </div>
      )        
    }
    handleClick = (e, filename) => {      
      const {Category} = this.props;
      const DROPBOX_LINK = "/dropbox/getlinkpdf?filename=";
      let files = '/library/for_intranet_access_only/'+Category+'/'+filename;
      //console.log(api.INTRANET_API+DROPBOX_LINK +files);
      let { access_token = "" } = this.props.loginReducer;
     // if (access_token === "") {
      // const request_token = localStorage.getItem("request_token");
      // await this.props.dispatch(actionGetAccessToken(request_token));
      //}
      axios.get(api.INTRANET_API+DROPBOX_LINK +files
        ,
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        }
      )
      .then(function (response) {
        // handle success
        let link_view = response.data.url;
        if(typeof link_view !== 'undefined'){
          link_view = link_view.replace('dl=0','raw=1');
          window.open(link_view);
          console.log('success');
        }
        else{
          //this.notify("error", "File not found, please contact your local HR.");   
          //console.log(response);
          const config = {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true
          };
          toast.error("File not found, please contact your local HR.", config);
        }
       
           
      })
      .catch(function (error) {
        // handle error
        const config = {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        };
        toast.error("File not found, please contact your local HR.", config);
        //this.notify("error", "File not found, please contact your local HR."); 
        console.log(error);
      });
    }
    handleInputChange = (e, tabs) => {
     // console.log(this.search.value);
      this.setState({ Search: this.search.value });
      this.get_library();
    }
    async get_library() {
      const queryString = qs.parse(this.props.sPram);
      const userinfo = JSON.parse(localStorage.getItem("userinfo"));
      const page = queryString.p || 1;    
      const {documentNo, CurrentURL, PageNum} = this.props;
      const DocumentNo = (typeof documentNo === 'undefined'? '': documentNo);
      const {PageSize} = this.state;
      const path = CurrentURL.split("/")[1];   
      const search = (typeof this.search == 'undefined'? '':this.search.value);   
      let activeClass= (path === "public_library" ?"brand_guideline":path);
      //console.log(localStorage.getItem("countryCode"));
      const param = {
        DocumentNo: DocumentNo,
        Category: activeClass,
        PageNum: PageNum,
        PageSize: PageSize,
        Search:search,
        CountryCode:localStorage.getItem("countryCode"),
        UserCode:userinfo.usercode
      }
      let { access_token = "" } = this.props.loginReducer;
      if (access_token === "") {
        const request_token = localStorage.getItem("request_token");
        await this.props.dispatch(actionGetAccessToken(request_token));
      }
     // console.log(param);
      await this.props.dispatch(actionLoadLibraryData(access_token, 'company_manuals', param));
      // console.log(this.props.libraryReducer.data);
      if (this.props.libraryReducer.data.length > 0) {
        this.setState({
          loading: 1,
          data: this.props.libraryReducer.data,
          firstLoadPage:false,
          PageTotal:this.props.libraryReducer.data.length

        });
      } else {
        this.setState({
          loading: 1,
          data: [],
          firstLoadPage:false,
          PageTotal:'0'
        });
      }
    }
    async sendEmailInterView(){
      
      const userinfo = JSON.parse(localStorage.getItem("userinfo"));
      const {Category} = this.props;
      const {data} = this.state;
      let category = Category.replace('_',' ');
      let country_of_work = 'HQ';
      if(userinfo.office != 'HQ'){
        country_of_work = userinfo.office+' '+userinfo.country
      }  
      const date = moment().format('DD/MM/Y h:mm:ss A');       
       //console.log(Body);
     // const userinfo = JSON.parse(localStorage.getItem("userinfo"));
      const url = api.INTRANET_API + "/library/sendmail";
      const Subject = '[Test] - Security read & execute document - '+userinfo.office+' - '+userinfo.firstname+' '+userinfo.lastname;
     // let Body = Body;
      const To = 'pornpichai@redplanethotels.com';
      const From = 'rphldeveloper@redplanethotels.com';
      const CC = '';
      const BCC = 'kittipong.thongjan@redplanethotels.com,visarut@redplanethotels.com';
      let Body = '<strong><font color=\"#FF0000\">'+userinfo.firstname+''+userinfo.lastname+' (from '+country_of_work+')</font></strong> has accessed \"<strong><font color=\"#FF0000\">Library</font></strong>\" >> \"<strong><font color=\"#FF0000\">'+category+'</font></strong>\" and read '+data[0]['Topic']+' at '+date;//.date("d/m/Y h:i:s A");
     
      const param = {
          ModuleCode:'Intranetview', 
          Subject:Subject, 
          Body:Body, 
          To:To, 
          From:From, 
          CC:CC, 
          BCC:BCC, 
          AttachFile:'', 
          AppCode:'INTRANET'
      } 
      //console.log(param);

      try {
          const res = await axios.post(url, param, {
              headers: { 
                Authorization: "Bearer " + this.props.loginReducer.access_token, 
                "Content-Type": "application/json"
              }
          });
          if(res.data.code===200)
          {
            //this.notify("success", "Save Permission Success");
            console.log('Success');
            //this.handleShowModal(false);
            //this.props.handleFilter();
          }
          else{
            console.log('Error');
            //this.notify("error","Save Permission Error");
          }
      } catch (err) {
          console.log({ err });
          // this.notify("error", "Save Permission Error");
          // this.notify("error", err.message);
          //window.location.href = "/connection_error?err="+err.message+"&page=ItemDefect&method=handleSubmit";
      }
    }
    async InsertInterViewLog(){
      const userinfo = JSON.parse(localStorage.getItem("userinfo"));      
      const url = api.INTRANET_API + "/library/insertInterViewLog";
      let country_of_work = 'HQ';
      if(userinfo.office != 'HQ'){
        country_of_work = userinfo.office+' '+userinfo.country
      }       
      const {Category} = this.props;
      let category1 = Category.split('_')[0];
          category1  = category1.charAt(0).toUpperCase()+ category1.slice(1);
      let category2 = Category.split('_')[1];
          category2  = category2.charAt(0).toUpperCase()+ category2.slice(1);
      const {data} = this.state;

      const param = {
        sUserCode:userinfo.usercode, 
        sName:userinfo.firstname+' '+userinfo.lastname, 
        sCountry_of_working:country_of_work, 
        sRead_Department:'Library', 
        sRead_Category:category1+' '+category2, 
        sRead_Topic:data[0]['Topic'], 
      } 
      //console.log(param);

      try {
          const res = await axios.post(url, param, {
              headers: { 
                Authorization: "Bearer " + this.props.loginReducer.access_token, 
                "Content-Type": "application/json"
              }
          });
          if(res.data.code===200)
          {
            //this.notify("success", "Save Permission Success");
            console.log('Success');
            //this.handleShowModal(false);
            //this.props.handleFilter();
          }
          else{
            console.log('Error');
            //this.notify("error","Save Permission Error");
          }
      } catch (err) {
           console.log({ err });
          // this.notify("error", "Save Permission Error");
          // this.notify("error", err.message);
          //window.location.href = "/connection_error?err="+err.message+"&page=ItemDefect&method=handleSubmit";
      }
    }
    async componentDidUpdate(prevProps) {
      const { CurrentURL, PageNum, CountryCode } = this.props;
      
      const CurrentURLPrevProps = prevProps.CurrentURL;
      const CurrentPagePrevProps = prevProps.PageNum;
      const CurrentCountryPrevProps = prevProps.CountryCode;

      //console.log(CurrentURL+'|'+CurrentURLPrevProps+'|'+PageNum+'|'+CurrentPagePrevProps);
      if (CurrentURL !== CurrentURLPrevProps || PageNum !== CurrentPagePrevProps || CountryCode !== CurrentCountryPrevProps) {
        this.setState({page:CurrentPagePrevProps});
        this.get_library();
      }
    }
    async componentDidMount() {
     // console.log('componentDidMount');
     const {Category} = this.props;
     await this.get_library();
     console.log(Category);
     if(Category == 'company_manuals'){
       this.sendEmailInterView();
     }     
     this.InsertInterViewLog();
    }
}
const mapStateToProps = state => {
    return {
        loginReducer: state.loginReducer,
        usersReducer: state.usersReducer,
        libraryReducer: state.libraryReducer
    };
};
export default connect(mapStateToProps)(LibraryView);