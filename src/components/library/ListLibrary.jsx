import React, { Component } from "react";
import { Prompt } from "react-router";
import { connect } from "react-redux";
import "react-toastify/dist/ReactToastify.css";
import Pagination from "components/Pagination.jsx";
import { Link } from "react-router-dom";
import { lang } from "Lang.jsx";
import ReactImageFallback from "react-image-fallback";
import { base_url_webapp,public_url, uniqid, ENV, s3_url_webapp, s3_url_intranet } from "../../Config_API.jsx";
import "react-toastify/dist/ReactToastify.css";
import { actionLoadLibraryData } from "actions/library/actionLibrary";
import { actionGetAccessToken } from "actions/login";
import qs from "query-string";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faPrint, faBook, faUser, faFolderOpen } from "@fortawesome/free-solid-svg-icons";
class ListLibrary extends Component {
    state = {
      documentNo:"",
      category:"",
      CurrentURL:"",
      firstLoadPage: true,
      display: {},
      data: "",
      loading:"1",
      active:true,
      Search:""
    };
    render() {
      const { aData, CurrentURL, Category  ,PageNum, PageSize} = this.props;
      const { category,firstLoadPage,data,Search } = this.state;
      const path = CurrentURL.split("/")[1];
      const query = Search;
     // console.log(PageNum);
      // console.log(CurrentURL);
     // this.get_library();
      let arrDisplay = [];      
      let PageTotal = 1;

      if(path == 'red_letter'){
         let i_tmp = 0;
         let i_row = 1;
        if(data.length > 0 ){
          data.map((each, key) => {
              let id_tmp = Math.random();
              PageTotal = each.TotalRow;   
              //console.log(i_row);
              if(i_tmp %2 == 0){
                i_row++;
              }        
              arrDisplay.push(
                  <div className={"column is-6 "+ (i_tmp %2 == 0 ? "border-red":"")+ (i_row %2 == 0 ? " odd":" even")} key={"column-"+id_tmp} >
                      <Link to={"./"+path+"/view/" + each.DocumentNo} className="is-12 has-text-danger company-title">
                          <div className="red-title is-pulled-left">
                            {each.Topic}
                          </div>
                          <div>
                            <figure className="image-red " key={"image-company-" + id_tmp}>
                                <ReactImageFallback
                                    src={s3_url_webapp+"/file/library/red_letter/covers/"+each.CoverImageFile}
                                    fallbackImage={public_url + "/images/no.png"}
                                    initialImage={public_url + "/images/loader.gif"}
                                    alt="image profile"
                                    className="red-imag"
                                    />
                            </figure>
                          </div>
                      </Link>
                  </div>
                );   
                  
               
                i_tmp++;              
          });
        }
      } else if(path == 'public_library'){
        let i_tmp = 0;
        let i_row = 1;
        if(data.length > 0 ){
          data.map((each, key) => {            
              let id_tmp = Math.random();
              PageTotal = each.TotalRow;
              if(i_tmp %2 == 0){
                i_row++;
              } 
                arrDisplay.push(
                  <div className={"column is-6 "+ (i_tmp %2 == 0 ? "border-red":"")+ (i_row %2 == 0 ? " odd":" even")} key={"column-"+id_tmp} >
                      <Link to={"./"+path+"/view/" + each.DocumentNo} className="is-12 has-text-danger company-title">
                          <div className="red-title is-pulled-left">
                            {each.Topic}
                          </div>
                          <div>
                            <figure className="image-red " key={"image-company-" + id_tmp}>
                                <ReactImageFallback
                                    src={s3_url_webapp+"/file/library/brand_guideline/covers/"+each.CoverImageFile}
                                    fallbackImage={public_url + "/images/no.png"}
                                    initialImage={public_url + "/images/loader.gif"}
                                    alt="image profile"
                                    className="red-imag"
                                    />
                            </figure>
                          </div>
                      </Link>
                  </div>
                );
              i_tmp++;
          });
        }
      } 
      return(
        <div>          
            <div  id="tab-company-search" className="column is-12 is-pulled-right control-search search-mobile">
              <div className="column is-3 is-pulled-right">
                <div className="control has-icons-left has-icons-right is-pulled-right control-search">
                <input className="input is-danger" type="text" placeholder="Search..." value={query}
                    ref={input => this.search = input}
                    onChange={((e) => this.handleInputChange(e, 'tab1'))}
                    id="search-input"
                />
                <span className="icon is-small is-left">
                    <FontAwesomeIcon icon={faSearch} />
                </span>
                </div>
              </div>
            </div>
          <div className={"columns is-multiline display-mobile"}>
            {arrDisplay}              
          </div>
        </div>
      )
    }
    handleInputChange = (e, tabs) => {
      //console.log(this.search.value);
      this.setState({ Search: this.search.value });
      this.get_library();
    }
    async get_library() {
      //console.log("get_library");
      const {
        documentNo
      } = this.state;
      const queryString = qs.parse(this.props.sPram);
      const userinfo = JSON.parse(localStorage.getItem("userinfo"));
      const page = queryString.p || 1;    
      const {CurrentURL , PageNum, PageSize} = this.props;
      const path = CurrentURL.split("/")[1];
      let activeClass= (path === "public_library" ?"brand_guideline":path.replace(" ", "_"));
      //let activeClass = "";
      const param = {
        DocumentNo: documentNo,
        Category: activeClass,
        PageNum: PageNum,
        PageSize: PageSize,
        Search:this.search.value,
        CountryCode:localStorage.getItem("countryCode"),
        UserCode:userinfo.usercode
      }
      let { access_token = "" } = this.props.loginReducer;
      if (access_token === "") {
        const request_token = localStorage.getItem("request_token");
        await this.props.dispatch(actionGetAccessToken(request_token));
      }
      await this.props.dispatch(actionLoadLibraryData(access_token, 'company_manuals', param));
      // console.log(this.props);
      if (this.props.libraryReducer.data.length > 0) {
        this.setState({
          loading: 1,
          data: this.props.libraryReducer.data,
          firstLoadPage:false,

        });
      } else {
        this.setState({
          loading: 1,
          data: [],
          firstLoadPage:false,
        });
      }
    }
    async componentWillMount() {
     // console.log("componentWillUnmount...");
    }
    async componentWillUnmount() {
      //console.log("componentWillUnmount...");
    }
    async componentDidUpdate(prevProps) {
     
      const { CurrentURL, PageNum } = this.props;
      //console.log(prevProps);
      const CurrentURLPrevProps = prevProps.CurrentURL;
      const CurrentPagePrevProps = prevProps.PageNum;
      if (CurrentURL !== CurrentURLPrevProps || PageNum !== CurrentPagePrevProps) {
       // console.log(this.props);
        this.get_library();
      }

    }
    async componentDidMount() {
       // console.log("componentDidMount");
        this.get_library();
    }
}
const mapStateToProps = state => {
    return {
        loginReducer: state.loginReducer,
        usersReducer: state.usersReducer,
        libraryReducer: state.libraryReducer
    };
};
export default connect(mapStateToProps)(ListLibrary);