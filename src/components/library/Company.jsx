import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { lang } from "Lang.jsx";
import ReactImageFallback from "react-image-fallback";
import { base_url_webapp,public_url, uniqid, ENV, s3_url_webapp, s3_url_intranet} from "../../Config_API.jsx";
import "react-toastify/dist/ReactToastify.css";
import { actionLoadLibraryData } from "actions/library/actionLibrary";
import { actionGetAccessToken } from "actions/login";
import qs from "query-string";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faPrint, faBook, faUser, faFolderOpen } from "@fortawesome/free-solid-svg-icons";
class Company extends Component {
    state = {
        documentNo:"",
        category:"",
        CurrentURL:"",
        firstLoadPage: true,
        display: {},
        data: "",
        loading:"1",
        Search:""
      };
    render() {
        const { aData, CurrentURL, PageNum, PageSize, Category} = this.props;
        const { category,firstLoadPage,data , Search} = this.state;
        const langCode = localStorage.getItem("langCode");
        const path = CurrentURL.split("/")[1];
        let activeClass= path.replace(" ", "_");
        const query = Search;
        //let activeClass= 'training_courses';
       //console.log(sPram);
        let arrDisplay = [];
        if(path == 'company_manuals'){
            let i_tmp = 0;
            if(data.length > 0 ){
                data.map((each, key) => {
                    let id_tmp = Math.random();
                    arrDisplay.push(
                        <div className="column is-3  content-company " key={"column-"+ id_tmp}>
                            <Link to={"./"+path+"/view/" + each.DocumentNo} className="is-12 has-text-danger company-title">
                                <figure className="image-company" key={"image-company-" + id_tmp}>
                                    <ReactImageFallback
                                        src={s3_url_webapp+"/file/library/company_manuals/covers/"+each.CoverImageFile}
                                        fallbackImage={public_url + "/images/no.png"}
                                        initialImage={public_url + "/images/loader.gif"}
                                        alt="image profile"
                                       />
                                </figure>
                                {each.Topic}
                            </Link>
                        </div>
                    );
                i_tmp++;
                });
            } 
        }
        else if(path == 'training_courses'){
            let i_tmp = 0;
            if(data.length > 0 ){
                data.map((each, key) => {
                    let id_tmp = Math.random();
                    arrDisplay.push(
                        <div className="column is-3  content-company" key={"column-"+id_tmp}>
                            <Link to={"./"+path+"/view/" + each.DocumentNo} className="is-12 has-text-danger company-title">
                                <figure className="image-train">
                                    <ReactImageFallback
                                        src={s3_url_webapp+"/file/library/training_courses/covers/"+each.CoverImageFile}
                                        fallbackImage={public_url + "/images/no.png"}
                                        initialImage={public_url + "/images/loader.gif"}
                                        alt="image profile"
                                        />{each.Topic}
                                </figure>
                                
                            </Link>
                        </div>
                    );
                    i_tmp++;
                });
            }
        }
        return ( 
            <div>  
                <div id="tab-company-search" className="column is-12 is-pulled-right control-search search-mobile">
                    <div className="column is-12 is-pulled-right">
                        <div className="control has-icons-left has-icons-right is-pulled-right control-search">
                        <input className="input is-danger" type="text" placeholder="Search..." value={query}
                            ref={input => this.search = input}
                            onChange={((e) => this.handleInputChange(e, 'tab1'))}
                            id="search-input"
                        />
                        <span className="icon is-small is-left">
                            <FontAwesomeIcon icon={faSearch} />
                        </span>
                        </div>
                    </div>
                </div>
                <div className={"column-company columns is-multiline display-mobile"}>
                {arrDisplay}
                </div>
            </div>
        )
        
    }    
    handleInputChange = (e, tabs) => {
        //console.log(this.search.value);
        this.setState({ Search: this.search.value });
        this.get_library();
    }
    async get_library() {
        const {
            documentNo
        } = this.state;   
        const {CurrentURL} = this.props;
        const userinfo = JSON.parse(localStorage.getItem("userinfo"));
        const path = CurrentURL.split("/")[1];
        const PageNum = '';
        const PageSize = '';
        let activeClass= path.replace(" ", "_");
        //console.log(localStorage.getItem("userinfo"));
        const param = {
            DocumentNo: documentNo,
            Category: activeClass,
            PageNum: PageNum,
            PageSize: PageSize,
            Search:this.search.value,
            CountryCode:localStorage.getItem("countryCode"),
            UserCode:userinfo.usercode
        }
        let { access_token = "" } = this.props.loginReducer;
        if (access_token === "") {
            const request_token = localStorage.getItem("request_token");
            await this.props.dispatch(actionGetAccessToken(request_token));
        }
        //console.log(param);
        await this.props.dispatch(actionLoadLibraryData(access_token, 'company_manuals', param));
        // console.log(this.props.libraryReducer.data);
        if (this.props.libraryReducer.data.length > 0) {
            this.setState({
            loading: 1,
            data: this.props.libraryReducer.data,
            firstLoadPage:false,

            });
        } else {
            this.setState({
            loading: 1,
            data: [],
            firstLoadPage:false,
            });
        }
    }
    async componentDidUpdate(prevProps) {
        //To Do....
        // const queryString = qs.parse(this.props.sPram);
        // const page = typeof queryString.p == "undefined" ? 1 : queryString.p;
        // if (this.state.page !== page) {
        //   this.get_library();
        // }
    }
    async componentDidMount() {
      // console.log(this.props);
       let CurrentURL = this.props.CurrentURL;
           CurrentURL =CurrentURL.split("/")[1];
           //console.log(CurrentURL);
       if(CurrentURL !== 'insurance' && CurrentURL !== 'admin'){
          this.get_library(); 
       }
        
    }
}
const mapStateToProps = state => {
    return {
        loginReducer: state.loginReducer,
        usersReducer: state.usersReducer,
        libraryReducer: state.libraryReducer
    };
};
export default connect(mapStateToProps)(Company);