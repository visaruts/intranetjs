import React, { Component } from "react";
//import { Prompt } from "react-router";
import { connect } from "react-redux";
import "react-toastify/dist/ReactToastify.css";
import Pagination from "components/Pagination.jsx";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from "react-router-dom";
import { lang } from "Lang.jsx";
/** moment datetime */
import moment from 'moment';
import 'moment/min/locales.min';
// import { timeout } from "q";
class TNCard extends Component {
  state = {
    display:{},
    data:{},
    permissionAdmin:false,
    showModal: false,
    showAssignTo:false,
    showReSchedule:false,
    userData:[],
    userDataOption:[],
    expectedDate:undefined, 
    assignedTo:"",
    status:"",
    remark:""
  };
  render() {
        
    if(lang("code")==="th"){
      moment.locale('th');
    }
    else if(lang("code")==="ja"){
      moment.locale('ja');
    }
    else if(lang("code")==="id"){
      moment.locale('id');
    }
    else
    {
      moment.locale('en');
    }

    const {aTNDetail} = this.props ;
    let  status ="";
    let PageNum = 0 ;
    let PageTotal = 0 ;
    let LinkName = 0 ;
    if(aTNDetail.TYPE==="CourseList"){
      aTNDetail.Status ="open";
      LinkName="Detail";
    }else if(aTNDetail.TYPE==="Participate"){
      aTNDetail.Status ="completed";
      LinkName="Evaluation";
    }else{
      aTNDetail.Status ="open";
      LinkName="Detail";
    }
    if(aTNDetail.Status.toLowerCase()==="open"){
      status =<span className="tag is-info form-tag uppercase  ">{aTNDetail.Status}</span> ;
    }else if(aTNDetail.Status.toLowerCase()==="completed"){
      status =<span className="tag is-success form-tag uppercase  ">{aTNDetail.Status}</span> ;
    }else{
      status =<span className="tag is-success form-tag uppercase  ">{aTNDetail.Status}</span> ;
    }
    // console.log(aTNDetail);
    const items = [] ;
    const id_tmp = Math.random();
    if(aTNDetail.length<=0){
      items.push( 
        <div id={"msg-card-0"+id_tmp} data-preview-id={"0"+"-"+id_tmp} className="card">
          <div className="card-content">
            <div className="columns">
              No data available in course list 
            </div>
          </div>
        </div>
      );
    }else{
      aTNDetail.map((each,key)=>{
        PageNum = each.PageNum ;
        PageTotal = each.PageTotal ;
        items.push(
          <div className="message">
          <div className="message-body">
            <div id={"msg-card-"+key+"-"+id_tmp} data-preview-id={key+"-"+id_tmp} className="card">
              <div className="card-content">
                <div className="columns">
                  <div className="column is-10">
                    <div className="msg-header">
                      <span className="msg-timestamp">
                      </span>
                    </div>
                    <div className="msg-subject">
                      <span className="msg-subject">
                        {/* <span className="tag is-success form-tag">Complated</span> */}
                        {status}
                        <Link to="" ><strong id={"fake-subject-"+key+"-"+id_tmp}>{each.masTitle} </strong> <span className="has-text-danger" > {each.rouName}</span></Link>
                      </span>
                    </div>
                    <div className="msg-snippet"><p id={"fake-snippet-"+key+"-"+id_tmp}>Localtion :{each.lcName}</p>
                    </div>
                    <span className="msg-from">
                      <small>Start date: <FontAwesomeIcon icon={"calendar"} /> {moment(each.rouDateStart).format('DD-MMMM-YYYY')}</small>
                    </span>
                  </div>
                  <div className="column text-right">
                    <Link className="button is-danger" to="" ><FontAwesomeIcon icon={"edit"} /> {LinkName}</Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        )
    });
    }
    return (
      <div className="column">
          {items}
        <Pagination recordsTotal={PageTotal} page={PageNum} link='training' />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    propertyReducer: state.propertyReducer,
    usersReducer: state.usersReducer
  };
};
export default connect(mapStateToProps)(TNCard);
