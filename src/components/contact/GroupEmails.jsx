import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { lang } from "Lang.jsx";
import moment from 'moment';
import ReactImageFallback from "react-image-fallback";
import { base_url_webapp,public_url, uniqid, ENV, s3_url_webapp, s3_url_intranet} from "../../Config_API.jsx";
import "react-toastify/dist/ReactToastify.css";
import {actionLoadContactData, actionLoadEmployeeContactData, actionLoadHotelContactData } from "actions/contact/actionContact";
import { actionGetAccessToken } from "actions/login";
import qs from "query-string";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faPrint, faBook, faUser, faFolderOpen } from "@fortawesome/free-solid-svg-icons";
class GroupEmails extends Component {
    state = {
        loading: '0',
        data: [],
        dataHotels: [],
        CountryCode: [],
        firstLoadPage:true,
        Search:''
    }
    render() {       
        const { 
            activeClass,
            data,
            CountryCode,
            dataHotels,
            firstLoadPage,
            Search
        } = this.state;
        
        const userinfo = JSON.parse(localStorage.getItem("userinfo"));
       // console.log(CountryCode);
        const query = Search;
        let arrDisplay = [];
        let arrDisplayGlobal = [];
        let arrDisplayHQ = [];
        let arrDisplayTH = [];
        let arrDisplayID = [];
        let arrDisplayPH = [];
        let arrDisplayJP = [];
        let i_gl = 0;
        if(data.length > 0){
            data.map((each, key) => {
                if(each.group_of === 'GLOBAL'){
                    arrDisplayGlobal.push(
                        <div key={"content-"+i_gl}>
                            <div className="column is-12" style={{lineHeight:'1.2rem', fontSize:'1.0em', fontWeight:'bold', padding:'5px 5px 5px'}} >
                                <div style={{color: '#d40032'}} ><b>{each.group_email_displayname}</b></div>
                            </div>            
                        </div>
                    );
                    i_gl++;
                }
            });
        }
        let i_hq = 0;
        if(data.length > 0){
            data.map((each, key) => {
                if(each.group_of === 'HQ'){
                    arrDisplayHQ.push(
                        <div key={"content-"+i_hq}>
                            <div className="column is-12" style={{lineHeight:'1.2rem', fontSize:'1.0em', fontWeight:'bold', padding:'5px 5px 5px'}} >
                                <div style={{color: '#d40032'}} ><b>{each.group_email_displayname}</b></div>
                            </div>            
                        </div>
                    );
                    i_hq++;
                }
            });
        }
        let i_th = 0;
        if(data.length > 0){
            data.map((each, key) => {
                if(each.group_of === 'Thailand'){
                    arrDisplayTH.push(
                        <div key={"content-"+i_th}>
                            <div className="column is-12" style={{lineHeight:'1.2rem', fontSize:'1.0em', fontWeight:'bold', padding:'5px 5px 5px'}} >
                                <div style={{color: '#d40032'}} ><b>{each.group_email_displayname}</b></div>
                            </div>            
                        </div>
                    );
                    i_th++;
                }
            });
        }
        let i_id = 0;
        if(data.length > 0){
            data.map((each, key) => {
                if(each.group_of === 'Indonesia'){
                    arrDisplayID.push(
                        <div key={"content-"+i_id}>
                            <div className="column is-12" style={{lineHeight:'1.2rem', fontSize:'1.0em', fontWeight:'bold', padding:'5px 5px 5px'}} >
                                <div style={{color: '#d40032'}} ><b>{each.group_email_displayname}</b></div>
                            </div>            
                        </div>
                    );
                    i_id++;
                }
            });
        }
        let i_ph = 0;
        if(data.length > 0){
            data.map((each, key) => {
                if(each.group_of === 'Philippines'){
                    arrDisplayPH.push(
                        <div key={"content-"+i_ph}>
                            <div className="column is-12" style={{lineHeight:'1.2rem', fontSize:'1.0em', fontWeight:'bold', padding:'5px 5px 5px'}} >
                                <div style={{color: '#d40032'}} ><b>{each.group_email_displayname}</b></div>
                            </div>            
                        </div>
                    );
                    i_ph++;
                }
            });
        }
        let i_jp = 0;
        if(data.length > 0){
            data.map((each, key) => {
                if(each.group_of === 'Japan'){
                    arrDisplayJP.push(
                        <div key={"content-"+i_jp}>
                            <div className="column is-12" style={{lineHeight:'1.2rem', fontSize:'1.0em', fontWeight:'bold', padding:'5px 5px 5px'}} >
                                <div style={{color: '#d40032'}} ><b>{each.group_email_displayname}</b></div>
                            </div>            
                        </div>
                    );
                    i_jp++;
                }
            });
        }
        
        arrDisplay.push(
            <div key={"content" + activeClass} >
                <div className="columns" style={{marginTop:'20px'}}>
                    <div  id="tab-admin-search" className="column is-12 is-pulled-right" style={{padding:'1px 1px 1px'}}>
                        <div className="column is-2 is-pulled-right" style={{padding:'5px 5px 5px'}}>
                            <div className="control has-icons-left has-icons-right is-pulled-right">
                            <input className="input is-danger" type="text" placeholder="Search..." value={query}
                                ref={input => this.search = input}
                                onChange={((e) => this.handleInputChange(e, 'tab1'))}
                                id="search-input"
                            />
                            <span className="icon is-small is-left">
                                <FontAwesomeIcon icon={faSearch} />
                            </span>
                            </div>
                        </div>
                    </div>
                </div>          
            </div>
        );
       
        let page = data.length > 0 ? 1 : 0;
        let total = data.length > 0 ? data.length : 0;
        return (
          <div className="notification">
            
            <div>
                {arrDisplay}   
            </div>
            <div className="columns" style={{marginTop:'10px'}} >
                <div className="column is-2 header-email-contact" style={{width:'200px', padding:'5px 5px 5px'}} >GLOBAL</div>
                <div className="column is-2 header-email-contact" style={{width:'170px', padding:'5px 5px 5px'}}>HQ</div>
                <div className="column is-2 header-email-contact" style={{width:'220px', padding:'5px 5px 5px'}}>Thailand</div>
                <div className="column is-2 header-email-contact" style={{width:'180px', padding:'5px 5px 5px'}}>Indonesia</div>
                <div className="column is-2 header-email-contact" style={{width:'240px', padding:'5px 5px 5px'}}>Philippines</div>
                <div className="column is-2 header-email-contact" style={{width:'160px', padding:'5px 5px 5px'}}>Japan</div>
            </div> 
            <div className="columns">
                <div className="column is-2 body-email-contact" style={{width:'200px', padding:'5px 5px 5px'}} >
                    {arrDisplayGlobal}   
                </div>
                <div className="column is-2 body-email-contact" style={{width:'170px', padding:'5px 5px 5px'}} >
                    {arrDisplayHQ}   
                </div>
                <div className="column is-2 body-email-contact" style={{width:'220px', padding:'5px 5px 5px'}} >
                    {arrDisplayTH}   
                </div>
                <div className="column is-2 body-email-contact" style={{width:'180px', padding:'5px 5px 5px'}} >
                    {arrDisplayID}   
                </div>
                <div className="column is-2 body-email-contact" style={{width:'240px', padding:'5px 5px 5px'}} >
                    {arrDisplayPH}   
                </div>
                <div className="column is-2 body-email-contact" style={{width:'160px', padding:'5px 5px 5px'}} >
                    {arrDisplayJP}   
                </div>
            </div>
            {/* <div className="columns" style={{marginLeft:'20px',marginTop:'20px'}} >
                {'Showing '+page+' to '+total+' of '+total+' entries'}
            </div>           */}
          </div>
        );
        
    } 
     async get_groupEmails() {
       
        let { access_token = "" } = this.props.loginReducer;
        if (access_token === "") {
          // console.log('err');
          //console.log(access_token);
          const request_token = localStorage.getItem("request_token");
          //console.log(request_token);
          await this.props.dispatch(actionGetAccessToken(request_token));
        }
        
        await this.props.dispatch(actionLoadContactData(access_token, 'groupemails'));
        console.log(this.props.contactReducer.data);
        if (this.props.contactReducer.data.length > 0) {
          this.setState({
            loading: 1,
            data: this.props.contactReducer.data,
            firstLoadPage:false
    
          });
        } else {
          this.setState({
            loading: 1,
            data: [],
            firstLoadPage:false
          });
        }
     }
     
    async componentDidUpdate(prevProps) {
        //To Do....
        // const queryString = qs.parse(this.props.sPram);
        // const page = typeof queryString.p == "undefined" ? 1 : queryString.p;
        // if (this.state.page !== page) {
        //   this.get_library();
        // }
    }
    async componentDidMount() {
        this.get_groupEmails();
    }
}
const mapStateToProps = state => {
    return {
        loginReducer: state.loginReducer,
        propertyReducer: state.propertyReducer,
        hrReducer: state.hrReducer,
        contactReducer: state.contactReducer,
        s3Reducer: state.s3Reducer
    };
};
export default connect(mapStateToProps)(GroupEmails);