import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { lang } from "Lang.jsx";
import moment from 'moment';
import Select from 'react-select';
import MomentLocaleUtils, {
    formatDate,
    parseDate,
  } from 'react-day-picker/moment';
/**react-day-picker */
import DayPickerInput from 'react-day-picker/DayPickerInput';
import ReactImageFallback from "react-image-fallback";
import { base_url_webapp,public_url, uniqid, ENV, s3_url_webapp, s3_url_intranet} from "../../Config_API.jsx";
import "react-toastify/dist/ReactToastify.css";
import { actionLoadContactData, actionLoadEntityData, actionLoadHotelContactData } from "actions/contact/actionContact";
import { actionGetAccessToken } from "actions/login";
import qs from "query-string";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faPrint, faBook, faUser, faFolderOpen } from "@fortawesome/free-solid-svg-icons";
class HotelContact extends Component {
    state = {
        loading: '0',
        data: [],
        dataHotels: [],
        dataEntitys: [],
        CountryCode: [],
        firstLoadPage:true,
        from:undefined,
        to:undefined,
        Search:'',
        optionCountry: [
            { label: ("All Country"), value: "" },
            { label: lang("IDN"), value: "IDN" },
            { label: lang("JPN"), value: "JPN" },
            { label: lang("PHI"), value: "PHI" },
            { label: lang("THA"), value: "THA" }
        ],
    }
    render() {       
        const { 
            activeClass,
            data,
            CountryCode,
            dataHotels,
            dataEntitys,
            firstLoadPage,
            Search,
            optionCountry,
            from,
            to
        } = this.state;
        const modifiers = { start: from, end: to };
        let selectCountry = "";
        let selectCountry_temp = ""; 
        let selectEntity_temp = "";
        let selectEntity = "";
        let selectProperty = "";
        let propertyName = "";
        let did_a = "";
        let did_b = "";
        let did_c = "";
        let did_d = "";
        const userinfo = JSON.parse(localStorage.getItem("userinfo"));
       // console.log(CountryCode);
        const query = Search;
        let arrDisplay = [];
        let arrAddress = [];
        let arrHeader = [];
        let arrDisplayOffice = [];
        let arrDisplayBody = [];
        let arrDisplayBodyNonBranded = [];
        let arrDisplayBodyUnder = [];
       // console.log(selectCountry);
        let io_tmp = 0;
        if(dataHotels.length > 0 ){
            dataHotels.map((each_o, key) => {
               
               if(each_o.isHotel == '0' && each_o.isNonBranded == '0' && each_o.isUnderDevelopment == '0'){
                    selectCountry = each_o.CountryCode;
                    selectCountry_temp = (each_o.CountryCode=="THA"?"COR":""); 
                    selectEntity_temp = (each_o.CountryCode=="THA"?"RPHL":""); 
                    selectProperty = each_o.PropertyCode;
                    //selectEntity = each.EntityCode;
                    //console.log(selectEntity+':'+selectCountry);
                   
                    propertyName = each_o.PropertyName;
                    did_a = each_o.PhoneNumber.split('(')[0];
                    did_a = did_a.replace('+','');
                    did_b = each_o.PhoneNumber.split(')')[0];
                    did_b = did_b.split('(')[1];
                    did_c = each_o.PhoneNumber.split(')')[1];
                    if(did_c.split('x')[1] != ''){did_d =  did_c.split('x')[1]}
                    arrDisplayOffice.push(
                        <div className={"columns"+  (io_tmp %2 == 0 ? " odd":" even")} style={{marginLeft:'20px'}}  key={"content-"+io_tmp}>
                            <div className="column is-1 body-hotel-contact" style={{padding:'1px 1px 1px'}} >
                               <select id={"country-"+each_o.CountryCode} key={'country-'+io_tmp} defaultValue={selectCountry}>
                                    {
                                        optionCountry.map((each, key) => {
                                            return <option key={key} value={each.value}>{each.label}</option>
                                        })
                                    }
                                </select>
                            </div>
                            <div className="column is-2 body-hotel-contact" style={{padding:'1px 1px 1px'}}>
                                { <select id={"entity-"+each_o.EntityCode} key={'entity-'+io_tmp} defaultValue={each_o.EntityCode} style={{width:'190px'}} >
                                    {
                                        dataEntitys.map((each1, key1) => {
                                            if(each1.CountryCode == selectCountry || (selectCountry_temp == each1.CountryCode && selectEntity_temp == each1.EntityCode)){
                                                
                                                return <option key={key1}  value={each1.EntityCode}>{each1.EntityName}</option>
                                            }
                                        })
                                    }
                                </select> }
                            </div>
                            <div className="column is-2 body-hotel-contact" style={{padding:'1px 1px 1px'}}>
                                <div>
                                    {
                                        dataHotels.map((each, key) => {
                                            if(each.CountryCode == selectCountry && selectProperty == each.PropertyCode){
                                                return <input type='text' 
                                                            style={{width:'190px'}}
                                                            key={'property-'+io_tmp}
                                                            defaultValue={propertyName}
                                                            onChange={e => this.handleCheckboxChange(e)}                                                            
                                                            id={"office-" + io_tmp}
                                                        />
                                            }
                                        })
                                    }
                                </div>
                                <div style={{fontSize:'12px', fontWeight:'bold', lineHeight:'1.3rem'}} >
                                    <input type='checkbox' 
                                        defaultChecked={each_o.isHotel}
                                        onChange={e => this.handleCheckboxChange(e)}
                                        id={"checkbox-" + io_tmp}
                                    />{'Hotel'}
                                </div>
                                <div style={{fontSize:'12px', fontWeight:'bold', lineHeight:'1.3rem'}}>
                                    <input type='checkbox' 
                                        defaultChecked={each_o.isNonBranded}
                                        onChange={e => this.handleCheckboxChange(e)}
                                        id={"checkbox-" + io_tmp}
                                    />{'Non-Branded Hotel'}
                                </div>
                                <div style={{fontSize:'12px', fontWeight:'bold', lineHeight:'1.3rem'}}>
                                    <input type='checkbox' 
                                        defaultChecked={each_o.isUnderDevelopment}
                                        onChange={e => this.handleCheckboxChange(e)}
                                        id={"checkbox-" + io_tmp}
                                    />{'Hotels Under Development'}
                                </div>                                
                            </div>
                            <div className="column is-2 body-hotel-contact" style={{padding:'1px 1px 1px', width:'228px'}}>
                                <div style={{fontSize:'12px', fontWeight:'bold'}}>{'Address1 :'}</div>
                                <div>
                                    <textarea style={{width:'220px', fontSize:'14px'}} rows="2" maxLength="70" defaultValue={each_o.AddressLine1} ></textarea>
                                </div>                                
                                <div style={{fontSize:'12px', fontWeight:'bold'}}>{'Address2 :'}</div>
                                <div>
                                    <textarea style={{width:'220px', fontSize:'14px'}} rows="2" maxLength="70" defaultValue={each_o.AddressLine2} ></textarea>
                                </div> 
                                <div style={{fontSize:'12px', fontWeight:'bold'}}>{'Phone :'}</div>
                                <div>
                                    {<span style={{'color':'#ff2100'}}>{'+ '}</span>}
                                    <input type="text" style={{width:'25px', textAlign:'center'}} defaultValue={did_a}/>
                                    {<span style={{'color':'#ff2100'}}>{' ('}</span>}
                                    <input type="text" style={{width:'25px', textAlign:'center'}} defaultValue={did_b}/>
                                    {<span style={{'color':'#ff2100'}}>{') '}</span>}
                                    <input type="text" style={{width:'75px'}} defaultValue={did_c}/>
                                    {<span style={{'color':'#ff2100'}}>{' x'}</span>}
                                    <input type="text" style={{width:'35px'}} defaultValue={did_d}/>                                    
                                </div>
                            </div>
                            <div className="column is-1 body-hotel-contact" style={{width:'60px',textAlign:'center',padding:'1px 1px 1px'}}>
                                <input type="text" style={{width:'30px'}} defaultValue={each_o.TotalKeys}/>
                            </div>
                            <div className="column is-1 body-hotel-contact" style={{width:'110px',padding:'1px 1px 1px'}}>
                                {/* {(each.DateOfAcquisition !== ''?'':moment(each.DateOfAcquisition).format('DD-MMM-YYYY'))} */}
                                <div style={{maxWidth:'50%'}}>
                                    <DayPickerInput className="input is-info" style={{maxWidth:'50%'}}
                                        value={from}
                                        format={"YYYY-MM-DD"}
                                        formatDate={formatDate}
                                        parseDate={each_o.DateOfAcquisition}
                                        dayPickerProps={{
                                        selectedDays: [from, { from, to }],
                                        disabledDays: { after: to },
                                        toMonth: to,
                                        modifiers,
                                        numberOfMonths: 1,
                                        onDayClick: () => this.to.getInput().focus(),
                                        locale: lang("code"),
                                        localeUtils: MomentLocaleUtils,
                                        }}
                                        onDayChange={this.handleFromChange}
                                    /> 
                                </div>
                            </div>
                            <div className="column is-1 body-hotel-contact body-hotel-contact-right" style={{width:'110px',padding:'1px 1px 1px'}}>
                                {/* {(each.OperationalDate !== ''?'':moment(each.OperationalDate).format('DD-MMM-YYYY'))} */}
                                <div style={{maxWidth:'50%'}}>
                                    <DayPickerInput className="input is-info" style={{maxWidth:'50%'}}
                                        value={from}
                                        format={"YYYY-MM-DD"}
                                        formatDate={formatDate}
                                        parseDate={each_o.OperationalDate}
                                        dayPickerProps={{
                                        selectedDays: [from, { from, to }],
                                        disabledDays: { after: to },
                                        toMonth: to,
                                        modifiers,
                                        numberOfMonths: 1,
                                        onDayClick: () => this.to.getInput().focus(),
                                        locale: lang("code"),
                                        localeUtils: MomentLocaleUtils,
                                        }}
                                        onDayChange={this.handleFromChange}
                                    /> 
                                </div>
                            </div>
                            <div className="column is-1 body-hotel-contact body-hotel-contact-right" style={{width:'140px',textAlign:'center',padding:'1px 1px 1px'}}>
                                <textarea style={{width:'135px', fontSize:'16px'}} defaultValue={each_o.Col1} ></textarea>
                            </div>
                            <div className="column is-1 body-hotel-contact body-hotel-contact-right" style={{width:'37px',textAlign:'center',padding:'1px 1px 1px'}}>{}</div>
                        </div>          
                    );
                    io_tmp++;
                }
            });
        }
        let ih_tmp = 0;
        if(dataHotels.length > 0 ){
             dataHotels.map((each2, key) => {
               
                if(each2.isHotel == '1' && each2.isNonBranded == '0' && each2.isUnderDevelopment == '0'){
                    selectCountry = each2.CountryCode;
                    selectCountry_temp = (each2.CountryCode=="THA"?"COR":""); 
                    selectEntity_temp = (each2.CountryCode=="THA"?"RPHL":""); 
                    selectProperty = each2.PropertyCode;
                    //selectEntity = each.EntityCode;
                    //console.log(selectEntity+':'+selectCountry);
                
                    propertyName = each2.PropertyName;
                    did_a = each2.PhoneNumber.split('(')[0];
                    did_a = did_a.replace('+','');
                    did_b = each2.PhoneNumber.split(')')[0];
                    did_b = did_b.split('(')[1];
                    did_c = each2.PhoneNumber.split(')')[1];
                   // if(did_cc.split('x')[1] != ''){did_dd =  did_cc.split('x')[1]}
                   arrDisplayBody.push(
                        <div className={"columns"+  (ih_tmp %2 == 0 ? " odd":" even")} style={{marginLeft:'20px'}}  key={"content-"+ih_tmp}>
                            <div className="column is-1 body-hotel-contact" style={{padding:'1px 1px 1px'}} >
                            <select id={"country-"+each2.CountryCode} key={'country-'+ih_tmp} defaultValue={selectCountry}>
                                    {
                                        optionCountry.map((each, key) => {
                                            return <option key={key} value={each.value}>{each.label}</option>
                                        })
                                    }
                                </select>
                            </div>
                            <div className="column is-2 body-hotel-contact" style={{padding:'1px 1px 1px'}}>
                                { <select id={"entity-"+each2.EntityCode} key={'entity-'+io_tmp} defaultValue={each2.EntityCode} style={{width:'190px'}} >
                                    {
                                        dataEntitys.map((each1, key1) => {
                                            if(each1.CountryCode == selectCountry || (selectCountry_temp == each1.CountryCode && selectEntity_temp == each1.EntityCode)){
                                                
                                                return <option key={key1}  value={each1.EntityCode}>{each1.EntityName}</option>
                                            }
                                        })
                                    }
                                </select> }
                            </div>
                            <div className="column is-2 body-hotel-contact" style={{padding:'1px 1px 1px'}}>
                                <div>
                                    {
                                        dataHotels.map((each, key) => {
                                            if(each.CountryCode == selectCountry && selectProperty == each.PropertyCode){
                                                return <input type='text' 
                                                            style={{width:'190px'}}
                                                            key={'property-'+ih_tmp}
                                                            defaultValue={propertyName}
                                                            onChange={e => this.handleCheckboxChange(e)}                                                            
                                                            id={"office-" + ih_tmp}
                                                        />
                                            }
                                        })
                                    }
                                </div>
                                <div style={{fontSize:'12px', fontWeight:'bold', lineHeight:'1.3rem'}} >
                                    <input type='checkbox' 
                                        defaultChecked={each2.isHotel}
                                        onChange={e => this.handleCheckboxChange(e)}
                                        id={"checkbox-" + ih_tmp}
                                    />{'Hotel'}
                                </div>
                                <div style={{fontSize:'12px', fontWeight:'bold', lineHeight:'1.3rem'}}>
                                    <input type='checkbox' 
                                        defaultChecked={each2.isNonBranded}
                                        onChange={e => this.handleCheckboxChange(e)}
                                        id={"checkbox-" + ih_tmp}
                                    />{'Non-Branded Hotel'}
                                </div>
                                <div style={{fontSize:'12px', fontWeight:'bold', lineHeight:'1.3rem'}}>
                                    <input type='checkbox' 
                                        defaultChecked={each2.isUnderDevelopment}
                                        onChange={e => this.handleCheckboxChange(e)}
                                        id={"checkbox-" + ih_tmp}
                                    />{'Hotels Under Development'}
                                </div>                                
                            </div>
                            <div className="column is-2 body-hotel-contact" style={{padding:'1px 1px 1px', width:'228px'}}>
                                <div style={{fontSize:'12px', fontWeight:'bold'}}>{'Address1 :'}</div>
                                <div>
                                    <textarea style={{width:'220px', fontSize:'14px'}} rows="2" maxLength="70" defaultValue={each2.AddressLine1} ></textarea>
                                </div>                                
                                <div style={{fontSize:'12px', fontWeight:'bold'}}>{'Address2 :'}</div>
                                <div>
                                    <textarea style={{width:'220px', fontSize:'14px'}} rows="2" maxLength="70" defaultValue={each2.AddressLine2} ></textarea>
                                </div> 
                                <div style={{fontSize:'12px', fontWeight:'bold'}}>{'Phone :'}</div>
                                <div>
                                    {<span style={{'color':'#ff2100'}}>{'+ '}</span>}
                                    <input type="text" style={{width:'25px', textAlign:'center'}} defaultValue={did_a}/>
                                    {<span style={{'color':'#ff2100'}}>{' ('}</span>}
                                    <input type="text" style={{width:'25px', textAlign:'center'}} defaultValue={did_b}/>
                                    {<span style={{'color':'#ff2100'}}>{') '}</span>}
                                    <input type="text" style={{width:'75px'}} defaultValue={did_c}/>
                                    {<span style={{'color':'#ff2100'}}>{' x'}</span>}
                                    <input type="text" style={{width:'35px'}} defaultValue={did_d}/>                                    
                                </div>
                            </div>
                            <div className="column is-1 body-hotel-contact" style={{width:'60px',textAlign:'center',padding:'1px 1px 1px'}}>
                                <input type="text" style={{width:'30px'}} defaultValue={each2.TotalKeys}/>
                            </div>
                            <div className="column is-1 body-hotel-contact" style={{width:'110px',padding:'1px 1px 1px'}}>
                                {/* {(each.DateOfAcquisition !== ''?'':moment(each.DateOfAcquisition).format('DD-MMM-YYYY'))} */}
                                <div style={{maxWidth:'50%'}}>
                                    <DayPickerInput className="input is-info" style={{maxWidth:'50%'}}
                                        value={from}
                                        format={"YYYY-MM-DD"}
                                        formatDate={formatDate}
                                        parseDate={each2.DateOfAcquisition}
                                        dayPickerProps={{
                                        selectedDays: [from, { from, to }],
                                        disabledDays: { after: to },
                                        toMonth: to,
                                        modifiers,
                                        numberOfMonths: 1,
                                        onDayClick: () => this.to.getInput().focus(),
                                        locale: lang("code"),
                                        localeUtils: MomentLocaleUtils,
                                        }}
                                        onDayChange={this.handleFromChange}
                                    /> 
                                </div>
                            </div>
                            <div className="column is-1 body-hotel-contact body-hotel-contact-right" style={{width:'110px',padding:'1px 1px 1px'}}>
                                {/* {(each.OperationalDate !== ''?'':moment(each.OperationalDate).format('DD-MMM-YYYY'))} */}
                                <div style={{maxWidth:'50%'}}>
                                    <DayPickerInput className="input is-info" style={{maxWidth:'50%'}}
                                        value={from}
                                        format={"YYYY-MM-DD"}
                                        formatDate={formatDate}
                                        parseDate={each2.OperationalDate}
                                        dayPickerProps={{
                                        selectedDays: [from, { from, to }],
                                        disabledDays: { after: to },
                                        toMonth: to,
                                        modifiers,
                                        numberOfMonths: 1,
                                        onDayClick: () => this.to.getInput().focus(),
                                        locale: lang("code"),
                                        localeUtils: MomentLocaleUtils,
                                        }}
                                        onDayChange={this.handleFromChange}
                                    /> 
                                </div>
                            </div>
                            <div className="column is-1 body-hotel-contact body-hotel-contact-right" style={{width:'140px',textAlign:'center',padding:'1px 1px 1px'}}>
                                <textarea style={{width:'135px', fontSize:'16px'}} defaultValue={each2.Col1} ></textarea>
                            </div>
                            <div className="column is-1 body-hotel-contact body-hotel-contact-right" style={{width:'37px',textAlign:'center',padding:'1px 1px 1px'}}>{}</div>
                        </div>          
                    );
                    ih_tmp++;
                }
            });
        }
        let in_tmp = 0;
        if(dataHotels.length > 0 ){
             dataHotels.map((each2, key) => {
               
                if(each2.isHotel == '0' && each2.isNonBranded == '1' && each2.isUnderDevelopment == '0'){
                    selectCountry = each2.CountryCode;
                    selectCountry_temp = (each2.CountryCode=="THA"?"COR":""); 
                    selectEntity_temp = (each2.CountryCode=="THA"?"RPHL":""); 
                    selectProperty = each2.PropertyCode;
                    //selectEntity = each.EntityCode;
                    //console.log(selectEntity+':'+selectCountry);
                
                    propertyName = each2.PropertyName;
                    did_a = each2.PhoneNumber.split('(')[0];
                    did_a = did_a.replace('+','');
                    did_b = each2.PhoneNumber.split(')')[0];
                    did_b = did_b.split('(')[1];
                    did_c = each2.PhoneNumber.split(')')[1];
//                    if(did_c.split('x')[1] != ''){did_d =  did_c.split('x')[1]}
                   arrDisplayBodyNonBranded.push(
                        <div className={"columns"+  (in_tmp %2 == 0 ? " odd":" even")} style={{marginLeft:'20px'}}  key={"content-"+in_tmp}>
                            <div className="column is-1 body-hotel-contact" style={{padding:'1px 1px 1px'}} >
                            <select id={"country-"+each2.CountryCode} key={'country-'+in_tmp} defaultValue={selectCountry}>
                                    {
                                        optionCountry.map((each, key) => {
                                            return <option key={key} value={each.value}>{each.label}</option>
                                        })
                                    }
                                </select>
                            </div>
                            <div className="column is-2 body-hotel-contact" style={{padding:'1px 1px 1px'}}>
                                { <select id={"entity-"+each2.EntityCode} key={'entity-'+in_tmp} defaultValue={each2.EntityCode} style={{width:'190px'}} >
                                    {
                                        dataEntitys.map((each1, key1) => {
                                            if(each1.CountryCode == selectCountry || (selectCountry_temp == each1.CountryCode && selectEntity_temp == each1.EntityCode)){
                                                
                                                return <option key={key1}  value={each1.EntityCode}>{each1.EntityName}</option>
                                            }
                                        })
                                    }
                                </select> }
                            </div>
                            <div className="column is-2 body-hotel-contact" style={{padding:'1px 1px 1px'}}>
                                <div>
                                    {
                                        dataHotels.map((each, key) => {
                                            if(each.CountryCode == selectCountry && selectProperty == each.PropertyCode){
                                                return <input type='text' 
                                                            style={{width:'190px'}}
                                                            key={'property-'+in_tmp}
                                                            defaultValue={propertyName}
                                                            onChange={e => this.handleCheckboxChange(e)}                                                            
                                                            id={"office-" + in_tmp}
                                                        />
                                            }
                                        })
                                    }
                                </div>
                                <div style={{fontSize:'12px', fontWeight:'bold', lineHeight:'1.3rem'}} >
                                    <input type='checkbox' 
                                        defaultChecked={each2.isHotel}
                                        onChange={e => this.handleCheckboxChange(e)}
                                        id={"checkbox-" + in_tmp}
                                    />{'Hotel'}
                                </div>
                                <div style={{fontSize:'12px', fontWeight:'bold', lineHeight:'1.3rem'}}>
                                    <input type='checkbox' 
                                        defaultChecked={each2.isNonBranded}
                                        onChange={e => this.handleCheckboxChange(e)}
                                        id={"checkbox-" + in_tmp}
                                    />{'Non-Branded Hotel'}
                                </div>
                                <div style={{fontSize:'12px', fontWeight:'bold', lineHeight:'1.3rem'}}>
                                    <input type='checkbox' 
                                        defaultChecked={each2.isUnderDevelopment}
                                        onChange={e => this.handleCheckboxChange(e)}
                                        id={"checkbox-" + in_tmp}
                                    />{'Hotels Under Development'}
                                </div>                                
                            </div>
                            <div className="column is-2 body-hotel-contact" style={{padding:'1px 1px 1px', width:'228px'}}>
                                <div style={{fontSize:'12px', fontWeight:'bold'}}>{'Address1 :'}</div>
                                <div>
                                    <textarea style={{width:'220px', fontSize:'14px'}} rows="2" maxLength="70" defaultValue={each2.AddressLine1} ></textarea>
                                </div>                                
                                <div style={{fontSize:'12px', fontWeight:'bold'}}>{'Address2 :'}</div>
                                <div>
                                    <textarea style={{width:'220px', fontSize:'14px'}} rows="2" maxLength="70" defaultValue={each2.AddressLine2} ></textarea>
                                </div> 
                                <div style={{fontSize:'12px', fontWeight:'bold'}}>{'Phone :'}</div>
                                <div>
                                    {<span style={{'color':'#ff2100'}}>{'+ '}</span>}
                                    <input type="text" style={{width:'25px', textAlign:'center'}} defaultValue={did_a}/>
                                    {<span style={{'color':'#ff2100'}}>{' ('}</span>}
                                    <input type="text" style={{width:'25px', textAlign:'center'}} defaultValue={did_b}/>
                                    {<span style={{'color':'#ff2100'}}>{') '}</span>}
                                    <input type="text" style={{width:'75px'}} defaultValue={did_c}/>
                                    {<span style={{'color':'#ff2100'}}>{' x'}</span>}
                                    <input type="text" style={{width:'35px'}} defaultValue={did_d}/>                                    
                                </div>
                            </div>
                            <div className="column is-1 body-hotel-contact" style={{width:'60px',textAlign:'center',padding:'1px 1px 1px'}}>
                                <input type="text" style={{width:'30px'}} defaultValue={each2.TotalKeys}/>
                            </div>
                            <div className="column is-1 body-hotel-contact" style={{width:'110px',padding:'1px 1px 1px'}}>
                                {/* {(each.DateOfAcquisition !== ''?'':moment(each.DateOfAcquisition).format('DD-MMM-YYYY'))} */}
                                <div style={{maxWidth:'50%'}}>
                                    <DayPickerInput className="input is-info" style={{maxWidth:'50%'}}
                                        value={from}
                                        format={"YYYY-MM-DD"}
                                        formatDate={formatDate}
                                        parseDate={each2.DateOfAcquisition}
                                        dayPickerProps={{
                                        selectedDays: [from, { from, to }],
                                        disabledDays: { after: to },
                                        toMonth: to,
                                        modifiers,
                                        numberOfMonths: 1,
                                        onDayClick: () => this.to.getInput().focus(),
                                        locale: lang("code"),
                                        localeUtils: MomentLocaleUtils,
                                        }}
                                        onDayChange={this.handleFromChange}
                                    /> 
                                </div>
                            </div>
                            <div className="column is-1 body-hotel-contact body-hotel-contact-right" style={{width:'110px',padding:'1px 1px 1px'}}>
                                {/* {(each.OperationalDate !== ''?'':moment(each.OperationalDate).format('DD-MMM-YYYY'))} */}
                                <div style={{maxWidth:'50%'}}>
                                    <DayPickerInput className="input is-info" style={{maxWidth:'50%'}}
                                        value={from}
                                        format={"YYYY-MM-DD"}
                                        formatDate={formatDate}
                                        parseDate={each2.OperationalDate}
                                        dayPickerProps={{
                                        selectedDays: [from, { from, to }],
                                        disabledDays: { after: to },
                                        toMonth: to,
                                        modifiers,
                                        numberOfMonths: 1,
                                        onDayClick: () => this.to.getInput().focus(),
                                        locale: lang("code"),
                                        localeUtils: MomentLocaleUtils,
                                        }}
                                        onDayChange={this.handleFromChange}
                                    /> 
                                </div>
                            </div>
                            <div className="column is-1 body-hotel-contact body-hotel-contact-right" style={{width:'140px',textAlign:'center',padding:'1px 1px 1px'}}>
                                <textarea style={{width:'135px', fontSize:'16px'}} defaultValue={each2.Col1} ></textarea>
                            </div>
                            <div className="column is-1 body-hotel-contact body-hotel-contact-right" style={{width:'37px',textAlign:'center',padding:'1px 1px 1px'}}>{}</div>
                        </div>          
                    );
                    in_tmp++;
                }
            });
         }
         let un_tmp = 0;
        if(dataHotels.length > 0 ){
            dataHotels.map((each2, key) => {
              
               if(each2.isHotel == '1' && each2.isNonBranded == '0' && each2.isUnderDevelopment == '1'){
                   selectCountry = each2.CountryCode;
                   selectCountry_temp = (each2.CountryCode=="THA"?"COR":""); 
                   selectEntity_temp = (each2.CountryCode=="THA"?"RPHL":""); 
                   selectProperty = each2.PropertyCode;
                   //selectEntity = each.EntityCode;
                   //console.log(selectEntity+':'+selectCountry);
               
                   propertyName = each2.PropertyName;
                   did_a = each2.PhoneNumber.split('(')[0];
                   did_a = did_a.replace('+','');
                   did_b = each2.PhoneNumber.split(')')[0];
                   did_b = did_b.split('(')[1];
                   did_c = each2.PhoneNumber.split(')')[1];
//                    if(did_c.split('x')[1] != ''){did_d =  did_c.split('x')[1]}
arrDisplayBodyUnder.push(
                       <div className={"columns"+  (un_tmp %2 == 0 ? " odd":" even")} style={{marginLeft:'20px'}}  key={"content-"+un_tmp}>
                           <div className="column is-1 body-hotel-contact" style={{padding:'1px 1px 1px'}} >
                           <select id={"country-"+each2.CountryCode} key={'country-'+un_tmp} defaultValue={selectCountry}>
                                   {
                                       optionCountry.map((each, key) => {
                                           return <option key={key} value={each.value}>{each.label}</option>
                                       })
                                   }
                               </select>
                           </div>
                           <div className="column is-2 body-hotel-contact" style={{padding:'1px 1px 1px'}}>
                               { <select id={"entity-"+each2.EntityCode} key={'entity-'+un_tmp} defaultValue={each2.EntityCode} style={{width:'190px'}} >
                                   {
                                       dataEntitys.map((each1, key1) => {
                                           if(each1.CountryCode == selectCountry || (selectCountry_temp == each1.CountryCode && selectEntity_temp == each1.EntityCode)){
                                               
                                               return <option key={key1}  value={each1.EntityCode}>{each1.EntityName}</option>
                                           }
                                       })
                                   }
                               </select> }
                           </div>
                           <div className="column is-2 body-hotel-contact" style={{padding:'1px 1px 1px'}}>
                               <div>
                                   {
                                       dataHotels.map((each, key) => {
                                           if(each.CountryCode == selectCountry && selectProperty == each.PropertyCode){
                                               return <input type='text' 
                                                           style={{width:'190px'}}
                                                           key={'property-'+un_tmp}
                                                           defaultValue={propertyName}
                                                           onChange={e => this.handleCheckboxChange(e)}                                                            
                                                           id={"office-" + un_tmp}
                                                       />
                                           }
                                       })
                                   }
                               </div>
                               <div style={{fontSize:'12px', fontWeight:'bold', lineHeight:'1.3rem'}} >
                                   <input type='checkbox' 
                                       defaultChecked={each2.isHotel}
                                       onChange={e => this.handleCheckboxChange(e)}
                                       id={"checkbox-" + un_tmp}
                                   />{'Hotel'}
                               </div>
                               <div style={{fontSize:'12px', fontWeight:'bold', lineHeight:'1.3rem'}}>
                                   <input type='checkbox' 
                                       defaultChecked={each2.isNonBranded}
                                       onChange={e => this.handleCheckboxChange(e)}
                                       id={"checkbox-" + un_tmp}
                                   />{'Non-Branded Hotel'}
                               </div>
                               <div style={{fontSize:'12px', fontWeight:'bold', lineHeight:'1.3rem'}}>
                                   <input type='checkbox' 
                                       defaultChecked={each2.isUnderDevelopment}
                                       onChange={e => this.handleCheckboxChange(e)}
                                       id={"checkbox-" + un_tmp}
                                   />{'Hotels Under Development'}
                               </div>                                
                           </div>
                           <div className="column is-2 body-hotel-contact" style={{padding:'1px 1px 1px', width:'228px'}}>
                               <div style={{fontSize:'12px', fontWeight:'bold'}}>{'Address1 :'}</div>
                               <div>
                                   <textarea style={{width:'220px', fontSize:'14px'}} rows="2" maxLength="70" defaultValue={each2.AddressLine1} ></textarea>
                               </div>                                
                               <div style={{fontSize:'12px', fontWeight:'bold'}}>{'Address2 :'}</div>
                               <div>
                                   <textarea style={{width:'220px', fontSize:'14px'}} rows="2" maxLength="70" defaultValue={each2.AddressLine2} ></textarea>
                               </div> 
                               <div style={{fontSize:'12px', fontWeight:'bold'}}>{'Phone :'}</div>
                               <div>
                                   {<span style={{'color':'#ff2100'}}>{'+ '}</span>}
                                   <input type="text" style={{width:'25px', textAlign:'center'}} defaultValue={did_a}/>
                                   {<span style={{'color':'#ff2100'}}>{' ('}</span>}
                                   <input type="text" style={{width:'25px', textAlign:'center'}} defaultValue={did_b}/>
                                   {<span style={{'color':'#ff2100'}}>{') '}</span>}
                                   <input type="text" style={{width:'75px'}} defaultValue={did_c}/>
                                   {<span style={{'color':'#ff2100'}}>{' x'}</span>}
                                   <input type="text" style={{width:'35px'}} defaultValue={did_d}/>                                    
                               </div>
                           </div>
                           <div className="column is-1 body-hotel-contact" style={{width:'60px',textAlign:'center',padding:'1px 1px 1px'}}>
                               <input type="text" style={{width:'30px'}} defaultValue={each2.TotalKeys}/>
                           </div>
                           <div className="column is-1 body-hotel-contact" style={{width:'110px',padding:'1px 1px 1px'}}>
                               {/* {(each.DateOfAcquisition !== ''?'':moment(each.DateOfAcquisition).format('DD-MMM-YYYY'))} */}
                               <div style={{maxWidth:'50%'}}>
                                   <DayPickerInput className="input is-info" style={{maxWidth:'50%'}}
                                       value={from}
                                       format={"YYYY-MM-DD"}
                                       formatDate={formatDate}
                                       parseDate={each2.DateOfAcquisition}
                                       dayPickerProps={{
                                       selectedDays: [from, { from, to }],
                                       disabledDays: { after: to },
                                       toMonth: to,
                                       modifiers,
                                       numberOfMonths: 1,
                                       onDayClick: () => this.to.getInput().focus(),
                                       locale: lang("code"),
                                       localeUtils: MomentLocaleUtils,
                                       }}
                                       onDayChange={this.handleFromChange}
                                   /> 
                               </div>
                           </div>
                           <div className="column is-1 body-hotel-contact body-hotel-contact-right" style={{width:'110px',padding:'1px 1px 1px'}}>
                               {/* {(each.OperationalDate !== ''?'':moment(each.OperationalDate).format('DD-MMM-YYYY'))} */}
                               <div style={{maxWidth:'50%'}}>
                                   <DayPickerInput className="input is-info" style={{maxWidth:'50%'}}
                                       value={from}
                                       format={"YYYY-MM-DD"}
                                       formatDate={formatDate}
                                       parseDate={each2.OperationalDate}
                                       dayPickerProps={{
                                       selectedDays: [from, { from, to }],
                                       disabledDays: { after: to },
                                       toMonth: to,
                                       modifiers,
                                       numberOfMonths: 1,
                                       onDayClick: () => this.to.getInput().focus(),
                                       locale: lang("code"),
                                       localeUtils: MomentLocaleUtils,
                                       }}
                                       onDayChange={this.handleFromChange}
                                   /> 
                               </div>
                           </div>
                           <div className="column is-1 body-hotel-contact body-hotel-contact-right" style={{width:'140px',textAlign:'center',padding:'1px 1px 1px'}}>
                               <textarea style={{width:'135px', fontSize:'16px'}} defaultValue={each2.Col1} ></textarea>
                           </div>
                           <div className="column is-1 body-hotel-contact body-hotel-contact-right" style={{width:'37px',textAlign:'center',padding:'1px 1px 1px'}}>{}</div>
                       </div>          
                   );
                   un_tmp++;
               }
           });
        }
        arrHeader.push(
            <div key={"content" + activeClass} style={{marginLeft:'-70px'}}>
                <div className="columns">
                    <div className="column is-12 left-title" style={{fontSize:'1.9rem',  fontWeight:'900'}} >{"MANAGE HOTELS CONTACT"}</div>
                </div>            
            </div>
        );
        arrDisplay.push(
            <div key={"content" + activeClass} >
                <div className="columns" style={{marginLeft:'-18px'}}>
                    <div  id="tab-admin-search" className="column is-9" style={{padding:'1px 1px 1px'}}>
                        <div className="column is-3" style={{padding:'5px 5px 5px'}}>
                            <div className="control has-icons-left has-icons-right is-pulled-right">
                            <input className="input is-danger" type="text" placeholder="Search for hotel name." value={query}
                                ref={input => this.search = input}
                                onChange={((e) => this.handleInputChange(e, 'tab1'))}
                                id="search-input"
                            />
                            <span className="icon is-small is-left" style={{padding:'1px 1px 1px'}}>
                                <FontAwesomeIcon icon={faSearch} />
                            </span>
                            </div>
                        </div>
                    </div>
                    <div className="column is-3 has-text-right" style={{marginTop:'5px', padding:'1px 1px 1px'}} >
                      <button className="button is-danger"> {'Create New Hotel/Office'} </button>
                    </div>
                </div>          
            </div>
        );
        return (
          <div className="notification">
            <div>
                {arrHeader}   
            </div>
            <div>
                {arrDisplay}   
            </div>
            <div className="columns" style={{marginTop:'20px'}}>
                <div className="column is-12 header-managehotel-contact" style={{textAlign:'left', fontSize:'20px', fontWeight:'bold'}}>Offices</div>
            </div> 
             <div className="columns" >
                <div className="column is-1 header-managehotel-contact">Country</div>
                <div className="column is-2 header-managehotel-contact" >Company</div>
                <div className="column is-2 header-managehotel-contact">Hotel/Location</div>
                <div className="column is-2 header-managehotel-contact" style={{width:'228px'}}>Address</div>
                <div className="column is-1 header-managehotel-contact" style={{width:'60px'}}>Total Keys</div>
                <div className="column is-1 header-managehotel-contact" style={{width:'110px'}}>Date Of Acquisition</div>
                <div className="column is-1 header-managehotel-contact" style={{width:'110px'}}>Operational Date</div>
                <div className="column is-1 header-managehotel-contact" style={{width:'140px'}}>Remark</div>
                <div className="column is-1 header-managehotel-contact" style={{width:'37px'}}></div>
            </div>
            <div style={{marginLeft:'-32px'}} >
                {arrDisplayOffice}
            </div> 
            <div className="columns even" style={{marginTop:'10px'}} >
                <div className="column is-12 header-managehotel-contact" style={{textAlign:'left', fontSize:'20px', fontWeight:'bold'}}>Hotels</div>
            </div>
            <div style={{marginLeft:'-32px'}}>
                {arrDisplayBody}
            </div>
            <div className="columns even" style={{marginTop:'10px'}} >
                <div className="column is-12 header-managehotel-contact" style={{textAlign:'left', fontSize:'20px', fontWeight:'bold'}}>Non-Branded Hotels</div>
            </div>
            <div style={{marginLeft:'-32px'}}>
                {arrDisplayBodyNonBranded}
            </div>
            <div className="columns even" style={{marginTop:'10px'}} >
                <div className="column is-12 header-managehotel-contact" style={{textAlign:'left', fontSize:'20px', fontWeight:'bold'}}>Hotels Under Development</div>
            </div>           
            <div style={{marginLeft:'-32px'}}>
                {arrDisplayBodyUnder}
            </div>           
          </div>
        );
        
    }    
    handleInputChange = (e, tabs) => {
        //console.log(this.search.value);
        this.setState({ Search: this.search.value });
        this.get_hotelscontact();
      }
     async handleCheckboxChange(e){
       const {CountryCode} = this.state;
       const {value, id ,checked} = e.target;
       if(CountryCode.indexOf(value) <= -1 && checked){
           CountryCode.push(value);  
         }
         else{
             let index = CountryCode.indexOf(value);
             CountryCode.splice(index,1);        
         }
       //console.log(CountryCode);
       this.get_hotelscontact();
     }
     async get_hotels() {
       
       let { access_token = "" } = this.props.loginReducer;
       if (access_token === "") {
         // console.log('err');
         //console.log(access_token);
         const request_token = localStorage.getItem("request_token");
         //console.log(request_token);
         await this.props.dispatch(actionGetAccessToken(request_token));
       }
       
       await this.props.dispatch(actionLoadContactData(access_token, 'hotels'));
       //console.log(this.props.contactReducer.data);
       if (this.props.contactReducer.data.length > 0) {
         this.setState({
           loading: 1,
           data: this.props.contactReducer.data,
           firstLoadPage:false
   
         });
       } else {
         this.setState({
           loading: 1,
           data: [],
           firstLoadPage:false
         });
       }
     }
     async get_hotelscontact() {
       const {CountryCode} = this.state;
       const search = (typeof this.search == 'undefined'? '':this.search.value);   
       let Country_Code = '';
       if(CountryCode.length > 0 ){
           CountryCode.map((each, key) => {
               Country_Code += each+',';
           });
       }
       const param = {
           CountryCode:Country_Code,
           Search:search
       }
       let { access_token = "" } = this.props.loginReducer;
       if (access_token === "") {
         // console.log('err');
         //console.log(access_token);
         const request_token = localStorage.getItem("request_token");
         //console.log(request_token);
         await this.props.dispatch(actionGetAccessToken(request_token));
       }
       
       await this.props.dispatch(actionLoadHotelContactData(access_token, 'hotelsContact',param));
       //console.log(param);
       //console.log(this.props.contactReducer.data);
       if (this.props.contactReducer.data.length > 0) {
         this.setState({
           loading: 1,
           dataHotels: this.props.contactReducer.data,
           firstLoadPage:false
   
         });
       } else {
         this.setState({
           loading: 1,
           dataHotels: [],
           firstLoadPage:false
         });
       }
     }
     async get_entity() {
       
        let IsActive = '1'; 
        let { access_token = "" } = this.props.loginReducer;
        if (access_token === "") {
          // console.log('err');
          //console.log(access_token);
          const request_token = localStorage.getItem("request_token");
          //console.log(request_token);
          await this.props.dispatch(actionGetAccessToken(request_token));
        }
        const param = {
            isactive:IsActive
        }
        //console.log(param);
        await this.props.dispatch(actionLoadEntityData(access_token, 'entity', param));
        //console.log(this.props.contactReducer.data);
        if (this.props.contactReducer.data.length > 0) {
          this.setState({
            loading: 1,
            dataEntitys: this.props.contactReducer.data,
            firstLoadPage:false
    
          });
        } else {
          this.setState({
            loading: 1,
            dataEntitys: [],
            firstLoadPage:false
          });
        }
      }
    async componentDidUpdate(prevProps) {
        //To Do....
        // const queryString = qs.parse(this.props.sPram);
        // const page = typeof queryString.p == "undefined" ? 1 : queryString.p;
        // if (this.state.page !== page) {
        //   this.get_library();
        // }
    }
    async componentDidMount() {
        const {CountryCode} = this.state;
        const userinfo = JSON.parse(localStorage.getItem("userinfo"));
        //this.setState({CountryCode:userinfo.country});
        if(userinfo.country == 'THA' || userinfo.office == 'HQ'){
            CountryCode.push('THA');
        }
        if(userinfo.country == 'JPN' || userinfo.office == 'HQ'){
            CountryCode.push('JPN');
        }
        if(userinfo.country == 'IDN' || userinfo.office == 'HQ'){
            CountryCode.push('IDN');
        }
        if(userinfo.country == 'PHI' || userinfo.office == 'HQ'){
            CountryCode.push('PHI');
        }
        //console.log(userinfo);
        this.get_hotels();
        this.get_hotelscontact();
        this.get_entity();
    }
}
const mapStateToProps = state => {
    return {
        loginReducer: state.loginReducer,
        propertyReducer: state.propertyReducer,
        hrReducer: state.hrReducer,
        contactReducer: state.contactReducer,
        s3Reducer: state.s3Reducer
    };
};
export default connect(mapStateToProps)(HotelContact);