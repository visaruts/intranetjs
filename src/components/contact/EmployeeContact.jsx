import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { lang } from "Lang.jsx";
import moment from 'moment';
import ReactImageFallback from "react-image-fallback";
import { base_url_webapp,public_url, uniqid, ENV, s3_url_webapp, s3_url_intranet} from "../../Config_API.jsx";
import "react-toastify/dist/ReactToastify.css";
import { actionLoadEmployeeContactData, actionLoadHotelContactData } from "actions/contact/actionContact";
import { actionGetAccessToken } from "actions/login";
import qs from "query-string";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faPrint, faBook, faUser, faFolderOpen } from "@fortawesome/free-solid-svg-icons";
class EmployeeContact extends Component {
    state = {
        loading: '0',
        data: [],
        dataHotels: [],
        CountryCode: [],
        firstLoadPage:true,
        Search:''
    }
    render() {       
        const { 
            activeClass,
            data,
            CountryCode,
            dataHotels,
            firstLoadPage,
            Search
        } = this.state;
        
        const userinfo = JSON.parse(localStorage.getItem("userinfo"));
       // console.log(CountryCode);
        const query = Search;
        let arrDisplay = [];
        let arrAddress = [];
        let arrHeader = [];
        let arrDisplaySelect = [];
        let arrDisplayBody = [];
        
        arrHeader.push(
            <div key={"content" + activeClass} style={{marginLeft:'-40px'}}>
                <div className="columns">
                    <div className="column is-10 left-title" style={{fontSize:'1.9rem', color:'#d40032', fontWeight:'900'}} >{"EMPLOYEE CONTACT"}</div>
                    <div className="column is-2 has-text-right" >
                      <button className="button is-danger"> {'Export excel'} </button>
                    </div>
                </div>            
            </div>
        );
        let checked_hq=false;
        let checked_tha=false;
        let checked_jpn=false;
        let checked_idn=false;
        let checked_phi=false;
        if(firstLoadPage){  
            if(userinfo.office == 'HQ'){
                checked_hq=true;
                checked_tha=true;
            }      
            if(userinfo.country == 'THA'){
                checked_tha=true;
            }
            if(userinfo.country == 'JPN' ){
                checked_jpn=true;
            }
            if(userinfo.country == 'IDN' ){
                checked_idn=true;
            }
            if(userinfo.country == 'PHI' ){
                checked_phi=true;
            }
        }
        arrDisplaySelect.push(
            <div key={"content" + activeClass} style={{marginLeft:'30px'}}>
                <div className="columns">
                    <div className="column is-5" >
                        <input type='checkbox' value={'HQ'} 
                            defaultChecked={checked_hq}
                            onChange={e => this.handleCheckboxChange(e)}
                            id={"checkbox-hq"}
                            type='checkbox'
                        />
                        <b>{'Headquarters '}</b> 
                        <input type='checkbox' value={'THA'} 
                            defaultChecked={checked_tha}
                            onChange={e => this.handleCheckboxChange(e)}
                            id={"checkbox-tha"}
                            type='checkbox'
                        />
                        <b>{'Thailand '}</b> 
                        <input type='checkbox' value={'JPN'} 
                            defaultChecked={checked_jpn}
                            onChange={e => this.handleCheckboxChange(e)}
                            id={"checkbox-jpn"}
                            type='checkbox'
                        />
                        <b>{'Japan '}</b> 
                        <input type='checkbox' value={'IDN'} 
                            defaultChecked={checked_idn}
                            onChange={e => this.handleCheckboxChange(e)}
                            id={"checkbox-idn"}
                            type='checkbox'
                        />
                        <b>{'Indoneasia '}</b> 
                        <input type='checkbox' value={'PHI'} 
                            defaultChecked={checked_phi}
                            onChange={e => this.handleCheckboxChange(e)}
                            id={"checkbox-phi"}
                            type='checkbox'
                        />
                        <b>{'Philippines '}</b> 
                    </div>
                </div>
            </div>
        );
        
        let i_tmp = 0;
        if(dataHotels.length > 0 ){
            dataHotels.map((each, key) => {
                if(each.isHotel == '0' && each.isNonBranded == '0'){
                    arrAddress.push(
                        <div key={"content-"+i_tmp}>
                            
                            <div className="column is-12" style={{borderBottom: '1px solid #cccccc', lineHeight:'1.2rem'}} >
                                <div style={{color: '#2c2c2c'}} ><b>{each.PropertyName}</b></div>
                                <div >{each.FullAddress}</div>
                                <div >{"Tel:"+each.PhoneNumber}</div>
                            </div>            
                        </div>
                    );
                    i_tmp++;
                }
            });
        }
        
        arrDisplay.push(
            <div key={"content" + activeClass} >
                <div className="columns" style={{marginTop:'20px'}}>
                    <div  id="tab-admin-search" className="column is-12 is-pulled-right" style={{padding:'1px 1px 1px'}}>
                        <div className="column is-2 is-pulled-right" style={{padding:'5px 5px 5px'}}>
                            <div className="control has-icons-left has-icons-right is-pulled-right">
                            <input className="input is-danger" type="text" placeholder="Search..." value={query}
                                ref={input => this.search = input}
                                onChange={((e) => this.handleInputChange(e, 'tab1'))}
                                id="search-input"
                            />
                            <span className="icon is-small is-left">
                                <FontAwesomeIcon icon={faSearch} />
                            </span>
                            </div>
                        </div>
                    </div>
                </div>          
            </div>
        );
        let ie_tmp = 0;

        if(data.length > 0 ){
            data.map((each, key) => {
                arrDisplayBody.push(
                    <div className={"columns"+  (ie_tmp %2 == 0 ? " odd":" even")} style={{marginLeft:'20px'}} key={'content-'+ie_tmp} >
                        <div className="column is-2 body-employee-contact" style={{width:'130px', padding:'5px 5px 5px'}} >{lang(each.CountryCode)}</div>
                        <div className="column is-2 body-employee-contact" style={{width:'250px', padding:'5px 5px 5px'}}>{each.UserFName+' '+each.UserLName}</div>
                        <div className="column is-2 body-employee-contact" style={{width:'210px', padding:'5px 5px 5px'}}>{each.JobTitle}</div>
                        <div className="column is-2 body-employee-contact" style={{width:'150px', padding:'5px 5px 5px'}}>{each.DID_Phone}</div>
                        <div className="column is-2 body-employee-contact" style={{width:'150px', padding:'5px 5px 5px'}}>{each.Cell_Phone}</div>
                        <div className="column is-2 body-employee-contact body-employee-contact-right" style={{width:'250px', padding:'5px 5px 5px'}}>{each.UserEmail}</div>
                    </div> 
                );
                ie_tmp++;
            });
        }
        let page = data.length > 0 ? 1 : 0;
        let total = data.length > 0 ? data.length : 0;
        return (
          <div className="notification">
            
            <div>
                {arrHeader}   
            </div>
            <div>
                {arrDisplaySelect}
            </div>
            <div style={{marginLeft:'30px'}}>
                {arrAddress}   
            </div>
            <div>
                {arrDisplay}   
            </div>
            <div className="columns" style={{marginLeft:'20px',marginTop:'10px'}} >
                <div className="column is-2 header-employee-contact" style={{width:'130px'}} >Country</div>
                <div className="column is-2 header-employee-contact" style={{width:'250px'}}>Name</div>
                <div className="column is-2 header-employee-contact" style={{width:'210px'}}>Title</div>
                <div className="column is-2 header-employee-contact" style={{width:'150px'}}>Land Phone</div>
                <div className="column is-2 header-employee-contact" style={{width:'150px'}}>Cellular Phone</div>
                <div className="column is-3 header-employee-contact" style={{width:'250px'}}>Email</div>
            </div> 
            <div>
                {arrDisplayBody}   
            </div> 
            <div className="columns" style={{marginLeft:'20px',marginTop:'20px'}} >
                {'Showing '+page+' to '+total+' of '+total+' entries'}
            </div>          
          </div>
        );
        
    }    
    handleInputChange = (e, tabs) => {
        //console.log(this.search.value);
        this.setState({ Search: this.search.value });
        this.get_employeecontact();
      }
     async handleCheckboxChange(e){
       const {CountryCode} = this.state;
       const {value, id ,checked} = e.target;
       if(CountryCode.indexOf(value) <= -1 && checked){
           CountryCode.push(value);  
         }
         else{
             let index = CountryCode.indexOf(value);
             CountryCode.splice(index,1);        
         }
       //console.log(CountryCode);
       this.get_hotelscontact();
       this.get_employeecontact();
     }
     
     async get_hotelscontact() {
       const {CountryCode} = this.state;
       const search = (typeof this.search == 'undefined'? '':this.search.value);   
       let Country_Code = '';
       
       if(CountryCode.length > 0 ){
           
           CountryCode.map((each, key) => {
                let val = each;
               if(each == 'HQ'){
                    val = 'THA';
               }
               Country_Code += val+',';
           });
       }
       const param = {
           CountryCode:Country_Code,
           Search:search
       }
       let { access_token = "" } = this.props.loginReducer;
       if (access_token === "") {
         // console.log('err');
         //console.log(access_token);
         const request_token = localStorage.getItem("request_token");
         //console.log(request_token);
         await this.props.dispatch(actionGetAccessToken(request_token));
       }
       
       await this.props.dispatch(actionLoadHotelContactData(access_token, 'hotelsContact',param));
       //console.log(param);
       //console.log(this.props.contactReducer.data);
       if (this.props.contactReducer.data.length > 0) {
         this.setState({
           loading: 1,
           dataHotels: this.props.contactReducer.data,
           firstLoadPage:false
   
         });
       } else {
         this.setState({
           loading: 1,
           dataHotels: [],
           firstLoadPage:false
         });
       }
     }
     async get_employeecontact() {
        const {CountryCode} = this.state;
        const search = (typeof this.search == 'undefined'? '':this.search.value);   
        let Country_Code = '';        
        if(CountryCode.length > 0 ){            
            CountryCode.map((each, key) => {
                Country_Code += each+',';
            });
        }
        const param = {
            CountryCode:Country_Code,
            Search:search
        }
        let { access_token = "" } = this.props.loginReducer;
        if (access_token === "") {
          // console.log('err');
          //console.log(access_token);
          const request_token = localStorage.getItem("request_token");
          //console.log(request_token);
          await this.props.dispatch(actionGetAccessToken(request_token));
        }
        
        await this.props.dispatch(actionLoadEmployeeContactData(access_token, 'employeeContact',param));
        //console.log(param);
        //console.log(this.props.contactReducer.data);
        if (this.props.contactReducer.data.length > 0) {
          this.setState({
            loading: 1,
            data: this.props.contactReducer.data,
            firstLoadPage:false
    
          });
        } else {
          this.setState({
            loading: 1,
            data: [],
            firstLoadPage:false
          });
        }
      }
    async componentDidUpdate(prevProps) {
        //To Do....
        // const queryString = qs.parse(this.props.sPram);
        // const page = typeof queryString.p == "undefined" ? 1 : queryString.p;
        // if (this.state.page !== page) {
        //   this.get_library();
        // }
    }
    async componentDidMount() {
        const {CountryCode} = this.state;
        const userinfo = JSON.parse(localStorage.getItem("userinfo"));
        //this.setState({CountryCode:userinfo.country});
        if(userinfo.office == 'HQ'){
            //CountryCode.push('HQ');
            CountryCode.push('HQ');
        }
        if(userinfo.country == 'THA'){
            CountryCode.push('THA');
        }
        if(userinfo.country == 'JPN'){
            CountryCode.push('JPN');
        }
        if(userinfo.country == 'IDN'){
            CountryCode.push('IDN');
        }
        if(userinfo.country == 'PHI'){
            CountryCode.push('PHI');
        }
        //console.log(userinfo);
        //this.get_hotels();
        this.get_hotelscontact();
        this.get_employeecontact();
    }
}
const mapStateToProps = state => {
    return {
        loginReducer: state.loginReducer,
        propertyReducer: state.propertyReducer,
        hrReducer: state.hrReducer,
        contactReducer: state.contactReducer,
        s3Reducer: state.s3Reducer
    };
};
export default connect(mapStateToProps)(EmployeeContact);