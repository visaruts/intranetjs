import React, { Component } from "react";
//import { Prompt } from "react-router";
import { connect } from "react-redux";
import "react-toastify/dist/ReactToastify.css";
import Pagination from "components/Pagination.jsx";
import { Link } from "react-router-dom";
import { lang } from "Lang.jsx";
import ReactImageFallback from "react-image-fallback";
import { public_url, uniqid, ENV, s3_url_webapp } from "../../Config_API.jsx";
/** moment datetime */
import moment from 'moment';
import 'moment/min/locales.min';
/** fontawesome */
import {faFileAlt, faImage } from "@fortawesome/free-solid-svg-icons";
import { faFileAlt as farFileAlt } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { timeout } from "q";
class HRCard extends Component {
  state = {
    display: {},
    data: {},
    permissionAdmin: false,
    showModal: false,
    showAssignTo: false,
    showReSchedule: false,
    userData: [],
    userDataOption: [],
    expectedDate: undefined,
    assignedTo: "",
    status: "",
    remark: ""
  };
  render() {
    if (lang("code") === "th") {
      moment.locale('th');
    }
    else if (lang("code") === "ja") {
      moment.locale('ja');
    }
    else if (lang("code") === "id") {
      moment.locale('id');
    }
    else {
      moment.locale('en');
    }
    const { aData, CurrentURL, PageNum, PageSize, Category } = this.props;
    // console.log(this.props);
    let status = "";
    //let PageNum = 1;
    let PageTotal = 1;
    let LinkName = 0;
    const items = [];
    const id_tmp = Math.random();
    console.log(Category);
    if (Category === "announcement") {
      if (aData.length <= 0) {
        items.push(
          <div className="card" key={"card-" + id_tmp}>
            <div className="card-content" key={"card-content-" + id_tmp}>
              <div className="columns" key={"columns-" + id_tmp}>
                No data available in {Category} list
              </div>
            </div>
          </div>
        );
      } else {
        let i_tmp = 0;
        aData.map((each, key) => {
          let id_tmp = Math.random();
          PageTotal = each.TotalRows;
          items.push(
            <div className={"message"} key={"message-" + id_tmp}>
              <div className="message-body" key={"message-body-" + id_tmp}>
                <div className="card" key={"card-" + id_tmp} >
                  <div className={"card-content " + (i_tmp % 2 == 0 ? "odd" : "even")} key={"card-content-" + id_tmp} >
                    <div className="media" key={"media-" + id_tmp} >
                      <div className="media-left" key={"media-left-" + id_tmp} >
                        <div className="announcement-date" key={"announcement-date-" + id_tmp}> {moment(each.StartDate).format('DD-MMMM-YYYY')}</div>
                      </div>
                      <div className="media-content" key={"media-content-" + id_tmp} >
                        <p className="is-12 has-text-danger announcement-detail" key={"announcement-detail-" + id_tmp} >
                          <Link to={"/announcement/" + each.AnnouncementID} className="has-text-danger">{each.Topic}</Link>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          );
          i_tmp++;
        });
      }
    } else if (Category === "new employee") {
      let i_tmp = 0;
      if (aData.length <= 0) {
        items.push(
          <div className="card">
            <div className="card-content">
              <div className="columns">
                No data available in {Category} list
              </div>
            </div>
          </div>
        );
      } else {
        aData.map((each, key) => {
          let id_tmp = Math.random();
          PageTotal = each.TotalRows;
          // console.log(each);
          items.push(
            <div className="message" key={"message-" + id_tmp}>
              <div className="message-body" key={"message-body-" + id_tmp}>
                <div className="card" key={"card-" + id_tmp} >
                  <div className={"card-content " + (i_tmp % 2 == 0 ? "odd" : "even")} key={"card-content-" + id_tmp} >
                    <div className="media">
                      <div className="media-left">
                        <figure className="image">
                          <ReactImageFallback
                            src={s3_url_webapp + "/file/WebPortal/IMG/" + each.AttachImageFile}
                            fallbackImage={s3_url_webapp + "/file/WebPortal/IMG/no-image.png"}
                            // fallbackImage={s3_url_webapp+"/file/WebPortal/IMG/"+each.AttachImageFile}
                            initialImage={public_url + "/images/loader.gif"}
                            alt="image profile"
                            className="my-image" />
                        </figure>
                      </div>
                      <div className="media-content">
                        <Link to={"/announcement/" + each.AnnouncementID} className="is-12 has-text-danger new-employee-title">{each.Topic}</Link>
                        <p className="subtitle is-12 new-employee-date">{"Announcement date :"} {moment(each.StartDate).format('DD-MMMM-YYYY')}</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          );
          i_tmp++;
        });
      }
    } else if (Category === "company regulation") {
      let i_tmp = 0;
      if (aData.length <= 0) {
        items.push(
          <div className="card">
            <div className="card-content">
              <div className="columns">
                No data available in {Category} list
              </div>
            </div>
          </div>
        );
      } else {
        aData.map((each, key) => {
          let id_tmp = Math.random();
          PageTotal = each.TotalRows;
          items.push(
            <div className="column is-one-quarter-desktop  content-company " key={"column-" + id_tmp}>
              <Link to={"/announcement/" + each.AnnouncementID} className="is-12 has-text-danger company-title">
                <figure className="image-company " key={"image-company-" + id_tmp}>
                  <ReactImageFallback
                    src={s3_url_webapp + "/file/WebPortal/IMG/" + each.AttachImageFile}
                    fallbackImage={public_url + "/images/no.png"}
                    initialImage={public_url + "/images/loader.gif"}
                    alt="image profile"
                    className="my-image" />
                </figure>
                {each.Topic}
              </Link>
            </div>
          );
          i_tmp++;
        });
      }
    } else if (Category === "policy procedure") {
      if (aData.length > 0) {
        PageTotal = aData[0].TotalRows;
      }
      // if(aData.length<=0){
      //   items.push( 
      //     <div  className="card">
      //       <div className="card-content">
      //         <div className="columns">
      //           No data available in {Category} list 
      //         </div>
      //       </div>
      //     </div>
      //   );
      // }else{
      //   aData.map((each,key)=>{
      //     let id_tmp = Math.random();
      //     PageTotal =  each.TotalRows;
      //     items.push(
      //       <div className="message" key={"message-"+id_tmp}>
      //         <div className="message-body"  key={"message-body-"+id_tmp}>
      //           <div className="card"  key={"card-"+id_tmp} >
      //             <div className="card-content card-policy"  key={"card-content-"+id_tmp} > 
      //               <div className="media" key={"media-"+id_tmp} >
      //                 <div className="media-left" key={"media-left-"+id_tmp} >
      //                   <div className="field" key={"field-"+id_tmp} >
      //                     <input 
      //                         checked={PolicyCheckedAll} 
      //                         // onClick={e => this.handleCheckboxClick(e)}
      //                         className="is-checkradio policy-checkbox is-danger" id={"checkbox-"+id_tmp} type="checkbox" />
      //                     <label className="policy-01" htmlFor={"checkbox-"+id_tmp} ></label>
      //                     <FontAwesomeIcon  style={{color:"rgb(114, 15, 97)",fontSize:"1.4rem"}} className="icon-book" icon={faBook} />
      //                   </div>
      //                 </div>
      //                 <div className="media-content"  key={"media-content-"+id_tmp} > 
      //                 <Link 
      //                   style={{color:"rgb(114, 15, 97)",fontSize:"1.2rem"}} 
      //                   to={"/announcement/"+each.AnnouncementID} 
      //                   className="is-12 policy-title">{" - "+each.Topic}
      //                 </Link>
      //                 </div>
      //               </div>
      //             </div>
      //           </div>
      //         </div>
      //       </div>
      //     );
      // });
      // }
    } else if (Category === "appraisal" || Category === "organization chart") {
      if (aData.length <= 0) {
        items.push(
          <div className="card" key={"card-" + id_tmp}>
            <div className="card-content" key={"card-content-" + id_tmp}>
              <div className="columns" key={"columns-" + id_tmp}>
                No data available in {Category} list
              </div>
            </div>
          </div>
        );
      } else {
        let i_tmp = 0;
        aData.map((each, key) => {
          let id_tmp = Math.random();
          PageTotal = each.TotalRows;
          let ClassColor = "policy-02";
          items.push(
            <div className={"message"} key={"message-" + id_tmp}>
              <div className="message-body" key={"message-body-" + id_tmp}>
                <div className="card" key={"card-" + id_tmp} >
                  <div className="card-content card-policy" key={"card-content-" + id_tmp} >
                    <div className="media" key={"media-" + id_tmp} >
                      <div className="media-left" key={"media-left-" + id_tmp} >
                        <div className="field" key={"field-" + id_tmp} >
                          <FontAwesomeIcon style={{ fontSize: "1.4rem" }} className={"icon-book " + ClassColor} icon={farFileAlt} />
                        </div>
                      </div>
                      <div className="media-content" key={"media-content-" + id_tmp} >
                        <Link
                          style={{ fontSize: "1.2rem" }}
                          to={"/announcement/" + each.AnnouncementID}
                          className={"is-12 policy-title " + ClassColor} >{" - " + each.Topic}
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          );
          i_tmp++;
        });
      }
    } else {
      items.push(
        <div className="message" >
          <div className="message-body">
            <div className="card" >
              <div className="card-content"  >
                <div className="media">
                  <div className="media-left">
                    No data available in {Category} list
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
    return (
      <div className={Category !== "company regulation" ? "column" : "column-company columns is-multiline"}>
        {items}
        {Category !== "company regulation" ? <Pagination recordsTotal={PageTotal} page={PageNum} pageSize={PageSize} link={CurrentURL}
          onClick={e => {
            this.setState({ active: false });
          }}
        /> : ""
        }
      </div>
    )
  }
}
const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    usersReducer: state.usersReducer
  };
};
export default connect(mapStateToProps)(HRCard);
