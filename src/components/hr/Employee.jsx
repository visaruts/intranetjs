import React, { Component } from "react";
//import { Prompt } from "react-router";
import { connect } from "react-redux";
import "react-toastify/dist/ReactToastify.css";
import Pagination from "components/Pagination.jsx";
import { Link } from "react-router-dom";
import { lang } from "Lang.jsx";
import ReactImageFallback from "react-image-fallback";
import { public_url, base_url_webapp, uniqid, ENV, s3_url_webapp } from "../../Config_API.jsx";
import { actionGetAccessToken } from "actions/login";
import { actionGetUserByHR } from "actions/hr/actionHR";
import qs from "query-string";
/** moment datetime */
import moment from 'moment';
import 'moment/min/locales.min';
// import Select from "components/Select.jsx";
import Select from 'react-select';
/** fontawesome */
import { faSearch, faFolderOpen, faImage, faPrint, faDownload } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import MappleToolTip from 'reactjs-mappletooltip'
import ReactTooltip from 'react-tooltip'
// import { timeout } from "q";
class Employee extends Component {
  state = {
    firstLoadPage: true,
    display: {},
    data: "",
    selectEntity: "",
    selectProperty: "",
    selectOffice: "",
    selectJobDept: "",
    selectJobTitle: "",
    selectStatus: "",
    selectCountry: "",
    btnFilterLoading: false,
    IsExport: true,
    UserCodeList: [],
    UserCodeListTmp: [],
    arrUserItems: [],
    boolUserCheckedAll: false,
    sPram: "",
    PageSize: 10,
    page: 1,
    Search: ""
  };

  render() {
    const mappleConfig = {
      mappleType: 'ching'
    }
    if (lang("code") === "th") {
      moment.locale('th');
    }
    else if (lang("code") === "ja") {
      moment.locale('ja');
    }
    else if (lang("code") === "id") {
      moment.locale('id');
    }
    else {
      moment.locale('en');
    }
    const { firstLoadPage, selectEntity, data, btnFilterLoading, IsExport, boolUserCheckedAll, UserCodeList, arrUserItems, page } = this.state;
    const { propertyReducer, entityReducer, aHRCountry, masterReducer } = this.props;
    let PageNum = page;
    let PageSize = 10;
    let PageTotal = typeof data[0] === "undefined" ? 1 : data[0].TotalRows;
    const CurrentURL = "hr/employee_profile";
    let aDataList = [];
    let optionTmp = [];
    let aProperty = propertyReducer.property;
    let aOptionEntity = [];
    let aOptionProperty = [];
    let aOptionStatus = [{ label: "ALL", value: "" }, { label: "Active", value: "1" }, { label: "InActive", value: "0" }];
    let aOptionCountry = aHRCountry.length > 1 ? [
      { label: "ALL", value: "" },
      { label: lang(aHRCountry[0].toUpperCase()), value: aHRCountry[0] },
      { label: lang(aHRCountry[1].toUpperCase()), value: aHRCountry[1] },
      { label: lang(aHRCountry[2].toUpperCase()), value: aHRCountry[2] },
      { label: lang(aHRCountry[3].toUpperCase()), value: aHRCountry[3] }
    ] : [{ label: lang(aHRCountry[0].toUpperCase()), value: aHRCountry[0] }];
    let aOptionJobTitle = masterReducer.JobTitle;
    let aOptionDeparment = masterReducer.JobDept;
    optionTmp = [{ label: "ALL", value: "" }];
    Object.keys(aOptionDeparment).map(function (key) {
      optionTmp.push({ value: aOptionDeparment[key].JobDept, label: aOptionDeparment[key].JobDept });
    })
    aOptionDeparment = optionTmp;
    optionTmp = [{ label: "ALL", value: "" }];
    Object.keys(aOptionJobTitle).map(function (key) {
      optionTmp.push({ value: aOptionJobTitle[key].JobTitle, label: aOptionJobTitle[key].JobTitle });
    })
    aOptionJobTitle = optionTmp;
    let OptionOffice = [{ label: "ALL", value: "" }, { label: "Office", value: "Office" }, { label: "Hotel", value: "hotel" }, { label: "HQ", value: "hq" }];
    aOptionEntity.push({ label: "ALL", value: "" });
    Object.keys(entityReducer).map(function (key) {
      if (typeof entityReducer[key].EntityCode !== "undefined") {
        aOptionEntity.push({ label: entityReducer[key].EntityName, value: entityReducer[key].EntityCode });
      }
    });
    aOptionProperty.push({ label: "ALL", value: "" });
    // console.log(aProperty);
    Object.keys(aProperty).map(function (key) {
      if (typeof aProperty[key].PropertyCode !== "undefined"
        && aProperty[key].DeptCode.toUpperCase() === "HOTEL"
        && aProperty[key].EntityCode.toUpperCase() === selectEntity
      ) {
        aOptionProperty.push({ label: aProperty[key].PropertyName, value: aProperty[key].PropertyCode });
      }
    });
    if (firstLoadPage) {
      this.get_user();
      this.setState({ firstLoadPage: false })
    }
    return (
      <div className="">
        <div className="column is-12" >
          <label style={{ fontSize: "1.5rem", fontWeight: "700" }}>Manage Employee</label>
          <hr style={{
            backgroundColor: "#ff3860",
            border: "none",
            display: "block",
            height: "2px",
            margin: "5px"
          }} />
        </div>
        <div className="column filter-emp">
          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label"> Entity : </label>
            </div>
            <div className="field-body">
              <div className="field filter-field-emp">
                <div className="control">
                  <div className="react-select is-danger">
                    <Select
                      // value={selectEntity}
                      defaultValue={aOptionEntity[0]}
                      options={aOptionEntity}
                      onChange={e => {
                        // console.log(selectEntity);
                        this.setState({ selectEntity: e.value });
                      }}>
                    </Select>
                  </div>
                </div>
              </div>
            </div>
            <div className="field-label is-normal">
              <label className="label"> Property : </label>
            </div>
            <div className="field-body">
              <div className="field filter-field-emp">
                <div className="control">
                  <div className="react-select">
                    <Select
                      className="react-select"
                      styles={{ borderColor: "#ff3860" }}
                      defaultValue={aOptionProperty[0]}
                      options={aOptionProperty}
                      onChange={e => {
                        this.setState({ selectProperty: e.value });
                      }} />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label"> Office : </label>
            </div>
            <div className="field-body">
              <div className="field filter-field-emp">
                <div className="control">
                  <div className="react-select">
                    <Select
                      defaultValue={OptionOffice[0]}
                      options={OptionOffice}
                      onChange={e => {
                        this.setState({ selectOffice: e.value });
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="field-label is-normal">
              <label className="label"> Deparment : </label>
            </div>
            <div className="field-body">
              <div className="field filter-field-emp">
                <div className="control">
                  <div className="react-select is-danger">
                    <Select
                      defaultValue={aOptionDeparment[0]}
                      options={aOptionDeparment}
                      onChange={e => {
                        this.setState({ selectJobDept: e.value });
                      }}
                      styles={{
                        menu: provided => ({ ...provided, zIndex: 9999 })
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label"> Job Title : </label>
            </div>
            <div className="field-body">
              <div className="field filter-field-emp">
                <div className="control">
                  <div className="react-select is-danger">
                    <Select
                      defaultValue={aOptionJobTitle[0]}
                      options={aOptionJobTitle}
                      onChange={e => {
                        this.setState({ selectJobTitle: e.value });
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="field-label is-normal">
              <label className="label"> Status  : </label>
            </div>
            <div className="field-body">
              <div className="field filter-field-emp">
                <div className="control">
                  <div className="react-select">
                    <Select
                      defaultValue={aOptionStatus[0]}
                      options={aOptionStatus}
                      onChange={e => {
                        this.setState({ selectStatus: e.value });
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label"> Country : </label>
            </div>
            <div className="field-body">
              <div className="field">
                <div className="control">
                  <div className="react-select">
                    <Select
                      defaultValue={aOptionCountry[0]}
                      options={aOptionCountry}
                      onChange={e => {
                        this.setState({ selectCountry: e.value });
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="field-label is-normal">
            </div>
            <div className="field-body">
            </div>
          </div>
          <div className="field is-horizontal">
            <div className="field-label is-normal">
              <label className="label"> </label>
            </div>
            <div className="field-body">
              <div className="field is-grouped">
                <div className="control">
                  <button
                    className={btnFilterLoading === true ? "button is-danger is-loading" : "button is-danger"}
                    onClick={e => {
                      this.setState({ btnFilterLoading: true });
                      this.get_user();

                    }}
                  >{"Filter"}
                  </button>
                </div>
                {/* <div className="control">
                  <button
                    disabled={true}
                    className="button is-danger">{"Export Excel"}
                  </button>
                </div> */}
              </div>
            </div>
            <div className="field-label is-normal">
            </div>
            <div className="field-body">
            </div>
          </div>
        </div>
        <div className="column is-12" >
          <label style={{ fontSize: "1.5rem", fontWeight: "700" }}>User List</label>
          <hr style={{
            backgroundColor: "#ff3860",
            border: "none",
            display: "block",
            height: "2px",
            margin: "5px"
          }} />
        </div>
        {data ?
          <div className="column is-12 is-pulled-right bottom0">
            <div className="column is-6 is-pulled-left">
              <form name="frm-check-key" id="frm-check-key" method="post"
                action={base_url_webapp + "en/hr/employee/export"}
                enctype="multipart/form-data" target="_blank">
                <input type="hidden" id="chksent" name="chksent" value="" />
                <input type="hidden" id="selEmployer" name="filter[selEmployer]" value="" />
                <input type="hidden" id="selProperty" name="filter[selProperty]" value="" />
                <input type="hidden" id="selOffice" name="filter[selOffice]" value="" />
                <input type="hidden" id="selDepartment" name="filter[selDepartment]" value="" />
                <input type="hidden" id="selJobTitle" name="filter[selJobTitle]" value="" />
                <input type="hidden" id="selCountryCode" name="filter[selCountryCode]" value="" />
                <input type="hidden" id="selStatus" name="filter[selStatus]" value="" />


              </form>
              <input
                onChange={e => this.handleCheckboxChangeAll(e)}
                defaultChecked={boolUserCheckedAll}
                checked={boolUserCheckedAll}
                className="is-checkradio is-danger" id={"checkbox-all-1"} type="checkbox" />
              <label htmlFor={"checkbox-all-1"}
                style={{ top: "0px" }}
              >
                <font className="select-all-label" >Select All </font>
              </label>
              <button className="button is-danger"
                disabled={IsExport}
                onClick={((e) => this.handleClickExport())}
              >
                <FontAwesomeIcon
                  icon={faDownload}
                />{" Export Excel"}</button>
            </div>
            <div className="column is-6 is-pulled-right control-search">
              <div className="control has-icons-left has-icons-right is-pulled-right control-search">
                <input className="input is-danger" type="text" placeholder="Search..."
                  ref={input => this.search = input}
                  onChange={((e) => this.handleInputChange(e, 'tab1'))}
                  id="search-input"
                />
                <span className="icon is-small is-left">
                  <FontAwesomeIcon icon={faSearch} />
                </span>
              </div>
            </div>
          </div>
          : ""

        }
        {
          data.length > 0 ?
            data.map((each, key) => {
              let id_tmp = Math.random();
              let checked = false;
              let style_content = {};
              try {
                checked = arrUserItems.indexOf(each.UserCode) !== -1 ? true : false;
                style_content = checked ? { backgroundColor: "#ff38608c" } : { backgroundColor: "rgba(10,10,10,.1)" }
                if (checked && each.IsActive == "1") {
                  style_content = { backgroundColor: "#23d160" }
                } else if (checked && each.IsActive == "0") {
                  style_content = { backgroundColor: "#7a7a7a" }
                }
              } catch (error) {
                checked = false;
                style_content = { backgroundColor: "rgba(10,10,10,.1)", width: "auto" }
              }
              aDataList.push(
                <div className="column is-6" style={{ padding: "0 0.5rem" }}>
                  <div className="message" style={{ marginBottom: "0" }} key={"message-" + id_tmp}>
                    <div
                      className={each.IsActive == "1" ? "message-body message-body-active" : "message-body message-body-inactive"}
                      key={"message-body-" + id_tmp}>
                      <div className="card card-employee" key={"card-" + id_tmp}
                        style={{ display: "flex", backgroundColor: "rgb(250, 250, 250)" }}
                      >
                        <div className="card-content "
                          style={style_content}
                        >
                          <div style={{
                            paddingTop: " 1rem"
                            , marginLeft: "-12px"
                            , position: "absolute"
                            , top: "50%"
                            , transform: "translateY(-50%)"
                          }}>
                            <input
                              defaultChecked={checked}
                              onChange={e => this.handleCheckboxChange(e)}
                              className={each.IsActive == "1" ? "is-checkradio is-success" : "is-checkradio is-dark"}
                              id={"checkbox-" + id_tmp} type="checkbox"
                              value={each.UserCode}
                            />
                            <label htmlFor={"checkbox-" + id_tmp} className={"checkbox-emp-list"}  ></label>
                          </div>
                        </div>
                        <div className={"card-content "} style={{ padding: "10px" }} key={"card-content-" + id_tmp} >
                          <div className="media">
                            <div className="">
                              <figure className="image">
                                <ReactImageFallback
                                  src={"https://s3.ap-southeast-1.amazonaws.com/rphlwebapp.redplanethotels.com/file/HR/Photo/" + each.Photo}
                                  fallbackImage={"https://bulma.io/images/placeholders/96x96.png"}
                                  // fallbackImage={s3_url_webapp+"/file/WebPortal/IMG/"+each.AttachImageFile}
                                  initialImage={public_url + "/images/loader.gif"}
                                  alt="image profile"
                                  className="image-emp" />
                              </figure>
                            </div>
                            <div className="media-content">
                              <p className="title-emp is-4">
                                <a href="" className="is-12 has-text-danger">
                                  {each.UserFName + " " + each.UserLName}
                                </a>
                              </p>
                              <p className="subtitle-emp">
                                <b>Status :</b>                                   {
                                  each.IsActive == "1" ? <span className="tag is-success is-normal">Active</span>
                                    : <span className="tag is-light is-normal">InActive</span>
                                }
                                <br /><b>Country :</b> {lang(each.CountryCode)}
                                <br /><b>Entity :</b> <span title={each.EntityName}> {each.EntityName.length >= 35 ? each.EntityName.substring(0, 35) + "..." : each.EntityName}</span>
                                <br /><b>Office : </b>{each.Office}
                                <br /><b>Deparment :</b>
                                <div id="o4xFNdKxMQkZCsy_mapple_908172311232" className="mappleTip" 
                                  style={{position: "fixed",zIndex:"10000", backgroundColor:"black"}}>
                                  <div style={{color: "white",padding:" 8px 12px",borderBottom:"4px solid red"}}
                                  >Preview Mapple</div>
                                  <span>
                                    <div className="tip" styles="width: 0px; position: absolute; height: 0px; border-top: 5px solid red; border-right: 5px solid transparent; left: calc(50% - 5px); border-left: 5px solid transparent;">
                                  </div>
                                  </span>
                                </div>
                                <span
                                  ref={ref => this.fooRef = ref}
                                  data-tip={each.JobDept}
                                  title={each.JobDept}
                                  data-event={'click focus'}

                                > {each.JobDept.length >= 45 ? each.JobDept.substring(0, 45) + "..." : each.JobDept}</span>
                                <br /><b>Job Title :</b> <span title={each.JobTitle}> {each.JobTitle.length >= 45 ? each.JobTitle.substring(0, 45) + "..." : each.JobTitle}</span>
                              </p>
                              <ReactTooltip />
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )
            })
            : ""
        }
        <div className="clear columns is-multiline" style={{ paddingRight: "1rem" }}>
          {aDataList}
        </div>
        <div style={{ paddingRight: "10px" }}>
          {data.length > 0 ? <Pagination style={{ marginRight: "0" }} recordsTotal={PageTotal} page={PageNum} pageSize={PageSize} link={CurrentURL}
            onClick={e => {
              this.setState({ active: false });
            }}
          /> : <div className="column clear"> {"---No data user---"}</div>}
        </div>

      </div>
    )
  }
  handleInputChange = (e, tabs) => {
    let Search = encodeURI(this.search.value);
    const queryString = qs.parse(this.props.sPram);
    const page = queryString.p || 1;
    const p_url = "p=" + page + "&query=" + Search;
    this.setState({ Search: Search });
    // this.props.history.push('/hr/' + activeClass + '?' + p_url);
    this.get_user();
  }
  handleClickExport = () => {
    const {
      arrUserItems,
      selectEntity,
      selectProperty,
      selectOffice,
      selectJobDept,
      selectJobTitle,
      selectStatus,
      selectCountry,
    } = this.state;
    // console.log(arrUserItems);
    document.getElementById("chksent").value = JSON.stringify(arrUserItems);
    document.getElementById("selEmployer").value = selectEntity;
    document.getElementById("selProperty").value = selectProperty;
    document.getElementById("selOffice").value = selectOffice;
    document.getElementById("selDepartment").value = selectJobDept;
    document.getElementById("selJobTitle").value = selectJobTitle;
    document.getElementById("selStatus").value = selectStatus;
    document.getElementById("selCountryCode").value = selectCountry;
    document.getElementById("frm-check-key").submit();
  }
  handleCheckboxChangeAll(e) {
    const { value, checked } = e.target;
    const { UserCodeList } = this.state;
    let { arrUserItems } = this.state;
    if (checked) {
      UserCodeList.map(function (item, i) {
        arrUserItems.push(item);
      });
    } else {
      arrUserItems = [];
    }
    arrUserItems = (checked) ? UserCodeList : []
    this.setState({
      boolUserCheckedAll: checked,
      IsExport: arrUserItems.length > 0 ? false : true,
      arrUserItems: arrUserItems
    });
  }
  handleCheckboxChange(e) {
    const { value, checked } = e.target;
    let { arrUserItems, boolUserCheckedAll } = this.state;
    const UserCodeList = this.props.hrReducer.data[0].UserCodeList.split(",");
    if (boolUserCheckedAll && checked) {
      UserCodeList.map(function (item, i) {
        arrUserItems.push(item);
      });
    } else {
      if (checked) {
        const index = arrUserItems.indexOf(value);
        if (index === -1) {
          arrUserItems.push(value);
        }
      } else {
        const index = arrUserItems.indexOf(value);
        if (index !== -1) {
          arrUserItems.splice(index, 1);
        }
      }
    }
    this.setState({
      boolUserCheckedAll: arrUserItems.length == UserCodeList.length ? true : false,
      IsExport: arrUserItems.length > 0 ? false : true,
      arrUserItems: arrUserItems
    });
  }
  async get_user() {
    const {
      selectEntity,
      selectProperty,
      selectOffice,
      selectJobDept,
      selectJobTitle,
      selectStatus,
      selectCountry,
      PageSize,
      Search
    } = this.state;
    const queryString = qs.parse(this.props.sPram);
    const page = queryString.p || 1;
    // console.log(this.props);
    const param = {
      Entity: selectEntity,
      Property: selectProperty,
      Office: selectOffice,
      JobDept: selectJobDept,
      JobTitle: selectJobTitle,
      Status: selectStatus,
      Country: selectCountry,
      PageNum: page,
      PageSize: PageSize,
      Search: Search
    }
    let { access_token = "" } = this.props.loginReducer;
    if (access_token === "") {
      const request_token = localStorage.getItem("request_token");
      await this.props.dispatch(actionGetAccessToken(request_token));
    }
    await this.props.dispatch(actionGetUserByHR(access_token, 'user', param));
    if (this.props.hrReducer.data.length > 0) {
      this.setState({
        loading: 1,
        data: this.props.hrReducer.data,
        btnFilterLoading: false,
        UserCodeList: this.props.hrReducer.data[0].UserCodeList.split(","),
        arrUserItems: [],
        IsExport: true,
        UserCodeListTmp: [],
        boolUserCheckedAll: false,
        PageNum: page,
        page: page
      });
    } else {
      this.setState({
        loading: 1,
        data: [],
        btnFilterLoading: false,
        UserCodeList: [],
        arrUserItems: [],
        IsExport: true,
        UserCodeListTmp: [],
        boolUserCheckedAll: false,
        PageNum: page,
        page: page
      });
    }
  }
  async componentDidUpdate(prevProps) {
    //To Do....
    const queryString = qs.parse(this.props.sPram);
    const page = typeof queryString.p == "undefined" ? 1 : queryString.p;
    if (this.state.page !== page) {
      this.get_user();
    }
  }
}

const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    usersReducer: state.usersReducer,
    entityReducer: state.entityReducer,
    propertyReducer: state.propertyReducer,
    masterReducer: state.masterReducer,
    hrReducer: state.hrReducer
  };
};
export default connect(mapStateToProps)(Employee);
