import React, { Component } from "react";
//import { Prompt } from "react-router";
import { connect } from "react-redux";
import "react-toastify/dist/ReactToastify.css";
import Pagination from "components/Pagination.jsx";
import { Link } from "react-router-dom";
import { lang } from "Lang.jsx";
import ReactImageFallback from "react-image-fallback";
import { api , public_url, base_url_webapp, uniqid, ENV, s3_url_webapp } from "../../Config_API.jsx";
/** action  */
import { actionGetAccessToken } from "actions/login";
import { actionLoadHRData } from "actions/hr/actionHR";
import qs from "query-string";
/** moment datetime */
import moment from 'moment';
import 'moment/min/locales.min';
// import Select from "components/Select.jsx";
import Select from 'react-select';
/** fontawesome */
import { faSearch, faChevronCircleLeft } from "@fortawesome/free-solid-svg-icons";
import { faFileAlt as farFileAlt } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import axios from "axios";
/** components for page */
// import { timeout } from "q";
class JobDescription extends Component {
  state = {
    firstLoadPage: true,
    display: {},
    data: [],
    sPram: "",
    PageSize: 10,
    page: 1,
    Search: "",
    template: "",
    loading: 0
  };
  render() {
    if (lang("code") === "th") {
      moment.locale('th');
    }
    else if (lang("code") === "ja") {
      moment.locale('ja');
    }
    else if (lang("code") === "id") {
      moment.locale('id');
    }
    else {
      moment.locale('en');
    }
    const { firstLoadPage,data } = this.state;
    const { page, Search } = this.props;
    //const { data } = this.props.hrReducer;
    let PageNum = page;
    let PageSize = 10;
    //let data = aData ;
    let PageTotal = typeof data[0] === "undefined" ? 1 : data[0].TotalRows;
    console.log(this.state);
    const CurrentURL = "hr/template/job_description";
    if (firstLoadPage) {
      this.getAnnouncement()
      this.setState({ firstLoadPage: false })
    }
    return (
      <div className="column is-12" style={{ padding: "0" }}>
        <div className="column is-12 is-pulled-right control-search" style={{ paddingTop: "0", paddingRight: "0" }}>
          <div className="control has-icons-left has-icons-right is-pulled-right control-search">
            <input className="input is-danger" type="text" placeholder="Search..." value={Search}
              ref={input => this.search = input}
              onChange={((e) => this.handleInputChange(e, 'tab1'))}
              id="search-input"
            />
            <span className="icon is-small is-left">
              <FontAwesomeIcon icon={faSearch} />
            </span>
          </div>
        </div>
        <div className="column is-12 clear" style={{ padding: "0" }}>
          {data.length > 0 ? <div className="list is-hoverable template">
            {data.map((each, key) => {
              let id_tmp = Math.random();
              return <div key={"list-item-" + id_tmp} className={key % 2 == 0 ? "list-item " : "list-item even"}>
                <Link to={""} className="has-text-danger" >
                  <span className="glyphicon glyphicon-book"></span>
                  {" - " + each.Topic}
                </Link>
                <div>Publication date :  {moment(each.StartDate).format('DD-MMMM-YYYY')}</div>
              </div>
            })
            }</div>
            :
            <div className="list is-hoverable">
              <div className="list-item">
                {"--- There is no document ---"}
              </div>
            </div>
          }
          <Pagination recordsTotal={PageTotal} page={PageNum} pageSize={PageSize} link={CurrentURL}
            onClick={e => {
              this.setState({ active: false });
            }}
          />
        </div>
      </div>
    )
  }
  handleInputChange = (e, tabs) => {
    let Search = encodeURI(this.search.value);
    const queryString = qs.parse(this.props.sPram);
    const page = queryString.p || 1;
    const p_url = "p=" + page + "&query=" + Search;
    this.setState({ Search: Search });
    // this.props.history.push('/hr/' + activeClass + '?' + p_url);
    this.getAnnouncement();
  }

  async getAnnouncement() {
    let Category = 'announcement';
    let query = "";
    try {
      const { userinfo, access_token } = this.props.loginReducer;
      const queryString = qs.parse(this.props.sPram);
      const page = queryString.p || 1;
      if (document.getElementById("search-input") !== null) {
        query =  document.getElementById("search-input").value;
      }
      const Type = "announcement";
      const Country = localStorage.getItem("countryCode").toUpperCase();
        const  AnnouncementID= "",
        Category= 'template',
        Department= "HR",
        StartDate= "",
        EndDate= "",
        PinnedCalendar= "",
        CreateBy= "",
        AnnouncementStatus= "OPEN",
        PageNum= page,
        PageSize= "10",
        Search= this.state.Search,
        Subtemplate= 'Job_Description'
      const res = await axios.get(
        api.INTRANET_API +
        "/hr/"+Type+"?AnnouncementID="+AnnouncementID+"&Category="+Category+"&Department="+Department
        +"&StartDate="+StartDate+"&EndDate="+EndDate+"&Country="+Country+"&PinnedCalendar="+PinnedCalendar+"&CreateBy="+CreateBy
        +"&AnnouncementStatus="+AnnouncementStatus+"&PageNum="+PageNum+"&PageSize="+PageSize+"&Search="+Search+"&Subtemplate="+ Subtemplate,
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        }
      );
      const {data}=res.data;
      if (data.length > 0) {
        console.log(data);
        this.setState({
          loading: 1,
          data: data,
          page: page
        });
      } else {
        this.setState({
          loading: 1,
          data: [],
          page: page
        });
      }
    } catch (err) {
      console.log("error");
      console.log(Category);
      console.log({ code: 500, message: err.message });
      this.setState({
        loading: 1,
        data: [],
        activeClass: Category
      });
    }
  }
   
  // async componentWillMount() {
  //   console.log("componentWillMount");
  //   const { firstLoadPage } = this.state;
  //   if (firstLoadPage) {
  //    //this.getAnnouncement();
  //   }
  // }
  // async componentDidUpdate(prevProps) {
  //   // //To Do....
  //   console.log('componentDidUpdate');
  //   const { firstLoadPage } = this.state;
  //   // const queryString = qs.parse(this.props.sPram);
  //   //const page = typeof this.props.page == "undefined" ? 1 : this.props.page;
  //   // //console.log(this.props.page) ;
  //   // console.log("this.state.page"+this.state.page) ;
  //   if (this.state.page !== this.props.page || firstLoadPage) {
  //     //this.getAnnouncement();
  //   }
  // }
  // async  componentWillUnmount(prevProps) {
  //   console.log('componentWillUnmount');
  //   //this.getAnnouncement();
  //   console.log('componentDidUpdate');
  //   const { firstLoadPage } = this.state;
  //   if (firstLoadPage) {
  //     //this.getAnnouncement();
  //   }
  // }
  //   async  componentDidMount (prevProps) {
  //      console.log('componentDidMount');
  //      const { firstLoadPage } = this.state;
  //      if (firstLoadPage) {
  //       // this.getAnnouncement();
  //      }
  //  }
}

const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    usersReducer: state.usersReducer,
    entityReducer: state.entityReducer,
    propertyReducer: state.propertyReducer,
    masterReducer: state.masterReducer,
    hrReducer: state.hrReducer
  };
};
export default connect(mapStateToProps)(JobDescription);
