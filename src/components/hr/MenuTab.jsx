import React, { Component } from "react";
//import { Prompt } from "react-router";
import { connect } from "react-redux";
import "react-toastify/dist/ReactToastify.css";
import Pagination from "components/Pagination.jsx";
import { Link } from "react-router-dom";
import { lang } from "Lang.jsx";
import ReactImageFallback from "react-image-fallback";
import { public_url, base_url_webapp, uniqid, ENV, s3_url_webapp } from "../../Config_API.jsx";
import { actionGetAccessToken } from "actions/login";
import { actionGetUserByHR } from "actions/hr/actionHR";
import qs from "query-string";
/** moment datetime */
import moment from 'moment';
import 'moment/min/locales.min';
// import Select from "components/Select.jsx";
import Select from 'react-select';
/** fontawesome */
import { faSearch, faFolderOpen, faImage, faPrint, faDownload, faBackward } from "@fortawesome/free-solid-svg-icons";
import { faFileAlt as farFileAlt } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { timeout } from "q";
class MenuTab extends Component {
  state = {
    firstLoadPage: true,
  };
  render() {
    const { firstLoadPage } = this.state;
    const { aData, page } = this.props;
    let PageNum = page;
    let PageSize = 10;
    let data = aData;
    let PageTotal = typeof data[0] === "undefined" ? 1 : data[0].TotalRows;
    console.log(data);
    const CurrentURL = "hr/template/job_description";
    if (firstLoadPage) {
      this.setState({ firstLoadPage: false })
    }
    return (
      <div></div>
    )
  }
  async componentWillMount() {
    console.log("componentWillUnmount...");
  }
  async componentWillUnmount() {
    console.log("componentWillUnmount...");
  }
  async componentDidUpdate(prevProps) {
    console.log("componentWillUnmount..");
  }
  async componentDidMount() {
    console.log("componentDidMount...");
  }
}

const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    usersReducer: state.usersReducer,
    entityReducer: state.entityReducer,
    propertyReducer: state.propertyReducer,
    masterReducer: state.masterReducer,
    hrReducer: state.hrReducer
  };
};
export default connect(mapStateToProps)(MenuTab);
