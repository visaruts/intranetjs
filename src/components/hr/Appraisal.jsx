import React, { Component } from "react";
//import { Prompt } from "react-router";
import { connect } from "react-redux";
import "react-toastify/dist/ReactToastify.css";
import Pagination from "components/Pagination.jsx";
import { Link } from "react-router-dom";
import { lang } from "Lang.jsx";
import { public_url, base_url_webapp, uniqid, ENV, s3_url_webapp} from "../../Config_API.jsx";

/** action  */
import { actionLoadHRData } from "actions/hr/actionHR";
import { actionGetAccessToken } from "actions/login";
import qs from "query-string";
/** moment datetime */
import moment from 'moment';
import 'moment/min/locales.min';
// import Select from "components/Select.jsx";
import Select from 'react-select';
/** fontawesome */
import { faSearch, faChevronCircleLeft, faPrint,faUser } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { timeout } from "q";
class Appraisal extends Component {
  state = {
    firstLoadPage: true,
    display: {},
    IsExport: true,
    IsPrint: true,
    UserCodeList: [],
    UserCodeListTmp: [],
    arrUserItems: [],
    arrPolicyItems: [],
    boolUserCheckedAll: false,
    sPram: "",
    PageSize: 20,
    page: 1,
    Search: "",
    CurrentURL: "hr/appraisal",
    data: this.props.aData
  };
  render() {
    if (lang("code") === "th") {
      moment.locale('th');
    }
    else if (lang("code") === "ja") {
      moment.locale('ja');
    }
    else if (lang("code") === "id") {
      moment.locale('id');
    }
    else {
      moment.locale('en');
    }
    const {
      boolPolicyCheckedAll,
      IsPrint,
      Search,
      firstLoadPage,
      arrPolicyItems,
      CurrentURL,
      page,
      data,
      PageSize
    } = this.state;
    const langCode = localStorage.getItem("langCode");
    const { userinfo } = this.props;
    const PolicyCheckedALL = boolPolicyCheckedAll ? 'checked' : '';
    let PageNum = page;
    let PageTotal = typeof data[0] === "undefined" ? 1 : data[0].TotalRows;
    if (firstLoadPage) {
      this.getAnnouncement();
      this.setState({ firstLoadPage: false })
    }
    return (
      <div className="column is-12" style={{ padding: "0" }}>
        <div className="content-policy" key={uniqid()}>
        <div id="appraisal-search" className="column is-12 is-pulled-right">
            <div className="is-8 is-pulled-left">
              <button
                onClick={e => {
                  this.setState({
                    BtnAppraisal: !this.state.BtnAppraisal
                  });
                }}
                className={
                  !this.state.BtnAppraisal ? "button is-danger" : "button is-danger is-inverted"
                }
              >
                <FontAwesomeIcon icon={faUser} /> &nbsp;  {"Administration"}
              </button>
              {this.state.BtnAppraisal ? <div className="appraisal-list">
                <ul className="appraisal-ul" ><li><a target="_blank" className="has-text-danger" href={base_url_webapp + langCode + "/admin/webportal/appraisal"} >{"Appraisal list "}</a> </li></ul>
                <ul className="appraisal-ul" ><li><a target="_blank" className="has-text-danger" href={base_url_webapp + langCode + "/admin/webportal/addnew_announcement/hr/appraisal"} >{"Upload Appraisal"}</a> </li></ul>
              </div> : ""}
            </div>
            <div className="is-6 is-pulled-right control-search" style={{ paddingRight: "0" }}>
              <div className="control has-icons-left has-icons-right is-pulled-right control-search">
                <input className="input is-danger" type="text" placeholder="Search..." value={Search}
                  ref={input => this.search = input}
                  onChange={((e) => this.handleInputChange(e))}
                  id="search-input"
                />
                <span className="icon is-small is-left">
                  <FontAwesomeIcon icon={faSearch} />
                </span>
              </div>
            </div>
          </div>
          <div className="column clear" >
            {data.length > 0 ? <div className="list is-hoverable appraisal">
              {data.map((each, key) => {
                let id_tmp = Math.random();
                let PageTotal = each.TotalRows;
                let checked = false;
                let Country = each.Country.split(",");
                let ClassColor = "appraisal-list";
                try {
                  checked = arrPolicyItems.indexOf(each.AnnouncementID) !== -1 ? true : false;
                } catch (error) {
                  checked = false;
                }
                return (
                  <div key={"list-item-" + id_tmp} className={key % 2 == 0 ? "list-item " : "list-item even"}>
                    <a href={"https://intranet.redplanethotels.com/en/announcement/id/"+each.AnnouncementID+"/10"} className={ClassColor} >
                      <span className="glyphicon glyphicon-book"></span>
                      {" - " + each.Topic}
                    </a>
                  </div>
                )


              })
              }</div>
              : ""
            }
            <div>
              <Pagination recordsTotal={PageTotal} page={PageNum} pageSize={PageSize} link={CurrentURL}
                onClick={e => {
                  this.setState({ active: false });
                }}
              />
            </div>
          </div>

        </div>
      </div>
    )
  }
  handleInputChange = (e) => {
    // const {CurrentURL} = this.state;
    //this.search.value = encodeURI(this.search.value);
    //document.getElementById("search-input").focus();
    this.setState({ Search: this.search.value });
    this.getAnnouncement();
  }
  handleCheckboxChangeAll(e) {
    const { value, checked } = e.target;
    const { arrPolicyAll } = this.state;
    let { arrPolicyItems } = this.state;
    if (checked) {
      // arrPolicyItems =arrPolicyAll ;
      arrPolicyAll.map(function (item, i) {
        arrPolicyItems.push(item);
      });
    } else {
      arrPolicyItems = [];
    }
    arrPolicyItems = (checked) ? arrPolicyAll : []
    this.setState({
      boolPolicyCheckedAll: checked,
      IsPrint: arrPolicyItems.length > 0 ? false : true,
      arrPolicyItems: arrPolicyItems
    });
  }
  handleCheckboxChange(e) {
    const { value, checked } = e.target;
    let { arrPolicyItems, boolPolicyCheckedAll } = this.state;
    const arrPolicyAll = this.props.hrReducer.data[0].AnnouncementIDAll.split(",");
    if (boolPolicyCheckedAll && checked) {
      arrPolicyAll.map(function (item, i) {
        arrPolicyItems.push(item);
      });
    } else {
      if (checked) {
        const index = arrPolicyItems.indexOf(value);
        if (index === -1) {
          arrPolicyItems.push(value);
        }
      } else {
        const index = arrPolicyItems.indexOf(value);
        if (index !== -1) {
          arrPolicyItems.splice(index, 1);
        }
      }
    }
    this.setState({
      boolPolicyCheckedAll: arrPolicyItems.length == arrPolicyAll.length ? true : false,
      IsPrint: arrPolicyItems.length > 0 ? false : true,
      arrPolicyItems: arrPolicyItems
    });
  }
  async componentDidUpdate(prevProps) {
    console.log("componentDidUpdate");
    //To Do....
    const queryString = qs.parse(this.props.sPram);
    const page = typeof queryString.p == "undefined" ? 1 : queryString.p;
    if (this.state.page !== page) {
      this.getAnnouncement();
    }
    if (this.state.Search !== "") {
      document.getElementById("search-input").focus();
    }
  }
  async componentDidMount() {
    console.log("componentDidMount");
    let { access_token = "", access_token_exp = 0 } = this.props.loginReducer; //this.state;
    if (access_token === "") {
      const request_token = localStorage.getItem("request_token");
      await this.props.dispatch(actionGetAccessToken(request_token));
    }
    await this.getAnnouncement();
  }
  async getAnnouncement() {
    let query = "";
    try {
      const { userinfo, access_token } = this.props.loginReducer;
      const queryString = qs.parse(this.props.sPram);
      const page = typeof queryString.p == "undefined" ? 1 : queryString.p;
      if (document.getElementById("search-input") !== null) {
        query = document.getElementById("search-input").value;
      }
      const Type = "announcement";
      const Country = localStorage.getItem("countryCode").toUpperCase();
      const param = {
        AnnouncementID: "",
        Category: "appraisal",
        Department: "HR",
        StartDate: "",
        EndDate: "",
        Country: Country,
        PinnedCalendar: "",
        CreateBy: "",
        AnnouncementStatus: "OPEN",
        PageNum: page,
        PageSize: "10",
        Search: query
      }
      await this.props.dispatch(actionLoadHRData(access_token, Type, param));
      // console.log(this.props.hrReducer.data[0].AnnouncementIDAll.split(","));
      if (this.props.hrReducer.data.length > 0) {
        this.setState({
          loading: 1,
          data: this.props.hrReducer.data,
          page: page,
          arrPolicyAll: this.props.hrReducer.data[0].AnnouncementIDAll.split(","),
          userinfo: userinfo
        });
      } else {
        this.setState({
          loading: 1,
          data: [],
          page: page
        });
      }
    } catch (err) {
      console.log("error");
      console.log({ code: 500, message: err.message });
      this.setState({
        loading: 1,
        data: [],
      });
    }
  }
}
const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    usersReducer: state.usersReducer,
    entityReducer: state.entityReducer,
    propertyReducer: state.propertyReducer,
    masterReducer: state.masterReducer,
    hrReducer: state.hrReducer
  };
};
export default connect(mapStateToProps)(Appraisal);
