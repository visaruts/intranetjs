import React, { Component } from "react";
import { connect } from "react-redux";
import "react-toastify/dist/ReactToastify.css";
import { Link } from "react-router-dom";
import { lang } from "Lang.jsx";
import Select from 'react-select';
import Iframe from 'react-iframe'
import ReactLoading from 'react-loading';
import { base_url_webapp, ENV, uniqid } from "Config_API.jsx";
/**react-day-picker */
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
/**react-datepicker */
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
 
/** moment datetime */
import moment from 'moment';
import 'moment/min/locales.min';
/** fontawesome */
import { faSearch, faPrint, faBook, faUser, faFileAlt, faChevronCircleLeft } from "@fortawesome/free-solid-svg-icons";
import { faFileAlt as farFileAlt } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class StatisticReport extends Component {
  state = {
    firstLoadPage: true,
    selectReport: "",
    selectReginal: { label: "Reginal + ALL Hotel", value: "REG_HOTEL" },
    selectCountry: "",
    isIframeLoad: true,
    selectJobLevel: false,
    Statistic: {
      AgeView: false,
      Age: "ALL",
      GenerationsView: false,
      Generations: "ALL",
      GenderView: false,
      Gender: "ALL",
      SeniorityView: false,
      Seniority: "ALL",
      NationalityView: false,
      Nationality: "ALL",
      JobGradingView: false,
      JobGrading: "ALL"
    },
    Report: {
      Monthly: { Country: "ALL", Month: "" },
      BirthdayOfMonth: ""
    },
    optionCountry: [
      { label: ("All Country"), value: "" },
      { label: lang("IDN"), value: "IDN" },
      { label: lang("JPN"), value: "JPN" },
      { label: lang("PHI"), value: "PHI" },
      { label: lang("THA"), value: "THA" }
    ],
    optionReginal: [
      { label: "Reginal + ALL Hotel", value: "REG_HOTEL" },
      { label: "Reginal", value: "REG" },
      { label: "ALL Hotel", value: "HOTEL" },
    ],
    disabledBtn:""
  };

  render() {
    const { firstLoadPage, Statistic, selectReport, optionCountry, selectJobLevel, isIframeLoad,disabledBtn } = this.state;
    const { property } = this.props.propertyReducer
    let { selectCountry, selectReginal, optionReginal } = this.state;
    const styleCountry = { maxWidth: "260px", padding: "0.5rem" }
    const styleReginal = { maxWidth: "260px", padding: "0.5rem" }
    const langCode = localStorage.getItem("langCode") || "en";
    selectCountry = selectCountry === "" ? optionCountry[0] : selectCountry;
    selectReginal = selectReginal === "" ? optionReginal[0] : selectReginal;
    optionReginal = [
      { label: "Reginal + ALL Hotel", value: "REG_HOTEL" },
      { label: "Reginal", value: "REG" },
      { label: "ALL Hotel", value: "HOTEL" }
    ]
    property.map((each, key) => {
      if (each.CountryCode === selectCountry.value && each.IsActive === 1 && each.IsHotel === 1) {
        optionReginal.push({ value: each.PropertyCode, label: each.PropertyCode + " - " + each.PropertyName });
      }
    })
    const thisReginal = selectReginal.value !== "REG_HOTEL" && selectReginal.value !== "REG" && selectReginal.value !== "HOTEL" ? "" : selectReginal.value;
    const thisProperty = selectReginal.value !== "REG_HOTEL" && selectReginal.value !== "REG" && selectReginal.value !== "HOTEL" ? selectReginal.value : "";
    let urlIframe = "";
    let urlExport = "";
    if (selectJobLevel) {
      urlIframe = base_url_webapp + "chart/js_hr_report_" + selectReport + ".php?Office=" + thisReginal + "&PropertyCode=" + thisProperty + "&CountryCode=" + selectCountry.value + "&showChart&level=1";
      urlExport = base_url_webapp + "chart/xls_hr_report_" + selectReport + ".php?Office=" + thisReginal + "&PropertyCode=" + thisProperty + "&CountryCode=" + selectCountry.value + "&showChart&level=1";
    } else {
      urlIframe = base_url_webapp + "chart/js_hr_report_" + selectReport + ".php?Office=" + thisReginal + "&PropertyCode=" + thisProperty + "&CountryCode=" + selectCountry.value + "&showChart";
      urlExport = base_url_webapp + "chart/xls_hr_report_" + selectReport + ".php?Office=" + thisReginal + "&PropertyCode=" + thisProperty + "&CountryCode=" + selectCountry.value + "&showChart";
    }
    if (firstLoadPage) {
      this.setState({ firstLoadPage: false });
    }
    return (
      <div>
        <div id="tab-hr-search" className="columns" >
          <div id="hr-statistic" className="column is-7 is-pulled-left" >
            <div className="column is-6">
              <label style={{ fontSize: "1.5rem", fontWeight: "normal" }}>{"Statistic"}</label>
              <hr />
            </div>
            <ul className="management-announcement">
              <li>
                {/*********************  Start Age **********************/}
                <Link
                  onClick={e => {
                    this.resetState();
                    this.setState({ selectReport: "age" });
                  }}
                  className="has-text-danger" to={"#"}> {"Age"} </Link>
                {selectReport === "age" ? <div className="age-content">
                  {!Statistic.AgeView ? <div className="age-radio">
                    <input
                      onChange={e => {
                        Statistic.Age = "ALL";
                        this.setState({ Statistic })
                      }}
                      checked={Statistic.Age === "ALL"}
                      type="radio" name="age" className="age-all" value={"ALL"} /><label> {"ALL"}</label>
                    <input
                      onChange={e => {
                        Statistic.Age = "HQ";
                        this.setState({ Statistic })
                      }}
                      checked={Statistic.Age === "HQ"}
                      type="radio" name="age" className="age-hq" value={"HQ"} /><label> {"HQ"}</label>
                    <input
                      onChange={e => {
                        Statistic.Age = "COUNTRY";
                        this.setState({ Statistic })
                      }}
                      checked={Statistic.Age === "COUNTRY"}
                      type="radio" name="age" className="age-country" value={"COUNTRY"} /><label> {"Country"}</label>
                  </div> : ""}
                  {Statistic.Age === "COUNTRY" && !Statistic.AgeView ?
                    <div>
                      <div className="select-country react-select is-danger" style={styleCountry} >
                        <Select
                          onChange={e => {
                            this.setState({ selectCountry: e })
                          }}
                          value={selectCountry}
                          defaultValue={selectCountry}
                          options={optionCountry}
                        />
                      </div>
                      <div className="select-reginal react-select is-danger" style={styleReginal} >
                        <Select
                          onChange={e => {
                            this.setState({ selectProperty: e.value })
                          }}
                          defaultValue={selectReginal}
                          options={optionReginal}
                        />
                      </div>
                    </div>
                    : ""}
                  {!Statistic.AgeView ? <div className="age-view">
                    <button
                      onClick={e => {
                        Statistic.AgeView = true;
                        this.setState(Statistic);
                      }}
                      className="button is-danger is-small">{"View"}
                    </button>
                  </div>
                    : <div >
                      <div className="">
                        <button
                          onClick={e => {
                            Statistic.AgeView = false;
                            this.setState(Statistic);
                          }}
                          className="button is-danger is-small is-pulled-right"
                        > <FontAwesomeIcon icon={faChevronCircleLeft} style={{ paddingRight: "2px" }} /> Back</button>
                      </div>
                      <Iframe
                        url={urlIframe}
                        className="iframe-statistic"
                        scrolling="no" frameborder="0" marginheight="0" marginwidth="0"
                      />
                      <button
                        onClick={e => {
                          window.open(urlExport, '_blank');
                        }}
                        className="button is-danger is-small"
                        style={{ top: "0.5rem" }}
                      >Export Excel</button>
                    </div>}
                </div> : ""}
              </li>
              {/*********************  Start Generations **********************/}
              <li>
                <Link
                  onClick={e => {
                    this.resetState();
                    this.setState({ selectReport: "generations" });
                  }}
                  className="has-text-danger" to={"#"}> {"Generations"} </Link>
                {selectReport === "generations" ? <div className="generations-content">
                  {!Statistic.GenerationsView ? <div className="generations-radio">
                    <input
                      onChange={e => {
                        this.setState({ Statistic: { Generations: "ALL" } })
                      }}
                      checked={Statistic.Generations === "ALL"}
                      type="radio" name="generations" className="generations-all" value={"ALL"} /><label> {"ALL"}</label>
                    <input
                      onChange={e => {
                        this.setState({ Statistic: { Generations: "HQ" } })
                      }}
                      checked={Statistic.Generations === "HQ"}
                      type="radio" name="generations" className="generations-hq" value={"HQ"} /><label> {"HQ"}</label>
                    <input
                      onChange={e => {
                        this.setState({ Statistic: { Generations: "COUNTRY" } })
                      }}
                      checked={Statistic.Generations === "COUNTRY"}
                      type="radio" name="generations" className="generations-country" value={"COUNTRY"} /><label> {"Country"}</label>
                  </div> : ""}
                  {Statistic.Generations === "COUNTRY" && !Statistic.GenerationsView ?
                    <div>
                      <div className="select-country react-select is-danger" style={styleCountry} >
                        <Select
                          onChange={e => {
                            this.setState({ selectCountry: e })
                          }}
                          value={selectCountry}
                          defaultValue={selectCountry}
                          options={optionCountry}
                        />
                      </div>
                      <div className="select-reginal react-select is-danger" style={styleReginal} >
                        <Select
                          onChange={e => {
                            this.setState({ selectProperty: e.value })
                          }}
                          defaultValue={selectReginal}
                          options={optionReginal}
                        />
                      </div>
                    </div>
                    : ""}
                  {!Statistic.GenerationsView ? <div className="generations-view">
                    <button
                      onClick={e => {
                        Statistic.GenerationsView = true;
                        this.setState(Statistic);
                      }}
                      className="button is-danger is-small">{"View"}
                    </button>
                  </div>
                    : <div >
                      <div className="">
                        <button
                          onClick={e => {
                            Statistic.GenerationsView = false;
                            this.setState(Statistic);
                          }}
                          className="button is-danger is-small is-pulled-right"
                        > <FontAwesomeIcon icon={faChevronCircleLeft} style={{ paddingRight: "2px" }} /> Back</button>
                      </div>
                      <Iframe
                        url={urlIframe}
                        className="iframe-statistic"
                        scrolling="no" frameborder="0" marginheight="0" marginwidth="0"
                      />
                      <button
                        onClick={e => {
                          window.open(urlExport, '_blank');
                        }}
                        className="button is-danger is-small"
                        style={{ top: "0.5rem" }}
                      >Export Excel</button>
                    </div>}
                </div> : ""}
              </li>
              {/*********************  Start Gender **********************/}
              <li> <Link
                onClick={e => {
                  this.resetState();
                  this.setState({ selectReport: "gender" });
                }}
                className="has-text-danger" to={"#"}> {"Gender"} </Link>
                {selectReport === "gender" ? <div className="gender-content">
                  {!Statistic.GenderView ? <div className="gender-radio">
                    <input
                      onChange={e => {
                        this.setState({ Statistic: { Gender: "ALL" } })
                      }}
                      checked={Statistic.Gender === "ALL"}
                      type="radio" name="gender" className="gender-all" value={"ALL"} /><label> {"ALL"}</label>
                    <input
                      onChange={e => {
                        this.setState({ Statistic: { Gender: "HQ" } })
                      }}
                      checked={Statistic.Gender === "HQ"}
                      type="radio" name="gender" className="gender-hq" value={"HQ"} /><label> {"HQ"}</label>
                    <input
                      onChange={e => {
                        this.setState({ Statistic: { Gender: "COUNTRY" } })
                      }}
                      checked={Statistic.Gender === "COUNTRY"}
                      type="radio" name="gender" className="gender-country" value={"COUNTRY"} /><label> {"Country"}</label>
                    <input
                      onChange={e => {
                        this.setState({ selectJobLevel: e.checked })
                      }}
                      type="checkbox" name="gender" className="gender-level" value={"1"} /><label> {"Job Level"}</label>
                  </div> : ""}
                  {Statistic.Gender === "COUNTRY" && !Statistic.GenderView ?
                    <div>
                      <div className="select-country react-select is-danger" style={styleCountry} >
                        <Select
                          onChange={e => {
                            this.setState({ selectCountry: e })
                          }}
                          value={selectCountry}
                          defaultValue={selectCountry}
                          options={optionCountry}
                        />
                      </div>
                      <div className="select-reginal react-select is-danger" style={styleReginal} >
                        <Select
                          onChange={e => {
                            this.setState({ selectProperty: e.value })
                          }}
                          defaultValue={selectReginal}
                          options={optionReginal}
                        />
                      </div>
                    </div>
                    : ""}
                  {!Statistic.GenderView ? <div className="gender-view">
                    <button
                      onClick={e => {
                        Statistic.GenderView = true;
                        this.setState(Statistic);
                      }}
                      className="button is-danger is-small">{"View"}
                    </button>
                  </div>
                    : <div >
                      <div className="">
                        <button
                          onClick={e => {
                            Statistic.GenderView = false;
                            this.setState(Statistic);
                          }}
                          className="button is-danger is-small is-pulled-right"
                        > <FontAwesomeIcon icon={faChevronCircleLeft} style={{ paddingRight: "2px" }} /> Back</button>
                      </div>
                      <Iframe
                        url={urlIframe}
                        className="iframe-statistic"
                        scrolling="no" frameborder="0" marginheight="0" marginwidth="0"
                      />
                      <button
                        onClick={e => {
                          window.open(urlExport, '_blank');
                        }}
                        className="button is-danger is-small"
                        style={{ top: "0.5rem" }}
                      >Export Excel</button>
                    </div>}
                </div> : ""}
              </li>
              {/*********************  Start Seniority **********************/}
              <li> <Link
                onClick={e => {
                  this.resetState();
                  this.setState({ selectReport: "seniority" });
                }}
                className="has-text-danger" to={"#"}> {"Seniority"} </Link>
                {selectReport === "seniority" ? <div className="seniority-content">
                  {!Statistic.SeniorityView ? <div className="seniority-radio">
                    <input
                      onChange={e => {
                        this.setState({ Statistic: { Seniority: "ALL" } })
                      }}
                      checked={Statistic.Seniority === "ALL"}
                      type="radio" name="seniority" className="seniority-all" value={"ALL"} /><label> {"ALL"}</label>
                    <input
                      onChange={e => {
                        this.setState({ Statistic: { Seniority: "HQ" } })
                      }}
                      checked={Statistic.Seniority === "HQ"}
                      type="radio" name="seniority" className="seniority-hq" value={"HQ"} /><label> {"HQ"}</label>
                    <input
                      onChange={e => {
                        this.setState({ Statistic: { Seniority: "COUNTRY" } })
                      }}
                      checked={Statistic.Seniority === "COUNTRY"}
                      type="radio" name="seniority" className="seniority-country" value={"COUNTRY"} /><label> {"Country"}</label>
                  </div> : ""}
                  {Statistic.Seniority === "COUNTRY" && !Statistic.SeniorityView ?
                    <div>
                      <div className="select-country react-select is-danger" style={styleCountry} >
                        <Select
                          onChange={e => {
                            this.setState({ selectCountry: e })
                          }}
                          value={selectCountry}
                          defaultValue={selectCountry}
                          options={optionCountry}
                        />
                      </div>
                      <div className="select-reginal react-select is-danger" style={styleReginal} >
                        <Select
                          onChange={e => {
                            this.setState({ selectProperty: e.value })
                          }}
                          defaultValue={selectReginal}
                          options={optionReginal}
                        />
                      </div>
                    </div>
                    : ""}
                  {!Statistic.SeniorityView ? <div className="gender-view">
                    <button
                      onClick={e => {
                        Statistic.SeniorityView = true;
                        this.setState(Statistic);
                      }}
                      className="button is-danger is-small">{"View"}
                    </button>
                  </div>
                    : <div >
                      <div className="">
                        <button
                          onClick={e => {
                            Statistic.SeniorityView = false;
                            this.setState(Statistic);
                          }}
                          className="button is-danger is-small is-pulled-right"
                        > <FontAwesomeIcon icon={faChevronCircleLeft} style={{ paddingRight: "2px" }} /> Back</button>
                      </div>
                      <Iframe
                        url={urlIframe}
                        className="iframe-statistic"
                        scrolling="no" frameborder="0" marginheight="0" marginwidth="0"
                      />
                      <button
                        onClick={e => {
                          window.open(urlExport, '_blank');
                        }}
                        className="button is-danger is-small"
                        style={{ top: "0.5rem" }}
                      >Export Excel</button>
                    </div>}
                </div> : ""}
              </li>
              {/*********************  Start Nationality **********************/}
              <li> <Link
                onClick={e => {
                  this.resetState();
                  this.setState({ selectReport: "nationality" });
                }}
                className="has-text-danger" to={"#"}> {"Nationality"} </Link>
                {selectReport === "nationality" ? <div className="nationality-content">
                  {!Statistic.NationalityView ? <div className="nationality-radio">
                    <input
                      onChange={e => {
                        this.setState({ Statistic: { Nationality: "ALL" } })
                      }}
                      checked={Statistic.Nationality === "ALL"}
                      type="radio" name="nationality" className="nationality-all" value={"ALL"} /><label> {"ALL"}</label>
                    <input
                      onChange={e => {
                        this.setState({ Statistic: { Nationality: "HQ" } })
                      }}
                      checked={Statistic.Nationality === "HQ"}
                      type="radio" name="nationality" className="nationality-hq" value={"HQ"} /><label> {"HQ"}</label>
                    <input
                      onChange={e => {
                        this.setState({ Statistic: { Nationality: "COUNTRY" } })
                      }}
                      checked={Statistic.Nationality === "COUNTRY"}
                      type="radio" name="nationality" className="nationality-country" value={"COUNTRY"} /><label> {"Country"}</label>
                  </div> : ""}
                  {Statistic.Nationality === "COUNTRY" && !Statistic.NationalityView ?
                    <div>
                      <div className="select-country react-select is-danger" style={styleCountry} >
                        <Select
                          onChange={e => {
                            this.setState({ selectCountry: e })
                          }}
                          value={selectCountry}
                          defaultValue={selectCountry}
                          options={optionCountry}
                        />
                      </div>
                      <div className="select-reginal react-select is-danger" style={styleReginal} >
                        <Select
                          onChange={e => {
                            this.setState({ selectProperty: e.value })
                          }}
                          defaultValue={selectReginal}
                          options={optionReginal}
                        />
                      </div>
                    </div>
                    : ""}
                  {!Statistic.NationalityView ? <div className="nationality-view">
                    <button
                      onClick={e => {
                        Statistic.NationalityView = true;
                        this.setState(Statistic);
                      }}
                      className="button is-danger is-small">{"View"}
                    </button>
                  </div>
                    : <div >
                      <div className="">
                        <button
                          onClick={e => {
                            Statistic.NationalityView = false;
                            this.setState(Statistic);
                          }}
                          className="button is-danger is-small is-pulled-right"
                        > <FontAwesomeIcon icon={faChevronCircleLeft} style={{ paddingRight: "2px" }} /> Back</button>
                      </div>
                      <Iframe
                        url={urlIframe}
                        className="iframe-statistic"
                        scrolling="no" frameborder="0" marginheight="0" marginwidth="0"
                      />
                      <button
                        onClick={e => {
                          window.open(urlExport, '_blank');
                        }}
                        className="button is-danger is-small"
                        style={{ top: "0.5rem" }}
                      >Export Excel</button>
                    </div>}
                </div> : ""}
              </li>
              {/*********************  Start Job Grading **********************/}
              <li> <Link
                onClick={e => {
                  this.resetState();
                  this.setState({ selectReport: "job_grading" });
                }}
                className="has-text-danger" to={"#"}> {"Job grading "}</Link>
                {selectReport === "job_grading" ? <div className="job-grading-content">
                  {!Statistic.JobGradingView ? <div className="job-grading-radio">
                    <input
                      onChange={e => {
                        this.setState({ Statistic: { JobGrading: "ALL" } })
                      }}
                      checked={Statistic.JobGrading === "ALL"}
                      type="radio" name="job-grading" className="job-grading-all" value={"ALL"} /><label> {"ALL"}</label>
                    <input
                      onChange={e => {
                        this.setState({ Statistic: { JobGrading: "HQ" } })
                      }}
                      checked={Statistic.JobGrading === "HQ"}
                      type="radio" name="job-grading" className="job-grading-hq" value={"HQ"} /><label> {"HQ"}</label>
                    <input
                      onChange={e => {
                        this.setState({ Statistic: { JobGrading: "COUNTRY" } })
                      }}
                      checked={Statistic.JobGrading === "COUNTRY"}
                      type="radio" name="job-grading" className="job-grading-country" value={"COUNTRY"} /><label> {"Country"}</label>
                  </div> : ""}
                  {Statistic.JobGrading === "COUNTRY" && !Statistic.JobGradingView ?
                    <div>
                      <div className="select-country react-select is-danger" style={styleCountry} >
                        <Select
                          onChange={e => {
                            this.setState({ selectCountry: e })
                          }}
                          value={selectCountry}
                          defaultValue={selectCountry}
                          options={optionCountry}
                        />
                      </div>
                      <div className="select-reginal react-select is-danger" style={styleReginal} >
                        <Select
                          onChange={e => {
                            this.setState({ selectProperty: e.value })
                          }}
                          defaultValue={selectReginal}
                          options={optionReginal}
                        />
                      </div>
                    </div>
                    : ""}
                  {!Statistic.JobGradingView ? <div className="job-grading-view">
                    <button
                      onClick={e => {
                        Statistic.JobGradingView = true;
                        this.setState(Statistic);
                      }}
                      className="button is-danger is-small">{"View"}
                    </button>
                  </div>
                    : <div >
                      <div className="">
                        <button
                          onClick={e => {
                            Statistic.JobGradingView = false;
                            this.setState(Statistic);
                          }}
                          className="button is-danger is-small is-pulled-right"
                        > <FontAwesomeIcon icon={faChevronCircleLeft} style={{ paddingRight: "2px" }} /> Back</button>
                      </div>
                      <Iframe
                        url={urlIframe}
                        className="iframe-statistic"
                        scrolling="no" frameborder="0" marginheight="0" marginwidth="0"
                      />
                      <button
                        onClick={e => {
                          window.open(urlExport, '_blank');
                        }}
                        className="button is-danger is-small"
                        style={{ top: "0.5rem" }}
                      >Export Excel</button>
                    </div>}
                </div> : ""}
              </li>
            </ul>
          </div>
          {/*********************  End STATISTIC **********************/}
          <div id="hr-report" className="column is-5 is-pulled-right" >
            <div className="column is-6">
              <label style={{ fontSize: "1.5rem", fontWeight: "normal" }}>{"Report"}</label>
              <hr />
            </div>
            <ul className="management-announcement">
              <li>
                <a
                  href={base_url_webapp + langCode + "/hr/library/seniority_report"} target="_blank"
                  className="has-text-danger" to={"#"}> {"Seniority Report "}</a>
              </li>
              <li>
                <Link
                  onClick={e => {
                    this.resetState();
                    this.setState({ selectReport: "monthly" });
                  }}
                  className="has-text-danger" to={"#"}> {"Monthly Entry/Departure"} </Link>
                {selectReport === "monthly" ? <div className="monthly-content">
                  <div>
                    <div className="field is-horizontal">
                      <div
                        style={{ margin: 0 }}
                        className="field-label is-normal">
                        <label
                          style={{ width: "60px", marginRight: 0 }}
                          className="field-label is-normal label"> Country : </label>
                      </div>
                      <div className="field-body">
                        <div className="field">
                          <div className="select-country react-select is-danger" style={styleCountry} >
                            <Select
                              onChange={e => {
                                this.setState({ selectCountry: e })
                              }}
                              value={selectCountry}
                              defaultValue={selectCountry}
                              options={optionCountry}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="field is-horizontal">
                      <div
                        style={{ margin: 0 }}
                        className="field-label is-normal">
                        <label
                          style={{ width: "60px", marginRight: 0 }}
                          className="field-label is-normal label"> Month : </label>
                      </div>
                      <div className="field-body">
                        <div className="field">
                          <div className="select-country" style={{ width: "250px", padding: "0.5rem" }} >
                            <DatePicker
                              className="input is-danger" style={{maxWidth: "100%"}}
                             // selected={this.state.Report.Monthly.Month}
                             value={this.state.Report.Monthly.Month}
                              onChange={date => {
                                date =  moment(date).format('MM-YYYY') ;
                                this.setState({ Report: {Monthly:{Month:date}} });
                              }}
                              dateFormat="MM-yyyy"
                              showMonthYearPicker
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="field is-horizontal">
                      <div className="field-body">
                        <div className="field">
                          {this.state.Report.Monthly.Month===""?
                           <button type="button"
                           disabled
                           style={{ marginRight: "1.2rem" }}
                           className="button is-danger is-small is-pulled-right">
                           <i className="fa fa-file-excel" style={{ marginRight: "0.25rem" }}></i> Export Excel
                           </button>
                          :
                          <button type="button"
                          onClick={e => {
                            {selectCountry.value = selectCountry.value===""?"all":selectCountry.value}
                            urlExport =  base_url_webapp+langCode+"/hr/export_monthly_entry_departure_report/"+selectCountry.value+"/"+this.state.Report.Monthly.Month;
                            window.open(urlExport, '_blank');
                          }}
                          style={{ marginRight: "1.2rem" }}
                          className="button is-danger is-small is-pulled-right">
                          <i className="fa fa-file-excel" style={{ marginRight: "0.25rem" }}></i> Export Excel
                          </button>
                          }
                        </div>
                      </div>
                    </div>
                  </div>
                </div> : ""}
              </li>
              <li>
                <Link
                  onClick={e => {
                    this.resetState();
                    this.setState({ selectReport: "birth of month" });
                  }}
                  className="has-text-danger" to={"#"}> {"Birthday of the month"} </Link>
                {selectReport === "birth of month" ? <div className="monthly-content">
                  <div>
                    <div className="field is-horizontal">
                      <div
                        style={{ margin: 0 }}
                        className="field-label is-normal">
                        <label
                          style={{ width: "60px", marginRight: 0 }}
                          className="field-label is-normal label"> Month : </label>
                      </div>
                      <div className="field-body">
                        <div className="field">
                          <div className="select-country" style={{ width: "250px", padding: "0.5rem" }} >
                            <DatePicker
                              className="input is-danger" style={{maxWidth: "100%"}}
                               //selected={this.state.Report.BirthdayOfMonth}
                               value={this.state.Report.BirthdayOfMonth}
                               onChange={date => {
                                date =  moment(date).format('MM') ;
                                this.setState({ Report: {BirthdayOfMonth:date} });
                              }}
                             dateFormat="MM"
                              showMonthYearPicker
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="field is-horizontal">
                      <div className="field-body">
                        <div className="field">
                          {this.state.Report.BirthdayOfMonth===""? 
                            <button type="button"
                            disabled
                            style={{ marginRight: "1.2rem" }}
                            className="button is-danger is-small is-pulled-right">
                            <i className="fa fa-file-excel" 
                            style={{ marginRight: "0.25rem" }}></i> Export Excel</button>:
                            <button type="button"
                            onClick={e => {
                              urlExport =  base_url_webapp+langCode+"en/hr/export_birthday_report/"+this.state.Report.BirthdayOfMonth;
                              window.open(urlExport, '_blank');
                            }}
                          style={{ marginRight: "1.2rem" }}
                          className="button is-danger is-small is-pulled-right">
                          <i className="fa fa-file-excel" 
                          style={{ marginRight: "0.25rem" }}></i> Export Excel</button>
                            }
                        </div>
                      </div>
                    </div>
                  </div>
                </div> : ""}
              </li>
              <li>
                <Link
                  onClick={e => {
                    alert("Coming Soon!!!");
                  }}
                  className="has-text-danger" to={"#"}> {"Roster report"} </Link>
              </li>
            </ul>
          </div>
        </div >
      </div>
    )
  }
  resetState() {
    this.setState(
      {
        selectReport: "",
        selectProperty: "",
        selectReginal: "",
        selectCountry: "",
        selectJobLevel: false,
        Statistic: {
          Age: "ALL",
          Generations: "ALL",
          Gender: "ALL",
          Seniority: "ALL",
          Nationality: "ALL",
          JobGrading: "ALL"
        },
        Report: {
          Monthly: { Country: "ALL", Month: "" },
          BirthdayOfMonth: ""
        }
      });

  }
  async componentWillMount() {
    // console.log("componentWillUnmount...");
  }
  async componentWillUnmount() {
    // console.log("componentWillUnmount...");
  }
  async componentDidUpdate(prevProps) {
    // console.log("componentWillUnmount...");
  }
  async componentDidMount() {
    // console.log("componentDidMount...");
  }
}

const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    usersReducer: state.usersReducer,
    entityReducer: state.entityReducer,
    propertyReducer: state.propertyReducer,
    masterReducer: state.masterReducer,
    hrReducer: state.hrReducer
  };
};
export default connect(mapStateToProps)(StatisticReport);
