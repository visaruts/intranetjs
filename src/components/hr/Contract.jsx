import React, { Component } from "react";
//import { Prompt } from "react-router";
import { connect } from "react-redux";
import "react-toastify/dist/ReactToastify.css";
import Pagination from "components/Pagination.jsx";
import { Link } from "react-router-dom";
import { lang } from "Lang.jsx";
import ReactImageFallback from "react-image-fallback";
import { public_url, base_url_webapp, uniqid, ENV, s3_url_webapp } from "../../Config_API.jsx";
import { actionGetAccessToken } from "actions/login";
import { actionGetUserByHR } from "actions/hr/actionHR";
import qs from "query-string";
/** moment datetime */
import moment from 'moment';
import 'moment/min/locales.min';
// import Select from "components/Select.jsx";
import Select from 'react-select';
/** fontawesome */
import { faSearch, faChevronCircleLeft } from "@fortawesome/free-solid-svg-icons";
import { faFileAlt as farFileAlt } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { timeout } from "q";
class Contract extends Component {
  state = {
    firstLoadPage: true,
    display: {},
    data: "",
    selectEntity: "",
    selectProperty: "",
    selectOffice: "",
    selectJobDept: "",
    selectJobTitle: "",
    selectStatus: "",
    selectCountry: "",
    btnFilterLoading: false,
    IsExport: true,
    UserCodeList: [],
    UserCodeListTmp: [],
    arrUserItems: [],
    boolUserCheckedAll: false,
    sPram: "",
    PageSize: 10,
    page: 1,
    Search: "",
    template:""
  };
  render() {
    if (lang("code") === "th") {
      moment.locale('th');
    }
    else if (lang("code") === "ja") {
      moment.locale('ja');
    }
    else if (lang("code") === "id") {
      moment.locale('id');
    }
    else {
      moment.locale('en');
    }
    const { firstLoadPage } = this.state;
    const { aData, page } = this.props;
    let PageNum = page;
    let PageSize = 10;
    let data = aData ;
    let PageTotal = typeof data[0] === "undefined" ? 1 : data[0].TotalRows;
    console.log(data);
    const CurrentURL = "hr/template/job_description";
    if (firstLoadPage) {
      //this.get_user();
      this.setState({ firstLoadPage: false })
    }
    return (
      <div className="column is-12" style={{padding:"0"}}>
        {/* <label style={{ fontSize: "1.5rem", fontWeight: "700" }}>Job Description</label> */}
        {/* <Link to={"/hr/template"} 
        className="button is-danger is-pulled-right button-download span" 
        title="Back Home"> 
          <FontAwesomeIcon icon={faChevronCircleLeft} /> 
        <span className="is-hidden-mobile">{" Back"}</span> 
        </Link> 
        <hr style={{
          backgroundColor: "#ff3860",
          border: "none",
          display: "block",
          height: "2px",
          margin: "5px"
        }} />*/}
        <div className="column is-12 is-pulled-right control-search" style={{paddingTop:"0",paddingRight:"0"}}>
          <div className="control has-icons-left has-icons-right is-pulled-right control-search">
            <input className="input is-danger" type="text" placeholder="Search..."
              ref={input => this.search = input}
              onChange={((e) => this.handleInputChange(e, 'tab1'))}
              id="search-input"
            />
            <span className="icon is-small is-left">
              <FontAwesomeIcon icon={faSearch} />
            </span>
          </div>
        </div>
        <div className="column is-12 clear" style={{padding:"0"}}>
          {data.length > 0 ? <div className="list is-hoverable template">
            {data.map((each, key) => {
                let id_tmp = Math.random();
              return <div key={"list-item-"+id_tmp} className={key%2==0?"list-item ":"list-item even"}>
                <Link to={""} className="has-text-danger" >
                  <FontAwesomeIcon className={"icon-book-template"} icon={farFileAlt} />
                  {" - "+each.Topic}
                </Link>
                <div>Publication date :  {moment(each.StartDate).format('DD-MMMM-YYYY')}</div>
              </div>
            })
          }</div>
            :
            <div class="list is-hoverable">
              <div className="list-item">
                {"--- There is no document ---"}
              </div>
            </div>
          }
          <Pagination recordsTotal={PageTotal} page={PageNum} pageSize={PageSize} link={CurrentURL}
            onClick={e => {
              this.setState({ active: false });
            }}
          />
      </div>
      </div>
    )
  }
  handleInputChange = (e, tabs) => {
    let Search = encodeURI(this.search.value);
    const queryString = qs.parse(this.props.sPram);
    const page = queryString.p || 1;
    const p_url = "p=" + page + "&query=" + Search;
    this.setState({ Search: Search });
    // this.props.history.push('/hr/' + activeClass + '?' + p_url);
    this.get_user();
  }
  async get_user() {
    const {
      selectEntity,
      selectProperty,
      selectOffice,
      selectJobDept,
      selectJobTitle,
      selectStatus,
      selectCountry,
      PageSize,
      Search
    } = this.state;
    const queryString = qs.parse(this.props.sPram);
    const page = queryString.p || 1;
    // console.log(this.props);
    const param = {
      Entity: selectEntity,
      Property: selectProperty,
      Office: selectOffice,
      JobDept: selectJobDept,
      JobTitle: selectJobTitle,
      Status: selectStatus,
      Country: selectCountry,
      PageNum: page,
      PageSize: PageSize,
      Search: Search
    }
    let { access_token = "" } = this.props.loginReducer;
    if (access_token === "") {
      const request_token = localStorage.getItem("request_token");
      await this.props.dispatch(actionGetAccessToken(request_token));
    }
    await this.props.dispatch(actionGetUserByHR(access_token, 'user', param));
    if (this.props.hrReducer.data.length > 0) {
      this.setState({
        loading: 1,
        data: this.props.hrReducer.data,
        btnFilterLoading: false,
        UserCodeList: this.props.hrReducer.data[0].UserCodeList.split(","),
        arrUserItems: [],
        IsExport: true,
        UserCodeListTmp: [],
        boolUserCheckedAll: false,
        PageNum: page,
        page: page
      });
    } else {
      this.setState({
        loading: 1,
        data: [],
        btnFilterLoading: false,
        UserCodeList: [],
        arrUserItems: [],
        IsExport: true,
        UserCodeListTmp: [],
        boolUserCheckedAll: false,
        PageNum: page,
        page: page
      });
    }
  }
  async componentDidUpdate(prevProps) {
    //To Do....
    const queryString = qs.parse(this.props.sPram);
    const page = typeof queryString.p == "undefined" ? 1 : queryString.p;
    if (this.state.page !== page) {
      this.get_user();
    }
  }
}

const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    usersReducer: state.usersReducer,
    entityReducer: state.entityReducer,
    propertyReducer: state.propertyReducer,
    masterReducer: state.masterReducer,
    hrReducer: state.hrReducer
  };
};
export default connect(mapStateToProps)(Contract);
