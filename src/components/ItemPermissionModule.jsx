import React, { Component } from "react";
import { connect } from "react-redux";
import { lang } from "../Lang.jsx";
import qs from "query-string";
import { api, ENV, CONFIG, base_url } from "../Config_API.jsx";
import axios from "axios";
import { Link } from "react-router-dom";
import Parser from "html-react-parser";
/**
 * Option for page
 */
import "animate.css/animate.min.css";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

//import { userlist, user_admin_dashboard } from "../db";

class ItemPermissionModule extends Component {
  state={
    query:"",
    userlist:[],
    data:{
      users:[],
      filter_users:[],
      users_selected_array:[],
      users_selected:[],
      filter_users_selected:[],
    }
  }
  
  notify = (type, message) => {
    const config = {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true
    };
    if (type === "error") {
      toast.error(message, config);
    } else if (type === "success") {
      toast.success(message, config);
    } else if (type === "warn") {
      toast.warn(message, config);
    } else if (type === "info") {
      toast.info(message, config);
    }
  };
  render() {
    //console.log(this.state);
    return <div className="notification">
      <div className="columns is-mobile has-background-white">
        <div className="column" style={{ flex: "none", width:"47%"}}>
          <nav className="panel">
            <div className="panel-heading has-background-link has-text-white">
              <div className="columns">
                <div className="column">
                  User List
                </div>
                <div className="column has-text-right">
                ( <a onClick={e=>{
                    this.handleSelectAll("unselect", true);
                  }}>Select All</a> | <a onClick={e=>{
                    this.handleSelectAll("unselect", false);
                  }}>Clear All</a> )
                </div>
              </div>
            </div>
            <div className="panel-block">
              <p className="control has-icons-left">
                <input className="input" type="text" placeholder={lang("Search")}
                onChange={e=>{
                  this.handleSearchUserList(e, "unselect");
                }} />
                <span className="icon is-left">
                  <i className="fas fa-search" aria-hidden="true"></i>
                </span>
              </p>
            </div>
            <div style={{overflow:"auto", height:"250px"}}>
            {
              this.state.data.filter_users.map((each, key)=>{
                  return <a 
                    key={each.UserCode} 
                    className={each.tmpSelect?"panel-block is-active":"panel-block"}
                    onClick={e=>{
                      var stateCopy = Object.assign({}, this.state);
                      stateCopy.data.filter_users = stateCopy.data.filter_users.slice();
                      stateCopy.data.filter_users[key] = Object.assign({}, stateCopy.data.filter_users[key]);
                      stateCopy.data.filter_users[key].tmpSelect = !this.state.data.filter_users[key].tmpSelect;
                      
                      stateCopy.data.users = stateCopy.data.users.slice();
                      stateCopy.data.users = stateCopy.data.users.map(row=>{
                        //console.log(row);
                        return {
                          ...row,
                          tmpSelect:(stateCopy.data.filter_users[key].UserCode===row.UserCode)?
                                      !row.tmpSelect
                                      :
                                      row.tmpSelect
                        }
                      });
                      //console.log(stateCopy.data.users);
                      //stateCopy.data.users[key] = Object.assign({}, stateCopy.data.users[key]);
                      //stateCopy.data.users[key].tmpSelect = !this.state.data.users[key].tmpSelect;
                      
                      //console.log(stateCopy);
                      this.setState(stateCopy);
                    }}
                  >
                    {each.Name} ( {each.JobTitle} )
                  </a>
              })
            }
            </div>
          </nav>
        </div>
        <div className="column has-text-centered" style={{  display: "grid",  alignItems: "center", backgroundColor:"#363636" }}>
          
          <div className="columns">
            <div className="column">
              <a onClick={e=>{
                this.handleAssign();
              }}>
                <span className="icon is-large has-text-white">
                  <i className="fas fa-arrow-alt-circle-right fa-3x"></i>
                </span>
              </a>
            </div>
          </div>
          <div className="columns">
            <div className="column">
              <a onClick={e=>{
                this.handleUnAssign();
              }}>
                <span className="icon is-large has-text-white">
                  <i className="fas fa-arrow-alt-circle-left fa-3x"></i>
                </span>
              </a>
            </div>
          </div>
        
        </div>
        <div className="column" style={{ flex: "none", width:"47%"}}>
        <nav className="panel">
            <div className="panel-heading has-background-link has-text-white">
              <div className="columns">
                <div className="column">
                  {lang(this.props.modulecode)}
                </div>
                <div className="column has-text-right">
                ( <a onClick={e=>{
                    this.handleSelectAll("selected", true);
                  }}>Select All</a> | <a onClick={e=>{
                    this.handleSelectAll("selected", false);
                  }}>Clear All</a> )
                </div>
              </div>
            </div>
            <div className="panel-block">
              <p className="control has-icons-left">
                <input className="input" type="text" placeholder={lang("Search")}
                onChange={e=>{
                  this.handleSearchUserList(e, "selected");
                }} />
                <span className="icon is-left">
                  <i className="fas fa-search" aria-hidden="true"></i>
                </span>
              </p>
            </div>
            <div style={{overflow:"auto", height:"250px"}}>
            {
              this.state.data.filter_users_selected.map((each, key)=>{
                  return <a 
                    key={each.UserCode} 
                    className={each.tmpSelect?"panel-block is-active":"panel-block"}
                    onClick={e=>{
                      var stateCopy = Object.assign({}, this.state);
                      stateCopy.data.filter_users_selected = stateCopy.data.filter_users_selected.slice();
                      stateCopy.data.filter_users_selected[key] = Object.assign({}, stateCopy.data.filter_users_selected[key]);
                      stateCopy.data.filter_users_selected[key].tmpSelect = !this.state.data.filter_users_selected[key].tmpSelect;
                      
                      stateCopy.data.users_selected = stateCopy.data.users_selected.slice();
                      stateCopy.data.users_selected = stateCopy.data.users_selected.map(row=>{
                        //console.log(row);
                        return {
                          ...row,
                          tmpSelect:(stateCopy.data.filter_users_selected[key].UserCode===row.UserCode)?
                                      !row.tmpSelect
                                      :
                                      row.tmpSelect
                        }
                      });
                      //stateCopy.data.users_selected[key] = Object.assign({}, stateCopy.data.users_selected[key]);
                      //stateCopy.data.users_selected[key].tmpSelect = !this.state.data.users_selected[key].tmpSelect;
                      this.setState(stateCopy);
                    }}
                  >
                    {each.Name} ( {each.JobTitle} )
                  </a>
              })
            }
            </div>
          </nav>
        </div>
      </div>
      
      <div className="columns">
        <div className="column has-text-right">
          <button type="button" className="button is-link"
          onClick={e=>{
            this.handleSubmit();
            //console.log(this.state.data.users_selected_array.toString());

          }}
          >Save</button>
        </div>
      </div>
    </div>;
  }
  getUsersSelected(users_selected_array){
    return this.state.userlist.filter(each => {
      return users_selected_array.indexOf(each.UserCode.toLowerCase()) >= 0;
    });
  }
  getUsers(users_selected_array){
    return this.state.userlist.filter(each => {
      return users_selected_array.indexOf(each.UserCode.toLowerCase()) === -1;
    });
  }
  async componentDidMount(){
/*
    let { access_token = "", access_token_exp = 0 } = this.props.loginReducer; //this.state;
    //console.log(this.props.loginReducer);
    if (access_token === "" || access_token_exp < Date.now()) {
      const request_token = localStorage.getItem("request_token");
      await this.props.dispatch(actionGetAccessToken(request_token));
      //console.log(request_token);
    }
    access_token = this.props.loginReducer.access_token;
    await this.props.dispatch(actionUsers(access_token));*/
    const access_token=this.props.access_token

    //const modulecode="ADMIN_DASHBOARD";
    const modulecode=this.props.modulecode;

    const res = await axios.get(
      api.MA_API +
        "module/user?modulecode="+modulecode,

      {
        headers: {
          Authorization: "Bearer " + access_token,
          "Content-Type": "application/json; charset=utf-8"
        }
      }
    );
    const user_admin_dashboard=res.data.data;
    //console.log(user_admin_dashboard);
    let userlist=[];
    this.props.users.map(each=>{
      if(each.IsActive===1){
        userlist.push({
          UserCode: each.UserCode.toLowerCase(), 
          Name: each.UserFName+" "+each.UserLName, 
          JobTitle: each.JobTitle,
          tmpSelect: false});
      }
    });
    //console.log(userlist);
    await this.setState({
      userlist
    });


    let users_selected_array=[];
    user_admin_dashboard.map(each=>{
      users_selected_array.push(each.UserCode.toLowerCase());
    });
    const users_selected = this.getUsersSelected(users_selected_array);
    const users = this.getUsers(users_selected_array);
    //console.log(users);

    this.setState({
      data:{
        ...this.state.data,
        users,
        filter_users:users,

        users_selected_array,
        users_selected,
        filter_users_selected:users_selected
      }
    });
  }

  handleSearchUserList(e, type){

    const query = e.target.value;
    const pattern = new RegExp(query, "i");
    if(type==="unselect"){
      const filter_users = this.state.data.users.filter(each => {
        return pattern.test(each.Name) || pattern.test(each.JobTitle);
      });
      this.setState({
        query,
        data:{
          ...this.state.data,
          filter_users
        }
        
      });
    }
    else if(type==="selected")
    {
      const filter_users_selected = this.state.data.users_selected.filter(each => {
        return pattern.test(each.Name);
      });
      this.setState({
        query,
        data:{
          ...this.state.data,
          filter_users_selected
        }
      });
    }
  }
  handleSelectAll(type, select){
    if(type==="unselect"){
      let {filter_users, users} = this.state.data;
      filter_users=filter_users.map(each=>{
        return {
          ...each,
          tmpSelect:select
        }
      });
      users=users.map(each=>{
        return {
          ...each,
          tmpSelect:select
        }
      });
      this.setState({
        data:{
          ...this.state.data,
          users,
          filter_users
        }
      });
    }
    else if(type==="selected")
    {
      let {filter_users_selected, users_selected} = this.state.data;
      filter_users_selected=filter_users_selected.map(each=>{
        return {
          ...each,
          tmpSelect:select
        }
      });
      users_selected=users_selected.map(each=>{
        return {
          ...each,
          tmpSelect:select
        }
      });
      this.setState({
        data:{
          ...this.state.data,
          users_selected,
          filter_users_selected
        }
      });
    }
  }

  handleAssign(){
    //console.log(this.state.data.users);
    let {users, users_selected_array} = this.state.data;
    users.map(each=>{
      if(each.tmpSelect){
        users_selected_array.push(each.UserCode);
      }
    });
    const users_selected = this.getUsersSelected(users_selected_array);
    users = this.getUsers(users_selected_array);
    //console.log(users_selected);
    //console.log(users);

    this.setState({
      data:{
        ...this.state.data,
        users_selected_array,
        users,
        filter_users:users,
        users_selected,
        filter_users_selected:users_selected
      }
    });
  }

  handleUnAssign(){
    //console.log(this.state.users);
    let {users_selected, users_selected_array} = this.state.data;
    users_selected.map(each=>{
      if(each.tmpSelect){
        users_selected_array.remove(each.UserCode);
      }
    });
    users_selected = this.getUsersSelected(users_selected_array);
    const users = this.getUsers(users_selected_array);
    //console.log(users_selected);
    //console.log(users);

    this.setState({
      data:{
        ...this.state.data,
        users_selected_array,
        users,
        filter_users:users,
        users_selected,
        filter_users_selected:users_selected
      }
    });
  }

  async handleSubmit(){
    //console.log(this.state.data.users_selected_array.toString());
    
    //const ModuleCode="ADMIN_DASHBOARD";
    const ModuleCode=this.props.modulecode;
    try {
      
      const url = api.MA_API + "module";
      const data = { 
        ModuleCode,
        UserList:this.state.data.users_selected_array.toString()
      };
      const access_token=this.props.loginReducer.access_token;
      //const access_token=this.props.access_token;

      const res = await axios.post(url, data, {
        headers: { Authorization: "Bearer " + access_token }
      });
      if(res.data.code===200)
      {
        this.notify("success", lang("Update Permission Success"));
      }
      else{
        console.log(res);
        this.notify("error", lang("Update Permission Fail"));
      }
    } catch (err) {
      console.log({ err });
      this.notify("error", lang("Update Permission Fail"));
      this.notify("error", err.message);
      //window.location.href = "/connection_error?err="+err.message+"&page=ItemDefect&method=handleSubmit";
    }
  }
            
  
}
const mapStateToProps = state => {
  return { landReducer: state.langReducer, 
    loginReducer: state.loginReducer, 
    usersReducer: state.usersReducer };
};
export default connect(mapStateToProps)(ItemPermissionModule);

Array.prototype.remove = function() {
  var what, a = arguments, L = a.length, ax;
  while (L && this.length) {
      what = a[--L];
      while ((ax = this.indexOf(what)) !== -1) {
          this.splice(ax, 1);
      }
  }
  return this;
};
