import React, { Component } from "react";
import { connect } from "react-redux";
import { lang } from "../Lang.jsx";

import { actionGetAccessToken } from "../actions/login";
import {
  actionGetPropertyByUserCode,
  actionSavePropertyCode
} from "../actions/property";

class PropertySelectBox extends Component {
  state = {
    access_token: localStorage.getItem("access_token") || "",
    country: {},
    property: []
  };
  render() {
    const {propertyCode}=this.props.propertyReducer;
    return (
      <select
        name="propertyCode"
        value={propertyCode}
        onChange={e => {
          //this.props.handleProperty(e);
          this.handleProperty(e);
        }}
      >
        {this.props.showSelectAll &&
        this.props.propertyReducer.property.length > 1 ? (
          <option value="" key="all">
            {lang("All Hotel")}
          </option>
        ) : (
          ""
        )}
        {Object.keys(this.state.country).map(each => {
          return (
            <optgroup label={lang(each)} key={each}>
              {this.state.country[each].map(p => {
                return (
                  <option value={p.PropertyCode} key={p.PropertyCode}>
                    {p.PropertyName}
                  </option>
                );
              })}
            </optgroup>
          );
        })}
      </select>
    );
  }

  async handleProperty(e) {
    console.log(e.target.value);
    const propertyCode = e.target.value;
    let PropertyCode = "",
      EntityCode = "",
      PropertyName = "",
      EntityName = "";
    if (propertyCode !== "") {
      const { property = [] } = this.props.propertyReducer;
      const p = this.state.property.find(each => {
        return each.PropertyCode === propertyCode;
      });
      
      PropertyCode = p.PropertyCode;
      EntityCode = p.EntityCode;
      PropertyName = p.PropertyName;
      EntityName = p.EntityName;
    }
    const payload = {
      PropertyCode,
      EntityCode,
      PropertyName,
      EntityName
    };
    await this.props.dispatch(actionSavePropertyCode(payload));

    if (this.props.onLoadTable) {
      if (propertyCode === this.props.propertyReducer.propertyCode) {
        //this.props.onLoadTable();
      }
    }

    if (this.props.saveState) {
      this.props.saveState();
    }
  }
  async getProperty(access_token) {
    try{
      if (this.props.propertyReducer.property.length === 0) {
        const onlyHotel = 1;
        await this.props.dispatch(
          actionGetPropertyByUserCode(access_token, onlyHotel)
        );
      }
      //console.log(this.props.propertyReducer);
      let country = {};
      const { property = [] } = this.props.propertyReducer;
      //console.log(property);
      if (property.length > 0) {
        property.map(each => {
          if (!country[each.CountryCode]) {
            country[each.CountryCode] = [];
          }
          country[each.CountryCode] = [...country[each.CountryCode], each];
          return;
        });
      }
      if (
        this.props.propertyReducer.propertyCode === "" &&
        (!this.props.showSelectAll ||
          this.props.propertyReducer.property.length == 1)
      ) {
        const payload = {
          PropertyCode: property[0].PropertyCode,
          EntityCode: property[0].EntityCode,
          PropertyName: property[0].PropertyName,
          EntityName: property[0].EntityName
        };
        this.props.dispatch(actionSavePropertyCode(payload));
      }
      //console.log(property);
      //console.log(country);
      this.setState({
        ...this.props.propertyReducer,
        country
      });
    }
    catch (err) {
      window.location.href = "/connection_error?err="+err.message;
    }
  }

  async componentDidMount() {
    let { access_token = "", access_token_exp = 0 } = this.props.loginReducer; //this.state;
    //console.log(this.props.loginReducer);
    if (access_token === "") {
      // || access_token_exp < Date.now()
      //console.log("actionGetAccessToken");
      const request_token = localStorage.getItem("request_token");
      await this.props.dispatch(actionGetAccessToken(request_token));
      //console.log(this.props.loginReducer.access_token);
    }
    access_token = this.props.loginReducer.access_token;

    //console.log("componentDidMount: " + access_token);
    await this.getProperty(access_token);

    if (this.props.saveState) {
      this.props.saveState();
    }
  }
}

const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    propertyReducer: state.propertyReducer
  };
};
export default connect(mapStateToProps)(PropertySelectBox);
