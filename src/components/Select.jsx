import React, { Component } from "react";
import { connect } from "react-redux";
import { ENV } from "../Config_API.jsx";
class Select extends Component {

  render() {
    // console.log(this.props);
    const { aOption ,id , className} = this.props;
    return (
      <div>
        <div class="control">
          <div class="select is-danger">
            <select id={id} className={className} >
              {
                aOption.map((each, key) => {
                  return <option value={each.value} > {each.label} </option>
                })
              }
            </select>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    propertyReducer: state.propertyReducer,
    usersReducer: state.usersReducer
  };
};
export default connect(mapStateToProps)(Select);
