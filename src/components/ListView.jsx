import React, { Component } from "react";
//import { Prompt } from "react-router";
import { connect } from "react-redux";
import { lang } from "../Lang.jsx";
//import { actionGetAccessToken } from "../actions/login";
/*import {
  actionGetPropertyByUserCode,
  actionSavePropertyCode
} from "../actions/property";*/
import Lightbox from 'react-lightbox-component';
/** moment datetime */
import moment from 'moment';
import 'moment/min/locales.min';

/**react-day-picker */
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import MomentLocaleUtils, {
  formatDate,
  parseDate,
} from 'react-day-picker/moment';
/**react-day-picker */

import axios from "axios";
import { api, base_url } from "../Config_API.jsx";
import Select from 'react-select';
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

//import PropertySelectBox from "../Components/PropertySelectBox.jsx";
import { getPermissionApplication } from "../Permission.jsx";

class ItemDefect extends Component {
  state = {
    display:{},
    data:{},
    permissionAdmin:false,
    showModal: false,
    showAssignTo:false,
    showReSchedule:false,
    userData:[],
    userDataOption:[],
    expectedDate:undefined, 
    assignedTo:"",
    status:"",
    remark:""
  };
  
  notify = (type, message) => {
    const config = {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true
    };
    if (type === "error") {
      toast.error(message, config);
    } else if (type === "success") {
      toast.success(message, config);
    } else if (type === "warn") {
      toast.warn(message, config);
    } else if (type === "info") {
      toast.info(message, config);
    }
  };

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  
  handleDayChange = this.handleDayChange.bind(this);
  render() {
    const {each, key}=this.props.data;
    //console.log(this.props);
    
    const status=this.capitalizeFirstLetter(each.Status);
    const location=this.capitalizeFirstLetter(each.Location);
    const defects=this.capitalizeFirstLetter(each.Defects);
    
    var images = []
    /*if(each.AttachFile1!==null){
      images[0]={
        src: 'uploads/1542265964729_Ft5_3mb.JPG',
        title: defects,
        description: defects + ' ( ' + location + ' )'
      }
    }
    if(each.AttachFile2!==null){
      images[1]={
        src: 'uploads/1542265964540_IMG_5936.jpg',
        title: defects,
        description: defects + ' ( ' + location + ' )'
      }
    }
    if(each.AttachFile3!==null){
      images[2]={
        src: 'uploads/1542265964729_Ft5_3mb.JPG',
        title: defects,
        description: defects + ' ( ' + location + ' )'
      }
    }*/
    
    if(each.AttachFile1!==null){
      images[0]={
        src: base_url+'/uploads/'+each.AttachFile1,
        title: defects,
        description: defects + ' ( ' + location + ' )'
      }
    }
    if(each.AttachFile2!==null){
      images[1]={
        src: base_url+'/uploads/'+each.AttachFile2,
        title: defects,
        description: defects + ' ( ' + location + ' )'
      }
    }
    if(each.AttachFile3!==null){
      images[2]={
        src: base_url+'/uploads/'+each.AttachFile3,
        title: defects,
        description: defects + ' ( ' + location + ' )'
      }
    }
    //moment.locale('ja');
    //moment(each.ExpectedDate).format('LLLL')
    //var localLocale = moment(each.ExpectedDate);

    const { expectedDate, assignedTo } = this.state;
    

    return (
      <div className="card" key={key} >
        <header className="card-header" 
                  onClick={(e)=>{
                    e.preventDefault();
                    const d=!this.state.display[key];
                    this.setState({
                      display:{...this.state.display,[key]:d}
                    })
                  }}>
          <div className="card-header-title">
            <div className="columns is-1-mobile" style={{width:"100%"}}>
              <div className="column field is-grouped is-grouped-multiline">
                <div>
                  <span style={{color: "rgb(35, 39, 41)", fontWeight: "bold", fontSize: "18px"}}>
                    {defects}
                  </span>
                  &nbsp;
                  <span className="has-text-grey-light" style={{fontSize: "14px"}}>
                    ( {(moment(each.RequestDate).format('LLLL')==="Invalid date")?
                    "":moment(each.RequestDate).startOf('minute').fromNow()} )
                  </span>
                  &nbsp;
                  <div className="tags has-addons">
                    
                      <span className="tag is-light">{location}</span>
                      <span className={"tag label-"+each.Status} style={{fontWeight: "normal"}}>{lang(status)}</span>
                      { each.Urgent===1?
                        <span className="tag is-danger" style={{fontWeight: "normal"}}>{lang("Urgent")}</span>:""
                      }
                  </div>
                </div>
                <div tabIndex="1" onClick={() => { this.setState({ isOpen: true }) }}>
                  <Lightbox images={images} thumbnailWidth='40px' thumbnailHeight='40px'
                  reactModalProps={{ shouldReturnFocusAfterClose: false }}
                  onCloseRequest={() => this.setState({ isOpen: false })}
                  />
                </div>
                
              </div>
              <div className="column has-text-right">
              
                {
                  this.props.propertyCode===""?
                    <span style={{color: "rgb(35, 39, 41)", fontWeight: "bold", fontSize: "14px"}}>
                      {each.PropertyName}, {lang(each.CountryCode)}<br />
                    </span>:""

                }
                
              </div>
              
            </div>
            
          </div>
          <a href="#" className="card-header-icon" aria-label="more options"
            onClick={(e)=>{
              e.preventDefault();
              const d=!this.state.display[key];
              this.setState({
                display:{...this.state.display,[key]:d}
              })
            }}
          >
            <FontAwesomeIcon icon={!this.state.display[key]?"angle-down":"angle-up"} />
          </a>
        </header>
        <div className="card-content" style={!this.state.display[key]?{display:"none"}:{}}>
          <div className="content">
            
            <strong>{lang("Ticket")}#</strong> {each.TicketID}<br />
            <strong>{lang("Expected Date")}:</strong><br /> {this.state.ExpectedDate_Text}<br />
            <strong>{lang("Reported By")}:</strong><br /> {each.Name}<br />
            <strong>{lang("Date Reported")}:</strong><br /> {this.state.RequestDate_Text}<br />
            <strong>{lang("Assigned To")}:</strong><br /> {each.Assigned_to_FullName}<br />
            <strong>{lang("Contact Info")}:</strong><br /> {each.ContactInfo}<br />
            <strong>{lang("Date Done")}:</strong><br /> {this.state.FinishDate_Text}<br />
            <strong>{lang("Remark")}:</strong><br />
            <div dangerouslySetInnerHTML={{__html: (each.Remark)}}>
            </div>
            
          </div>
          <div className="has-text-centered">
          {(this.props.showEdit && this.state.permissionAdmin)?
            <button className="button is-link" style={{marginRight:"3px"}}
              onClick={e=>{
                this.handleShowModal(true)
              }}
            >
              <FontAwesomeIcon
                icon="pencil-alt" style={{marginRight:"3px"}}
              />
              <span>{lang("Edit Status")}</span>
            </button>
          :""}
          {this.props.showCancel?
            <button className="button is-black"
              onClick={e=>{
                if(window.confirm(lang("Would you like to cancel this defect"))){
                  this.handleCancel()
                }
                
              }}
            >
              <FontAwesomeIcon
                icon="trash-alt" style={{marginRight:"3px"}}
              />
              <span>{lang("Cancel")}</span>
            </button>
          :""}
          </div>
        </div>
        
        <div className="modal" style={this.state.showModal?{display:"flex"}:{display:"none"}}>
          <div className="modal-background" onClick={(e)=>{
              e.preventDefault();
              this.setState({
                showModal:!this.state.showModal
              })
            }}></div>
          <div className="modal-card">
            <header className="modal-card-head">
              <p className="modal-card-title" style={{fontSize: "100%"}}>{lang("Would you like to change status")}</p>
              <button className="delete" aria-label="close"
                onClick={e=>{
                  this.handleShowModal(false)
                }}
              ></button>
            </header>
            <section className="modal-card-body">
              {/** Content */}
              <form ref={(el) => this.myFormRef = el}>
                <div className="field">
                  <div className="control">
                    <strong>{lang("Ticket")}#:</strong> {each.TicketID}
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                    <strong>{lang("Defects")}:</strong> {defects}
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                    <strong>{lang("Location")}:</strong> {location}
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                    <strong>{lang("Status")}:</strong>
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                    
                    <div className="select is-info">
                      <select value={this.state.status}
                        onChange={e=>{
                          this.handleStatus(e)
                        }}
                      >
                        {
                          each.Status==="open"?
                            <option value="open">{lang("Open")}</option>
                            :""
                        }
                        {
                          each.Status==="cancel"?
                            <option value="cancel">{lang("Cancel")}</option>
                            :""
                        }
                        <option value="on-progress">{lang("On-progress")}</option>
                        <option value="re-schedule">{lang("Re-schedule")}</option>
                        <option value="re-assigned">{lang("Re-assigned")}</option>
                        <option value="closed">{lang("Closed")}</option>
                      </select>
                    </div>
                  </div>
                </div>
                {this.state.showAssignTo?
                  <div>
                    <div className="field">
                      <div className="control">
                        <strong>{lang("Assign To")}:</strong>
                      </div>
                    </div>
                    <div className="field">
                      <div className="control">
                      <Select
                        className="basic-single is-info"
                        classNamePrefix="select"
                        value={this.state.userDataOption.filter(option => 
                          option.value === assignedTo)
                        }
                        isDisabled={false}
                        isLoading={false}
                        isClearable={true}
                        isRtl={false}
                        isSearchable={true}
                        name="assignedTo"
                        options={this.state.userDataOption}
                        onChange={this.handleChangeAssignedTo}
                      />
                      </div>
                    </div>
                  </div>:""
                }
                
                {this.state.showReSchedule?
                  <div>
                    <div className="field">
                      <div className="control">
                        <strong>{lang("Expected Date")}:</strong>
                      </div>
                    </div>
                    <div className="field">
                      <div className="control item-date">
                      {/*<p>
                        {isExpectedEmpty && 'Please type or pick a day'}
                        {!isExpectedEmpty && !expectedDate && 'This day is invalid'}
                        {expectedDate && isExpectedDisabled && 'This day is disabled'}
                        {expectedDate &&
                          !isExpectedDisabled &&
                          `You chose ${expectedDate}`}
                        </p>*/}
                      <DayPickerInput className="input is-info"
                        format={"YYYY-MM-DD"}
                        value={expectedDate}
                        formatDate={formatDate}
                        parseDate={parseDate}
                        onDayChange={this.handleDayChange}
                        dayPickerProps={{
                          selectedDays: [expectedDate],
                          locale: lang("code"),
                          localeUtils: MomentLocaleUtils,
                          numberOfMonths: 1,
                        }}
                      />
                      </div>
                    </div>
                  </div>:""
                }
                
                <div className="field">
                  <div className="control">
                    <strong>{lang("Remark")}:</strong>
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                    <textarea className="textarea is-info" onChange={e=>{
                      this.handleRemark(e)
                    }}></textarea>
                  </div>
                </div>
              </form>
                
            </section>
            <footer className="modal-card-foot">
              <button className="button is-success"
                onClick={e=>{
                  this.handleSubmit()
                }}
              >{lang("Yes")}</button>
              <button className="button"
                onClick={e=>{
                  this.handleShowModal(false)
                }}>{lang("No")}</button>
            </footer>
          </div>
        </div>

      </div>  
    );
  }

  async handleSubmit(){
    const {status, expectedDate, assignedTo, remark} = this.state;
    const {each}=this.props.data;
    const url = api.MA_API + "dashboard/updateStatus";
    const data = { 
      TicketID:each.TicketID,
      Status: status,
      Remark: remark,
      Assigned_to: assignedTo,
      ExpectedDate: expectedDate
    };
    console.log(this.props.loginReducer.access_token);
    console.log(data);
    try {
      const res = await axios.post(url, data, {
        headers: { Authorization: "Bearer " + this.props.loginReducer.access_token }
      });
      if(res.data.code===200)
      {
        this.notify("success", lang("Update Status Success"));
        this.handleShowModal(false);
        this.props.handleFilter();
      }
      else{
        this.notify("error", lang("Update Status Error"));
      }
    } catch (err) {
      console.log({ err });
      this.notify("error", lang("Update Status Error"));
      this.notify("error", err.message);
      //window.location.href = "/connection_error?err="+err.message+"&page=ItemDefect&method=handleSubmit";
    }
  }

  async handleCancel(){
    const {expectedDate, assignedTo, remark} = this.state;
    const {each}=this.props.data;
    const url = api.MA_API + "dashboard/updateStatus";
    const data = { 
      TicketID:each.TicketID,
      Status: "cancel",
      Remark: remark,
      Assigned_to: assignedTo,
      ExpectedDate: expectedDate
    };
    try {
      const res = await axios.post(url, data, {
        headers: { Authorization: "Bearer " + this.props.loginReducer.access_token }
      });
      if(res.data.code===200)
      {
        this.notify("success", lang("Update Status Success"));
        this.props.handleFilter();
      }
      else{
        this.notify("error", lang("Update Status Error"));
      }
    } catch (err) {
      console.log({ err });
      this.notify("error", lang("Update Status Error"));
      this.notify("error", err.message);
      //window.location.href = "/connection_error?err="+err.message+"&page=ItemDefect&method=handleSubmit";
    }
  }
  handleDayChange(expectedDate, modifiers, dayPickerInput) {
    //const input = dayPickerInput.getInput();
    this.setState({
      expectedDate: moment(expectedDate).format('YYYY-MM-DD')        
    });
  }

  handleChangeAssignedTo = (selectedOption) => {
    //console.log(selectedOption.value);
    if(selectedOption){
      const {value=undefined}=selectedOption;
      this.setState({
        assignedTo: value
      })
    }else{
      this.setState({
        assignedTo: undefined
      })
    }
  }

  clearForm = () => { 
    this.myFormRef.reset();
  }
  
  handleShowModal(showModal){
    this.clearForm();
    
    let showAssignTo=false;
    let showReSchedule=false;
    const {each}=this.props.data;
    if(showModal)
    {
      if(each.Status==="re-assigned"){
        this.handleReAssign();
        showAssignTo=true;
      }
      if(each.Status==="re-schedule"){
        showReSchedule=true;
      }
    }
    //console.log(each.Assigned_to);
    this.setState({
      status:each.Status,
      assignedTo:each.Assigned_to,
      expectedDate:(moment(each.ExpectedDate).format('YYYY-MM-DD')==="Invalid date")?
        undefined:moment(each.ExpectedDate).format('YYYY-MM-DD'),
      showModal,
      showAssignTo,
      showReSchedule
    });
    console.log(this.props.data.each);
  }

  handleStatus(e){
    let showAssignTo=false;
    if(e.target.value==="re-assigned"){
      this.handleReAssign();
      showAssignTo=true;
    }
    let showReSchedule=false;
    if(e.target.value==="re-schedule"){
      showReSchedule=true;
    }
    this.setState({
      status:e.target.value,
      showAssignTo,
      showReSchedule
    });
  }
  
  handleRemark(e){
    this.setState({
      remark:e.target.value
    })
  }

  async handleReAssign(){
    if(this.state.userData.length===0){
      const {each}=this.props.data;
      const res = await axios.get(
        api.MA_API +
          "user/ticket?ticketID="+
          each.TicketID,

        {
          headers: {
            Authorization: "Bearer " + this.props.loginReducer.access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        }
      );
      const {data}=res.data;
      let userDataOption=[];
      data.map(each=>{
        userDataOption.push({value: each.UserCode, label: each.UserFName+" "+each.UserLName});
      });
      //console.log(userDateOption);
      
      this.setState({
        userData: res.data,
        userDataOption
      });
    }
  }

  getMomentTime(){
    const {each}=this.props.data;

    if(this.props.langCode==="th"){
      moment.locale('th');
    }
    else if(this.props.langCode==="ja"){
      moment.locale('ja');
    }
    else if(this.props.langCode==="id"){
      moment.locale('id');
    }
    else
    {
      moment.locale('en');
    }
    
    this.setState({
      ExpectedDate_Text: (moment(each.ExpectedDate).format('LLLL')==="Invalid date")?
                      "":moment(each.ExpectedDate).format('LLLL'),
      RequestDate_Text: (moment(each.RequestDate).format('LLLL')==="Invalid date")?
                      "":moment(each.RequestDate).format('LLLL'),
      FinishDate_Text: (moment(each.FinishDate).format('LLLL')==="Invalid date")?
                      "":moment(each.FinishDate).format('LLLL'),
      expectedDate:(moment(each.ExpectedDate).format('YYYY-MM-DD')==="Invalid date")?
        undefined:moment(each.ExpectedDate).format('YYYY-MM-DD'), 
      isExpectedDisabled:false, 
      isExpectedEmpty:(moment(each.ExpectedDate).format('YYYY-MM-DD')==="Invalid date")
    });
  }
  async componentDidMount(){
    await this.getMomentTime();
    
    if(this.props.applicationList){
      const {each}=this.props.data;

      let showAssignTo=false;
      if(each.Status==="re-assigned"){
        this.handleReAssign();
        showAssignTo=true;
      }

      this.setState({
        permissionAdmin: getPermissionApplication(this.props.applicationList,"ADMIN_DASHBOARD"),
        status: each.Status,
        showAssignTo,
        assignedTo:each.Assigned_to
      });
    }
  }
  componentDidUpdate(prevProps){   
    if(prevProps.langCode!==this.props.langCode){
      this.getMomentTime();
    } 
  }
  
}

const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    propertyReducer: state.propertyReducer,
    usersReducer: state.usersReducer
  };
};
export default connect(mapStateToProps)(ItemDefect);
