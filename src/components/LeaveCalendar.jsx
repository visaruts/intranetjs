import React, { Component } from "react";
import { Prompt } from "react-router";
import { connect } from "react-redux";
import { lang } from "../Lang.jsx";

export default class LeaveCarlendar extends Component {
  state = {
    month:["January","February","March","April","May","June","July","August","September","October","November","December"],
    day:[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
    leave_color:{
      "leave": "#0C0",
      "sick": "#e01f5f"
    },
  }
  render() {
    const {month, day, leave_color}=this.state;
    const {thisYear, calendar, selectedCalendar}=this.props;
    return (
      <div>
        <table border="1" style={{fontSize:"10px", borderColor: "#000000"}}>
          <tbody>
            <tr>
              <td style={{padding:"2px"}}>{thisYear}</td>
              {
                day.map((d,key)=>{
                  return <td key={key} className="has-text-centered" style={{padding:"2px", width:"20px"}}>{d}</td>;
                })
              }
            </tr>
            {
              month.map((month_name, month_key)=>{
                return <tr key={month_key}>
                  <td>{month_name}</td>
                  {
                    day.map((day_number,day_key)=>{
                      const ymd=thisYear.toString() + "-" + ("0" + (month_key+1).toString()).slice(-2) + "-" + ("0" + day_number.toString()).slice(-2);
                      let backgroundColor="#ffffff";
                      let holiday="";
                      let text_half="";
                      let calendar_temp=null;
                      if(selectedCalendar[ymd] && calendar[ymd].IsHoliday_Office===0)
                      {
                        calendar_temp=selectedCalendar[ymd];   
                      }
                      else if(calendar[ymd])
                      {
                        calendar_temp=calendar[ymd];                                
                      }
                      //console.log(selectedCalendar[ymd]);

                      if(calendar_temp!==null){
                        if(calendar_temp.IsHoliday_Office===1)
                        {
                          if(calendar_temp.Holiday!==null)
                          {
                            backgroundColor="#b5b0b0";
                            holiday=calendar_temp.Holiday;
                          }
                          else
                          {
                            backgroundColor="#837c7c";
                          }
                          
                        }
                        else
                        {
                          if(calendar_temp.leave_fullday!==null){
                            backgroundColor=leave_color[calendar_temp.leave_type];
                            if(calendar_temp.leave_fullday!=="full"){
                              text_half="0.5";
                            }
                            if(selectedCalendar[ymd])
                            {
                              if(selectedCalendar[ymd].leave_fullday!=="null"){
                                text_half="";
                              }
                            }
                          }
                        }
                        
                      }
                      return <td 
                      key={day_key} 
                      className="has-text-centered" 
                      style={{padding:"2px", backgroundColor, color:"#ffffff"}}
                      title={holiday}
                      >
                        {text_half}
                      </td>;
                    })
                  }
                </tr>;
              })
            }
          </tbody>
        </table>        
      </div>
    );
  }

  componentDidMount() {
    
  }
}
