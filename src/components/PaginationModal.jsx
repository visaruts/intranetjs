import React, { Component } from "react";
import { withRouter } from "react-router";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { lang } from "../Lang.jsx";
import { CONFIG } from "../Config_API.jsx";
//import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class PaginationModal extends Component {
  state = {};


  render() {
    //console.log(this.props);
    const { recordsTotal, page, link, pageSize } = this.props;
    // Page _GET
    //const page = parseInt(this.props.location.search.replace(/\?p\=/, "")) || 1;
    const totalCount = parseInt(recordsTotal);
    const totalPage = Math.ceil(totalCount / pageSize); //Round Up
    //console.log("recordsTotal:"+CONFIG.page_length);
    //new Array(totalPage); // cannot use to loop
    const pages = new Array(totalPage).fill().map((each, idx) => idx + 1);

    var current = parseInt(page),
      last = pages.length,
      delta = 2,
      left = current - delta,
      right = current + delta + 1,
      range = [],
      rangeWithDots = [],
      l;
    for (let i = 1; i <= last; i++) {
      if (i === 1 || i === last || i >= left && i < right) {
        range.push(i);
      }
    }
    for (let i of range) {
      if (l) {
        if (i - l === 2) {
          rangeWithDots.push(l + 1);
        } else if (i - l !== 1) {
          rangeWithDots.push('...');
        }
      }
      rangeWithDots.push(i);
      l = i;
    }


    return (
      <div className="columns" style={{width:'990px'}}>
        <div className="column is-5">
          <nav className="pagination" role="navigation" aria-label="pagination">
            <ul className="pagination-list">

              {
                (parseInt(page) > 1) ?
                  <Link to={`/${link}?p=${parseInt(page) - 1}`}>
                    <li className="pagination-link">
                      {lang("datatable").paginate.previous}
                    </li>
                  </Link>
                  :
                  <Link to={`/${link}?p=${parseInt(page)}`} className="disabled-link" disabled>
                    <li className="pagination-link">
                      {lang("datatable").paginate.previous}
                    </li>
                  </Link>
              }
              {
                (parseInt(page) < pages.length) ?
                  <Link to={`/${link}?p=${parseInt(page) + 1}`}>
                    <li className="pagination-link">
                      {lang("datatable").paginate.next}
                    </li>
                  </Link>
                  :
                  <Link to={`/${link}?p=${parseInt(page)}`} className="disabled-link" disabled>
                    <li className="pagination-link">
                      {lang("datatable").paginate.next}
                    </li>
                  </Link>
              }

              {lang("Page") + ": "}
              <div className="select" style={{ paddingLeft: "0.5em" }}>
                <select value={page} onChange={(e) => {
                  this.props.history.push(`/${link}?p=${e.target.value}`);
                }}>
                  {
                    pages.map(each => {
                      return <option key={each} value={each}>{each}</option>
                    })
                  }
                </select>
              </div>
            </ul>
          </nav>
        </div>
        <div className="column is-7">
          <nav className="pagination is-right" role="navigation" aria-label="pagination">
            <div >
              <span>{lang("Total Record", [recordsTotal])}</span>{' '}
            </div>
            <ul className="pagination-list">
              {
                rangeWithDots.map((each, key) => {
                  if (each === "...") return (
                    <li key={key}><span className="pagination-ellipsis">&hellip;</span></li>
                  )
                  return (
                    <Link key={key} to={`/${link}?p=${parseInt(each)}`}>
                      <li className={(parseInt(each) === parseInt(page)) ? "pagination-link is-current" : "pagination-link"} key={key}>
                        {each}
                      </li>
                    </Link>
                  );
                })}
            </ul>
          </nav>
        </div>
      </div>
    );
  }


}

const mapStateToProps = state => {
  return {};
};
//export default connect(mapStateToProps)(Pagination);
// withRouter for use this.props.history.push in this component
export default withRouter(connect(mapStateToProps)(PaginationModal));
