import React from "react";
import "./assets/css/bootstrap.min.css";
import "bulma/css/bulma.min.css";
import 'bulma-extensions/dist/css/bulma-extensions.min.css'
import 'bulma-extensions/dist/js/bulma-extensions.min.js'

// import "react-lightbox-component/build/css/index.css";
import { connect } from "react-redux";
import "./assets/fonts/fonts/font.css";
import "./assets/css/fontLemonSans.scss";
// import "./assets/css/fontKanit.scss";

import Routes from "./Routes.jsx";
import Navbar from "./Navbar.jsx";
import Footer from "./Footer.jsx";
import { ToastContainer } from "react-toastify";
import { actionAuthFromStorage } from "./actions/login";
import qs from "query-string";
//import Access_token from "./Access_token.jsx";

//import firebase from "firebase";
//import "firebase/firestore";

import { library } from '@fortawesome/fontawesome-svg-core';
import { faEnvelope, faKey, faAngleUp, faFilter, faDownload, faFileExcel, 
  faFilePdf, faFolder, faFolderOpen, faArchive, faInbox, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
library.add(faEnvelope, faKey, faAngleUp, faFilter, faDownload, faFileExcel, 
  faFilePdf, faFolder, faFolderOpen, faArchive, faInbox, faTrashAlt);
export default class App extends React.Component {
  state={
    stylesheetPath :"fontLemonSans"
    //stylesheetPath :"fontKanit"
  }
  render() {
    const font =  localStorage.getItem("font") !== null ?localStorage.getItem("font") :this.state.stylesheetPath ;
    // const {hiddenFooter,hiddenNavbar} = this.props.configReducer ;
    return (
      <div className={font }>
        <ToastContainer />
        <Navbar />
        <section className="is-light">
        <Routes />
        </section>
        <Footer />
      </div>
    );
  }
  async componentDidMount() {
    // console.log("componentDidMount...");
    const queryString = qs.parse(window.location.search);
    const font = queryString.font;

    if(typeof font !== "undefined"){
      localStorage.setItem("font",font);
      this.setState({stylesheetPath:font})
    }
  }
}