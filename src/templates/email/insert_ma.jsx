import React from "react";
import ReactDOMServer from 'react-dom/server';
import { lang } from "../../Lang.jsx";
import { base_url } from "../../Config_API.jsx";
const view = (data={},langCode="en") => {
    const {name, createDate, hotel, defects, location, urgent, contactInfo} = data;
    const css_th={
        backgroundColor: "#ccc",
        color: "#222",
        fontWeight: "normal",
        paddingRight: "5px",
        paddingLeft: "5px"
    };
    const css_td={backgroundColor: "#f0f0f0", color: "#222"};
    const css_td_header={backgroundColor: "#b01116", color: "#fff"};
    const css_a={fontSize: "16px"};
    
    const html = (<html>
    <head>
    <meta httpEquiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    </head>
    <body>
    <table width="100%" border="0" cellSpacing="2" cellPadding="4" 
        style={{
            fontFamily: "Helvetica Neue, Helvetica, Arial, sans-serif",
            fontSize: "12px",
            lineHeight: "18px"}}>
        <tr>
            <td width="120" style={css_td_header}>{lang("Process Name")}:</td>
            <td style={css_td}>{lang("New Report A Problem")}</td>
        </tr>
        <tr>
            <td style={css_td_header}>{lang("Requested By")}:</td>
            <td style={css_td}>{name}</td>
        </tr>
        <tr>
            <td style={css_td_header}>{lang("Requested Date")}:</td>
            <td style={css_td}>{createDate}</td>
        </tr>
        <tr>
            <td style={css_td_header}>{lang("Hotel")}:</td>
            <td style={css_td}>{hotel}</td>
        </tr>
        <tr>
            <td style={css_td_header}>{lang("Defects")}:</td>
            <td style={css_td}>{defects}</td>
        </tr>
        <tr>
            <td style={css_td_header}>{lang("Location")}:</td>
            <td style={css_td}>{location}</td>
        </tr>
        <tr>
            <td style={css_td_header}>{lang("Urgent")}:</td>
            <td style={css_td}>{(urgent)?lang("Yes"):lang("No")}</td>
        </tr>
        <tr>
            <td style={css_td_header}>{lang("Contact Info")}:</td>
            <td style={css_td}>{contactInfo}</td>
        </tr>
    </table>
    {lang("Dashboard URL")} : {base_url + "/dashboard"+(langCode==="en"?"":"?lang="+langCode)}
    </body>
    </html>);
    
    return ReactDOMServer.renderToString(html);
};

export { view };
