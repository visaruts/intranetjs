import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import qs from "query-string";
import { Base64 } from "js-base64";
import { actionAuthFromStorage, actionGetAccessToken } from "./actions/login";

import Login from "./pages/Login.jsx";
import Error404 from "./pages/Error404.jsx";
import Error500 from "./pages/Error500.jsx";
import ErrorPermission from "./pages/ErrorPermission.jsx";

import Home from "./pages/Home.jsx";
import Leave from "./pages/Leave.jsx";
import Signature from "./pages/signature/IndexSignature.jsx";
import Training from "./pages/training/ListView";
import HR from "./pages/hr/IndexHR.jsx";
import Announcement from "./pages/announcement/IndexAnnouncement.jsx";
import Library from "./pages/library/IndexLibrary.jsx";
import AdminLibrary from "./pages/admin/LibraryAdmin.jsx";
import Contact from "./pages/contact/IndexContact.jsx";
import Administrator from "./pages/Administrator.jsx";


const ProtectRoute = connect(state => {
  return {
    ...state.loginReducer
  };
})(props => {
  let { request_token = "" } = props;
  if (request_token === "") {
    request_token = localStorage.getItem("request_token") || "";
  }
  //console.log(props);
  const login_redirect =
    window.location.pathname === "/"
      ? "/login"
      : "/login?redirect=" +
        Base64.encode(window.location.pathname + window.location.search);
  //console.log(request_token);
  return (
    <div>
      {request_token !== "" ? (
        <Route {...props} />
      ) : (
        <Redirect to={login_redirect} />
      )}
    </div>
  );
});

export default class Routes extends Component {
  render() {
    return (
      <div>
        <Switch>
          <ProtectRoute exact path="/" component={Home} />
          <Route path="/login" component={Login} />
          {/************* My Acount ************/}
          <ProtectRoute path="/leave" component={Leave} />
          <ProtectRoute path="/signature" component={Signature} />
          {/************* DEPARTMENT  ************/}
          <ProtectRoute path="/announcement" component={Announcement} />
          <ProtectRoute path="/hr" component={HR} />
          <ProtectRoute path="/training" component={Training} />
          {/************* LIBRARY  ************/}
          <ProtectRoute path="/library" component={Library} />
          <ProtectRoute path="/admin/library" component={AdminLibrary} />
          {/************* CONTACT  ************/}
          <ProtectRoute path="/contact" component={Contact} />          
          {/*Coming soon page */}
          <Route path="/connection_error" component={Error500} />
          <Route path="/error_permission" component={ErrorPermission} />
          <Route path="*" component={Error404} />
        </Switch>
      </div>
    );
  }
}
