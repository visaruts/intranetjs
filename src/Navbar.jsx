import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
//import "./assets/css/main.scss";
import "./assets/js/main";
import { actionChangeLang, actionLoadLang } from "./actions/lang";
import { actionChangeCountry, actionLoadCountry } from "./actions/country";
import { actionGetEntityByUserCode } from "./actions/entity";
import { actionGetPropertyByUserCode } from "./actions/property";
import { actionGetAccessToken } from "./actions/login";
import { actionGetApplication } from "./actions/users";
import { actionHiddenNavbar, actionHiddenFooter } from "./actions/config";
import qs from "query-string";
import { public_url, uniqid, ENV } from "./Config_API.jsx";
import "react-toastify/dist/ReactToastify.css";
import { lang } from "./Lang.jsx";
/** fontawesome */
import { faSignOutAlt } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { actionGetJobDept, actionGetJobTitle } from "./actions/master";

class Navbar extends Component {
  state = {
    userinfo: {},
    langCode: "en",
    name: "",
    active: false,
    module: "home",
    access_token: "",
    countryCode: "",
    permission_application: [],
    entity: []
  };

  render() {
    this.navSubMenuHide("navbar-dropdown-level2-admin");
    this.navSubMenuHide("navbar-dropdown-level2-approval");
    this.navSubMenuHide("navbar-dropdown-level2-timerecording");
    const { active, permission_application, countryCode,module, userinfo } = this.state;
    //console.log(permission_application);
    const request_token = localStorage.getItem("request_token") || "";
    const navbarBurgerClassName = `navbar-burger burger${
      active ? " is-active" : ""
      } `;
    const navbarClassName = `navbar-menu${active ? " is-active" : ""} `;

    let lang_en = "?lang=en";
    let lang_ja = "?lang=ja";
    let lang_th = "?lang=th";
    let lang_id = "?lang=id";
    let country_cor = "?country=cor";
    let country_idn = "?country=idn";
    let country_jpn = "?country=jpn";
    let country_phi = "?country=phi";
    let country_tha = "?country=tha";
    const this_country = localStorage.getItem("countryCode") ;
   // console.log(this.state.module);
    // console.log(window.location.pathname);
    // console.log(window.location.pathname);
    const {hiddenNavbar} = this.props.configReducer ;
    return (
      <div>
        {!hiddenNavbar?
        <div>
        <nav
          className="navbar is-dark is-fixed-top"
          role="navigation"
          aria-label="main navigation"
        >
          <div className="navbar-brand">
            {/** check login */
              (request_token && window.location.pathname !== "/login") ? (
                <Link
                  key={uniqid()}
                  to="#"
                  role="button"
                  className={navbarBurgerClassName}
                  aria-label="menu"
                  aria-expanded="false"
                  data-target="navbarBasic"
                  onClick={e => {
                    this.setState({
                      active: !active
                    });
                    this.loadNavMobile();
                    let level1 = document.getElementsByClassName("navbar-dropdown");
                    for (let sublevel of level1) {
                      sublevel.classList.add("hidden")
                    }
                    let level2_admin = document.getElementsByClassName("navbar-dropdown-level2-admin");
                    for (let sublevel of level2_admin) {
                      sublevel.classList.add("hidden")
                    }
                    let level2_approval = document.getElementsByClassName("navbar-dropdown-level2-approval");
                    for (let sublevel of level2_approval) {
                      sublevel.classList.add("hidden")
                    }
                    let level2_timerecording = document.getElementsByClassName("navbar-dropdown-level2-timerecording");
                    for (let sublevel of level2_timerecording) {
                      sublevel.classList.add("hidden")
                    }
                  }
                  }
                >
                  <span aria-hidden="true" />
                  <span aria-hidden="true" />
                  <span aria-hidden="true" />
                </Link>
              ) : (
                  ""
                )}
            <Link className="navbar-item" to="/" key={uniqid()}>
              <img
                src={public_url + "/images/logo.png"}
                alt="Logo"
                style={{ marginTop: "7px" }}
              />
            </Link>
            {ENV !== "PROD" ? (
              <span className="text-uat"> ( {ENV} )</span>
            ) : (
                ""
              )}

          </div>
          {/** check login */
            request_token && window.location.pathname !== "/login" ? (
              <div id="navbarBasic" className={navbarClassName}>
                <div className="navbar-start navbar-item has-dropdown dropdown is-hoverable"
                  onMouseOver={e => {
                    if (window.innerWidth >= 1024) {//check desktop// 
                      //this.navSubMenuHide("navbar-dropdown-level2");
                      //this.navSubMenu("navbar-dropdown");
                    }
                  }}
                  onMouseLeave={e => {
                    if (window.innerWidth >= 1024) {//check desktop// 
                      this.navSubMenuHide("navbar-dropdown-level2-admin");
                      this.navSubMenuHide("navbar-dropdown-level2-approval");
                      this.navSubMenuHide("navbar-dropdown-level2-timerecording");
                    }
                  }}
                >
                  <Link
                    id="my-account"
                    className={"navbar-item navbar-link"}
                    to="#"
                    onClick={e => {
                      this.navSubMenu("navbar-dropdown-my-account");
                    }}
                    onMouseOver={e => {
                      if (window.innerWidth >= 1024) {//check desktop// 
                        this.navSubMenuShow("navbar-dropdown");
                      }
                    }}
                  >
                    {lang("menu_my_account").toUpperCase()}
                  </Link>
                  <div className="navbar-dropdown navbar-dropdown-my-account">
                    <Link
                      id={uniqid()}
                      className={"navbar-item"}
                      to="/leave"
                      onClick={e => {
                        this.setState({ active: false, module: "leave" });
                        this.navSubMenuHide('navbar-dropdown');
                      }}
                    >
                      {lang("menu_my_leave")}
                    </Link>
                    <Link
                      id={uniqid()}
                      className={"navbar-item"}
                      to="#"
                      onClick={e => {
                        this.setState({ active: false, module: "leave" });
                        this.navSubMenuHide('navbar-dropdown');
                      }}
                    >
                      {lang("menu_leave_my_team")}
                    </Link>
                    <Link
                      id={uniqid()}
                      className={"navbar-item"}
                      to="#"
                      onClick={e => {
                        this.setState({ active: false, module: "leave" });
                        this.navSubMenuHide('navbar-dropdown');
                      }}
                    >
                      {lang("menu_corporate_calendar")}
                    </Link>
                    <a className={"navbar-item"} target="_bank" href={'/signature/'+userinfo.username}>{lang("menu_email_signature")}</a>
                    {/* <Link
                      id={uniqid()}
                      className={"navbar-item"}
                      to={"/signature/"+userinfo.username}
                      onClick={e => {
                        this.setState({ active: false, module: "leave" });
                        this.navSubMenuHide('navbar-dropdown');
                      }}
                    >
                      {lang("menu_email_signature")}
                    </Link> */}
                    <Link
                      id={uniqid()}
                      className={"navbar-item"}
                      to="#"
                      onClick={e => {
                        this.setState({ active: false, module: "leave" });
                        this.navSubMenuHide('navbar-dropdown');
                      }}
                    >
                      {lang("menu_my_romm")}
                    </Link>
                    <Link
                      id={uniqid()}
                      className={"navbar-item"}
                      to="#"
                      onClick={e => {
                        this.setState({ active: false, module: "leave" });
                        this.navSubMenuHide('navbar-dropdown');
                      }}
                    >
                      {lang("menu_travel_schedule")}
                    </Link>
                    <div
                      id={uniqid()}
                      onMouseOver={e => {
                        if (window.innerWidth >= 1024) {//check desktop// 
                          this.navSubMenuShow("navbar-dropdown-level2-approval");
                        }
                      }}
                      onMouseLeave={e => {
                        if (window.innerWidth >= 1024) {//check desktop// 
                          this.navSubMenuHide("navbar-dropdown-level2-approval");
                          // this.navSubMenuHide('navbar-dropdown');
                        }
                      }}
                      className="navbar-item nav-link-right navbar-link-mobile"
                      to="/"
                      onClick={e => { this.navSubMenu("navbar-dropdown-level2-approval"); }}
                    >
                      <div className="title-sub-level2">{"Approval Status"}</div>
                      <div className="navbar-dropdown-level2-approval">
                        <Link
                          className="navbar-item" to="/">{lang("menu_leave_form")}
                        </Link>
                        <Link
                          onClick={e => {
                            this.setState({ active: false, module: "leave" });
                          }}
                          className="navbar-item" to="/">{lang("menu_room")} </Link>
                      </div>
                    </div>
                    <div
                      id={uniqid()}
                      onMouseOver={e => {
                        if (window.innerWidth >= 1024) {//check desktop// 
                          this.navSubMenuShow("navbar-dropdown-level2-admin");
                        }
                      }}
                      onMouseLeave={e => {
                        if (window.innerWidth >= 1024) {//check desktop// 
                          this.navSubMenuHide("navbar-dropdown-level2-admin");
                          // this.navSubMenuHide('navbar-dropdown');
                        }
                      }}
                      className="navbar-item nav-link-right level1-admin navbar-link-mobile"
                      onClick={e => { this.navSubMenu("navbar-dropdown-level2-admin"); }}
                    >
                     <span className="title-sub-level2"> {lang("menu_administrator")}</span> 
                      <div className="navbar-dropdown-level2-admin">
                        <Link id={uniqid()} className="navbar-item" to="/">{lang("menu_leave_form")}  </Link>
                        <Link id={uniqid()} className="navbar-item" to="/">{lang("menu_corporate_calendar")} </Link>
                        <Link id={uniqid()} className="navbar-item" to="/">{lang("menu_room")} </Link>
                        <Link id={uniqid()} className="navbar-item" to="/">{lang("menu_travel_schedule")} </Link>
                      </div>
                    </div>
                    <div
                      id={uniqid()}
                      onMouseOver={e => {
                        if (window.innerWidth >= 1024) {//check desktop// 
                          this.navSubMenuShow("navbar-dropdown-level2-timerecording");
                        }
                      }}
                      onMouseLeave={e => {
                        if (window.innerWidth >= 1024) {//check desktop// 
                          this.navSubMenuHide("navbar-dropdown-level2-timerecording");
                          // this.navSubMenuHide('navbar-dropdown');
                        }
                      }}
                      className="navbar-item nav-link-right navbar-link-mobile"
                      to="/"
                      onClick={e => { this.navSubMenu("navbar-dropdown-level2-timerecording"); }}
                    >
                      <span className="title-sub-level2">{lang("menu_time_recording")}</span>
                      <div className="navbar-dropdown-level2-timerecording">
                        <Link
                          onClick={e => {
                            this.setState({ active: false, module: "leave" });
                          }}
                          className="navbar-item" to="/">{lang("menu_manage_shift_types")}  </Link>
                        <Link
                          onClick={e => {
                            this.setState({ active: false, module: "leave" });
                          }}
                          className="navbar-item" to="/">{lang("menu_duty_roster")} </Link>
                        <Link
                          onClick={e => {
                            this.setState({ active: false, module: "leave" });
                          }}
                          className="navbar-item" to="/">{lang("menu_dtr_entry")} </Link>
                        <Link
                          onClick={e => {
                            this.setState({ active: false, module: "leave" });
                          }}
                          className="navbar-item" to="/">{lang("menu_attendance_record")} </Link>
                        <Link
                          onClick={e => {
                            this.setState({ active: false, module: "leave" });
                          }}
                          className="navbar-item" to="/">{lang("menu_payroll")} </Link>
                        <Link
                          onClick={e => {
                            this.setState({ active: false, module: "leave" });
                          }}
                          className="navbar-item" to="/">{lang("menu_pending_approval")} </Link>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="navbar-end">
                  <div className="navbar-item  is-hidden-mobile">
                    <div className="buttons" id="lang-desktop">
                      {/** check login */
                        request_token && countryCode.toUpperCase() === "JPN" ? (
                          <div>
                            <Link to={lang_en} onClick={e => this.handdleChangeLang("en")}>
                              <img className="flag" alt="en" src={public_url + "/images/flag-en.png"} />
                            </Link>
                            &nbsp;
                            <Link to={lang_ja} onClick={e => this.handdleChangeLang("ja")}>
                              <img className="flag" alt="jp" src={public_url + "/images/flag-jp.png"} />
                            </Link>
                          </div>

                        ) : ""
                      }
                      {
                        (this.state.userinfo.country === "JPN" || this.state.userinfo.initial_usergroup === "IT-CORP") ?
                          "" : ""
                      }
                      {/* {
                  (this.state.userinfo.country==="IDN" || this.state.userinfo.initial_usergroup==="IT-CORP")?
                  <span>&nbsp;
                  <Link to={lang_id} onClick={e => this.handdleChangeLang("id")}>
                    <img className="flag" src={base_url+"/images/flag-id.png"} />
                  </Link></span>:""
                }
                {
                  (this.state.userinfo.country==="THA" || this.state.userinfo.initial_usergroup==="IT-CORP")?
                  <span>&nbsp;
                  <Link to={lang_th} onClick={e => this.handdleChangeLang("th")}>
                    <img className="flag" src={base_url+"/images/flag-th.png"} />
                  </Link></span>:""
                }  */}
                    </div>
                  </div>
                  <Link
                    className={this.state.module === "home" ? "navbar-item is-active" : "navbar-item"}
                    to="/"
                    onClick={e => {
                      this.setState({ active: false, module: "home" });
                    }}
                  >
                    {lang("menu_home").toUpperCase()}
                  </Link>
                  <div className="navbar-item has-dropdown dropdown is-hoverable">
                    <Link
                      className={this.state.module === "department"||this.state.module === "hr" ? "navbar-item  navbar-link is-active" : "navbar-item  navbar-link"}
                      to="#"
                      onClick={e => {
                        this.navSubMenu("navbar-dropdown-department");
                      }}
                      onMouseOver={e => {
                        if (window.innerWidth >= 1024) {//check desktop// 
                          this.navSubMenuShow("navbar-dropdown-department");
                        }
                      }}
                    >
                      {lang("menu_department").toUpperCase()}
                    </Link>
                    <div className="navbar-dropdown dropdown-sub navbar-dropdown-department">
                      <Link
                        className={" navbar-item"} to="/hr"
                        onClick={e => {
                          this.setState({ active: false, module: "department" });
                          this.navSubMenuHide('navbar-dropdown');
                        }}
                      >{lang("menu_hr")}</Link>
                      <Link
                        className={" navbar-item"} to="/training"
                        onClick={e => {
                          this.setState({ active: false, module: "training" });
                          this.navSubMenuHide('navbar-dropdown');
                        }}
                      >{lang("menu_training")}</Link>
                      <Link className={" navbar-item"} to="/finance" >{lang("menu_finance")}</Link>
                      <Link className={" navbar-item"} to="/it" >{lang("menu_it")}</Link>
                      <Link className={" navbar-item"} to="/operation" >{lang("menu_operations")}</Link>
                      <Link className={" navbar-item"} to="/reporting_system" >{lang("menu_reporting_system")}</Link>
                      <Link className={" navbar-item"} to="/marketing" >{lang("menu_marketing_communication")}</Link>
                    </div>
                  </div>
                  <div className="navbar-item has-dropdown dropdown is-hoverable">
                    <Link
                      className={this.state.module === "library"? "navbar-item  navbar-link is-active" : "navbar-item  navbar-link"}
                      to="#"
                      onClick={e => {
                        this.navSubMenu("navbar-dropdown-library");
                      }}
                      onMouseOver={e => {
                        if (window.innerWidth >= 1024) {//check desktop// 
                          this.navSubMenuShow("navbar-dropdown-library");
                        }
                      }}
                    >
                      {lang("menu_library").toUpperCase()}
                    </Link>
                    <div className="navbar-dropdown dropdown-sub navbar-dropdown-library">
                      <Link className={"navbar-item menu_unlocked"} to="/library/company_manuals"
                       onClick={e => {
                        this.setState({ active: false, module: "library" });
                        this.navSubMenuHide('navbar-dropdown');
                      }}
                      >{lang("menu_company_manuals")}</Link>
                      <Link className={"navbar-item menu_unlocked"} to="/library/training_courses" 
                        onClick={e => {
                          this.setState({ active: false, module: "library" });
                          this.navSubMenuHide('navbar-dropdown');
                        }}
                      >{lang("menu_training_courses")}</Link>
                      <Link className={"navbar-item menu_unlocked"} to="/library/insurance"
                        onClick={e => {
                          this.setState({ active: false, module: "library" });
                          this.navSubMenuHide('navbar-dropdown');
                        }}
                      >{lang("menu_insurance")}</Link>
                      <Link className={"navbar-item menu_unlocked"} to="/library/admin" 
                        onClick={e => {
                          this.setState({ active: false, module: "library" });
                          this.navSubMenuHide('navbar-dropdown');
                        }}
                      >{lang("menu_administration")}</Link>
                      <Link className={"navbar-item"} to="/library/red_letter"
                        onClick={e => {
                          this.setState({ active: false, module: "library" });
                          this.navSubMenuHide('navbar-dropdown');
                        }}
                      >
                        <img
                          style={{
                            transform: "scale(1.5)",
                            paddingLeft: "1rem"
                          }}
                          src="https://intranet.redplanethotels.com/images/redletter_logo.png" alt="" />
                      </Link>
                      <Link className={"navbar-item"} to="/library/public_library"
                        onClick={e => {
                          this.setState({ active: false, module: "library" });
                          this.navSubMenuHide('navbar-dropdown'); 
                        }}
                      >{lang("menu_public_library")}</Link>
                    </div>
                  </div>
                  <div className="navbar-item has-dropdown dropdown is-hoverable">
                    <Link
                      className={"navbar-item navbar-link"}
                      to="#"
                      onClick={e => {
                        this.navSubMenu("navbar-dropdown-contact");
                      }}
                      onMouseOver={e => {
                        if (window.innerWidth >= 1024) {//check desktop// 
                          this.navSubMenuShow("navbar-dropdown-contact");
                        }
                      }}
                    >
                      {lang("menu_contact").toUpperCase()}
                    </Link>
                    <div className="navbar-dropdown dropdown-sub navbar-dropdown-contact">
                      <Link className={"navbar-item item-menu"} to="/contact/hotel_contact" 
                        onClick={e => {
                          this.setState({ active: false, module: "library" });
                          this.navSubMenuHide('navbar-dropdown');
                        }}
                      >{lang("menu_hotel_contact")}</Link>
                      <Link className={"navbar-item"} to="/contact/employee_contact"
                        onClick={e => {
                          this.setState({ active: false, module: "library" });
                          this.navSubMenuHide('navbar-dropdown');
                        }}
                      >{lang("menu_employee_contact")}</Link>
                      <Link className={"navbar-item"} to="/contact/group_emails" 
                        onClick={e => {
                          this.setState({ active: false, module: "library" });
                          this.navSubMenuHide('navbar-dropdown');
                        }}
                      >{lang("menu_group_emails")}</Link>
                      <Link className={"navbar-item  menu_unlocked"} to="/contact/contact_admin" 
                        onClick={e => {
                          this.setState({ active: false, module: "library" });
                          this.navSubMenuHide('navbar-dropdown');
                        }}
                      >{lang("menu_administrator")}</Link>
                      <Link className={"navbar-item  menu_unlocked"} to="/contact/manage_hotel_contact" 
                        onClick={e => {
                          this.setState({ active: false, module: "library" });
                          this.navSubMenuHide('navbar-dropdown');
                        }}
                      >{lang("menu_manage_hotel_contact")}</Link>
                    </div>
                  </div>
                  <Link
                    className={"navbar-item"}
                    to="/"
                    onClick={e => {
                      this.setState({ active: false, module: "training" });
                    }}
                  >
                    {lang("menu_webapp").toUpperCase()}
                  </Link>
                  <div className="navbar-item has-dropdown dropdown is-hoverable">
                    <Link
                      className={"navbar-item navbar-link"}
                      to="/"
                      onClick={e => {
                        this.navSubMenu("navbar-dropdown-administrator");
                      }}
                      onMouseOver={e => {
                        if (window.innerWidth >= 1024) {//check desktop// 
                          this.navSubMenuShow("navbar-dropdown-administrator");
                        }
                      }}
                    >
                      {lang("menu_administrator").toUpperCase()}
                    </Link>
                    <div className="navbar-dropdown dropdown-sub navbar-dropdown-administrator">
                      <Link className={" navbar-item"} to="/" >{"Manage Application"}</Link>
                      <Link className={" navbar-item"} to="/" >{"Manage Tab Application"}</Link>
                    </div>
                  </div>
                  <div className="navbar-item has-dropdown dropdown is-hoverable">
                    <Link
                      id="country"
                      className={"navbar-item navbar-link"}
                      to="/"
                      onClick={e => {
                        this.navSubMenu("navbar-dropdown-country");
                      }}
                      onMouseOver={e => {
                        if (window.innerWidth >= 1024) {//check desktop// 
                          this.navSubMenuShow("navbar-dropdown-country");
                        }
                      }}
                    >
                      {lang("country").toUpperCase()}
                    </Link>
                   
                    <div className="navbar-dropdown dropdown-sub navbar-dropdown-country">
                      <Link onClick={e => this.handdleChangeCountry("cor")} className={ this_country==="cor"?"is-active navbar-item":"navbar-item"} to={country_cor} >{lang("COR")}</Link>
                      <Link onClick={e => this.handdleChangeCountry("jpn")} className={ this_country==="jpn"?"is-active navbar-item":"navbar-item"} to={country_jpn} >{lang("JPN")}</Link>
                      <Link onClick={e => this.handdleChangeCountry("idn")} className={ this_country==="idn"?"is-active navbar-item":"navbar-item"} to={country_idn} >{lang("IDN")}</Link>
                      <Link onClick={e => this.handdleChangeCountry("phi")} className={ this_country==="phi"?"is-active navbar-item":"navbar-item"} to={country_phi} >{lang("PHI")}</Link>
                      <Link onClick={e => this.handdleChangeCountry("tha")} className={ this_country==="tha"?"is-active navbar-item":"navbar-item"} to={country_tha} >{lang("THA")}</Link>
                    </div>
                  </div>
                  <Link
                    className={"navbar-item logout"}
                    to="/"
                    onClick={e => {
                      localStorage.clear();
                      window.location.href = "/";
                    }}
                  >
                    <FontAwesomeIcon className="is-hidden-desktop" icon={faSignOutAlt} />  {lang("Logout").toUpperCase()}
                  </Link>

                </div>
              </div>
            ) : (
                ""
              )}
        </nav>
                <div className="container is-fullhd has-text-right">
                <br /><br />
              </div>
      </div>:
      <div></div>
      }
      </div>
     
    );
  }
  async loadNavMobile() {
    if (window.innerWidth < 1024) {//check mobile show link// 
      // let account = document.getElementById("my-account");
      // console.log(account);
    }
  }
  async navSubMenu(ClassName) {
    let level = document.getElementsByClassName(ClassName);
    for (let sublevel of level) {
      sublevel.className.indexOf("hidden") >= 0 ? sublevel.classList.remove("hidden") : sublevel.classList.add("hidden");
    }
  }
  async navSubMenuHide(ClassName) {
    let level = document.getElementsByClassName(ClassName);
    for (let sublevel of level) {
      sublevel.classList.add("hidden");
    }
  }
  async navSubMenuShow(ClassName) {
    let level = document.getElementsByClassName(ClassName);
    for (let sublevel of level) {
      sublevel.classList.remove("hidden")
    }
  }

  async getName(userinfo, langCode) {
    let name = "";
    if (userinfo.country === "THA" && langCode === "th") {
      name = userinfo.firstname_local + " " + userinfo.lastname_local;
    } else if (userinfo.country === "JPN" && langCode === "ja") {
      name = userinfo.firstname_local + " " + userinfo.lastname_local;
    } else {
      name = userinfo.firstname + " " + userinfo.lastname;
    }
    await this.setState({ name });
  }
  async componentWillMount() {
    //console.log("componentWillMount");
    await this.props.dispatch(actionLoadLang());
    await this.props.dispatch(actionLoadCountry());

  }

  async componentDidMount() {
    // console.log("componentDidMount");
    const request_token = localStorage.getItem("request_token") || "";

    if (request_token !== "") {
      let { access_token = "" } = this.state;
      let langCode = localStorage.getItem("langCode") || "en";

      const queryString = qs.parse(window.location.search);
      if (queryString.lang) {
        langCode = queryString.lang;
      }
      const pathname = window.location.pathname.split('/');
      //console.log(pathname);
      if (pathname.length > 0) {
        if (pathname[1] !== "") {
          this.setState({
            module: pathname[1]
          });
        }
      }

      //console.log(langCode);
      let userinfo = {};
      if (access_token === "") {
        //console.log(request_token);
        await this.props.dispatch(actionGetAccessToken(request_token));
        access_token = this.props.loginReducer.access_token;
      }

      await this.setState({ userinfo, langCode });
      userinfo = JSON.parse(localStorage.getItem("userinfo")) || {};
      //console.log(userinfo);

      await this.props.dispatch(actionChangeLang(langCode));

      let countryCode = localStorage.getItem("countryCode") || "";
      if (countryCode == "") {
        countryCode = userinfo.office == "HQ" ? "cor" : userinfo.country;
      }
      await this.props.dispatch(actionChangeCountry(countryCode));

      await this.setState({ userinfo, langCode, countryCode });
      this.getName(userinfo, langCode);

      /****** Get Entity */
      await this.props.dispatch(actionGetEntityByUserCode(access_token));
      /****** Get Property */
      await this.props.dispatch(actionGetPropertyByUserCode(access_token));
      /****** Get JobDept */
      await this.props.dispatch(actionGetJobDept(access_token, "GET_JOBDEPT"));
      /****** Get JobTitle */
      await this.props.dispatch(actionGetJobTitle(access_token, "GET_JOBTITLE"));
      /** Get Permission */
      await this.props.dispatch(actionGetApplication(access_token));
      const { application = [] } = this.props.usersReducer;
      let permission_application = [];
      application.map(each => {
        permission_application.push(each.ModuleCode);
      });
      await this.setState({
        permission_application
      });
      //console.log(permission_application);
    }
  }
  async handdleChangeLang(langCode) {
    await this.props.dispatch(actionChangeLang(langCode));
    await this.setState({ langCode });
    this.getName(this.state.userinfo, langCode);
  }
  async handdleChangeCountry(countryCode) {
    await this.props.dispatch(actionChangeCountry(countryCode));
    await this.setState({ countryCode });
  }
}
const mapStateToProps = state => {
  return {
    landReducer: state.langReducer,
    countryReducer: state.countryReducer,
    loginReducer: state.loginReducer,
    usersReducer: state.usersReducer,
    entityReducer: state.entityReducer,
    properReducer: state.properReducer,
    masterReducer: state.masterReducer,
    configReducer: state.configReducer
  };
};
export default connect(mapStateToProps)(Navbar);
