/**
 * Require to all page
 */
import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import axios from "axios";
import qs from "query-string";
import { lang } from "../Lang";
import { Base64 } from "js-base64";
import Parser from "html-react-parser";
import "../assets/css/login.css";
import { actionAuthLogin, actionGetAccessToken } from "../actions/login";

/**
 * Option for page
 */
import Joi from "joi-browser";
import "animate.css/animate.min.css";
const schema = Joi.object().keys({
  username: Joi.string().required(),
  password: Joi.string().required()
});

class Login extends Component {
  state = {
    user: {
      username: "",
      password: ""
    },
    errors: [],
    error_auth: { code: 401, message: "", type: "" }
  };
  render() {
    //console.log(this.props);

    return (
      <div className="is-fullhd">
        <div className="hero-section">
          {/* <div className="hero-section-text" /> */}
        </div>
        <div className="notification notification-login is-hidden-mobile">
          <div className="columns ">
            <div className="column has-text-centered form_login">
              <form method="POST"
                onSubmit={e => {
                  this.handleLogin(e);
                }}>
                <div className="columns from-login">
                  <div className="column">
                    <div className="field ">
                      <p className="help is-white">
                        <span> {this.displayErrorAuthenMessage()} </span>
                      </p>
                      <p className="control has-icons-left">
                        <input
                          name="username"
                          className={this.errorClassName("username", "input")}
                          type="text"
                          placeholder={lang("Username")}
                          onChange={e => {
                            this.handleChange(e);
                          }}
                        />
                        <span className="icon is-small is-left">
                          <i className="fas fa-user" />
                        </span>
                      </p>
                      <p className="help is-white">
                        {this.displayErrorMessage("username")}
                      </p>
                    </div>
                    <div className="field ">
                      <p className="control has-icons-left">
                        <input
                          name="password"
                          className={this.errorClassName("password", "input")}
                          type="password"
                          placeholder={lang("Password")}
                          onChange={e => {
                            this.handleChange(e);
                          }}
                        />
                        <span className="icon is-small is-left">
                          <i className="fas fa-lock" />
                        </span>
                      </p>
                      <p className="help is-white">
                        {this.displayErrorMessage("password")}
                      </p>
                    </div>
                    <div className="field ">
                      <div className="control has-text-centered">
                        <button
                          id="btn-login"
                          className="button is-white"
                        >
                          {lang("login")}
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div className="notification is-hidden-desktop" style={{padding:"0 0.8rem 0.8rem 0.8rem"}}>
          <div className="columns" >
            <div className="column login_welcome">{lang("login_welcome")}</div>
            <div className="column">
              <form method="POST"
                onSubmit={e => {
                  this.handleLogin(e);
                }}>
                <div className="columns">
                  <div className="column">
                    <div className="field">
                      <p className="control has-icons-left">
                        <input
                          name="username"
                          className={this.errorClassName("username", "input")}
                          type="text"
                          placeholder={lang("Username")}
                          onChange={e => {
                            this.handleChange(e);
                          }}
                        />
                        <span className="icon is-small is-left">
                          <i className="fas fa-user" />
                        </span>
                      </p>

                      <p className="help is-danger">
                        {this.displayErrorMessage("username")}
                      </p>
                      <p className="help is-danger">
                        {this.displayErrorAuthenMessage()}
                      </p>
                    </div>
                  </div>
                  <div className="column">
                    <div className="field">
                      <p className="control has-icons-left">
                        <input
                          name="password"
                          className={this.errorClassName("password", "input")}
                          type="password"
                          placeholder={lang("Password")}
                          onChange={e => {
                            this.handleChange(e);
                          }}
                        />
                        <span className="icon is-small is-left">
                          <i className="fas fa-lock" />
                        </span>
                      </p>

                      <p className="help is-danger">
                        {this.displayErrorMessage("password")}
                      </p>
                    </div>
                  </div>
                  <div className="column">
                    <div className="field">
                      <div className="control">
                        <button
                          id="btn-login"
                          className="button is-danger is-outlined"
                        >
                          {lang("login")}
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
  displayErrorMessage(fieldName) {
    const { errors } = this.state;
    //console.log(errors);
    const error = errors.find(each => {
      //return each.path.includes(fieldName);
      return each.context.key === fieldName;
    });

    return error
      ? error.message //+ ", " + error.context.key + ", " + error.type
      : "";
  }

  errorClassName(fieldName, default_class = "input") {
    return this.displayErrorMessage(fieldName) === ""
      ? default_class + " input-login"
      : default_class + " is-danger animated shake" + " input-login";
  }

  handleChange(e) {
    const key = e.target.name;
    const value = e.target.value;
    this.save_state(key, value);
  }
  save_state(key, value) {
    const user = { ...this.state.user, [key]: value };
    /*
    const { error } = Joi.validate(user, schema, { abortEarly: false });
    const errors = error ? error.details : [];

    this.setState({
      user,
      errors
    });*/
    this.setState({
      user
    });
  }
  async handleLogin(e) {
    e.preventDefault();
    const { user } = this.state;
    Object.keys(user).map(each => {
      //console.log(each + " " + user[each]);
      this.save_state(each, user[each]);
    });

    const { error } = Joi.validate(user, schema, { abortEarly: false });
    const errors = error ? error.details : [];

    this.setState({
      user,
      errors
    });
    console.log(errors);
    if (errors.length === 0) {
      //console.log(user.username + ", " + user.password);
      await this.props
        .dispatch(actionAuthLogin(user.username, user.password))
      //.then(() => {
      console.log(this.props);
      let error_auth = {};
      if (this.props.loginReducer.code === 200) {
        console.log(this.props.loginReducer);

        await this.props.dispatch(
          actionGetAccessToken(this.props.loginReducer.request_token)
        );
        const q = qs.parse(this.props.location.search);
        //console.log(q); //{Base64.decode(q.redirect)}
        if (q.redirect) {
          const redirect = Base64.decode(q.redirect);
          //this.props.history.push(redirect);
          window.location.href = redirect;
        } else {
          //this.props.history.push("/");
          window.location.href = "/";
        }
      } else if (this.props.loginReducer.code === 403) {
        error_auth = {
          code: this.props.loginReducer.code,
          message: this.props.loginReducer.message,
          type: "InActive"
        };
        this.setState({
          error_auth
        });
      } else if (this.props.loginReducer.code === 401) {
        let type = "Unauthorized";
        const {
          ret_code,
          ret_message
        } = this.props.loginReducer.blockLogin;
        if (ret_code === 101) {
          type = "blocked";
        } else if (ret_code === 103.1) {
          type = "block2";
        } else if (ret_code === 102) {
          type = "block1";
        } else if (ret_code === 104) {
          type = "block1";
        }
        error_auth = {
          code: this.props.loginReducer.code,
          message: this.props.loginReducer.message,
          type
        };
        this.setState({
          error_auth
        });
      } else {
        error_auth = {
          code: this.props.loginReducer.code,
          message: this.props.loginReducer.message,
          type: "Error"
        };
        this.setState({
          error_auth
        });
      }
      //});
    }
  }

  displayErrorAuthenMessage() {
    return lang("login_" + this.state.error_auth.type);
  }
}

const mapStateToProps = state => {
  return { loginReducer: state.loginReducer };
};
export default connect(mapStateToProps)(Login);
