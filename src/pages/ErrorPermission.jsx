/**
 * Require to all page
 */
import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";
import qs from "query-string";
import { lang } from "../Lang.jsx";
import { Base64 } from "js-base64";
import Parser from "html-react-parser";

export default class ErrorPermission extends Component {
  render() {
    return (
      <div>
        <div className="notification" style={{backgroundColor:"#fff"}}>
          <div className="columns is-vcentered">
            <div className="column has-text-centered is-one-quarter"><img src="images/NoPermissionToAccess.jpg" /></div>
            <div className="column has-text-left has-text-danger" style={{fontSize:"24px"}}><h3>{lang("Do not have permission")}</h3></div>
          </div>
        </div>
      </div>
    );
  }
}
