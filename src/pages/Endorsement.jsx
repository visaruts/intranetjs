import React, { Component } from "react";
import { connect } from "react-redux";
import { lang } from "../Lang.jsx";
import { actionGetAccessToken } from "../actions/login";
import qs from "query-string";
import { api, ENV, CONFIG, base_url } from "../Config_API.jsx";
import {TinyButton as ScrollUpButton} from "react-scroll-up-button";
import axios from "axios";
import { Link } from "react-router-dom";
import Parser from "html-react-parser";
import Select from 'react-select';
/** fontawesome */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
/** moment datetime */
import moment from 'moment';
import 'moment/min/locales.min';


import Pagination from "../components/Pagination.jsx";
/**
 * Option for page
 */
import Joi from "joi-browser";
import "animate.css/animate.min.css";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const schema = Joi.object().keys({
  moveToFolder: Joi.string().regex(/^[a-zA-Z0-9]{3,20}$/)
    .max(20)
    .required()
});


class Endorsement extends Component {
  state = {
    display:{},
    access_token:"",
    showFilter:true,
    archiveFolder:[],
    selectedFolder:"inbox",
    message:[],
    page:1,
    recordsTotal:0,
    search:"",
    showModal:false,
    dataForm:{
      From:"",
      Subject:"",
      To:[],
      Description:"",
      TicketID:""
    },
    userData:[],
    userDataOption:[],
    showMoveToArchive:{},
    moveToFolder:{},
    statusMoveToFolder:{}
  };
  notify = (type, message) => {
    const config = {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true
    };
    if (type === "error") {
      toast.error(message, config);
    } else if (type === "success") {
      toast.success(message, config);
    } else if (type === "warn") {
      toast.warn(message, config);
    } else if (type === "info") {
      toast.info(message, config);
    }
  };
  render() {
    
    const { page, recordsTotal, search } = this.state;
    const { From, Subject, To, Description, TicketID } = this.state.dataForm;
    moment.locale(lang("code"));
    //console.log(this.props);
    return <div className="notification">
    <ScrollUpButton />
    <div className="has-text-centered">
      <h1 className="title">
        {lang("MD_ENDORSEMENT")}{" "}
        {ENV !== "PROD" ? (
          <span className="has-text-danger"> ( {ENV} )</span>
        ) : (
          ""
        )}
      </h1>
    </div>
    <br />
    <div>
      
      <div className="columns">
        <div className="message is-black column is-one-quarter" style={{padding: "0rem"}}>
        
          <div className="has-text-right message-header nav-filter"
              onClick={(e)=>{
                e.preventDefault();
                this.setState({
                  showFilter:!this.state.showFilter
                })
              }}>
            <a href="#" aria-label="hide filter"
              onClick={(e)=>{
                e.preventDefault();
                this.setState({
                  showFilter:!this.state.showFilter
                })
              }}
              style={{textDecoration: "none"}}
            >
              {this.state.showFilter?lang("MD_ENDORSEMENT")+" ":lang("MD_ENDORSEMENT")+" "} 
              <FontAwesomeIcon icon={this.state.showFilter?"angle-up":"angle-down"} />
            </a>
          </div>

          <div className="message-body" style={
            this.state.showFilter?
              {display: ""}:
              {display: "none"}
            }>
            
            <div className="field">
              <div className="control">
                
                
                <div className="columns is-variable is-1 is-multiline  is-mobile">
                  <div className="column">
                    <input className="input " value={search} type="text" placeholder={lang("Search")}
                    onChange={e => {
                      this.setState({search:e.target.value});
                    }} />
                  </div>
                  <div className="column is-one-quarter" style={{textAlign:"right"}}>
                    <input type="button" className="button " value={lang("Search")} 
                    onClick={e=>{
                      this.getMessage("",1,this.state.search);
                      this.setState({
                        selectedFolder:"search",
                        display:{}
                      })
                    }}
                    />
                  </div>
                </div>
                <div className="columns">
                  <div className="column">
                    <Link to="/endorsement/form/new" style={{width:"100%"}} type="button" className="button is-success"
                      >
                      <FontAwesomeIcon icon={"envelope"} />
                      <span style={{marginLeft:"5px"}}>
                        {lang("New endorsement")} 
                      </span>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
            
            <div className={this.state.selectedFolder==="inbox"?"folder-list is-active":"folder-list"}>
              <Link to={"/endorsement"}
                onClick={e=>{
                  this.getMessage("inbox", 1, "");
                  this.setState({
                    //selectedFolder:"inbox",
                    search:"",
                    display:{}
                  })
                }}
              >
                <FontAwesomeIcon icon={"inbox"} className="folder-icon" />
                <span className="folder-list-text">{lang("Inbox")}</span>
              </Link>
            </div>
            <div className="folder-list">
              <Link to={"#"}>
                <FontAwesomeIcon icon={"archive"} className="folder-icon" />
                <span className="folder-list-text">{lang("Archive Folder")}</span>
              </Link>
            </div>
            
            {
              this.state.archiveFolder.map((each,key)=>{
                return <div className={this.state.selectedFolder==="archive/"+each.ArchiveFolder?"folder-list folder-archive is-active":"folder-list folder-archive"} key={key}>
                  <Link to={"/endorsement/archive/"+each.ArchiveFolder}
                  onClick={e=>{
                    this.getMessage("archive/"+each.ArchiveFolder, 1, "");
                    this.setState({
                      //selectedFolder:"archive/"+each.ArchiveFolder,
                      search:"",
                      display:{}
                    })
                  }}>
                    <FontAwesomeIcon icon={"folder"} className="folder-icon" />
                    <span className="folder-list-text">{each.ArchiveFolder}</span>
                  </Link>
                </div>
              })
            }
            
            <div className={this.state.selectedFolder==="delete"?"folder-list is-active":"folder-list"}>
              <Link to={"/endorsement/delete"}
                  onClick={e=>{
                    this.getMessage("delete", 1, "");
                    this.setState({
                      //selectedFolder:"delete",
                      search:"",
                      display:{}
                    })
                  }}>
                <FontAwesomeIcon icon={"trash-alt"} className="folder-icon" />
                <span className="folder-list-text">{lang("Deleted Item")}</span>
              </Link>
            </div>
          
          </div>

        </div>  
        <div className="column">
            <Pagination  recordsTotal={recordsTotal} page={page} link={'endorsement/'+((this.state.selectedFolder==="inbox" || this.state.selectedFolder==="search")?"":this.state.selectedFolder)} />
            {this.state.message.map((each,key)=>{

              return <div className="card" key={each.EnID}>
              <header className="card-header" style={{cursor: "pointer"}}
              onClick={(e)=>{
                e.preventDefault();
                const d=!this.state.display[each.EnID];
                this.readEndorsement(each.EnID)
                this.setState({
                  display:{...this.state.display,[each.EnID]:d}
                })
              }}
              >
                <div className="card-header-title">
                
                  <div className="columns is-desktop" style={{width: "100%"}}>
                    <div className="column is-half">
                      <FontAwesomeIcon icon={(each.Readed===1 || typeof this.state.display[each.EnID] !== 'undefined')?"envelope-open":"envelope"} />
                      <span className="message-title">{each.EnSubject}</span>
                      {
                        (each.ArchiveFolder==="DELETE/")?
                        <div className="has-text-info" style={{fontSize:"12px"}}>
                          <em>
                            {lang("This item will be deleted by")+" "+
                            ((moment(each.MoveDate).format('LL')==="Invalid date")?
                                "":moment(each.MoveDate).add(7, 'days').fromNow())}
                          </em>
                        </div>
                        :""
                      }
                      <em>
                      {
                        (this.state.selectedFolder==="search")?
                        <span className="has-text-grey-light" style={{fontSize:"12px"}}> {each.ArchiveFolder}</span>:""
                      }</em>
                    </div>
                    <div className="column is-half message-has-text-position">
                      <div className="message-title has-text-grey-light" style={{fontSize:"14px"}}>
                        
                          {(moment(each.CreateDate).format('LLLL')==="Invalid date")?
                            "":moment(each.CreateDate).startOf('minute').fromNow()} 
                          <br />{""+lang("From")+" "}{each.EnFromName}
                        
                      </div>
                    </div>
                  </div>
                  
                </div>
              </header>
              <div className="card-content" style={!this.state.display[each.EnID]?{display:"none"}:{}}>
                <div className="content">
                  <div className="columns">
                    <div className="column">
                      <strong>{lang("From")}: </strong>{each.EnFromName}<br />
                      <strong>{lang("To")}: </strong>{each.EnToName}<br />
                      <strong>{lang("Time")}: </strong>{(moment(each.CreateDate).format('LLLL')==="Invalid date")?
                                "":moment(each.CreateDate).format('LLLL')} <br />
                      <strong>{lang("Ticket")}#: </strong>{each.RelateTicketID!==""?each.RelateTicketID:lang("NoTicket")}
                      <hr />
                      <article className="message is-link">
                        <div className="message-body">
                          {Parser(each.EnDescription)}
                        </div>
                      </article>
                    </div>
                  </div>
                  <div className="columns">
                    <div className="column">
                    {(each.ArchiveFolder==="DELETE/")?"":
                      <a href="#delete"
                        onClick={e=>{
                          this.handleDelete(each.EnID);
                        }}
                      >
                        <FontAwesomeIcon icon={"trash-alt"} className="folder-icon" />
                        <span className="message-title">{lang("Delete")}</span>
                      </a>
                    }
                      <a href="#archive" 
                        onClick={e=>{
                          this.setState({
                            showMoveToArchive:{
                              ...this.state.showMoveToArchive,
                              [each.EnID]:!this.state.showMoveToArchive[each.EnID]
                            }
                          })
                        }}
                      >
                        <FontAwesomeIcon icon={"archive"} className="folder-icon message-title" />
                        <span className="message-title">{lang("Move")}</span>
                      </a>
                    </div>
                    <div className="column has-text-right">
                      <Link to={"/endorsement/form/reply/"+each.EnID}>
                        <FontAwesomeIcon icon={"reply"} className="folder-icon" />
                        <span className="message-title">{lang("Reply")}</span>
                      </Link>
                      <Link to={"/endorsement/form/replyall/"+each.EnID}>
                        <FontAwesomeIcon icon={"reply-all"} className="folder-icon message-title" />
                        <span className="message-title">{lang("Reply All")}</span>
                      </Link>
                      <Link to={"/endorsement/form/forward/"+each.EnID}>
                        <FontAwesomeIcon icon={"forward"} className="folder-icon message-title" />
                        <span className="message-title">{lang("Forward")}</span>
                      </Link>
                    </div>
                  </div>
                  <div className="columns"
                  style={this.state.showMoveToArchive[each.EnID]?{}:{display:"none"}}
                  >
                    <div className="column">
                      <div className="field">
                        <p className="control">
                          {(each.ArchiveFolder==="INBOX/")?"":
                            <button type="button" className="button is-link" 
                              onClick={e=>{
                                this.handleMoveToInbox(each.EnID);
                              }} 
                            >{lang("Move to INBOX")}</button>
                          }
                        </p>
                      </div>
                      <div className="field is-grouped is-grouped-multiline">
                      
                        <p className="control">
                          <a className="button is-static">
                          {lang("move_folder")+": Archive / "}
                          </a>
                        </p>
                        <p className="control">
                          <input type="text" className="input" style={{width:"200px"}}
                            onChange={e=>{
                              const {error} = Joi.validate({ moveToFolder: e.target.value }, schema);
                              console.log({error});
                              if(error===null){
                                this.setState({
                                  moveToFolder:{
                                    [each.EnID]: e.target.value
                                  }
                                });
                              }else{
                                this.setState({
                                  moveToFolder:{
                                    [each.EnID]: ""
                                  }
                                });
                              }
                            }}
                          />
                        </p>
                        <p className="control">
                          <button type="button" className="button is-link" 
                            onClick={e=>{
                              this.handleMoveFolder(this.state.moveToFolder[each.EnID],each.EnID);
                            }} 
                            disabled={(this.state.moveToFolder[each.EnID]==="" || 
                                        this.state.moveToFolder[each.EnID]===undefined || 
                                        "archive/"+this.state.moveToFolder[each.EnID].toLowerCase()+"/"===each.ArchiveFolder.toLowerCase())
                                        ?"disabled":""}  
                          >{lang("Move")}</button>
                            
                        </p>
                        <p className="control">
                          <input type="button" value={lang("Cancel")} className="button" 
                            onClick={e=>{
                              this.setState({
                                showMoveToArchive:{
                                  ...this.state.showMoveToArchive,
                                  [each.EnID]:!this.state.showMoveToArchive[each.EnID]
                                }
                              })
                            }} />
                        </p>
                      </div>
                    </div>
                  </div>
                  
                </div>
              </div>
              
            </div>
            })}
            
            <Pagination  recordsTotal={recordsTotal} page={page} link={'endorsement/'+((this.state.selectedFolder==="inbox" || this.state.selectedFolder==="search")?"":this.state.selectedFolder)} />
          
        </div>            
      </div>
      
    </div>
    
    

  </div>;
  }

  async componentDidMount(){
    console.log("componentDidMount");
    const request_token = localStorage.getItem("request_token") || "";
    let access_token="";
    if (request_token !== "") {
      await this.props.dispatch(actionGetAccessToken(request_token));
      access_token = this.props.loginReducer.access_token;
      await this.setState({
        access_token
      })
    }
    //console.log(this.props);
    const {parent_folder="", sub_folder=""} = this.props.match.params;
    let folder="inbox";
    if(parent_folder!=="" && sub_folder!=="")
    {
      folder=parent_folder+"/"+sub_folder;
      await this.setState({
        selectedFolder:folder
      });
    }else if(parent_folder==="delete")
    {
      folder=parent_folder;
      await this.setState({
        selectedFolder:folder
      });
    }
    await this.getArchive_list();
    await this.getMessage(folder, this.state.page, "");
    await this.setState({
      dataForm:{
        ...this.state.dataForm,
        From: this.props.loginReducer.userinfo.firstname + " " + this.props.loginReducer.userinfo.lastname
      }
    })
  }
  async componentDidUpdate(prevProps, prevState){
    
    console.log("componentDidUpdate");

    const queryString = qs.parse(this.props.location.search);
    const queryString_prev = qs.parse(prevProps.location.search);

    const prevPage = queryString_prev.p || 1;
    const page = queryString.p || 1;
    //console.log(prevPage);
    //console.log(page);
    
    const {parent_folder="", sub_folder=""} = this.props.match.params;
    let folder="inbox";
    if(parent_folder!=="" && sub_folder!=="")
    {
      folder=parent_folder+"/"+sub_folder;
    }else if(parent_folder==="delete")
    {
      folder=parent_folder;
    }


    if (prevPage !== page || prevProps.location.pathname!==this.props.location.pathname) {
      console.log(folder);
      await this.getMessage(folder, page, "");
      await this.setState({
        selectedFolder:folder
      });
    }
  }
  async getArchive_list(){
    const {access_token}=this.state;
    const res = await axios.get(
      api.MA_API +
        "endorsement/archive_list",
      {
        headers: {
          Authorization: "Bearer " + access_token,
          "Content-Type": "application/json; charset=utf-8"
        }
      }
    );
    const {data}=res.data;
    this.setState({
      archiveFolder: data
    });
  }
  async readEndorsement(EnID){
    const {access_token}=this.state;
    const url=api.MA_API +
    "endorsement/message/"+EnID;
    //console.log(url);

    const res_message = await axios.get(
      url,
      {
        headers: {
          Authorization: "Bearer " + access_token,
          "Content-Type": "application/json; charset=utf-8"
        }
      }
    );
  }

  async getMessage(folder, pageNum, search){
    folder=(folder==="search")?"":folder;
    const {access_token}=this.state;
    const queryString = qs.parse(this.props.location.search);
    const page = pageNum || queryString.p ;
    const url=api.MA_API +
    "endorsement/folder?folder="+folder+"&search="+search+"&start="+(((page-1)*CONFIG.page_length))+
    "&length="+CONFIG.page_length+"&order=CreateDate%20desc";
    //console.log(url);

    const res_message = await axios.get(
      url,
      {
        headers: {
          Authorization: "Bearer " + access_token,
          "Content-Type": "application/json; charset=utf-8"
        }
      }
    );
    const {data,recordsTotal}=res_message.data;
    //console.log(res_message.data);
    await this.setState({
      selectedFolder: folder,
      message: data,
      recordsTotal,
      page
    });
  }

  async handleDelete(EnID){
    if(window.confirm(lang("confirm_delete_endorsement"))){
      const {access_token}=this.state;
      const url=api.MA_API +
      "endorsement/move/"+EnID;
      const data={
        MoveToFolder:"DELETE/"
      }
      
      const res = await axios.post(
        url,data,
        {
          headers: {
            Authorization: "Bearer " + access_token
          }
        }
      );
      //const {data}=res.data;
      console.log(res.data);
      const {parent_folder="", sub_folder=""} = this.props.match.params;
      let folder="inbox";
      if(parent_folder!=="" && sub_folder!=="")
      {
        folder=parent_folder+"/"+sub_folder;
        this.setState({
          selectedFolder:folder
        });
      }
      this.getArchive_list();
      //this.getMessage(folder, this.state.page, "");
      if(res.data.code===200)
      {
        await this.setState({
          showMoveToArchive:{},
          moveToFolder:{}
        });
        this.getArchive_list();
        this.notify("success", lang("success_del_endorsement"));
        this.props.history.push("/endorsement");
      }
      else
      {
        this.notify("error", lang("fail_del_endorsement"));
      }
      this.props.history.push("/endorsement/delete");
    }

  }

  async handleMoveFolder(folder,EnID){
    const {access_token}=this.state;
    const url=api.MA_API +
    "endorsement/move/"+EnID;
    const data={
      MoveToFolder:"ARCHIVE/"+folder+'/'
    }
    
    const res = await axios.post(
      url,data,
      {
        headers: {
          Authorization: "Bearer " + access_token
        }
      }
    );
    //const {data}=res.data;
    console.log(res.data);
    if(res.data.code===200)
    {
      await this.setState({
        showMoveToArchive:{},
        moveToFolder:{}
      });
      this.getArchive_list();
      this.notify("success", lang("success_moveto_endorsement"));
      this.props.history.push("/endorsement");
    }
    else
    {
      this.notify("error", lang("fail_moveto_endorsement"));
    }
    this.props.history.push("/endorsement/archive/"+folder);

  }

  async handleMoveToInbox(EnID){
    const {access_token}=this.state;
    const url=api.MA_API +
    "endorsement/move/"+EnID;
    const data={
      MoveToFolder:"INBOX/"
    }
    
    const res = await axios.post(
      url,data,
      {
        headers: {
          Authorization: "Bearer " + access_token
        }
      }
    );
    //const {data}=res.data;
    console.log(res.data);
    if(res.data.code===200)
    {
      await this.setState({
        showMoveToArchive:{},
        moveToFolder:{}
      });
      this.getArchive_list();
      this.notify("success", lang("success_moveto_endorsement"));
      this.props.history.push("/endorsement");
    }
    else
    {
      this.notify("error", lang("fail_moveto_endorsement"));
    }

  }
  

}
const mapStateToProps = state => {
  return { landReducer: state.langReducer, loginReducer: state.loginReducer };
};
export default connect(mapStateToProps)(Endorsement);
