import React, { Component } from "react";
import { Prompt } from "react-router";
import { connect } from "react-redux";
import { lang } from "../Lang.jsx";
import { api, ENV } from "../Config_API.jsx";
import { view } from "../templates/email/insert_ma.jsx";
import {TinyButton as ScrollUpButton} from "react-scroll-up-button";
import axios from "axios";
/** moment datetime */
import moment from 'moment';
import 'moment/min/locales.min';


/**react-day-picker */
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import MomentLocaleUtils, {
  formatDate,
  parseDate,
} from 'react-day-picker/moment';
/**react-day-picker */
/**react-day-picker */
import DatePicker from "../components/DatePicker.jsx";
/**react-day-picker */
import LeaveCalendar from "../components/LeaveCalendar.jsx";

import { actionGetAccessToken } from "../actions/login";
import { Calendar_HQ, aLeave } from "../db.json"

class Leave extends Component {
  state = {
    thisYear:2019,
    month:["January","February","March","April","May","June","July","August","September","October","November","December"],
    day:[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
    leave_color:{
      "leave": "#0C0",
      "sick": "#e01f5f"
    },
    aLeaveDetail:[],
    calendar:{},
    from:undefined,
    from_Half:null,
    to:undefined,
    to_Half:null,
    selectedLeaveType:null,
    selectedCalendar:{},
    seletedDayCount:0.0,
    errSelectedDate:"",
  }
  handleFromChange = this.handleFromChange.bind(this);
  handleToChange = this.handleToChange.bind(this);
  render() {
    const {thisYear, month, day, calendar, leave_color, aLeaveDetail,
      from, to, selectedLeaveType, from_Half, to_Half,
      selectedCalendar, seletedDayCount,
      errSelectedDate
    }=this.state;
    
    const modifiers = { start: from, end: to };

    
    if(lang("code")==="th"){
      moment.locale('th');
    }
    else if(lang("code")==="ja"){
      moment.locale('ja');
    }
    else if(lang("code")==="id"){
      moment.locale('id');
    }
    else
    {
      moment.locale('en');
    }

    return (
      <div className="notification">
        <ScrollUpButton />
        <div className="columns is-gapless is-multiline">
          <div className="column box margin5 is-two-thirds" style={{borderRadius: "0px"}}>
            <div className="column" style={{height:"500px", overflow:"auto"}}>
              {/*moment(from).format('YYYY-MM-DD')} {moment(to).format('YYYY-MM-DD')} {from_Half} {to_Half} {selectedLeaveType} 
              {seletedDayCount*/}
              <div>
                <h3>LEAVE REQUEST FORM     (Please refer to the leave policy)</h3>
                <hr style={{margin: "1rem 0"}} />
              </div>
              <div className="columns">
                <div className="column is-narrow" style={{marginTop: "5px"}}>
                  {lang("From")} 
                </div>
                <div className="column is-narrow">
                  <div className="field">
                    <div className="control filter-date">
                      <DayPickerInput className="input is-info" style={{maxWidth: "100%"}}
                        value={from}
                        format={"YYYY-MM-DD"}
                        formatDate={formatDate}
                        parseDate={parseDate}
                        dayPickerProps={{
                          selectedDays: [from, { from, to }],
                          disabledDays: { after: to },
                          toMonth: to,
                          modifiers,
                          numberOfMonths: 1,
                          onDayClick: () => this.to.getInput().focus(),
                          locale: lang("code"),
                          localeUtils: MomentLocaleUtils,
                        }}
                        onDayChange={this.handleFromChange}
                      /> 
                    </div>
                  </div>
                </div>
                <div className="column is-narrow" style={{fontSize:"12px"}}>
                  <input type="radio" name="from-half" id="from-half-1" checked={from_Half==="1"} onClick={(e)=>{
                    this.handleCheckHalf("from","1");
                  }} onChange={(e)=>{}} /><label htmlFor="from-half-1"> {lang("Half Morning")}</label> <br />
                  <input type="radio" name="from-half" id="from-half-2" checked={from_Half==="2"} onClick={(e)=>{
                    this.handleCheckHalf("from","2");
                  }} onChange={(e)=>{}} /><label htmlFor="from-half-2"> {lang("Half Afternoon")}</label>
                </div>
                <div className="column is-narrow" style={{marginTop: "5px"}}>
                  {lang("To")} 
                </div>
                <div className="column is-narrow">
                  <div className="field">
                    <div className="control filter-date">
                      <DayPickerInput className="input is-info" style={{maxWidth: "100%"}}
                        ref={el => (this.to = el)}
                        value={to}
                        format={"YYYY-MM-DD"}
                        formatDate={formatDate}
                        parseDate={parseDate}
                        dayPickerProps={{
                          selectedDays: [from, { from, to }],
                          disabledDays: { before: from },
                          modifiers,
                          month: from,
                          fromMonth: from,
                          numberOfMonths: 1,
                          locale: lang("code"),
                          localeUtils: MomentLocaleUtils,
                        }}
                        onDayChange={this.handleToChange}
                      />
                    </div>
                  </div>
                </div>
                <div className="column is-narrow" style={{fontSize:"12px"}}>
                  <input type="radio" name="to-half" id="to-half-1" checked={to_Half==="1"} onClick={(e)=>{
                    this.handleCheckHalf("to","1");
                  }} onChange={(e)=>{}} /><label htmlFor="to-half-1"> {lang("Half Morning")}</label> <br />
                  <input type="radio" name="to-half" id="to-half-2" checked={to_Half==="2"} onClick={(e)=>{
                    this.handleCheckHalf("to","2");
                  }} onChange={(e)=>{}} /><label htmlFor="to-half-2"> {lang("Half Afternoon")}</label>
                </div>
              </div>
              <div className="columns">
                <div className="column is-four-fifths">
                  <div>
                    <input type="radio" value="leave" name="leave-type" id="leave-type-annual-leave" checked={selectedLeaveType==="leave"} onClick={(e)=>{
                        this.handleTypeLeave(e);
                      }} onChange={(e)=>{}} /><label htmlFor="leave-type-annual-leave"> {lang("leave")} </label>
                    <input type="radio" value="sick" name="leave-type" id="leave-type-sick" checked={selectedLeaveType==="sick"} onClick={(e)=>{
                        this.handleTypeLeave(e);
                      }} onChange={(e)=>{}} /><label htmlFor="leave-type-sick"> {lang("sick")} </label>
                    
                  </div>
                  <div>
                    {"Total"}: {seletedDayCount} {"Day(s)"}
                  </div>
                </div>
                
                <div className="column has-text-right">
                  <button type="button" className="button is-info"
                    onClick={e=>{}}>Save</button>
                </div>
              </div>
              <hr style={{margin: "0px 0px 0px 0px"}} />
              <h3>{"Historical Leave"}</h3>
              
              {/*<div className="has-text-info">{moment(from).format('YYYY-MM-DD')},{moment(to).format('YYYY-MM-DD')},{from_Half},{to_Half}</div>
              <div className="has-text-danger">{errSelectedDate}</div>*/}
              <br />
              <div>{"Validate"}</div>
              <hr style={{margin: "0px 0px 0px 0px"}} />
              {
                aLeaveDetail.map((each,key)=>{
                  let backgroundColor="#cfc";
                  /*if(){
                    backgroundColor="#cfc";
                  }*/

                  console.log(each);
                  return <div className="box-detail-leave" key={key} style={{
                      backgroundColor,
                      fontSize:"14px"
                    }}>
                      <div className="columns">
                        <div className="column is-narrow">
                          <span className="tag is-success">{"Yes"}</span>
                        </div>
                        <div className="column">
                          <strong><em>{lang(each.Type)}</em></strong>
                          <br />
                          <strong>{"Total"}</strong> {each.Totalday} {"day(s)"}
                        </div>
                        <div className="column">
                          <strong>{lang("From")}</strong> {moment(each.StartDate).format('LL')}
                          {(each.StartDate_half==="1")?
                            <div>
                              ( {lang("Half Morning")} )
                            </div>:""}
                          {(each.StartDate_half==="2")?
                            <div>
                              ( {lang("Half Afternoon")} )
                            </div>:""}
                        </div>
                        <div className="column">
                          <strong>{lang("To")}</strong> {moment(each.EndDate).format('LL')}
                          {(each.EndDate_half==="1")?
                            <div>
                              ( {lang("Half Morning")} )
                            </div>:""}
                          {(each.EndDate_half==="2")?
                            <div>
                              ( {lang("Half Afternoon")} )
                            </div>:""}
                        </div>
                        <div className="column is-narrow has-text-right">
                          <button type="button" className="button is-danger is-small">{lang("Cancel")}</button>
                        </div>
                      </div>
                    
                  </div>
                })
              }

            </div>
          </div>
          <div className="column box margin5 is-one-thirds" style={{borderRadius: "0px"}}>
            <a href="#" className="box-header-icon" aria-label="more options">
              <span className="icon">
                <i className="fas fa-angle-right" aria-hidden="true"></i>
              </span>
            </a>
            <div className="column">
              {"Leave"}
            </div>
          </div>
          <div className="column box margin5 is-full" style={{borderRadius: "0px"}}>
            <div className="columns" style={{margin:"3px 3px"}}>
              <div className="column is-narrow" style={{overflow:"auto"}}>
                
                <LeaveCalendar 
                  thisYear={thisYear} 
                  selectedCalendar={selectedCalendar} 
                  calendar={calendar} />

              </div>
              
              <div className="column">
                {"Leave"}
              </div>
            </div>
          </div>
        </div>
        
      </div>
    );
  }
  async handleTypeLeave(e){
    let val = e.target.value;
    await this.setState({
      selectedLeaveType:val
    });
    this.handleSelectDate();
  }
  async handleCheckHalf(from_to,val){
    const {from_Half, to_Half}=this.state;
    if(from_to==="from"){
      await this.setState({
        from_Half:from_Half===val?null:val
      });
    }
    else if(from_to==="to"){
      await this.setState({
        to_Half:to_Half===val?null:val
      });
    }
    this.handleSelectDate();
  }
  showFromMonth() {
    const { from, to } = this.state;
    if (!from) {
      return;
    }
    if (moment(to).diff(moment(from), 'months') < 2) {
      this.to.getDayPicker().showMonth(from);
    }
  }
  async handleFromChange(from, modifiers, dayPickerInput) {
    // Change the from date and focus the "to" input field
    const input = dayPickerInput.getInput();
    await this.setState({ 
      from,
      //from_Half:"2",
      //selectedLeaveType:"leave"
    });
    this.handleSelectDate();
    //console.log(from);
  }
  async handleToChange(to, modifiers, dayPickerInput) {
    const input = dayPickerInput.getInput();
    await this.setState({ to,
      //to_Half:null 
    }, this.showFromMonth);
    this.handleSelectDate();  
    //console.log(to);
  }

  handleSelectDate(){
    const {
      calendar, day, thisYear, month, 
      from, to, selectedLeaveType, 
      from_Half, to_Half
    }=this.state;
    if(from!==undefined 
      && to!==undefined 
      && selectedLeaveType!==null)
    {
      console.log(from);
      console.log(to);
      var date1 = new Date(moment(from).format('YYYY-MM-DD'));
      var date2 = new Date(moment(to).format('YYYY-MM-DD'));
      var dateAdd = new Date(moment(from).format('YYYY-MM-DD'));
      //var timeDiff = Math.abs(date2.getTime() - date1.getTime());
      var timeDiff = (date2.getTime() - date1.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
      console.log(diffDays);
      if(diffDays>=0)
      {
        var dateAdd_str=dateAdd.getFullYear()+"-"+("0"+(dateAdd.getMonth()+1).toString()).slice(-2)+"-"+("0"+dateAdd.getDate()).slice(-2);
        let selectedCalendar={};
        let leave_fullday="";
        if(diffDays===0){
          if(from_Half==="1" && to_Half===null){
            leave_fullday="morning";
          }else if(from_Half==="1" && to_Half==="1"){
            leave_fullday="morning";
          }else if(from_Half==="2" && to_Half===null){
            leave_fullday="afternoon";
          }else if(from_Half==="2" && to_Half==="2"){
            leave_fullday="afternoon";
          }else{
            leave_fullday="full";
          }
          console.log(from_Half);
        }
        else
        {
          if(from_Half==="1"){
            leave_fullday="morning";
          }else if(from_Half==="2"){
            leave_fullday="afternoon";
          }else{
            leave_fullday="full";
          }
        }
        
        selectedCalendar = {
          ...selectedCalendar,
          [dateAdd_str]:{
            ...selectedCalendar[dateAdd_str],
            leave_type:selectedLeaveType,
            leave_fullday,
            Calendar_Date: dateAdd_str,
            Calendar_DayOfWeek: "",
            Holiday: null,
            IsHoliday_Office: 0
          }
        };

        //console.log("      >> " + dateAdd_str);
        for(var d=0;d<diffDays;d++){
          dateAdd.setDate(dateAdd.getDate()+1);
          dateAdd_str=dateAdd.getFullYear()+"-"+("0"+(dateAdd.getMonth()+1).toString()).slice(-2)+"-"+("0"+dateAdd.getDate()).slice(-2);
          //console.log("      >> " + dateAdd_str);
          if(to_Half==="1"){
            leave_fullday="morning";
          }else if(to_Half==="2"){
            leave_fullday="afternoon";
          }else{
            leave_fullday="full";
          }
          selectedCalendar = {
            ...selectedCalendar,
            [dateAdd_str]:{
              ...selectedCalendar[dateAdd_str],
              leave_type:selectedLeaveType,
              leave_fullday,
              Calendar_Date: dateAdd_str,
              Calendar_DayOfWeek: "",
              Holiday: null,
              IsHoliday_Office: 0
            }
          };
        }
        let seletedDayCount=0;
        let isDuplicate=false;
        let dateDuplicate=[];
        month.map((month_name, month_key)=>{
          day.map((day_number,day_key)=>{
            const ymd=thisYear.toString() + "-" + ("0" + (month_key+1).toString()).slice(-2) + "-" + ("0" + day_number.toString()).slice(-2);                  
            //console.log(calendar[ymd]);
            if(selectedCalendar[ymd] && calendar[ymd].IsHoliday_Office===0)
            {
              if(selectedCalendar[ymd].leave_fullday==="full")
              {
                seletedDayCount=seletedDayCount+1;
              }
              else
              {
                seletedDayCount=seletedDayCount+0.5;
              }
              //console.log(selectedCalendar[ymd]);
              console.log(calendar[ymd]);
              console.log(selectedCalendar[ymd]);
            }
            if(selectedCalendar[ymd] && 
              calendar[ymd].leave_type!==null && 
              (
                calendar[ymd].leave_fullday===selectedCalendar[ymd].leave_fullday ||
                (
                  calendar[ymd].leave_fullday==="full" || selectedCalendar[ymd].leave_fullday==="full"
                )
              )

            )
            {
              isDuplicate=true;
              dateDuplicate.push(ymd);
              //console.log(calendar[ymd]);
            }

          });
        });
        
        if(!isDuplicate)
        {
          this.setState({
            selectedCalendar,
            seletedDayCount,
            errSelectedDate:""
          });
        }
        else
        {
          this.setState({
            selectedCalendar:{},
            seletedDayCount,
            errSelectedDate:`Sorry, you have duplicate date >> ${dateDuplicate.toString()}`
          });
        }
      }
      else
      {
        if(lang("code")==="th"){
          moment.locale('th');
        }
        else if(lang("code")==="ja"){
          moment.locale('ja');
        }
        else if(lang("code")==="id"){
          moment.locale('id');
        }
        else
        {
          moment.locale('en');
        }
        let startDate=moment(from).format('LL');
        let endDate=moment(to).format('LL');
        this.setState({
          selectedCalendar:{},
          from:undefined,
          to:undefined,
          errSelectedDate:`Sorry, your start date( ${startDate} ) and end date( ${endDate} ) is incorrect. Please kindly check!`
        });
      }

    }
    else{
      this.setState({
        selectedCalendar:{},
        errSelectedDate:""
      });
    }
    
  }

  async componentDidMount() {
    let { access_token = "", access_token_exp = 0 } = this.props.loginReducer; //this.state;
    //console.log(this.props.loginReducer);
    if (access_token === "") {
      const request_token = localStorage.getItem("request_token");
      await this.props.dispatch(actionGetAccessToken(request_token));
    }
    access_token = this.props.loginReducer.access_token;
    //const url="https://intranet.redplanethotels.com/en/leave/leave_api/get_weekend/THA/2019/HQ";
    //const res = await axios.get(url);
    const res=Calendar_HQ;
    let calendar = {};
    res.map(each=>{
      calendar = {
        ...calendar,
        [each.Calendar_Date]:{
          ...each,
          leave_type:null,
          leave_fullday:null
        }
      };
    });

    aLeave.map(each=>{
      var date1 = new Date(each.StartDate);
      var date2 = new Date(each.EndDate);
      var dateAdd = new Date(each.StartDate);
      var timeDiff = Math.abs(date2.getTime() - date1.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
      console.log("Start: " + each.StartDate + ", End: " + each.EndDate + " >> " + diffDays.toString());
      let dateAdd_str=dateAdd.getFullYear()+"-"+("0"+(dateAdd.getMonth()+1).toString()).slice(-2)+"-"+("0"+dateAdd.getDate()).slice(-2);
      //console.log(dateAdd_str);
      if(calendar[dateAdd_str]){
        
        let {leave_fullday}=calendar[dateAdd_str];
        if(leave_fullday!==null){
          leave_fullday="full";
          
          calendar = {
            ...calendar,
            [dateAdd_str]:{
              ...calendar[dateAdd_str],
              leave_fullday
            }
          };
          //console.log(leave_fullday);
        }
        else
        {
          //if(date1===date2){
          if(diffDays===0){
            if(each.StartDate_half==="1" && each.EndDate_half===null){
              leave_fullday="morning";
            }else if(each.StartDate_half==="1" && each.EndDate_half==="1"){
              leave_fullday="morning";
            }else if(each.StartDate_half==="2" && each.EndDate_half===null){
              leave_fullday="afternoon";
            }else if(each.StartDate_half==="2" && each.EndDate_half==="2"){
              leave_fullday="afternoon";
            }else{
              leave_fullday="full";
            }
            
          }
          else
          {
            if(each.StartDate_half==="1"){
              leave_fullday="morning";
            }else if(each.StartDate_half==="2"){
              leave_fullday="afternoon";
            }else{
              leave_fullday="full";
            }
          }
          
          calendar = {
            ...calendar,
            [dateAdd_str]:{
              ...calendar[dateAdd_str],
              leave_type:each.Type,
              leave_fullday
            }
          };
        }
        //console.log("      >> " + dateAdd_str);
        for(var d=0;d<diffDays;d++){
          dateAdd.setDate(dateAdd.getDate()+1);
          dateAdd_str=dateAdd.getFullYear()+"-"+("0"+(dateAdd.getMonth()+1).toString()).slice(-2)+"-"+("0"+dateAdd.getDate()).slice(-2);
          //console.log("      >> " + dateAdd_str);
          if(each.EndDate_half==="1"){
            leave_fullday="morning";
          }else if(each.EndDate_half==="2"){
            leave_fullday="afternoon";
          }else{
            leave_fullday="full";
          }
          calendar = {
            ...calendar,
            [dateAdd_str]:{
              ...calendar[dateAdd_str],
              leave_type:each.Type,
              leave_fullday
            }
          };
        }
      }
    });
    
    //console.log(calendar);
    await this.setState({
      aLeaveDetail:aLeave,
      calendar,
      //from: moment().format('YYYY-MM-DD')
    });
    /*var y=2019;
    var m=1;
    var d=2;
    console.log(y.toString() + "-" + ("0" + m.toString()).slice(-2) + "-" + ("0" + d.toString()).slice(-2));*/
  }
}

const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    propertyReducer: state.propertyReducer
  };
};
export default connect(mapStateToProps)(Leave);
