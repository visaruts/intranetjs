import React, { Component } from "react";
import { Prompt } from "react-router";
import { connect } from "react-redux";
import { lang } from "../Lang.jsx";
import { api, ENV } from "../Config_API.jsx";
import { view } from "../templates/email/insert_ma.jsx";
import axios from "axios";
import moment from 'moment';

import { actionGetAccessToken } from "../actions/login";
import PropertySelectBox from "../components/PropertySelectBox.jsx";
/**
 * Option for page
 */
import Joi from "joi-browser";
import "animate.css/animate.min.css";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const schema = Joi.object().keys({
  location: Joi.string()
    .max(50)
    .required(),
  yourMessage: Joi.string()
    .max(255)
    .required(),
  yourName: Joi.string()
    .max(50)
    .required(),
  contactInfo: Joi.string().max(50)
});

class ReportAProblem extends Component {
  state = {
    access_token: localStorage.getItem("access_token") || "",
    country: {},
    property: [],
    code: 0,
    upload: 0,
    message: "",
    formData: {
      entityCode: "",
      propertyCode: "",
      contactInfo: "",
      location: "",
      yourMessage: "",
      yourName: "",
      isUrgent: false
    },
    file1: null,
    file2: null,
    file3: null,
    file1Name: "",
    file2Name: "",
    file3Name: "",
    errors: [],
    errorFiles: []
  };
  notify = (type, message) => {
    const config = {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true
    };
    if (type === "error") {
      toast.error(message, config);
    } else if (type === "success") {
      toast.success(message, config);
    } else if (type === "warn") {
      toast.warn(message, config);
    } else if (type === "info") {
      toast.info(message, config);
    }
  };
  render() {
    //console.log(this.props.loginReducer.access_token);

    const formData = this.state.formData;
    const { file1, file2, file3, file1Name, file2Name, file3Name } = this.state;
    return (
      <div className="container">
        <Prompt
          when={(formData.Location || "") !== ""}
          message={lang("leavePage")}
        />
        <div className="notification">
          <div className="has-text-centered">
            <h1 className="title">
              {lang("MD_PROBLEM")}{" "}
              {ENV !== "PROD" ? (
                <span className="has-text-danger"> ( {ENV} )</span>
              ) : (
                ""
              )}
            </h1>
          </div>

          <form id="fReportProblem">
            {/*<div>{JSON.stringify(this.props.propertyReducer.entityCode)}</div>
            <div>{JSON.stringify(this.props.propertyReducer.propertyCode)}</div>
              <div>{JSON.stringify(this.state.formData)}</div>*/}
            <div className="field">
              <label className="label has-text-danger">{lang("Hotel")}</label>
              <div className="control">
                <div className="select  ">
                  <PropertySelectBox
                    saveState={e => {
                      this.setState({
                        formData: {
                          ...this.state.formData,
                          propertyCode: this.props.propertyReducer.propertyCode,
                          entityCode: this.props.propertyReducer.entityCode
                        }
                      });
                    }}
                  />
                </div>

                <div className="has-text-danger">
                  {this.displayErrorMessage("propertyCode")}
                </div>
              </div>
            </div>

            <div className="field">
              <label className="label">{lang("Contact Info")}</label>
              <div className="control">
                <input
                  name="contactInfo"
                  className="input "
                  type="text"
                  placeholder={lang("Contact Info")}
                  onChange={e => this.handleChange(e, "contactInfo")}
                  value={formData.contactInfo}
                />
              </div>
            </div>

            <div className="field">
              <label className="label has-text-danger">
                {lang("Location")}
              </label>
              <div className="control">
                <input
                  name="location"
                  className="input "
                  type="text"
                  placeholder={
                    lang("Location") + " (eg.room 319 / parking lot)"
                  }
                  onChange={e => this.handleChange(e, "location")}
                  value={formData.location}
                />
              </div>

              <div className="has-text-danger">
                {this.displayErrorMessage("location")}
              </div>
            </div>

            <div className="field">
              <label className="label has-text-danger">
                {lang("Your message")}
              </label>
              <div className="control">
                <textarea
                  name="yourMessage"
                  className="textarea "
                  placeholder={lang("Your message")}
                  rows="5"
                  onChange={e => this.handleChange(e, "yourMessage")}
                  value={formData.yourMessage}
                />
              </div>

              <div className="has-text-danger">
                {this.displayErrorMessage("yourMessage")}
              </div>
            </div>

            <div className="field">
              <label className="label has-text-danger">
                {lang("Your Name")}
              </label>
              <div className="control">
                <input
                  name="yourName"
                  className="input "
                  type="text"
                  placeholder={lang("Your Name")}
                  onChange={e => this.handleChange(e, "yourName")}
                  value={formData.yourName}
                />
              </div>

              <div className="has-text-danger">
                {this.displayErrorMessage("yourName")}
              </div>
            </div>

            <div className="field">
              <label className="label">{lang("Is it urgent")}</label>
              <div className="control">
                <div className="select ">
                  <select
                    name="isUrgent"
                    onChange={e => this.handleChange(e, "isUrgent")}
                    defaultValue={formData.isUrgent}
                  >
                    <option value={true}>{lang("Yes")}</option>
                    <option value={false}>{lang("No")}</option>
                  </select>
                </div>
              </div>
            </div>
            {file1Name !== ""?
              <div>
                <a href={URL.createObjectURL(file1)} target="_blank">
                  <img src={URL.createObjectURL(file1)} style={{height:"120px"}} border="0" />
                </a>
              </div>:""}
            <div className="field">
              <label className="label">{lang("Attached  Image File")} 1</label>
              <div className="control">
                <div className="file has-name is-fullwidth">
                  <label className="file-label">
                    <input
                      accept=".jpg,.png"
                      className="file-input "
                      type="file"
                      name="file1"
                      onChange={e => this.handleUploadFile(e, "file1")}
                      defaultValue={file1Name}
                    />
                    <span className="file-cta">
                      <span className="file-icon">
                        <i className="fas fa-upload" />
                      </span>
                      <span className="file-label">
                        {lang("Choose a file")}
                      </span>
                    </span>
                    <span className="file-name">{file1Name}</span>
                  </label>
                  {file1Name !== "" ? (
                    <button
                      className="button"
                      onClick={e => {
                        this.handleRemoveFile("file1");
                      }}
                    >
                      {lang("Remove")}
                    </button>
                  ) : (
                    ""
                  )}
                </div>
              </div>
              <div className="has-text-danger">
                {this.displayErrorMessage("file1", "file")}
              </div>
            </div>
            {file2Name !== ""?
              <div>
                <a href={URL.createObjectURL(file2)} target="_blank">
                  <img src={URL.createObjectURL(file2)} style={{height:"120px"}} border="0" />
                </a>
              </div>:""}
            <div className="field">
              <label className="label">{lang("Attached  Image File")} 2</label>
              <div className="control">
                <div className="file has-name is-fullwidth">
                  <label className="file-label">
                    <input
                      accept=".jpg,.png"
                      className="file-input "
                      type="file"
                      name="file2"
                      onChange={e => this.handleUploadFile(e, "file2")}
                      defaultValue={file2Name}
                    />
                    <span className="file-cta">
                      <span className="file-icon">
                        <i className="fas fa-upload" />
                      </span>
                      <span className="file-label">
                        {lang("Choose a file")}
                      </span>
                    </span>
                    <span className="file-name">{file2Name}</span>
                  </label>
                  {file2Name !== "" ? (
                    <button
                      className="button"
                      onClick={e => {
                        this.handleRemoveFile("file2");
                      }}
                    >
                      {lang("Remove")}
                    </button>
                  ) : (
                    ""
                  )}
                </div>
              </div>
              <div className="has-text-danger">
                {this.displayErrorMessage("file2", "file")}
              </div>
            </div>
            {file3Name !== ""?
              <div>
                <a href={URL.createObjectURL(file3)} target="_blank">
                  <img src={URL.createObjectURL(file3)} style={{height:"120px"}} border="0" />
                </a>
              </div>:""}
            <div className="field">
              <label className="label">{lang("Attached  Image File")} 3</label>
              <div className="control">
                <div className="file has-name is-fullwidth">
                  <label className="file-label">
                    <input
                      accept=".jpg,.png"
                      className="file-input "
                      type="file"
                      name="file3"
                      onChange={e => this.handleUploadFile(e, "file3")}
                      defaultValue={file3Name}
                    />
                    <span className="file-cta">
                      <span className="file-icon">
                        <i className="fas fa-upload" />
                      </span>
                      <span className="file-label">
                        {lang("Choose a file")}
                      </span>
                    </span>
                    <span className="file-name">{file3Name}</span>
                  </label>
                  {file3Name !== "" ? (
                    <button
                      className="button"
                      onClick={e => {
                        this.handleRemoveFile("file3");
                      }}
                    >
                      {lang("Remove")}
                    </button>
                  ) : (
                    ""
                  )}
                </div>
              </div>
              <div className="has-text-danger">
                {this.displayErrorMessage("file3", "file")}
              </div>
            </div>
            <div className="filed">
              <div className="has-text-danger">
                <span className="icon is-small is-left">
                  <i className="fas fa-info-circle" />
                </span>{" "}
                {lang("Please attached file only format")}
              </div>
              <div className="has-text-danger">
                <span className="icon is-small is-left">
                  <i className="fas fa-info-circle" />
                </span>{" "}
                {lang("Size Limited per File")}
              </div>
            </div>
            <hr />
            <div className="filed">
              <div className="control has-text-centered">
                <button
                  className={
                    this.state.upload === 0
                      ? "button is-link"
                      : "button is-link is-loading"
                  }
                  onClick={e => {
                    this.handleSubmit(e);
                  }}
                  disabled={this.state.upload === 0 ? "" : "disabled"}
                >
                  <span className="ico">
                    <i className="fas fa-save" /> {lang("Submit")}
                  </span>
                </button>
              </div>
            </div>

            {/**/}
          </form>
        </div>
      </div>
    );
  }
  async handleSubmit(e) {
    e.preventDefault();

    let { access_token = "" } = this.props.loginReducer;
    const { location, yourMessage, yourName } = this.state.formData;
    const { error } = Joi.validate(
      { location, yourMessage, yourName },
      schema,
      {
        abortEarly: false
      }
    );
    const errors = error ? error.details : [];
    const errorFiles =
      Object.keys(this.state.errorFiles).find(each => {
        return this.state.errorFiles[each].message !== "";
      }) || "";
    //console.log(errors);
    //console.log(errorFiles);
    await this.setState({
      errors
    });
    try {
      
      if (errors.length === 0 && errorFiles === "") {
        
        /** Get Admin Dashboard by PropertyCode */
        const admin_dashboard = await this.getAdminDashBoard();
        /** Get Admin Dashboard by PropertyCode */
        if(admin_dashboard.code===200)
        {

          const form_data = new FormData();
          /*form_data.append("entityCode", this.props.propertyReducer.entityCode);
          form_data.append(
            "propertyCode",
            this.props.propertyReducer.propertyCode
          );*/
          Object.keys(this.state.formData).map(each => {
            form_data.append(each, this.state.formData[each]);
          });
          if (this.state.file1Name !== "") {
            form_data.append("file1", this.state.file1, this.state.file1Name);
          }
          if (this.state.file2Name !== "") {
            form_data.append("file2", this.state.file2, this.state.file2Name);
          }
          if (this.state.file3Name !== "") {
            form_data.append("file3", this.state.file3, this.state.file3Name);
          }
          //console.log(form_data);
          const res = await axios.post(api.MA_API + "form", form_data, {
            headers: {
              Authorization: "Bearer " + access_token,
              "Content-Type": "multipart/form-data"
            },
            onUploadProgress: progressEvent => {
              this.setState({
                upload: progressEvent.loaded / progressEvent.total
              });
              //console.log(progressEvent.loaded / progressEvent.total);
            }
          });

          const data_view={
            name: this.state.formData.yourName, 
            createDate: moment().format('YYYY-MM-DD'), 
            hotel: this.props.propertyReducer.propertyName, 
            defects:this.state.formData.yourMessage, 
            location:this.state.formData.location, 
            urgent:this.state.formData.isUrgent, 
            contactInfo:this.state.formData.contactInfo
          };
          const insert_view=view(data_view);
          //console.log(insert_view);
          console.log(admin_dashboard.email);
          const data_send_email = {
            ModuleCode: "MA_Defect",
            Subject:((ENV !== "PROD")?"[Test] ":"")+"Maintenance Defects : New Report A Problem (" + data_view.hotel + ")",
            Body:view(data_view),
            To:(ENV !== "PROD")?"pornpichai@redplanethotels.com":admin_dashboard.email,
            From:"rphldeveloper@redplanethotels.com",
            CC:"",
            BCC: (ENV !== "PROD")?"":"rphldeveloper@redplanethotels.com",
            AttachFile:"",
            AppCode:"MADEFECT"
          };
          const res_job_email = await axios.post(api.LOGIN_API+"job_email", data_send_email, {
            headers: {
              Authorization: "Bearer " + access_token
            }});

          //console.log(res_job_email);
          this.setState({
            code: 0,
            upload: 0,
            message: "",
            formData: {
              ...this.state.formData,
              contactInfo: "",
              location: "",
              yourMessage: "",
              isUrgent: false
            },
            file1: null,
            file2: null,
            file3: null,
            file1Name: "",
            file2Name: "",
            file3Name: "",
            errors: [],
            errorFiles: []
          });
          this.notify("success", lang("Send Report A Problem Success"));
        }
        else
        {
          this.notify("error", admin_dashboard.message);
        }
      } else {
        //console.log(errors);
        //console.log(errorFiles);
      }
    } catch (err) {
      //console.log(err);
      const api_err = err.response.data.message || "";
      if (api_err !== "") {
        if (err.response.data.message === "jwt expired") {
          this.notify("error", lang("SessionTimeout"));
        } else {
          //console.log({ err });
          this.notify("error", lang("Send Report Fail"));
          this.notify("error", err.response.data.message);
        }
      } else {
        //console.log({ err });
        this.notify("error", err.message);
      }
      setTimeout(() => {
        this.setState({
          upload:0
        })
      }, 5000);
      
    }
  }
  async handleRemoveFile(key) {
    await this.setState({
      [key]: null,
      [key + "Name"]: "",
      errorFiles: { [key]: { message: "" } }
    });
  }
  handleUploadFile(e, key) {
    let val = e.target.files[0];
    let name = "";
    //console.log(e.target.files[0]);
    let errorFiles = {
      ...this.state.errorFiles,
      [key]: { message: "" }
    };
    if (val) {
      name = e.target.files[0].name;
      if (e.target.files[0].size > 1024 * 1024 * 4)
        errorFiles = {
          ...this.state.errorFiles,
          [key]: { message: lang("File size is more than") }
        };
    }
    //console.log(URL.createObjectURL(val));
    this.setState({
      [key]: val,
      [key + "Name"]: name,
      errorFiles
    });
  }

  handleChange(e, key) {
    //console.log(e.target.type);
    let val = "";
    let entityCode = this.state.formData.entityCode;
    if (e.target.type === "checkbox") {
      val = e.target.checked;
    } else {
      val = e.target.value;
      if (key === "propertyCode") {
        const p = this.state.property.find(each => {
          return each.PropertyCode === e.target.value;
        });
        entityCode = p.EntityCode;
      }
    }
    const formData = { ...this.state.formData, [key]: val, entityCode };
    this.setState({
      formData
    });
  }
  displayErrorMessage(fieldName, fieldType = "text") {
    const { errors, errorFiles } = this.state;
    //console.log(errors);
    let error = {};
    let message = "";
    if (fieldType === "file") {
      /*error = Object.keys(errorFiles).find(each => {
        return each === fieldName;
      });*/
      error = errorFiles[fieldName];
    } else {
      error = errors.find(each => {
        //return each.path.includes(fieldName);
        //console.log(each);
        message = lang(each.context.key + "." + each.type) || "";
        return each.context.key === fieldName;
      });
    }

    return error
      ? //? error.message //+ ", " + error.context.key + ", " + error.type
        message !== ""
        ? message
        : error.message
      : "";
  }

  errorClassName(fieldName, default_class = "input") {
    return this.displayErrorMessage(fieldName) === ""
      ? default_class
      : default_class + " is-danger animated shake";
  }
  async componentDidMount() {
    let { access_token = "", access_token_exp = 0 } = this.props.loginReducer; //this.state;
    //console.log(this.props.loginReducer);
    if (access_token === "") {
      // || access_token_exp < Date.now()
      //console.log("actionGetAccessToken");
      const request_token = localStorage.getItem("request_token");
      await this.props.dispatch(actionGetAccessToken(request_token));
      //console.log(this.props.loginReducer.access_token);
    }
    //access_token = this.props.loginReducer.access_token;
    
    this.setState({
      formData: {
        ...this.state.formData,
        yourName:
          this.props.loginReducer.userinfo.firstname +
          " " +
          this.props.loginReducer.userinfo.lastname
      }
    });
    
  }
  componentDidUpdate(){
    const admin_dashboard = this.getAdminDashBoard();
  }
  async getAdminDashBoard(){
    try{
      const { access_token } = this.props.loginReducer;
      const { propertyCode} = await this.props.propertyReducer;
      if(propertyCode!==""){
        //console.log(propertyCode);
        const res = await axios.get(api.MA_API + "dashboard/get_admin_hotel?propertycode="+
          propertyCode, {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        });
        //console.log(res);
        let email="";
        if(res.data.data.length>0)
        {
          res.data.data.map(each=>{
            email+=","+each.UserEmail
          });
          email=email.substring(1);
          return {code:200, email};
        }
        //console.log(email);
        return {code:500, message: "No Admin"};
      }
    } catch (err) {
      return {code:500, message: err.message};
    }
  }
}

const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    propertyReducer: state.propertyReducer
  };
};
export default connect(mapStateToProps)(ReportAProblem);
