
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { lang } from "Lang.jsx";
import moment from 'moment';
import axios from "axios";
import "assets/css/contact.scss";
import Iframe from 'react-iframe'
import { actionGetAccessToken } from "actions/login";
import { actionLoadContactData, actionLoadHotelContactData } from "actions/contact/actionContact";
import HotelContact from "components/contact/HotelContact.jsx";
import EmployeeContact from "components/contact/EmployeeContact.jsx";
import GroupEmails from "components/contact/GroupEmails.jsx";
import Administrator from "components/contact/Administrator.jsx";
import ManageHotelContact from "components/contact/ManageHotelContact.jsx";
import qs from "query-string";
import { faBackward, faDownload } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faPrint, faBook, faUser, faFolderOpen } from "@fortawesome/free-solid-svg-icons";

class IndexContact extends Component {
  state = {
    loading: '0',
    data: [],
    dataHotels: [],
    CountryCode: [],
    firstLoadPage:true,
    currentlocation:'1',
    Search:''
  }
  
  render() {
    const {activeClass ,prePropslocation,currentlocation} = this.state;
    //const ldap = require("ldap");
    const path = this.props.location.pathname.split("/")[2]; 
    const CurrentURL = "contact/" + path;
    //console.log(currentlocation);
    let arrDisplay = [];
    if(path == 'hotel_contact'){
        arrDisplay.push(
        <div key={"content" + activeClass}>
          <div className="column clear">
            <HotelContact />
          </div>
        </div>
        );
    }
    if(path == 'employee_contact'){
        arrDisplay.push(
        <div key={"content" + activeClass}>
          <div className="column clear">
            <EmployeeContact />
          </div>
        </div>
        );
    }
    if(path == 'group_emails'){
        arrDisplay.push(
        <div key={"content" + activeClass}>
            <div className="column clear">
                <GroupEmails />
            </div>
        </div>
        );
    }
    if(path == 'contact_admin'){
        arrDisplay.push(
        <div key={"content" + activeClass}>
            <div className="column clear">
                <Administrator  CurrentURL={CurrentURL} prePropslocation={prePropslocation} currentlocation={currentlocation}/>
            </div>
        </div>
        );
    }
    if(path == 'manage_hotel_contact'){
      arrDisplay.push(
        <div key={"content" + activeClass}>
            <div className="column clear">
                <ManageHotelContact />
            </div>
        </div>
      );
  }
    return (
      <div className="notification">
        <div>
            {arrDisplay}   
        </div>                   
      </div>
    );
  }
  
  
  async componentWillMount() {
    //console.log("componentWillUnmount...");
  }
  async componentWillUnmount() {
   // console.log("componentWillUnmount...");
  }
  async componentDidUpdate(prevProps) {
    const url_page = (prevProps.location.search == '' ?'1':prevProps.location.search);
    //console.log(prevProps);
    //console.log(this.props);
    let page_ = url_page.split('=')[1];    

    const curPage = (this.props.location.search == '' ?'1':this.props.location.search);
    let curPage_ = curPage.split('=')[1];
    if(page_ != curPage_){
        //console.log(this.props.location.search);
        this.setState({
            prePropslocation: prevProps.location.search,
            currentlocation: this.props.location.search
        });
    }
    //console.log(this.props.location.search);
    
  }
  async componentDidMount() {
    //console.log(prevProps.location.search);
    //console.log(this.props.location.search);
    const page = (typeof this.props.location.search == 'undefined' || this.props.location.search == ''?'1':this.props.location.search );
    //console.log(page);
    this.setState({
        currentlocation: page
    });
    //console.log("componentDidMount");
    // var client = ldap.createClient({
    //     url: "ldap://52.8.108.162"
    // });
  }
}
const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    propertyReducer: state.propertyReducer,
    hrReducer: state.hrReducer,
    contactReducer: state.contactReducer,
    s3Reducer: state.s3Reducer
  };
};
export default connect(mapStateToProps)(IndexContact);
