
import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { lang } from "Lang.jsx";
import { base_url_webapp, ENV, uniqid} from "Config_API.jsx";
import axios from "axios";
/** library style sheet*/
import "assets/css/hr.scss";
import qs from "query-string";
import ReactLoading from 'react-loading';
import "animate.css/animate.min.css";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import Joi from "joi-browser";
/** action  */
import { actionGetAccessToken } from "actions/login";
import { actionLoadHRData } from "actions/hr/actionHR";
/** components for page */
import Pagination from "components/Pagination.jsx";
import TitleMenu from "components/TitleMenu.jsx";
import Select from "components/Select.jsx";
import HRCard from "components/hr/HRCard.jsx";
import HRList from "components/hr/HRList.jsx";
import Policy from "components/hr/Policy.jsx";
import Employee from "components/hr/Employee.jsx";
import JobDescription from "components/hr/JobDescription.jsx";
import Contract from "components/hr/Contract.jsx";
import StatisticReport from "components/hr/StatisticReport.jsx";
import Appraisal from "components/hr/Appraisal";
import Organization from "components/hr/Organization";

/** fontawesome */
import { faSearch, faPrint, faBook, faUser, faFileAlt, faCommentsDollar } from "@fortawesome/free-solid-svg-icons";
import { faFileAlt as farFileAlt } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


const schema = Joi.object().keys({
  PasswordIDN: Joi.string()
    .max(50)
    .required(),
  PasswordJPN: Joi.string()
    .max(255)
    .required(),
  PasswordPHI: Joi.string()
    .max(50)
    .required(),
  PasswordTHA: Joi.string()
    .max(50)
    .required(),
});
class IndexHR extends Component {

  state = {
    loading: '0',
    data: [],
    activeClass: 'announcement',
    active: false,
    page: 1,
    HRCurrentPage: 'announcement',
    boolPolicyCheckedAll: false,
    arrPolicyItems: [],
    arrPolicyAll: [],
    IsPrint: true,
    PageSize: 10,
    BtnAppraisal: false,
    userinfo: [],
    employeePassword: [],
    isAuhEmp: false,
    templates: "",
    btnLoginEmp: false,
    errors: [],
    errorFiles: [],
    selectReport: "",
    isMobile: window.innerWidth <= 500,
  }
  notify = (type, message) => {
    const config = {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true
    };
    if (type === "error") {
      toast.error(message, config);
    } else if (type === "success") {
      toast.success(message, config);
    } else if (type === "warn") {
      toast.warn(message, config);
    } else if (type === "info") {
      toast.info(message, config);
    }
  };
 
  componentWillMount() {
    console.log(window.innerWidth );
    console.log("componentWillMount");
    window.addEventListener('resize', this.handleWindowSizeChange);
  }
  componentWillUnmount() {
    console.log("componentWillUnmount");
    window.removeEventListener('resize', this.handleWindowSizeChange);
  }

  handleWindowSizeChange = () => {
    this.setState({ isMobile: window.innerWidth <= 500 });
  };

  arrPage = [];
  render() {
    const { isMobile,
      userinfo,
      data,
      active,
      activeClass,
      page,
      boolPolicyCheckedAll,
      isAuhEmp,
      IsPrint,
      arrPolicyItems,
      PageSize,
      loading,
      templates,
      btnLoginEmp,
      Statistic,
      Report,
      selectReport } = this.state;
    //const isAuhEmp = localStorage.getItem("isAuhEmp")||false;
    const localAuhEmp = localStorage.getItem("isAuhEmp") || false;
    const PolicyCheckedALL = boolPolicyCheckedAll ? 'checked' : '';
    const queryString = qs.parse(this.props.location.search);
    const query = queryString.query;
    const CurrentURL = "hr/" + activeClass.replace(" ", "_");
    const langCode = localStorage.getItem("langCode");
    let aHRCountry = [];
    let country_temp = userinfo.country;
    let formPassword = "";
    // country_temp = country_temp.toLowerCase() ;
    aHRCountry = (userinfo.office === "HQ" ? ['idn', 'jpn', 'phi', 'tha'] : [country_temp]);
    const pStyle0 = {
      paddingTop: '0px',
      paddingLeft: '0px',
      paddingRight: '0px',
    };
    let items = [];
    if (!active) {
      this.getAnnouncement();
      this.setState({ active: true });
    }
    let arrDisplay = [];
    // console.log("activeClass:" + activeClass);
    if (activeClass === "announcement"
      || activeClass === "new employee"
    ) {
      arrDisplay.push(
        <div className="hr-content" key={"content" + activeClass}>
          <div id="tab-hr-search" className="column is-12 is-pulled-right control-search">
            <div className="control has-icons-left has-icons-right is-pulled-right control-search">
              <input className="input is-danger" type="text" placeholder="Search..." value={query}
                ref={input => this.search = input}
                onChange={((e) => this.handleInputChange(e, 'tab1'))}
                id="search-input"
              />
              <span className="icon is-small is-left">
                <FontAwesomeIcon icon={faSearch} />
              </span>
            </div>
          </div>
          <div className="column clear" style={pStyle0}>
            <HRList aData={data} CurrentURL={CurrentURL} PageNum={page} PageSize={PageSize} Category={activeClass} />
          </div>
        </div>
      );
    } else if (activeClass === "policy procedure") {
      if (data.length > 0) {
         arrDisplay.push(<Policy aData={data} CurrentURL={CurrentURL} PageNum={page} PageSize={PageSize} sPram={this.props.location.search}  userinfo={userinfo} />);
      } else {
        items.push(
          <div className="message" >
            <div className="message-body">
              <div className="card" >
                <div className="card-content"  >
                  <div className="media">
                    <div className="media-left">
                      No data available in {activeClass} list
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        );
      }
    } else if (activeClass === "employee profile") {
      if ((localAuhEmp || isAuhEmp) && typeof country_temp != "undefined") {
        arrDisplay.push(
          <div>
            <Employee aHRCountry={aHRCountry} sPram={this.props.location.search} />
          </div>
        );
      } else {
        arrDisplay.push(
          <div key="content-employee">
            <div className="column is-12" > </div>
            <div className="column is-12" >
              <label style={{ fontSize: "1.5rem", fontWeight: "normal" }}>Enter Password</label>
              <hr />
              {ENV !== "PROD" ? <div><font color="red">{"IDN:123456, JPN:1234567, PHI:1234, THA:12345"}</font></div> : ""}
            </div>
            {
              (aHRCountry.indexOf('idn') >= -1) ? <div className="field is-horizontal">
                <div className="field-label is-normal">
                  <label className="label" htmlFor="field-label">Indonesia</label>
                </div>
                <div className="field-body">
                  <div className="field">
                    <p className="control has-icons-left">
                      <input className="input password-hr" name="PasswordIDN" type="password" placeholder="Password"
                        id={"password-idn"} />
                      <span className="icon is-small is-left">
                        <i className="fas fa-lock"></i>
                      </span>
                    </p>
                    <div className="has-text-danger">
                      {this.displayErrorMessage("PasswordIDN")}
                    </div>
                  </div>
                </div>
              </div> : ""}
            {
              (aHRCountry.indexOf('jpn') >= -1) ? <div className="field is-horizontal">
                <div className="field-label is-normal">
                  <label className="label" htmlFor="field-label">Japan</label>
                </div>
                <div className="field-body">
                  <div className="field">
                    <p className="control has-icons-left">
                      <input className="input password-hr" name="PasswordJPN" type="password" placeholder="Password"
                        id={"password-jpn"} />
                      <span className="icon is-small is-left">
                        <i className="fas fa-lock"></i>
                      </span>
                    </p>
                    <div className="has-text-danger">
                      {this.displayErrorMessage("PasswordJPN")}
                    </div>
                  </div>
                </div>
              </div> : ""
            }
            {
              (aHRCountry.indexOf('phi') >= -1) ? <div className="field is-horizontal">
                <div className="field-label is-normal">
                  <label className="label" htmlFor="field-label">Philippines</label>
                </div>
                <div className="field-body">
                  <div className="field">
                    <p className="control has-icons-left">
                      <input className="input password-hr" name="PasswordPHI" type="password" placeholder="Password"
                        id={"password-phi"} />
                      <span className="icon is-small is-left">
                        <i className="fas fa-lock"></i>
                      </span>
                    </p>
                    <div className="has-text-danger">
                      {this.displayErrorMessage("PasswordPHI")}
                    </div>
                  </div>
                </div>
              </div> : ""
            }
            {
              (aHRCountry.indexOf('tha') >= -1) ? <div className="field is-horizontal">
                <div className="field-label is-normal">
                  <label className="label" htmlFor="field-label">Thailand</label>
                </div>
                <div className="field-body">
                  <div className="field">
                    <p className="control has-icons-left">
                      <input className="input password-hr" name="PasswordTHA" type="password" placeholder="Password"
                        id={"password-tha"} />
                      <span className="icon is-small is-left">
                        <i className="fas fa-lock"></i>
                      </span>
                    </p>
                    <div className="has-text-danger">
                      {this.displayErrorMessage("PasswordTHA")}
                    </div>
                  </div>
                </div>
              </div> : ""
            }
            <div className="field is-horizontal">
              <div className="field-label is-normal">
              </div>
              <div className="field-body">
                <div className="field">
                  <p className="control has-icons-left">
                    <button
                      className={btnLoginEmp === true ? "button is-danger is-loading" : "button is-danger"}
                      onClick={e => {
                        this.setState({ btnLoginEmp: true });
                        this.handleClickEmployee();

                      }}
                    > {"Submit"} </button>
                  </p>
                </div>
              </div>
            </div>
          </div>
        );
      }
    } else if (activeClass === "administration") {
      arrDisplay.push(<div key={"administration-"+uniqid()} className="column clear" style={pStyle0}>
        <div className="column is-12" >
          <label style={{ fontSize: "1.5rem", fontWeight: "normal" }}>{"Employee Profile"}</label>
          <hr/>
        </div>
        <div className="columns is-multiline hr-admin-emp">
          <div className="column is-4" >
            <a href={base_url_webapp + langCode + "/hr/employee/addnew"} target={"_blank"} className={"button is-danger min-width240"}> {"Create new employee profile"} </a>
          </div>
          <div className="column is-8"  style={isMobile?{paddingTop:"0"}:{}}>
            <a href={base_url_webapp + langCode + "/hr/config_key"}
              target={"_blank"}
              className={"button is-danger min-width240"}
            > {"Change password for encryption "} </a>
          </div>
          <div className="column is-12"  style={{paddingTop:"0"}}>
            <a href={base_url_webapp + langCode + "/hr/employee/replace_temp"} target={"_blank"} className={"button is-danger min-width240"}> {"Temporary Supervisor"} </a>
          </div>
          <div className="column is-12"  style={{paddingTop:"0"}}>
            <button className={"button is-danger is-inverted min-width240"} > {" Manage Announcement "} </button>
          </div>
        </div>
        <ul className="management-announcement">
          <li> <a className="has-text-danger" href={base_url_webapp + langCode + "/admin/webportal"} target="_blank"> Announcement list </a> </li>
          <li> <a className="has-text-danger" href={base_url_webapp + langCode + "/admin/webportal/addnew_announcement/hr/announcement"} target="_blank"> Create announcement </a> </li>
          <li> <a className="has-text-danger" href={base_url_webapp + langCode + "/admin/webportal/addnew_announcement/hr/new_employee"} target="_blank"> Create new employee announcement </a> </li>
          <li> <a className="has-text-danger" href={base_url_webapp + langCode + "/admin/webportal/addnew_announcement/hr/policy_procedure"} target="_blank"> Upload policy &amp; procedure </a> </li>
          <li> <a className="has-text-danger" href={base_url_webapp + langCode + "/admin/webportal/addnew_announcement/hr/template"} target="_blank"> Upload template </a> </li>
          <li> <a className="has-text-danger" href={base_url_webapp + langCode + "/admin/webportal/organization_chart"} target="_blank"> Organisation chart list </a> </li>
          <li> <a className="has-text-danger" href={base_url_webapp + langCode + "/admin/webportal/addnew_announcement/hr/organization_chart"} target="_blank"> Upload organisation chart </a> </li>
          <li> <a className="has-text-danger" href={base_url_webapp + langCode + "/admin/webportal/addnew_announcement/hr/application_form"} target="_blank"> Upload Application Form  </a> </li>
          <li> <a className="has-text-danger" href={base_url_webapp + langCode + "/admin/webportal/application_form"} target="_blank"> Application Form list </a> </li>
          <li> <a className="has-text-danger" href={base_url_webapp + langCode + "/admin/webportal/addnew_announcement/hr/company_regulation"} target="_blank"> Upload Company Regulation </a> </li>
          <li> <a className="has-text-danger" href={base_url_webapp + langCode + "/admin/webportal/company_regulation"} target="_blank"> Company Regulation list </a> </li>
        </ul>
      </div >
      );
    } else if (activeClass === "template") {
      arrDisplay.push(
        <div key={"template-"+uniqid()} className="column clear" >
          {templates === "" ? <div className="column is-12" >
            <Link
              to={"/hr/template/job_description"}
              className={"button is-danger min-width150"}
              onClick={e => {
                this.setState({ templates: "job description" });
              }}
            > {"Job Description "}
            </Link>
          </div> : ""}
          {templates === "" ? <div className="column is-12" >
            <Link
              to={"/hr/template/contract"}
              className={"button is-danger min-width150"}
              onClick={e => {
                this.setState({ templates: "contract" });
              }}
            > {"Contract"} </Link>
          </div> : ""}
          {templates === "job description" ? <JobDescription aData={data} page={page} /> : ""}
          {templates === "contract" ? <Contract aData={data} page={page} /> : ""}
        </div >
      );
    } else if (activeClass === "statistic report") {
      arrDisplay.push(
        <StatisticReport key="" />
      );

    } else if (activeClass === "organization chart") {
      arrDisplay.push( 
      <Organization aData={data} CurrentURL={CurrentURL} PageNum={page} PageSize={PageSize} Category={activeClass} />
      );
    } else if (activeClass === "company regulation") {
      let aOption = [];
      const Country = localStorage.getItem("countryCode").toUpperCase();
      aOption = [{ value: "", label: '--- Select Entity ---' }, { value: "", label: 'AAAAAAAAA' }];
      arrDisplay.push(
        <div key={"content" + uniqid()}>
          <div id="tab-company-search" className="column is-12 is-pulled-right control-search">
            {(Country === "IDN" ? <div className="column is-6 is-pulled-left">
              <Select aOption={aOption} />
            </div> : "")
            }
            <div className="column is-6 is-pulled-right">
              <div className="control has-icons-left has-icons-right is-pulled-right control-search">
                <input className="input is-danger" type="text" placeholder="Search..." value={query}
                  ref={input => this.search = input}
                  onChange={((e) => this.handleInputChange(e, 'tab1'))}
                  id="search-input"
                />
                <span className="icon is-small is-left">
                  <FontAwesomeIcon icon={faSearch} />
                </span>
              </div>
            </div>
          </div>
          <div className="column clear" style={pStyle0}>
            <HRCard aData={data} CurrentURL={CurrentURL} PageNum={page} PageSize={PageSize} Category={activeClass} />
          </div>
        </div>
      );
    } else if (activeClass === "appraisal") {
      arrDisplay.push(
        <Appraisal aData={data} CurrentURL={CurrentURL} PageNum={page} PageSize={PageSize} Category={activeClass} />
      );
    }
    if (loading !== 1) {
      arrDisplay = [];
      arrDisplay.push(<div className="column is-12" key={uniqid()}> 
      {/* <ReactLoading type={"spinningBubbles"} className="hr-loading" /> */}
        <ReactLoading type={"spokes"} color={"#4a4a4a"} className="hr-loading" />
        <div className="hr-loading">{"Loading..."}</div>
      </div>);
    }
    return (
      <div className="notification">
        <div className="left-title">{"HR"}</div>
        <TitleMenu title={activeClass === "statistic report" ? "STATISTIC/REPORT" : activeClass.toUpperCase()} />
        <div id="frame-hr" className="columns ">
          {!isMobile ? <div id="tab-hr" className="column is-2"  >
            <ul className="menu-list">
              <li className={activeClass == 'announcement' ? 'active' :""}><Link
                onClick={e => {
                  this.setState({ active: false, activeClass: "announcement", loading: 0 });
                }}
                to={"/hr/announcement"} id="announcement" className={'navbar-item'} >{"Announcements"}</Link></li>
              <li className={activeClass == 'new employee' ? 'active' : ""}><Link
                onClick={e => {
                  this.setState({ active: false, activeClass: "new employee", loading: 0 });
                }}
                to="/hr/new_employee" id="new_employee" className={'navbar-item'}>{"New Employees"}</Link></li>
              <li className={activeClass == 'policy procedure' ? 'active' : ""}><Link
                onClick={e => {
                  this.setState({ active: false, activeClass: "policy procedure", loading: 0 });
                }}
                to="/hr/policy_procedure" id="policies_procedure" className={'navbar-item'}>{"Policies & Procedures"}</Link></li>
              <li className={activeClass == 'employee profile' ? 'active' : ""}><Link
                onClick={e => {
                  this.setState({ active: false, activeClass: "employee profile", loading: 0 });
                }}
                to="/hr/employee_profile" id="employee_profile" className={'navbar-item'}>{"Employee Profile"}
                &nbsp;<span className="icon_unlocked">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
              </Link>
              </li>
              <li className={activeClass == 'administration' ? 'active' : ""}><Link
                onClick={e => {
                  this.setState({ active: false, activeClass: "administration", loading: 0 });
                }}
                to={"/hr/administration"} id="administration" className={'navbar-item'}>{"Administration"}
                &nbsp;<span className="icon_unlocked">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
              </Link>
              </li>
              <li className={activeClass == 'template' ? 'active' : ""}><Link
                onClick={e => {
                  this.setState({ active: false, activeClass: "template", templates: "" });
                }}
                to={"/hr/template"} id="templates" className={'navbar-item'}>{"Templates"}
                &nbsp;<span className="icon_unlocked">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
              </Link></li>
              <li className={activeClass == 'statistic report' ? 'active' : ""}><Link
                onClick={e => {
                  this.setState({ active: false, activeClass: "statistic report", loading: 0 });
                }}
                to={"/hr/statistic_report"} id="statisti_report" className={'navbar-item'}>{"Statistic/Report"}
                &nbsp;<span className="icon_unlocked">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
              </Link></li>
              <li className={activeClass == 'organization chart' ? 'active' : ""}><Link
                onClick={e => {
                  this.setState({ active: false, activeClass: "organization chart", loading: 0 });
                }}
                to={"/hr/organization_chart"} id="organization_chart" className={'navbar-item'}>
                {"Organisation Charts"}
              </Link></li>
              <li className={activeClass == 'company regulation' ? 'active' : ""}><Link
                onClick={e => {
                  this.setState({ active: false, activeClass: "company regulation", loading: 0 });
                }}
                to={"/hr/company_regulation"} id="company_regulation" className={'navbar-item'}>
                {"Company Regulation"}
              </Link></li>
              <li className={activeClass == 'appraisal' ? 'active' : ""}><Link
                onClick={e => {
                  this.setState({ active: false, activeClass: "appraisal", loading: 0 });
                }}
                to={"/hr/appraisal"} id="appraisal" className={'navbar-item'}>
                {"Appraisals"}
              </Link></li>
            </ul>
          </div> : ""}
          {!isMobile ? <div 
            className="column is-10 "
            style={{ padding: "0"}}
          >{arrDisplay}
          </div> : ""}
          {isMobile ? <div className="accordions">
            <article className={this.state.activeClass === "announcement" ? "accordion is-active" : "accordion"}>
              <Link
                className={this.state.activeClass === "announcement" ? "accordion-header toggle navbar-link" : "accordion-header toggle"}
                onClick={e => {
                  this.setState({ active: false, activeClass: "announcement", loading: 0 });
                }}
                to={"/hr/announcement"}
              >
                <p>Announcements</p>
              </Link>
              <div className="accordion-body">
                <div className="accordion-content">
                  {this.state.activeClass === "announcement" ? arrDisplay : ""}
                </div>
              </div>
            </article>
            <div className={this.state.activeClass === "new employee" ? "accordion is-active" : "accordion"}>
              <Link
                className={this.state.activeClass === "new employee" ? "accordion-header toggle navbar-link" : "accordion-header toggle"}
                onClick={e => {
                  this.setState({ active: false, activeClass: "new employee", loading: 0 });
                }}
                to={"/hr/new_employee"}
              >
                <p>New Employee</p>
              </Link>
              <div className="accordion-body">
                <div className="accordion-content">
                  {this.state.activeClass === "new employee" ? arrDisplay : ""}
                </div>
              </div>
            </div>
            <div className={this.state.activeClass === "policy procedure" ? "accordion is-active" : "accordion"}>
              <Link
                className={this.state.activeClass === "policy procedure" ? "accordion-header toggle navbar-link" : "accordion-header toggle"}
                onClick={e => {
                  this.setState({ active: false, activeClass: "policy procedure", loading: 0 });
                }}
                to={"/hr/policy_procedure"}
              >
                <p>Policies & Procedures</p>
              </Link>
              <div className="accordion-body">
                <div className="accordion-content">
                  {this.state.activeClass === "policy procedure" ? arrDisplay : ""}
                </div>
              </div>
            </div>
            <div className={this.state.activeClass === "employee profile" ? "accordion is-active" : "accordion"}>
              <Link
                className={this.state.activeClass === "employee profile" ? "accordion-header toggle navbar-link" : "accordion-header toggle"}
                onClick={e => {
                  this.setState({ active: false, activeClass: "employee profile", loading: 0 });
                }}
                to={"/hr/employee_profile"}
              >
                <p>Employee Profile &nbsp;<span className="icon_unlocked">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
              </Link>
              <div className="accordion-body">
                <div className="accordion-content">
                  {this.state.activeClass === "employee profile" ? arrDisplay : ""}
                </div>
              </div>
            </div>
            <div className={this.state.activeClass === "administration" ? "accordion is-active" : "accordion"}>
              <Link
                className={this.state.activeClass === "administration" ? "accordion-header toggle navbar-link" : "accordion-header toggle"}
                onClick={e => {
                  this.setState({ active: false, activeClass: "administration", loading: 0 });
                }}
                to={"/hr/administration"}
              >
                <p>Administration &nbsp;<span className="icon_unlocked">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
              </Link>
              <div className="accordion-body">
                <div className="accordion-content">
                  {this.state.activeClass === "administration" ? arrDisplay : ""}
                </div>
              </div>
            </div>
            <div className={this.state.activeClass === "template" ? "accordion is-active" : "accordion"}>
              <Link
                className={this.state.activeClass === "template" ? "accordion-header toggle navbar-link" : "accordion-header toggle"}
                onClick={e => {
                  this.setState({ active: false, activeClass: "template", loading: 0 });
                }}
                to={"/hr/template"}
              >
                <p>Templates &nbsp;<span className="icon_unlocked">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
              </Link>
              <div className="accordion-body">
                <div className="accordion-content">
                  {this.state.activeClass === "template" ? arrDisplay : ""}
                </div>
              </div>
            </div>
            <div className={this.state.activeClass === "statistic report" ? "accordion is-active" : "accordion"}>
              <Link
                className={this.state.activeClass === "statistic report" ? "accordion-header toggle navbar-link" : "accordion-header toggle"}
                onClick={e => {
                  this.setState({ active: false, activeClass: "statistic report", loading: 0 });
                }}
                to={"/hr/statistic_report"}
              >
                <p>Statistic/Report &nbsp;<span className="icon_unlocked">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
              </Link>
              <div className="accordion-body">
                <div className="accordion-content">
                  {this.state.activeClass === "statistic report" ? arrDisplay : ""}
                </div>
              </div>
            </div>
            <div className={this.state.activeClass === "organization chart" ? "accordion is-active" : "accordion"}>
              <Link
                className={this.state.activeClass === "organization chart" ? "accordion-header toggle navbar-link" : "accordion-header toggle"}
                onClick={e => {
                  this.setState({ active: false, activeClass: "organization chart", loading: 0 });
                }}
                to={"/hr/organization_chart"}
              >
                <p>Organisation Charts </p>
              </Link>
              <div className="accordion-body">
                <div className="accordion-content">
                  {this.state.activeClass === "organization chart" ? arrDisplay : ""}
                </div>
              </div>
            </div>
            <div className={this.state.activeClass === "company regulation" ? "accordion is-active" : "accordion"}>
              <Link
                className={this.state.activeClass === "company regulation" ? "accordion-header toggle navbar-link" : "accordion-header toggle"}
                onClick={e => {
                  this.setState({ active: false, activeClass: "company regulation", loading: 0 });
                }}
                to={"/hr/company_regulation"}
              >
                <p>Company Regulation </p>
              </Link>
              <div className="accordion-body">
                <div className="accordion-content">
                  {this.state.activeClass === "company regulation" ? arrDisplay : ""}
                </div>
              </div>
            </div>
            <div className={this.state.activeClass === "appraisal" ? "accordion is-active" : "accordion"}>
              <Link
                className={this.state.activeClass === "appraisal" ? "accordion-header toggle navbar-link" : "accordion-header toggle"}
                onClick={e => {
                  this.setState({ active: false, activeClass: "appraisal", loading: 0 });
                }}
                to={"/hr/appraisal"}
              >
                <p>Appraisals</p>
              </Link>
              <div className="accordion-body">
                <div className="accordion-content">
                  {this.state.activeClass === "appraisal" ? arrDisplay : ""}
                </div>
              </div>
            </div>
          </div> : ""}
        </div >
      </div>
    );

  }

  handleClickPrintPolicy = () => {
    const { arrPolicyItems } = this.state;
    document.getElementById("chksent").value = JSON.stringify(arrPolicyItems);
    document.getElementById("frm-check-key").submit();
  }
  handleClickEmployee = () => {
    const { arrPolicyItems } = this.state;
    const PasswordIDN = document.getElementById("password-idn").value;
    const PasswordJPN = document.getElementById("password-jpn").value;
    const PasswordPHI = document.getElementById("password-phi").value;
    const PasswordTHA = document.getElementById("password-tha").value;
    const { error } = Joi.validate(
      { PasswordIDN, PasswordJPN, PasswordPHI, PasswordTHA },
      schema,
      {
        abortEarly: false
      }
    );
    const errors = error ? error.details : [];
    console.log(errors);
    this.setState({
      errors
    });
    const param = {
      "key": {
        "IDN": PasswordIDN,
        "JPN": PasswordJPN,
        "PHI": PasswordPHI,
        "THA": PasswordTHA
      }
    };
    if (errors.length === 0) {
      this.chk_key(param);
    } else {
      this.setState({ btnLoginEmp: false });
    }
  }

  async chk_key(param) {
    const { userinfo } = this.props.loginReducer;
    console.log(userinfo);
    const langCode = localStorage.getItem("langCode") || "en";
    const sPram = '/api/auto_login/do_key/'+userinfo.usercode+'/'+userinfo.country+'/'+(userinfo.office=="HQ"?"CORP":"")
    await axios.post(base_url_webapp + langCode + sPram, {
      data: param
    },
      {
        headers: {
          //'Content-Type': 'application/json'
          //'Content-Type': 'multipart/form-data'
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }
    ).then(response => {
      if (response.status === 200) {
        this.notify("success", lang("Success"));
        this.setState({ isAuhEmp: true, btnLoginEmp: false });
        localStorage.setItem("isAuhEmp", true);
      } else {
        this.notify("error", response.ret_msg);
      }
      // console.log(response);
    })
      .catch(error => {
        console.log(error);
        this.notify("error", 'Incorrect password');
        this.setState({ btnLoginEmp: false });
      });
  }
  handleInputChange = (e, tabs) => {
    const { activeClass } = this.state;
    this.search.value = encodeURI(this.search.value);
    const queryString = qs.parse(this.props.location.search);
    const page = queryString.p || 1;
    const p_url = "p=" + page + "&query=" + this.search.value;
    this.props.history.push('/hr/' + activeClass + '?' + p_url);
    this.getAnnouncement();
  }
  handleCheckboxChangeAll(e) {
    const { value, checked } = e.target;
    const { arrPolicyAll } = this.state;
    let { arrPolicyItems } = this.state;
    if (checked) {
      // arrPolicyItems =arrPolicyAll ;
      arrPolicyAll.map(function (item, i) {
        arrPolicyItems.push(item);
      });
    } else {
      arrPolicyItems = [];
    }
    arrPolicyItems = (checked) ? arrPolicyAll : []
    this.setState({
      boolPolicyCheckedAll: checked,
      IsPrint: arrPolicyItems.length > 0 ? false : true,
      arrPolicyItems: arrPolicyItems
    });
  }
  handleCheckboxChange(e) {
    const { value, checked } = e.target;
    let { arrPolicyItems, boolPolicyCheckedAll } = this.state;
    const arrPolicyAll = this.props.hrReducer.data[0].AnnouncementIDAll.split(",");
    if (boolPolicyCheckedAll && checked) {
      arrPolicyAll.map(function (item, i) {
        arrPolicyItems.push(item);
      });
    } else {
      if (checked) {
        const index = arrPolicyItems.indexOf(value);
        if (index === -1) {
          arrPolicyItems.push(value);
        }
      } else {
        const index = arrPolicyItems.indexOf(value);
        if (index !== -1) {
          arrPolicyItems.splice(index, 1);
        }
      }
    }
    this.setState({
      boolPolicyCheckedAll: arrPolicyItems.length == arrPolicyAll.length ? true : false,
      IsPrint: arrPolicyItems.length > 0 ? false : true,
      arrPolicyItems: arrPolicyItems
    });
  }
  displayErrorMessage(fieldName, fieldType = "text") {
    const { errors, errorFiles } = this.state;
    let error = {};
    let message = "";
    if (fieldType === "file") {
      error = errorFiles[fieldName];
    } else {
      error = errors.find(each => {
        message = lang(each.context.key + "." + each.type) || "";
        return each.context.key === fieldName;
      });
    }
    return error
      ? //? error.message //+ ", " + error.context.key + ", " + error.type
      message !== ""
        ? message
        : error.message
      : "";
  }

  errorClassName(fieldName, default_class = "input") {
    return this.displayErrorMessage(fieldName) === ""
      ? default_class
      : default_class + " is-danger animated shake";
  }
  async componentDidUpdate(prevProps) {
    //To Do....
    //console.log(this.props.location.pathname);
    //console.log(prevProps.location.pathname);
    //Back to page
    if (this.props.location.pathname === "/hr/template"
      && prevProps.location.pathname !== "/hr/template") {
      this.setState({
        IsActive: false, templates: ""
      });
    }
    const prevPropsquery = qs.parse(prevProps.location.search);
    const queryString = qs.parse(this.props.location.search);
    const page = typeof queryString.p == "undefined" ? 1 : queryString.p;
    if (this.state.page !== page || prevPropsquery.country !== queryString.country) {
      this.getAnnouncement();
    }
  }
  async componentDidMount() {
    //console.log("componentDidMount");
    let { access_token = "", access_token_exp = 0 } = this.props.loginReducer; //this.state;
    if (access_token === "") {
      const request_token = localStorage.getItem("request_token");
      await this.props.dispatch(actionGetAccessToken(request_token));
    }
    await this.getAnnouncement();
    const pathname = this.props.location.pathname;
    if (pathname.indexOf("job_description") > 0) {
      this.setState({ templates: "job description" });
    } else if (pathname.indexOf("contract") > 0) {
      this.setState({ templates: "contract" });
    }
  }
  async getAnnouncement() {
    let Category = 'announcement';
    let query = "";
    try {
      const { userinfo, access_token } = this.props.loginReducer;
      const queryString = qs.parse(this.props.location.search);
      const page = typeof queryString.p == "undefined" ? 1 : queryString.p;
      try {
        Category = this.props.location.pathname.split("/")[2].replace("_", " ");
      } catch (error) {
        Category = 'announcement'
      }
      if (Category === "employee profile") {
        this.setState({
          loading: 1,
          data: [],
          activeClass: Category,
          page: page,
          userinfo: userinfo
        });
      } else {
        if (document.getElementById("search-input") !== null) {
          query = document.getElementById("search-input").value;
        }
        const Type = "announcement";
        const Country = localStorage.getItem("countryCode").toUpperCase();
        const param = {
          AnnouncementID: "",
          Category: Category,
          Department: "HR",
          StartDate: "",
          EndDate: "",
          Country: Country,
          PinnedCalendar: "",
          CreateBy: "",
          AnnouncementStatus: "OPEN",
          PageNum: page,
          PageSize: "10",
          Search: query,
          Subtemplate: this.state.templates 
        }
        await this.props.dispatch(actionLoadHRData(access_token, Type, param));
        // console.log(this.props.hrReducer.data[0].AnnouncementIDAll.split(","));
        if (this.props.hrReducer.data.length > 0) {
          this.setState({
            loading: 1,
            data: this.props.hrReducer.data,
            activeClass: Category,
            page: page,
            arrPolicyAll: this.props.hrReducer.data[0].AnnouncementIDAll.split(","),
            userinfo: userinfo
          });
        } else {
          this.setState({
            loading: 1,
            data: [],
            activeClass: Category,
            page: page
          });
        }
      }
    } catch (err) {
      console.log("error");
      console.log(Category);
      console.log({ code: 500, message: err.message });
      this.setState({
        loading: 1,
        data: [],
        activeClass: Category
      });
    }
  }
}

const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    usersReducer: state.usersReducer,
    propertyReducer: state.propertyReducer,
    hrReducer: state.hrReducer,
    countryReducer: state.countryReducer,
  };
};
export default connect(mapStateToProps)(IndexHR);
