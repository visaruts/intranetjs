import React, { Component } from "react";
import { Prompt } from "react-router";
import { connect } from "react-redux";
import { lang } from "../Lang.jsx";
import { api, ENV } from "../Config_API.jsx";
import { view } from "../templates/email/insert_ma.jsx";
import axios from "axios";
import moment from 'moment';
import { actionHiddenNavbar, actionHiddenFooter } from "../actions/config";
import { actionGetAccessToken } from "../actions/login";

class Home extends Component {
  state = {}
  render() {

    return (
      <div className="home-bg">
        <div className="form_dep" 
          style={{
            marginTop:"105px", 
            marginLeft:"100px", 
            transform:"rotate(5deg)",
            fontSize: "52px"
          }}
        >                                  	
          <a className="label-title" href="/hr">
            <div style={{height:"50px"}}>&nbsp;
            </div>
              <div style={{height:"110px", width:"160px", marginLeft:"10px"}} className="department text-center">
                <strong>HR</strong>                    
              </div>
          </a>
        </div>

        <div className="form_dep" 
          style={{
            marginTop:"325px", 
            marginLeft:"150px", 
            transform:"rotate(-15deg)",
            fontSize: "40px"
          }}
        >  
          <a className="label-title" href="https://intranet.redplanethotels.com/en/hr">
            <div style={{height:"50px"}}>&nbsp;
            </div>
              <div style={{height:"110px", width:"160px", marginLeft:"10px"}} className="department text-center">
                <strong>Training</strong>                    
              </div>
          </a>
        </div>

        <div className="form_dep" 
          style={{
            marginTop:"185px", 
            marginLeft:"350px", 
            transform:"rotate(-10deg)",
            fontSize: "20px"
          }}
        >  
          <a className="label-title" href="https://intranet.redplanethotels.com/en/hr">
            <div style={{height:"50px"}}>&nbsp;
            </div>
              <div style={{height:"110px", width:"160px", marginLeft:"10px"}} className="department text-center">
                <strong>Marketing & Communication</strong>                    
              </div>
          </a>
        </div>

        <div className="form_dep" 
          style={{
            marginTop:"405px", 
            marginLeft:"500px", 
            transform:"rotate(-10deg)",
            fontSize: "40px"
          }}
        >  
          <a className="label-title" href="https://intranet.redplanethotels.com/en/hr">
            <div style={{height:"50px"}}>&nbsp;
            </div>
              <div style={{height:"110px", width:"160px", marginLeft:"10px"}} className="department text-center">
                <strong>Finance</strong>                    
              </div>
          </a>
        </div>

        <div className="form_dep" 
          style={{
            marginTop:"105px", 
            marginLeft:"830px", 
            transform:"rotate(-10deg)",
            fontSize: "40px"
          }}
        >  
          <a className="label-title" href="https://intranet.redplanethotels.com/en/hr">
            <div style={{height:"50px"}}>&nbsp;
            </div>
              <div style={{height:"110px", width:"160px", marginLeft:"10px"}} className="department text-center">
                <strong>IT</strong>                    
              </div>
          </a>
        </div>
        <div className="form_dep" 
          style={{
            marginTop:"145px", 
            marginLeft:"1060px", 
            transform:"rotate(10deg)",
            fontSize: "30px"
          }}
        >  
          <a className="label-title" href="https://intranet.redplanethotels.com/en/hr">
            <div style={{height:"50px"}}>&nbsp;
            </div>
              <div style={{height:"110px", width:"160px", marginLeft:"10px"}} className="department text-center">
                <strong>Operations</strong>                    
              </div>
          </a>
        </div>
      </div>
    );
  }
  async componentDidMount() {
    // await  this.props.dispatch(actionHiddenNavbar());
    // await  this.props.dispatch(actionHiddenFooter());
  }
}

const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    propertyReducer: state.propertyReducer,
    configReducer: state.configReducer
  };
};
export default connect(mapStateToProps)(Home);
