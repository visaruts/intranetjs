import React, { Component } from "react";
import { lang } from "../Lang.jsx";

export default class ReportAProblem extends Component {
  render() {
    console.log(this.props);
    return <div>{lang("MD_REPORT")}</div>;
  }
}
