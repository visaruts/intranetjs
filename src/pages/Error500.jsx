/**
 * Require to all page
 */
import React, { Component } from "react";
import { connect } from "react-redux";
import axios from "axios";
import qs from "query-string";
import { lang } from "../Lang.jsx";
import { Base64 } from "js-base64";
import Parser from "html-react-parser";
import { Link } from "react-router-dom";

export default class Error500 extends Component {
  render() {
    //const err=this.props.location.search.replace(/\?err\=/, "");
    const queryString = qs.parse(this.props.location.search);
    const {err,page,method}=queryString;
    return (
      <div>
        <div className="notification" style={{backgroundColor:"#fff"}}>
          <div className="columns is-vcentered">
            <div className="column has-text-centered is-one-quarter"><img src="images/NoPermissionToAccess.jpg" /></div>
            <div className="column has-text-left" style={{fontSize:"24px"}}>
              <span className="has-text-danger">
                {lang("NetworkError")}
              </span>
              <br />
              {lang("Message")}: <span className="has-text-danger">{decodeURI(err)}</span>
              <br />
              {lang("Page")}: <span className="has-text-danger">{decodeURI(page)}</span>
              <br />
              {lang("Method")}: <span className="has-text-danger">{decodeURI(method)}</span>
              <br />
              <span>{lang("captureToRPHLDeveloper")}</span>
              <hr />
              <span>
                {lang("gotoLoginPage")}{' '}
                <Link
                  to="/"
                  className="has-text-danger"
                  onClick={e => {
                    localStorage.clear();
                    window.location.href = "/";
                  }}
                >{lang("click")}</Link>
              </span>
            </div>
          </div>
        </div>
        
      </div>
    );
  }
  componentDidMount() {
    //localStorage.clear();
  }
}
