
import React, { Component } from "react";
import { Prompt } from "react-router";
import { connect } from "react-redux";
import { lang } from "Lang.jsx";
import { api, ENV } from "Config_API.jsx";
import axios from "axios";

import {actionGetAccessToken } from "actions/login";
import {actionLoadTNListData } from "actions/training/listview";
import Pagination from "components/Pagination.jsx";
import TNCard from "components/training/TNCard.jsx";
import TitleMenu from "components/TitleMenu.jsx";
import qs from "query-string";
// import {TitleMenu} from "components/TitleMenu.jsx";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
class Training extends Component {
  state = {
    loading:'0',
    TNParticipate:[],
    TNCourseList:[],
    TNPage:'CourseList',
    data:[]
  }
  render() { 
  const {data,TNPage} = this.state;
  const aTNCourseList= data||[];
  const queryString = qs.parse(this.props.location.search);
  const query = queryString.query || "";
  aTNCourseList.TYPE = TNPage ;
  //  console.log("props");
  //  console.log(this.props);
    return (
      <div className="notification">
        <TitleMenu title={("Course List")} />
        <div className="is-pulled-right">
          <button className="button is-danger" >
            <FontAwesomeIcon icon={"user-cog"} />   { " Course List For Admin" }
          </button>
        </div>
        <br></br><br></br>
        <div className="tabs is-medium is-boxed bg-training">
          <div className="columns">
            <div className="column">
              <ul>
                <li id="tab1" className="is-active has-text-danger"><a
                  onClick={((e) => this.handleClick(e, 'tab1'))}
                ><span> List Of Training Course</span></a>
                </li>
                <li id="tab2" ><a
                  onClick={((e) => this.handleClick(e, 'tab2'))}
                >Partucipated Course</a></li>
              </ul>
            </div>
          </div>
        </div>
        <div id="con-tab1" className="bg-training" >
          <div className="column is-one-quarter">
            <div class="field">
              <p class="control has-icons-left has-icons-right">
                <input className="input is-info"  type="text" placeholder="Search..." value={query}
                ref={input => this.search = input}
                onChange={((e) => this.handleInputChange(e,'tab1'))}
                id="search-course"
                />
                <span className="icon is-small is-left">
                <FontAwesomeIcon icon={"search"} /> 
                </span>
              </p>
            </div>
          </div>
          <TNCard aTNDetail={aTNCourseList} />
        </div>
        {/* <div id="con-tab2" className="hidden bg-training" >
          <div className="column is-one-quarter">
            <div class="field">
              <p class="control has-icons-left has-icons-right">
                <input className="input is-info" type="text" placeholder="Search..."
                ref={input => this.search2 = input}
                onChange={((e) => this.handleInputChange(e,'tab2'))}
                />
                <span className="icon is-small is-left">
                <FontAwesomeIcon icon={"search"} /> 
                </span>
              </p>
            </div>
          </div>
          <TNCard aTNDetail={aTNParticipate} />
        </div> */}
      </div>
    );
  }
  handleClick = (e, tabs) => {
    if(tabs==='tab1'){
      document.getElementById("tab1").classList.add('is-active','has-text-danger'); 
      document.getElementById("tab2").classList.remove('is-active', 'has-text-danger'); 
      // document.getElementById("con-tab1").classList.remove('hidden');
      // document.getElementById("con-tab2").classList.add('hidden');
      this.getCourseList();
    }else{
      document.getElementById("tab1").classList.remove('is-active', 'has-text-danger')
      document.getElementById("tab2").classList.add('is-active', 'has-text-danger');
      // document.getElementById("con-tab1").classList.add('hidden');
      // document.getElementById("con-tab2").classList.remove('hidden');
      this.getParticipate();
    }
  }
  handleInputChange = (e, tabs) => {
    this.search.value = encodeURI(this.search.value) ;
    const queryString = qs.parse(this.props.location.search);
    const page = queryString.p || 1;
    const p_url = page+"&query="+this.search.value;
    this.props.history.push('/training?'+p_url)
    const {TNPage} = this.state ;
    if(TNPage==="CourseList"){
      this.getCourseList()
    }else{
      this.getParticipate()
    }
  }
  async componentDidUpdate(prevProps) {
    // TO do...
  }
  async componentDidMount() {
    //console.log("componentDidMount");
    let { access_token = "", access_token_exp = 0 } = this.props.loginReducer; //this.state;
    if (access_token === "") {
      const request_token = localStorage.getItem("request_token");
      await this.props.dispatch(actionGetAccessToken(request_token));
    }
    //await this.getParticipate();
    await this.getCourseList();
  }
  async getCourseList(){
    try{
      const { access_token } = this.props.loginReducer;
      const {userinfo } =this.props.loginReducer
      const queryString = qs.parse(this.props.location.search);
      const page = queryString.p || 1;
      const query =  document.getElementById("search-course").value  || "";
      const UserCode  = userinfo.usercode ;//= "keiko.n" ;
      // let {query} = this.state ;
      //console.log(query);
      const Type = "course_list";
      await this.props.dispatch(actionLoadTNListData(access_token,Type,UserCode,10,page,query));
      if(this.props.tnlistReducer.data.length>0)
      {
        this.setState({
          loading:1 ,
          TNCourseList:this.props.tnlistReducer.data,
          data: this.props.tnlistReducer.data ,
          TNPage:'CourseList'
        })
      }else{
        this.setState({ loading:1 ,data:[], TNPage:'CourseList'});
      }
    } catch (err) {
      console.log("error");
      console.log( {code:500, message: err.message});
    }
  }
  async getParticipate(){
    try{
      const { access_token } = this.props.loginReducer;
      const {userinfo } =this.props.loginReducer
      const queryString = qs.parse(this.props.location.search);
      const page = queryString.p || 1;
      const query =  document.getElementById("search-course").value  || "";
      const UserCode  =  "Thanyalak" ; //userinfo.usercode ;//= "keiko.n" ;
      const Type = "participate";
      await this.props.dispatch(actionLoadTNListData(access_token,Type,UserCode,10,page,query));
      //console.log(this.props.tnlistReducer.data);
      if(this.props.tnlistReducer.data.length>0)
      {
        this.setState({
          loading:1 ,
          TNParticipate:this.props.tnlistReducer.data,
          data: this.props.tnlistReducer.data ,
          TNPage:'Participate',
        })
      }else{
        this.setState({ loading:1 ,data:[], TNPage:'Participate'});
      }
    } catch (err) {
      console.log("error");
      console.log( {code:500, message: err.message});
    }
  }
}

const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    propertyReducer: state.propertyReducer,
    tnlistReducer: state.tnlistReducer
  };
};
export default connect(mapStateToProps)(Training);
