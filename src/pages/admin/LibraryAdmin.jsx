
import React, { Component } from "react";
import { connect } from "react-redux";
//import { lang } from "../Lang.jsx";
import axios from "axios";
import { api } from "../../Config_API.jsx";
import { toast } from "react-toastify";
import "assets/css/library.scss";
import { Link } from "react-router-dom";
import qs from "query-string";
import { actionGetAccessToken } from "actions/login";
import { actionLoadUserApplicationData } from "actions/library/actionLibrary";
import { actionGetUserByHR } from "actions/hr/actionHR";
import TitleMenu from "components/TitleMenu.jsx";
import PaginationModal from "components/PaginationModal.jsx";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faChevronCircleLeft, faPrint,faBackward, faDownload, faPlusCircle} from "@fortawesome/free-solid-svg-icons";
// import Modal from '@material-ui/core/Modal';

class LibraryAdmin extends Component {
    state = {
        loading: '0',
        data: [],
        dataHR:[],
        dataHRAll:[],
        Category:"",
        aURLS3: [],
        page: 1,
        loadIframe:false,
        PageSize: 10,
        firstLoadPage:true,
        firstLoadModel:0,
        PageTotal:1,
        modalState: false,
        selectUser:[],
        Search:''
    }      

    notify = (type, message) => {
        const config = {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
        };
        if (type === "error") {
          toast.error(message, config);
        } else if (type === "success") {
          toast.success(message, config);
        } else if (type === "warn") {
          toast.warn(message, config);
        } else if (type === "info") {
          toast.info(message, config);
        }
    };

    constructor(props) {
        super(props);       
        
        this.toggleModal = this.toggleModal.bind(this);
    }
    toggleModal() {    
        this.setState((prev, props) => {
            const newState = !prev.modalState;
            
            return { modalState: newState };
        });
    }
    render() {

    const { 
        activeClass,
        Category,
        firstLoadPage,
        data,
        dataHR,
        page,
        PageSize,
        PageTotal,
        Search,
        loadPage,
        selectUser
        } = this.state;
    const path = this.props.location.pathname.split("/")[3];
    const titlename = path.replace('_',' ');  
    const query = Search; 
    let page_ = (typeof page == 'undefined' ?'1':page)
    let Module = "";  
    if(path == 'company_manuals'){
        Module = 'COMPANY';  
    }
    else if(path == 'training_courses'){
        Module = 'TRAININGCOURSE'; 
    }
    else {
        Module = path.toUpperCase(); 
    }

    const Modal = ({ children, closeModal, modalState,  PageTotal, page, PageSize, CurrentURL, Search }) => {
        if(!modalState) {
            return null;
        }
        return(
            <div className="modal is-active" >
            <div className="modal-background" onClick={closeModal} />
            <div className="modal-card" style={{width:'1000px', color:'black'}}>
                <div className="modal-card-head" style={{padding:'5px 5px 5px'}}>
                    <div className="column is-1" style={{padding:'5px 5px 5px'}}>
                        <img src='http://webappuat.redplanethotels.com/img/logo-57x57.png'/>
                    </div>
                    <div className='column is-10' >
                        <p style={{color:'#B01116', fontSize:'18px', fontWeight:'100'}}>WEB APPLICATION</p>
                        <p style={{fontSize:'0.7rem', fontWeight:'100'}}>Red Planet Hotels Co.,Ltd.</p>
                    </div>
                    <div className="column is-1 is-pulled-right" >
                        <button className="delete" style={{marginLeft:'40px', marginTop:'-20px'}} onClick={closeModal} />
                    </div>              
                </div>
                <div className="modal-card-body" >
                    <div className="content">
                        {children}
                    </div>
                </div>
                <div className="modal-card-foot">
                    <div>
                    <PaginationModal style={{padding:'5px 5px 5px'}} recordsTotal={PageTotal} page={page} pageSize={PageSize} link={CurrentURL}
                    onClick={e => {
                        //console.log(e);
                        this.setState({ active: true });
                    }}
                    />
                    <button 
                    className="button is-danger is-pulled-right"
                onClick={((e) =>{this.handleClickSelect()})} 
                    >{' Select'}</button>
                    </div>
                </div>
            </div>
            </div>
        );
    }
    Modal.propTypes = {
        // closeModal: React.PropTypes.func.isRequired,
        // modalState: React.PropTypes.bool.isRequired,
        // title: React.PropTypes.string
    }
        
   // console.log(page_);
    
    let arrEmployee = [];
    let arrDisplay = [];
    //console.log(page);
    let i_tmp = 0;
    if(dataHR.length >0){
        dataHR.map((each, key) => { 
            let checked=false;
            try {
                checked = selectUser.indexOf(each.UserCode) !== -1 ? true : false;
            } catch (error) {
                checked = false;
            }
            arrEmployee.push(
                <div className="columns" key={i_tmp}>
                    <div className="column is-1 body-admin-popup">
                        <input type='checkbox' value={each.UserCode} 
                          defaultChecked={checked}
                          onChange={e => this.handleCheckboxChange(e)}
                          id={"checkbox-" + i_tmp}
                          type='checkbox'
                        />
                    </div>
                    <div className="column is-2 body-admin-popup">{each.UserCode}</div>
                    <div className="column is-2 body-admin-popup">{each.UserMName}</div>
                    <div className="column is-3 body-admin-popup">{each.UserFName+' '+each.UserLName}</div>
                    <div className="column is-4 body-admin-popup">{each.JobTitle}</div>
                </div> 
            ); 
            i_tmp++; 
        });
    }
    if(data.length > 0 ){        
       // console.log(data);
        data.map((each, key) => {
            arrDisplay.push(
                <div style={{marginLeft:'150px'}} className="columns is-multiline" key={"content" + i_tmp}>
                    <div className="column is-1 body-admin">
                        <a className="has-text-danger"
                        onClick={e => this.handleDetete(e)}
                        id={each.UserCode}
                        >Delete
                        </a>                        
                    </div>
                    <div className="column is-2 body-admin">
                        <input className="input " type="text" value={each.UserCode} readOnly />
                    </div>
                    <div className="column is-4 body-admin">
                        <input className="input " type="text" value={each.UserFName} readOnly />
                    </div>
                    <div className="column is-3 body-admin">
                        <input className="input " type="text" value={each.JobTitle} readOnly />
                    </div>
                </div>
            );
        i_tmp++;
        });
        arrDisplay.push(
            <div style={{marginLeft:'150px', marginTop:'10px'}} className="columns is-multiline" key={"content" + i_tmp}>
                <button className="button is-danger"
                onClick={e=>{
                  this.handleSubmit()
                }}
                >Save</button>
            </div>
        );
    } 
    return (
      <div className="notification">
           <div key={"content" + activeClass}>
                <div className="has-text-right button-download">
                    <Link to={"/library/admin"} className="button is-danger" title="Back"> <FontAwesomeIcon icon={faChevronCircleLeft} /> <span className="is-hidden-mobile">Back</span> </Link>
                </div>
                <TitleMenu title={'ASSIGN PERMISSION '+titlename} />
                <div style={{borderTop:'1px solid #cccccc'}}></div>
                <div  className="columns">
                    <div style={{marginLeft:'40px',marginTop:'10px'}} className="has-text-right column is-10">
                        <button className="button is-danger" onClick={this.toggleModal}><FontAwesomeIcon icon={faPlusCircle} /> <span className="is-hidden-mobile">Add Employee</span></button>
                    </div>
                </div>
                <div style={{marginLeft:'150px',marginTop:'-10px'}} className="columns is-multiline">
                    <div className="column is-1 header-admin"></div>
                    <div className="column is-2 header-admin">User Code</div>
                    <div className="column is-4 header-admin">Full name</div>
                    <div className="column is-3 header-admin">{'Job Title'}</div>
                </div>
                {arrDisplay}   
                <Modal closeModal={this.toggleModal} 
                modalState={this.state.modalState} 
                PageTotal={PageTotal} page={page_} PageSize={PageSize} CurrentURL={'admin/library/'+path}
                Search={Search}
                >
                <div className="columns">
                    <div  id="tab-admin-search" className="column is-12 is-pulled-right" style={{padding:'1px 1px 1px'}}>
                        <div className="column is-3 is-pulled-right" style={{padding:'5px 5px 5px'}}>
                            <div className="control has-icons-left has-icons-right is-pulled-right">
                            <input className="input is-danger" type="text" placeholder="Search..." value={query}
                                ref={input => this.search = input}
                                onChange={((e) => this.handleInputChange(e, 'tab1'))}
                                id="search-input"
                            />
                            <span className="icon is-small is-left">
                                <FontAwesomeIcon icon={faSearch} />
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="columns">
                    <div className="column is-1 header-admin"></div>
                    <div className="column is-2 header-admin-popup">User Code</div>
                    <div className="column is-2 header-admin-popup">Middle Name</div>
                    <div className="column is-3 header-admin-popup">Full Name</div>
                    <div className="column is-4 header-admin-popup">Job Title</div>
                </div>                 
                {arrEmployee}
                </Modal>
            </div>
      </div>
    );
  }
   
  handleInputChange = (e, tabs) => {
    //console.log(this.search.value);
    //console.log(e.target);
    //console.log(this.search.value);
    this.setState({ Search: this.search.value });
    this.get_user();
    
   // document.getElementById("search-input").focus();
  }
  handleClickSelect () {
    //this.get_UserApplication(); 
    const { dataHR, dataHRAll, selectUser} = this.state;
    
    const path = this.props.location.pathname.split("/")[3];    
    const userinfo = JSON.parse(localStorage.getItem("userinfo"));
    const data = [];
    let Module = "";
    let ModuleName = ""; 
    if(path == 'company_manuals'){
        Module = 'COMPANY'; 
        ModuleName =  'Company Manuals';
    }
    else if(path == 'training_courses'){
        Module = 'TRAININGCOURSE'; 
        ModuleName =  'Training Course';
    }
    else if(path == 'insurance'){
        Module = 'INSURANCE'; 
        ModuleName =  'Insurance';
    }
    let i_tmp = data.length;
    selectUser.map((eachSel, key) => { 
        dataHRAll.map((each, key) => {
            //console.log(each.UserCode);
            //console.log(eachSel);
            if(each.UserCode.toUpperCase() == eachSel.toUpperCase()){
                data.push({
                    'AppCode':'LIBRARY', 
                    'ModuleCode':Module,
                    'ModuleName':ModuleName,
                    'UserCode':each.UserCode,
                    'UserFName':each.UserFName+' '+each.UserLName,
                    'CreatedBy':userinfo.usercode, 
                    'IsActive':1, 
                    'JobTitle':each.JobTitle,
                    'count':i_tmp
                });
                i_tmp++;
            }
        });
    });
    //console.log(data);
    //document.getElementById("search-input").value('');
    //this.toggleModal;    
    this.setState({
        loading: 1,
        modalState: false,
        firstLoadPage:false,
        loadPage :true,
        data:data,
        page:'1',
        Search:"",
        firstLoadModel:0,
    });

    this.get_user();
    //console.log(this.state);
    
  }
  handleCheckboxChange(e) {
    const { value, checked } = e.target;
    const {selectUser} = this.state;
    if(selectUser.indexOf(value) <= -1 && checked){
      selectUser.push(value);  
    }
    else{
        let index = selectUser.indexOf(value);
        selectUser.splice(index,1);        
    }
    console.log(selectUser);
  }
  handleDetete(e) {
    const { id } = e.target;
    const {selectUser} = this.state;
    let index = selectUser.indexOf(id);
        selectUser.splice(index,1); 
    
        this.handleClickSelect();
    
    // this.setState({});
    //console.log(selectUser);
  }
  async get_user() {
    const {
      selectEntity,
      selectProperty,
      selectOffice,
      selectJobDept,
      selectJobTitle,
      selectStatus,
      selectCountry,
      Search,      
      //page,
      PageSize,
      PageTotal
    } = this.state;   
 
    const page = (this.props.location.search == '' ?'1':this.props.location.search);
    let curPage_ = '1';
    //console.log(this.state.Search);
    if(typeof page.split('=')[1] != 'undefined' && Search == ''){
        curPage_ = page.split('=')[1];
    }
    
   // const page = queryString.p || 1;
   // console.log(Search);
    const param = {
      Entity: '',
      Property: '',
      Office: '',
      JobDept: '',
      JobTitle: '',
      Status: '1',
      Country: '',
      PageNum: curPage_,
      PageSize: PageSize,
      Search: Search
    }
    let { access_token = "" } = this.props.loginReducer;
    if (access_token === "") {
      const request_token = localStorage.getItem("request_token");
      await this.props.dispatch(actionGetAccessToken(request_token));
    }
    //console.log(param);
    await this.props.dispatch(actionGetUserByHR(access_token, 'user', param));
    //console.log(this.props.hrReducer.data[0]);
    if (this.props.hrReducer.data.length > 0) {
      this.setState({
        loading: 1,
        dataHR: this.props.hrReducer.data,
        btnFilterLoading: false,
        UserCodeList: this.props.hrReducer.data[0].UserCodeList.split(","),
        arrUserItems: [],
        IsExport: true,
        UserCodeListTmp: [],
        boolUserCheckedAll: false,
        PageNum: page,
       // page: page,
        PageTotal:this.props.hrReducer.data[0].TotalRows
      });
    } else {
      this.setState({
        loading: 1,
        dataHR: [],
        btnFilterLoading: false,
        UserCodeList: [],
        arrUserItems: [],
        IsExport: true,
        UserCodeListTmp: [],
        boolUserCheckedAll: false,
        PageNum: page,
       // page: page,
        PageTotal:0
      });
    }
  }
  async get_userAll() {
    const param = {
      Entity: '',
      Property: '',
      Office: '',
      JobDept: '',
      JobTitle: '',
      Status: '1',
      Country: '',
      PageNum: '1',
      PageSize: '10000',
      Search: ''
    }
    let { access_token = "" } = this.props.loginReducer;
    if (access_token === "") {
      const request_token = localStorage.getItem("request_token");
      await this.props.dispatch(actionGetAccessToken(request_token));
    }
    //console.log(param);
    await this.props.dispatch(actionGetUserByHR(access_token, 'user', param));
   // console.log(this.props.hrReducer.data);
    if (this.props.hrReducer.data.length > 0) {
      this.setState({
        dataHRAll: this.props.hrReducer.data,
       
      });
    } else {
      this.setState({
        dataHRAll: [],
      });
    }
  }
  async get_UserApplication() {
    //console.log("get_library");   
    const {firstLoadPage, selectUser} = this.state; 
    const userinfo = JSON.parse(localStorage.getItem("userinfo"));
    //console.log(userinfo.usercode);  
    const path = this.props.location.pathname.split("/")[3];
    let Module = "";  
    if(path == 'company_manuals'){
        Module = 'COMPANY';  
    }
    else if(path == 'training_courses'){
        Module = 'TRAININGCOURSE'; 
    }
    else {
        Module = path.toUpperCase(); 
    }
    const param = {
      AppCode: 'LIBRARY',
      ModuleCode:Module,
      UserCode:userinfo.usercode
    }
    let { access_token = "" } = this.props.loginReducer;
    if (access_token === "") {
      const request_token = localStorage.getItem("request_token");
      await this.props.dispatch(actionGetAccessToken(request_token));
    }
   // console.log(param);
    await this.props.dispatch(actionLoadUserApplicationData(access_token, 'user_application', param));
    //console.log(this.props.libraryReducer.data);
    if(this.props.libraryReducer.data[0]['count'] > 0){
        if (this.props.libraryReducer.data.length > 0) {
            if(firstLoadPage){
                const data = this.props.libraryReducer.data;
                data.map((each, key) => {
                    selectUser.push(each.UserCode);
                });  
            }
            //console.log(selectUser);
            this.setState({
                loading: 1,
                data: this.props.libraryReducer.data,
                firstLoadPage:false,
        
            });
        } 
        else 
        {
            this.setState({
                loading: 1,
                data: [],
                firstLoadPage:false,
            });
        }
    } else {
        localStorage.setItem('Msg', "You don't have permission to access this system");
        window.location.href = '/library/admin';
    }
  }
  async handleSubmit(){
    const {data} = this.state;
    const userinfo = JSON.parse(localStorage.getItem("userinfo"));
    const url = api.INTRANET_API + "/library/assign_permission";

    const AppCode = data[0].AppCode;
    const ModuleCode = data[0].ModuleCode;
    const CreateBy = userinfo.usercode;
    const IsActive = '1';
    let UserCode = "";
    data.map((each, key) => {
        console.log(each);
        UserCode += each.UserCode+'|';
    });
    const param = {
        AppCode:AppCode,
        ModuleCode:ModuleCode,
        UserCode:UserCode,
        CreateBy:CreateBy,
        IsActive:IsActive
    } 
    console.log(param);

    try {
        const res = await axios.post(url, param, {
          headers: { 
           Authorization: "Bearer " + this.props.loginReducer.access_token, 
           "Content-Type": "application/json"
        }
        });
        if(res.data.code===200)
        {
          this.notify("success", "Save Permission Success");
          //this.handleShowModal(false);
          //this.props.handleFilter();
        }
        else{
          this.notify("error","Save Permission Error");
        }
    } catch (err) {
        console.log({ err });
        this.notify("error", "Save Permission Error");
        this.notify("error", err.message);
        //window.location.href = "/connection_error?err="+err.message+"&page=ItemDefect&method=handleSubmit";
    }

    //console.log(param);
  } 
  async componentWillMount() {
    //console.log("componentWillUnmount...");
  }
  async componentWillUnmount() {
   // console.log("componentWillUnmount...");
  }
  async componentDidUpdate(prevProps) {
    //console.log("componentDidU/pdate"); 
    
    
    const {firstLoadModel, modalState} = this.state;
   // 
    //console.log(firstLoadModel);
    
    const PreURL =  prevProps.location.pathname;    
    const CurURL =  this.props.location.pathname;

    const url_page = (prevProps.location.search == '' ?'1':prevProps.location.search);
   // console.log(url_page);
    let page_ = url_page.split('=')[1];    

    const curPage = (this.props.location.search == '' ?'1':this.props.location.search);
    
    let curPage_ = curPage.split('=')[1];
    if(modalState){  
        if(firstLoadModel==0){
            this.setState({
                Search:"",  
                page:curPage_,
                firstLoadModel:1
            }); 
            this.get_user();
        }
    }
   // console.log(prevProps);
   // console.log(this.props);
    if (this.state.Search!=="" && modalState){
        document.getElementById("search-input").focus();
    }
    //console.log(this.state.Search);
    if(PreURL == CurURL){   
        
      if(page_ !== curPage_ ){

         this.setState({page:curPage_});
         this.get_user();                      
      }
    }
    else{
      this.get_user();      
      //console.log('error');
      this.setState({page:'1'});
    }   
  }
  async componentDidMount() {
    //console.log("componentDidMount");
    this.get_user();
    this.get_userAll();
    this.get_UserApplication();
  }
}
const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    propertyReducer: state.propertyReducer,
    hrReducer: state.hrReducer,
    libraryReducer: state.libraryReducer,
    s3Reducer: state.s3Reducer
  };
};
export default connect(mapStateToProps)(LibraryAdmin);
