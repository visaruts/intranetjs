
import React, { Component } from "react";
import { Prompt } from "react-router";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { lang } from "Lang.jsx";
import { api, ENV, s3_url } from "Config_API.jsx";
import axios from "axios";
import "assets/css/hr.scss";
import Iframe from 'react-iframe'
import { actionGetAccessToken } from "actions/login";
import { actionLoadHRData } from "actions/hr/actionHR";
import { actionLoadS3Data } from "actions/s3/actionS3";
import TitleMenu from "components/TitleMenu.jsx";
import qs from "query-string";
import ReactLoading from 'react-loading';
import { faBackward, faDownload, faChevronCircleLeft} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
class Announcement extends Component {
  state = {
    loading: '0',
    data: [],
    aURLS3: [],
    loadIframe:false
  }
  render() {
    const { data, aURLS3,loading ,loadIframe} = this.state;
    let aData = [];
    let Topic = "Amnnouncement";
    const URLGoogleDoc = "https://docs.google.com/viewerng/viewer?embedded=true&url=";
    let arrDisplay = [];
    if (data.length > 0) {
        Topic = data[0]['Topic'];
        let Category = data[0]['Category'];
        Category =Category.toLowerCase().replace("&","");
        Category =Category.replace("  ","_");
        Category =Category.replace(" ","_");
      aURLS3.map((Name, key) => {
          let sURLS3 = Name;
        if (typeof sURLS3 !== "undefined") { sURLS3 = sURLS3.replace("", ""); } else { sURLS3="";}
        arrDisplay.push(<div className="column is-12 content-announcement" key={key}>
          <div className="has-text-right button-download">
            <Link to={"/hr/" + Category} className="button is-danger" title="Back Home"> <FontAwesomeIcon icon={faChevronCircleLeft} /> <span className="is-hidden-mobile">Back Home </span> </Link>
            <a href={sURLS3} className="button is-warning" title="Download"> <FontAwesomeIcon icon={faDownload} />  <span className="is-hidden-mobile">Download </span> </a>
          </div>
          <Iframe  onLoad={this.onLoadHandler} url={URLGoogleDoc + encodeURIComponent(sURLS3)} key={"Iframe"+key}
            className="iframe-announcement hidden"
          />
          <ReactLoading type={"spokes"} color={"#4a4a4a"} className="announcement-loading" />
          <div className="announcement-loading">{"Loading..."}</div>
          </div>);
      });
    }
    if(loading!==1){
      arrDisplay = [];
      arrDisplay.push(<div  className="column is-12">
          {/* <ReactLoading type={"spokes"} color={"#4a4a4a"} className="announcement-loading" />
        <div className="announcement-loading">{"Loading..."}</div> */}
      </div>);
    }
    return (
      <div className="notification">
        <TitleMenu title={Topic} />
        <div className="columns">
          {arrDisplay}
        </div >
      </div>
    );
  }
  handleClick = (e, tabs) => {
    //To Do...
  }
  onLoadHandler = (e) => {
    //console.log("onLoadHandler");
    let iframe = document.getElementsByClassName("iframe-announcement");
    for (let row of iframe) {
      row.classList.remove("hidden")
    }
    let loading = document.getElementsByClassName("announcement-loading");
    for (let row of loading) {
      row.classList.add("hidden")
    }

    //this.setState({loading: 1,loadIframe:true});
  }
  async componentDidUpdate(prevProps) {
    //To Do....
    // console.log(prevProps);
  }
  async componentDidMount() {
    console.log("componentDidMount");
    let { access_token = "", access_token_exp = 0 } = this.props.loginReducer; //this.state;
    if (access_token === "") {
      const request_token = localStorage.getItem("request_token");
      await this.props.dispatch(actionGetAccessToken(request_token));
    }
    await this.getAnnouncement();
  }
  async getURLS3(Name) {
    try {
      const { access_token } = this.props.loginReducer;
      const param = { AppName: "INTRANET", Public: false, PathFile: "file/WebPortal/PDF/" + Name }
      await this.props.dispatch(actionLoadS3Data(access_token, param));
      let {aURLS3} = this.state
      const url = this.props.s3Reducer.data.url;
      aURLS3.push(url);
      // console.log(aURLS3);
      this.setState({ aURLS3:aURLS3})
    }catch (e) {
      console.log('caught error', e);
    }
  }
  async getAnnouncement() {
    try {
      const { access_token } = this.props.loginReducer;
      let AnnouncementID = 'announcement';
      try {
        AnnouncementID = this.props.location.pathname.split("/")[2].replace("_", " ");
      } catch (error) {
        AnnouncementID = '111111'
      }
      const Type = "announcement";
      const param = {
        AnnouncementID: AnnouncementID,
        Category: "",
        Department: "",
        StartDate: "",
        EndDate: "",
        Country: "",
        PinnedCalendar: "",
        CreateBy: "",
        AnnouncementStatus: "OPEN",
        PageNum: "1",
        PageSize: "1",
        Search: ""
      }
      await this.props.dispatch(actionLoadHRData(access_token, Type, param));
      // console.log(this.props.hrReducer.data);
      if (this.props.hrReducer.data.length > 0) {
        let data = [];
        await this.props.hrReducer.data.map((Row, key) => {
          const aAttachPdfFiles = Row.AttachPdfFiles.split(",");
          data[0] = [];
          data[0]['Topic'] = Row.Topic;
          let i = 0;
          data[0]['Url'] = [];
          data[0]['Category'] = Row.Category;
          aAttachPdfFiles.map((Name, key) => {
            if (Name !== "") {
              //console.log(Name);
              this.getURLS3(Name);
              data[0]['Url'][i] = Name;
            }
            i++;
          });
        });
        this.setState({
          loading: 1,
          data: data
        })
      } else {
        this.setState({ loading: 1, data: [] });
      }
    } catch (err) {
      console.log("error");
      console.log({ code: 500, message: err.message });
      this.setState({ loading: 1, data: [] });
    }
  }
}
const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    propertyReducer: state.propertyReducer,
    hrReducer: state.hrReducer,
    s3Reducer: state.s3Reducer
  };
};
export default connect(mapStateToProps)(Announcement);
