
import React, { Component } from "react";
import { Prompt } from "react-router";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { lang } from "Lang.jsx";
import { toast } from "react-toastify";
import { api, ENV, s3_url } from "Config_API.jsx";
import axios from "axios";
import "assets/css/library.scss";
import Iframe from 'react-iframe'
import { actionGetAccessToken } from "actions/login";
import { actionLoadLibraryData } from "actions/library/actionLibrary";
import { actionLoadS3Data } from "actions/s3/actionS3";
import Pagination from "components/Pagination.jsx";
import TitleMenu from "components/TitleMenu.jsx";
import Company from "components/library/Company.jsx";
import ListLibrary from "components/library/ListLibrary.jsx";
import LibraryView from "components/library/LibraryView.jsx";
import Admin from "components/library/Admin.jsx";
import qs from "query-string";
import ReactLoading from 'react-loading';
import { faBackward, faDownload } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faPrint, faBook, faUser, faFolderOpen } from "@fortawesome/free-solid-svg-icons";
class IndexLibrary extends Component {
  state = {
    loading: '0',
    data: [],
    aURLS3: [],
    page: '1',
    loadIframe:false,
    PageSize: 6,
    firstLoadPage:true,
    PageTotal:1
  }
  
  render() {
    const { 
      userinfo, 
      data, 
      active, 
      activeClass, 
      page, 
      boolPolicyCheckedAll, 
      IsPrint, 
      arrPolicyItems, 
      PageSize, 
      loading, 
      isAuhEmp, 
      templates,
      PageTotal
    } = this.state;
    const queryString = qs.parse(this.props.location.search);
    //console.log(queryString);
    const query = queryString.query;
    let arrDisplay = [];
    const path = this.props.location.pathname.split("/")[2]; 
    const view = this.props.location.pathname.split("/")[3]; 
    const DocumentNo = this.props.location.pathname.split("/")[4]; 
    const CurrentURL = "library/" + path;
    const CountryCode = localStorage.getItem("countryCode");
    //console.log(data);
    //let PageTotal = 1;
    const pStyle0 = {
      paddingTop: '0px'
    };

    if(path == 'company_manuals' && typeof view === "undefined"){
      arrDisplay.push(
        <div key={"content" + activeClass}>
          <div className="column clear" style={pStyle0}>
            <Company CurrentURL={CurrentURL} Category={activeClass} sPram={queryString}/>
          </div>
        </div>
      );
            
    }else if(path == 'training_courses' && typeof view === "undefined"){
      arrDisplay.push(
        <div key={"content" + activeClass}>
          <TitleMenu title={'TRAINING COURSES'} />
          <div className="column clear" style={pStyle0}>
              <Company CurrentURL={CurrentURL} Category={activeClass} sPram={queryString}/>
          </div>
        </div>
      );
    }
    else if(path == 'insurance' && typeof view === "undefined"){
      arrDisplay.push(
        <div key={"content" + activeClass}>
          <TitleMenu title={'INSURANCE'} />
          <div className="column clear" style={pStyle0}>
              <Admin CurrentURL={CurrentURL} Category={activeClass}/>
          </div>
        </div>
      );
    }
    else if(path == 'admin' && typeof view === "undefined"){
      arrDisplay.push(
        <div key={"content" + activeClass}>
          <TitleMenu title={'ADMINISTRATION'} />
          <Admin CurrentURL={CurrentURL} Category={activeClass}/>
        </div>
      );
    }
    else if(path == 'red_letter' && typeof view === "undefined"){
      arrDisplay.push(
        <div key={"content" + activeClass}>
          <TitleMenu title={'RED LETTER'} />
          <div className="column clear" style={pStyle0}>
            <ListLibrary CurrentURL={CurrentURL} PageNum={page} PageSize={PageSize} Category={activeClass}/>
          </div>
          <div className="row-pagination" >
            <Pagination  recordsTotal={PageTotal} page={page} pageSize={PageSize} link={CurrentURL}
            onClick={e => {
              //console.log(e);
              this.setState({ active: true });
            }}
            />
          </div>
        </div>
      );
    }
    else if(path == 'public_library' && typeof view === "undefined"){
      arrDisplay.push(
        <div key={"content" + activeClass}>
          <TitleMenu title={'PUBLIC LIBRARY'} />                  
          <div className="column clear" style={pStyle0}>
            <ListLibrary CurrentURL={CurrentURL} PageNum={page} PageSize={PageSize} Category={activeClass}/>
          </div>
          <div className="row-pagination"  >
            <Pagination recordsTotal={PageTotal} page={page} pageSize={PageSize} link={CurrentURL}
              onClick={e => {
                //console.log(e);
                this.setState({ active: true });
              }}
              />
          </div>
        </div>
      );
    }
    else if(view == 'view'){
      arrDisplay.push(
        <div key={"content" + activeClass}>
          <LibraryView PageNum={page} PageTotal={PageTotal} CountryCode={CountryCode} CurrentURL={CurrentURL} documentNo={DocumentNo} Category={path}/>
        </div>
      );
    }
    return (
      <div className="notification">
            {arrDisplay}          
      </div>
    );
  }
  async get_library() {
    //console.log("get_library");
    const {
      documentNo
    } = this.state;
     const queryString = qs.parse(this.props.sPram);
     const userinfo = JSON.parse(localStorage.getItem("userinfo"));
     const page = queryString.p || 1;    
    // const {CurrentURL , PageNum, PageSize} = this.props;
    // const path = CurrentURL.split("/")[1];
    const path = this.props.location.pathname.split("/")[2]; 
    let activeClass= (path === "public_library" ?"brand_guideline":path);
    //let activeClass = 'training_courses';
    //console.log(localStorage.userinfo);
    //console.log(userinfo.usercode);
    const param = {
      DocumentNo: '',
      Category: activeClass,
      PageNum: '',
      PageSize: '',
      Search:'',
      CountryCode:localStorage.getItem("countryCode"),
      UserCode:userinfo.usercode
    }
    let { access_token = "" } = this.props.loginReducer;
    if (access_token === "") {
      const request_token = localStorage.getItem("request_token");
      await this.props.dispatch(actionGetAccessToken(request_token));
    }
    //console.log(param);
    await this.props.dispatch(actionLoadLibraryData(access_token, 'company_manuals', param));
    //console.log(this.props.libraryReducer.data);
    if (this.props.libraryReducer.data.length > 0) {
      this.setState({
        loading: 1,
        data: this.props.libraryReducer.data,
        firstLoadPage:false,
        PageTotal:this.props.libraryReducer.data.length

      });
    } else {
      this.setState({
        loading: 1,
        data: [],
        firstLoadPage:false,
        PageTotal:'0'
      });
    }
  }
  
  async componentWillMount() {
    //console.log("componentWillUnmount...");
  }
  async componentWillUnmount() {
   // console.log("componentWillUnmount...");
  }
  async componentDidUpdate(prevProps) {
    //console.log("componentDidUpdate");
    const PreURL =  prevProps.location.pathname;    
    const CurURL =  this.props.location.pathname;

    const url_page = (prevProps.location.search == '' ?'1':prevProps.location.search);
    const page_ = url_page.split('=')[1];

    const curPage = this.props.location.search;
    let curPage_ = curPage.split('=')[1];
    const chkCountry = curPage.split('=')[0];
    //const chkLang = curPage.split('=')[0];
    // console.log(curPage);
    
    if(PreURL == CurURL){
     
      if(page_ !== curPage_ ){
        curPage_  = (chkCountry == '?country'?'1':curPage_);
        //console.log(PreURL +'|'+CurURL+'|'+curPage_);
        if(chkCountry != '?lang'){
          this.setState({page:curPage_});
        }           
        if(chkCountry == '?country'){
          //this.setState({loading: '1',firstLoadPage:false});
          this.get_library();                   
        }
        
      }
    }
    else{
      this.get_library();      
      //console.log(data);
      this.setState({page:'1'});
    }   
    
    
  }
  async componentDidMount() {
    //console.log("componentDidMount");
    const Msg = localStorage.getItem("Msg");
    //console.log(Msg);
    if(Msg != ""){
      const config = {
          position: "top-right",
          autoClose: 5000,
          hideProgressBar: true,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true
      };
      toast.error(Msg, config);
      localStorage.setItem('Msg', "");            
    }
    this.get_library();
  }
}
const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    propertyReducer: state.propertyReducer,
    hrReducer: state.hrReducer,
    libraryReducer: state.libraryReducer,
    s3Reducer: state.s3Reducer
  };
};
export default connect(mapStateToProps)(IndexLibrary);
