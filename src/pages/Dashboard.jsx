import React, { Component } from "react";
import { connect } from "react-redux";
import { lang } from "../Lang.jsx";
import { actionGetAccessToken } from "../actions/login";
import {
  actionLoadData,
  //actionSaveData
} from "../actions/dashboard";
import { actionGetApplication } from "../actions/users";
import { ENV, CONFIG, report_url } from "../Config_API.jsx";
import qs from "query-string";
import {TinyButton as ScrollUpButton} from "react-scroll-up-button";

/**react-day-picker */
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import MomentLocaleUtils, {
  formatDate,
  parseDate,
} from 'react-day-picker/moment';
import moment from 'moment';
import Helmet from 'react-helmet';
/**react-day-picker */

import PropertySelectBox from "../components/PropertySelectBox.jsx";
import ItemDefect from "../components/ItemDefect.jsx";
import Pagination from "../components/Pagination.jsx";

import { checkPermissionApplication } from "../Permission.jsx";

/** fontawesome */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


class Dashboard extends Component {
  state = {
    page:1,
    propertyCode:"",
    search:"",
    ticketID:"",
    defectStatus:{
      Open:true,
      OnProgress:true,
      ReSchedule:true,
      ReAssigned:true,
      Closed:false,
      Cancel:false
    },
    urgent:{
      Yes:true,
      No:true
    },
    order:"RequestDate desc",
    from: new Date(new Date().setMonth(new Date().getMonth() - 3)),
    fromIsEmpty:true,
    fromIsDisabled: false,
    to: new Date(),
    toIsEmpty:true,
    toIsDisabled: false,
    data:[],
    showFilter:true,
    applicationList:[]
  };

  //handleSearch = _.debounce(this.handleSearch, 600);
  
  //handleStartDayChange = this.handleStartDayChange.bind(this);
  //handleEndDayChange = this.handleEndDayChange.bind(this);

  handleFromChange = this.handleFromChange.bind(this);
  handleToChange = this.handleToChange.bind(this);
  

  render() {
    ////console.log(this.props.dashboardReducer);
    // Page _GET
    //const queryString = qs.parse(this.props.location.search);
    //const page = queryString.p || 1;//parseInt(this.props.location.search.replace(/\?p\=/, "")) || 1;
    const recordsTotal=this.props.dashboardReducer.recordsTotal;
    
    const { page, from, to } = this.state;
    const modifiers = { start: from, end: to };
    

    return (
      <div className="notification">
        <ScrollUpButton />
        <div className="has-text-centered">
          <h1 className="title">
            {lang("MD_DASHBOARD")}{" "}
            {ENV !== "PROD" ? (
              <span className="has-text-danger"> ( {ENV} )</span>
            ) : (
              ""
            )}
          </h1>
        </div>
        <br />
        <div>
          
          <div className="columns">
            <div className="message is-black column is-one-quarter" style={{padding: "0rem"}}>
            
              <div className="has-text-right message-header nav-filter"
                  onClick={(e)=>{
                    e.preventDefault();
                    this.setState({
                      showFilter:!this.state.showFilter
                    })
                  }}>
                <a aria-label="hide filter"
                  onClick={(e)=>{
                    e.preventDefault();
                    this.setState({
                      showFilter:!this.state.showFilter
                    })
                  }}
                  style={{textDecoration: "none"}}
                >
                  {this.state.showFilter?lang("Hide Filter")+" ":lang("Show Filter")+" "} 
                  <FontAwesomeIcon icon={this.state.showFilter?"angle-up":"angle-down"} />
                </a>
              </div>

              <div className="message-body" style={
                this.state.showFilter?
                  {display: ""}:
                  {display: "none"}
                }>
                <div className="filed">
                  {lang("Hotel")}:
                </div>
                <div className="filed">
                  <div className="select is-info">
                    <PropertySelectBox 
                      showSelectAll="true" 
                      lang={lang("Hotel")} 
                      propertyCode={this.props.dashboardReducer.propertyCode}
                      saveState={e => {
                        this.setState({
                          propertyCode: this.props.propertyReducer.propertyCode
                        });
                      }}
                    />
                  </div>
                </div>
                <div className="filed">
                  {lang("Search")}
                </div>
                <div className="field">
                  <div className="control">
                    <input className="input is-info" type="text" value={this.state.search} placeholder={lang("Search")}
                      onChange={e => {
                        this.setState({search:e.target.value});
                      }} />
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                      {lang("Status")}
                  </div>
                </div>
                <div className="field">
                  <div className="columns is-mobile">
                    <div className="column">
                      <div className="field">
                          <input id="switchOpen" type="checkbox" className="is-checkradio is-info is-circle"
                            defaultChecked={this.state.defectStatus.Open}
                            onClick={e=>{
                              this.setState({
                                defectStatus:{
                                  ...this.state.defectStatus,
                                  Open:!this.state.defectStatus.Open
                                }
                              })
                            }} />
                          <label htmlFor="switchOpen">{lang("Open")}</label>
                      </div>
                    </div>
                    <div className="column">
                      <div className="field">
                          <input id="switchOnProgress" type="checkbox" className="is-checkradio is-info is-circle"
                            defaultChecked={this.state.defectStatus.OnProgress}
                            onClick={e=>{
                              this.setState({
                                defectStatus:{
                                  ...this.state.defectStatus,
                                  OnProgress:!this.state.defectStatus.OnProgress
                                }
                              })
                            }} />
                          <label htmlFor="switchOnProgress">{lang("On-progress")}</label>
                      </div>
                    </div>
                  </div>
                  <div className="columns is-mobile">
                    <div className="column">
                      <div className="field">
                          <input id="switchReSchedule" type="checkbox" className="is-checkradio is-info is-circle"
                            defaultChecked={this.state.defectStatus.ReSchedule}
                            onClick={e=>{
                              this.setState({
                                defectStatus:{
                                  ...this.state.defectStatus,
                                  ReSchedule:!this.state.defectStatus.ReSchedule
                                }
                              })
                            }} />
                          <label htmlFor="switchReSchedule">{lang("Re-schedule")}</label>
                      </div>
                    </div>
                    <div className="column">
                      <div className="field">
                          <input id="switchReAssigned" type="checkbox" className="is-checkradio is-info is-circle"
                            defaultChecked={this.state.defectStatus.ReAssigned}
                            onClick={e=>{
                              this.setState({
                                defectStatus:{
                                  ...this.state.defectStatus,
                                  ReAssigned:!this.state.defectStatus.ReAssigned
                                }
                              })
                            }} />
                          <label htmlFor="switchReAssigned">{lang("Re-assigned")}</label>
                      </div>
                    </div>
                  </div>
                  <div className="columns is-mobile">
                    <div className="column">
                      <div className="field">
                          <input id="switchClosed" type="checkbox" className="is-checkradio is-info is-circle"
                            defaultChecked={this.state.defectStatus.Closed}
                            onClick={e=>{
                              this.setState({
                                defectStatus:{
                                  ...this.state.defectStatus,
                                  Closed:!this.state.defectStatus.Closed
                                }
                              })
                            }} />
                          <label htmlFor="switchClosed">{lang("Closed")}</label>
                      </div>
                    </div>
                    <div className="column">
                      <div className="field">
                          <input id="switchCancel" type="checkbox" className="is-checkradio is-info is-circle"
                            defaultChecked={this.state.defectStatus.Cancel}
                            onClick={e=>{
                              this.setState({
                                defectStatus:{
                                  ...this.state.defectStatus,
                                  Cancel:!this.state.defectStatus.Cancel
                                }
                              })
                            }} />
                          <label htmlFor="switchCancel">{lang("Cancel")}</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                      {lang("Urgent")}
                  </div>
                </div>
                <div className="field">
                  <div className="columns is-mobile">
                    <div className="column">
                      <div className="field">
                          <input id="urgentYes" type="checkbox" className="is-checkradio is-info is-circle"
                            defaultChecked={this.state.urgent.Yes}
                            onClick={e=>{
                              this.setState({
                                urgent:{
                                  ...this.state.urgent,
                                  Yes:!this.state.urgent.Yes
                                }
                              })
                            }} />
                          <label htmlFor="urgentYes">{lang("Yes")}</label>
                      </div>
                    </div>
                    <div className="column">
                      <div className="field">
                      <input id="urgentNo" type="checkbox" className="is-checkradio is-info is-circle"
                            defaultChecked={this.state.urgent.No}
                            onClick={e=>{
                              this.setState({
                                urgent:{
                                  ...this.state.urgent,
                                  No:!this.state.urgent.No
                                }
                              })
                            }} />
                          <label htmlFor="urgentNo">{lang("No")}</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                      {lang("Date Reported")}
                  </div>
                </div>
                <div className="field">
                  <div className="control filter-date">
                    <DayPickerInput className="input is-info"
                      value={from}
                      placeholder={lang("From")}
                      format={"YYYY-MM-DD"}
                      formatDate={formatDate}
                      parseDate={parseDate}
                      dayPickerProps={{
                        selectedDays: [from, { from, to }],
                        disabledDays: { after: to },
                        toMonth: to,
                        modifiers,
                        numberOfMonths: 2,
                        onDayClick: () => this.to.getInput().focus(),
                        locale: lang("code"),
                        localeUtils: MomentLocaleUtils,
                      }}
                      onDayChange={this.handleFromChange}
                    /> 
                    {' '}{lang("To")}{' '}               
                    <DayPickerInput className="input is-info"
                      ref={el => (this.to = el)}
                      value={to}
                      placeholder={lang("To")}
                      format={"YYYY-MM-DD"}
                      formatDate={formatDate}
                      parseDate={parseDate}
                      dayPickerProps={{
                        selectedDays: [from, { from, to }],
                        disabledDays: { before: from },
                        modifiers,
                        month: from,
                        fromMonth: from,
                        numberOfMonths: 2,
                        locale: lang("code"),
                        localeUtils: MomentLocaleUtils,
                      }}
                      onDayChange={this.handleToChange}
                    />
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                      {lang("Sort By")}
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                    <div className="select is-info">
                      <select name="sortBy" value={this.state.order}
                      onChange={e=>{
                        this.setState({
                          order:e.target.value
                        })
                      }}
                      >
                        <option value="RequestDate desc">{lang("New to old")}</option>
                        <option value="RequestDate asc">{lang("Old to new")}</option>
                      </select>
                    </div>
                  </div>
                </div>
                
                <div className="field">
                  <div className="control has-text-centered has-text-grey-light">
                      ---------- {lang("Or")} ----------
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                      {lang("Ticket")}#
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                    <input className="input is-info" type="text" value={this.state.ticketID} placeholder={lang("Ticket")}
                      onChange={e => {
                        this.setState({ticketID:e.target.value});
                      }} />
                  </div>
                </div>

                <div className="field">
                  <div className="control has-text-centered">
                    <button className="button is-link" style={{marginRight:"3px", marginBottom:"3px"}} onClick={e=>{
                      this.handleFilter()
                    }}>
                      <FontAwesomeIcon icon={"filter"} /> {lang("Filter")}
                    </button>
                  </div>
                </div>

                <hr />

                <div className="field">
                  <div className="control">
                      {lang("Download")} <FontAwesomeIcon icon={"download"} />
                  </div>
                </div>

                <div className="field">
                  <div className="control has-text-centered">
                    
                    <button className="button is-warning" style={{marginRight:"3px", marginBottom:"3px"}} onClick={e=>{
                      this.handleExport("excel")
                    }}>
                      <FontAwesomeIcon icon={"file-excel"} /> {lang("Export Excel")}
                    </button>
                    <button className="button is-warning" style={{marginRight:"3px", marginBottom:"3px"}} onClick={e=>{
                      this.handleExport("pdf")
                    }}>
                      <FontAwesomeIcon icon={"file-pdf"} /> {lang("Export PDF")}
                    </button>
                  </div>
                </div>
              
              </div>

            </div>  
            <div className="column">
              <Pagination  recordsTotal={recordsTotal} page={page} link='dashboard' />
              <div className="columns">
                <div className="column">
                  {
                    (this.props.dashboardReducer.data.length>0) ?
                      this.props.dashboardReducer.data.map((each,key)=>{
                      return <ItemDefect 
                                applicationList={this.props.usersReducer.application}
                                langCode={lang("code")} 
                                data={{each,key: each.TicketID}} 
                                key={each.TicketID} 
                                propertyCode={this.props.dashboardReducer.propertyCode} 
                                showEdit={true}
                                showCancel={true}
                                handleFilter={()=>{this.handleFilter()}}
                              />})
                    :
                      <div className="message">
                        <div className="message-body">
                          {lang("No Data")}
                        </div>
                      </div>
                  }
                </div>
              </div>
              <Pagination  recordsTotal={recordsTotal} page={page} link='dashboard' />
            </div>            
          </div>
          
        </div>
        
        <Helmet>
          <style>{`
            .InputFromTo .DayPicker-Day--selected:not(.DayPicker-Day--start):not(.DayPicker-Day--end):not(.DayPicker-Day--outside) {
              background-color: #f0f8ff !important;
              color: #4a90e2;
            }
            .InputFromTo .DayPicker-Day {
              border-radius: 0 !important;
            }
            .InputFromTo .DayPicker-Day--start {
              border-top-left-radius: 50% !important;
              border-bottom-left-radius: 50% !important;
            }
            .InputFromTo .DayPicker-Day--end {
              border-top-right-radius: 50% !important;
              border-bottom-right-radius: 50% !important;
            }
            .InputFromTo .DayPickerInput-Overlay {
              width: 550px;
            }
            .InputFromTo-to .DayPickerInput-Overlay {
              margin-left: -198px;
            }
          `}</style>
        </Helmet>
      </div>
    );
  }
  getDefectStatus(){
    let defectStatus="";
    Object.keys(this.state.defectStatus).map((each, key)=>{
      let s="";
      if(this.state.defectStatus[each]){
        switch(each){
          case "Open": s="Open"; break;
          case "OnProgress": s="On-progress"; break;
          case "ReSchedule": s="Re-schedule"; break;
          case "ReAssigned": s="Re-assigned"; break;
          case "Closed": s="Closed"; break;
          case "Cancel": s="Cancel"; break;
          default: s="Cancel"; break;
        }
        defectStatus+=",'"+s+"'";
      }
    });
    return defectStatus.substring(1, defectStatus.length);
  }
  showFromMonth() {
    const { from, to } = this.state;
    if (!from) {
      return;
    }
    if (moment(to).diff(moment(from), 'months') < 2) {
      this.to.getDayPicker().showMonth(from);
    }
  }
  handleFromChange(from, modifiers, dayPickerInput) {
    // Change the from date and focus the "to" input field
    const input = dayPickerInput.getInput();
    this.setState({ 
      from,
      fromIsEmpty: !input.value.trim(),
      fromIsDisabled: modifiers.disabled === true 
    });
    //console.log(from);
  }
  handleToChange(to, modifiers, dayPickerInput) {
    const input = dayPickerInput.getInput();
    this.setState({ 
      to,
      toIsEmpty: !input.value.trim(),
      toIsDisabled: modifiers.disabled === true  }, this.showFromMonth);
      
    //console.log(to);
  }

  getDefectUrgent(){
    let urgent="";
    Object.keys(this.state.urgent).map((each, key)=>{
      let s="";
      if(this.state.urgent[each]){
        switch(each){
          case "Yes": s="1"; break;
          case "No": s="0"; break;
          default: s="0"; break;
        }
        urgent+=",'"+s+"'";
      }
    });
    return urgent.substring(1, urgent.length);
  }
  async handleFilter() {
    //console.log("handleFilter");
    //this.props.location.search="";
    this.props.history.push({
      search:""
    });
    const { access_token } = this.props.loginReducer;
    const {page, propertyCode, search, order}=this.state;
    //const page=1;
    const defectStatus=this.getDefectStatus();
    const urgent=this.getDefectUrgent();
    ////console.log(defectStatus);
    if(this.state.ticketID==="")
    {
      this.props.history.push("/dashboard");
      await this.props.dispatch(actionLoadData(access_token, propertyCode
        , search, ((page-1)*CONFIG.page_length), CONFIG.page_length, order, defectStatus, urgent,
        moment(this.state.from).format('YYYY-MM-DD'),
        moment(this.state.to).format('YYYY-MM-DD')));
    }
    else{
      this.props.history.push("/dashboard/"+this.state.ticketID);
      this.handleTicketID(access_token,this.state.ticketID);
    }
    this.setState({
      data: this.props.dashboardReducer.data ,
      page
    })
  }
  async handleExport(fileType="excel"){
    this.handleFilter();
    const type="DASHBOARD";
    const { usercode }=this.props.loginReducer.userinfo;
    const entityCode = "";
    const pageSize = "";
    const pageNum = "";
    const {propertyCode, status,urgent,search,from,to,order}=this.props.dashboardReducer;

    /*const url="http://rphnavapp/ReportServer?/MA_Defect&rs:Format="+fileType+"&Type="+type+
                "&UserCode="+usercode+"&Status="+status+"&Urgent="+urgent+
                "&EntityCode="+entityCode+"&PropertyCode="+propertyCode+
                "&PageSize="+pageSize+"&PageNum="+pageNum+"&Search="+search+
                "&StartRequestDate="+from+"&EndRequestDate="+to+"&OrderBy="+order;*/
    let url=report_url;
    const report_name="MA_Defect";
    const params={
      Type:type,
      UserCode:usercode,
      Status:status,
      Urgent:urgent,
      EntityCode:entityCode,
      PropertyCode:propertyCode,
      PageSize:pageSize,
      PageNum:pageNum,
      Search:search,
      StartRequestDate:from,
      EndRequestDate:to,
      OrderBy:order
    }
    url=url+"?rpt_name="+report_name+"&type="+fileType+"&params="+JSON.stringify(params);
    ////console.log(url);
    window.open(url, '_blank');
  }
  async componentDidUpdate(prevProps) {
    ////console.log(this.props.location.search); // ?p=1
    const { access_token } = this.props.loginReducer;
    
    const queryString = qs.parse(this.props.location.search);
    const queryString_prev = qs.parse(prevProps.location.search);

    const prevPage = queryString_prev.p || 1;// || this.state.page; //prevProps.location.search.replace(/\?p\=/, "")
    const page = queryString.p || 1;// || this.state.page;
    const defectStatus=this.getDefectStatus();
    const urgent=this.getDefectUrgent();
    ////console.log(page);
    if (prevPage !== page) {
      const {propertyCode, search, page_length, order}=this.props.dashboardReducer;
      await this.props.dispatch(actionLoadData(access_token, propertyCode
        , search, ((page-1)*page_length), page_length, order, defectStatus, urgent, 
        moment(this.state.from).format('YYYY-MM-DD'), 
        moment(this.state.to).format('YYYY-MM-DD')));
      this.setState({
        data: this.props.dashboardReducer.data ,
        page
      })
    }
    
  }
  
  async componentDidMount() {
    let { access_token = "", access_token_exp = 0 } = this.props.loginReducer; //this.state;
    //console.log(this.props.loginReducer);
    if (access_token === "" || access_token_exp < Date.now()) {
      const request_token = localStorage.getItem("request_token");
      await this.props.dispatch(actionGetAccessToken(request_token));
      //console.log(request_token);
    }
    access_token = this.props.loginReducer.access_token;
    //console.log(access_token);
    if(access_token!=="")
    {
      
      /** Get Application By UserCode & Check Permission */
      await this.props.dispatch(actionGetApplication(access_token));
      const {application=[]}=this.props.usersReducer;
      //checkPermissionApplication(application,"MD_DASHBOARD");
      /** Get Application By UserCode & Check Permission */


      const queryString = qs.parse(this.props.location.search);
      ////console.log(queryString.p)
      ////console.log(queryString.lang)
      const page = queryString.p || this.state.page;

      
      const {data, propertyCode, search, page_length, order}=this.props.dashboardReducer;
      
      const defectStatus=this.getDefectStatus();
      const urgent=this.getDefectUrgent();
      ////console.log(defectStatus);
      //console.log(this.props);
      const {ticketID}=this.props.match.params;
      if(!ticketID){
        if(data.length===0 || propertyCode!==this.props.propertyReducer.propertyCode){
          await this.props.dispatch(
            actionLoadData(
              access_token, 
              this.props.propertyReducer.propertyCode, 
              "", 
              ((page-1)*CONFIG.page_length), 
              CONFIG.page_length, 
              this.state.order, 
              defectStatus,
              urgent, 
              moment(this.state.from).format('YYYY-MM-DD'), 
              moment(this.state.to).format('YYYY-MM-DD')
            )
          );
        }
        else
        {
          await this.props.dispatch(actionLoadData(access_token, propertyCode
            , search, ((page-1)*page_length), page_length, order, defectStatus, urgent, 
            moment(this.state.from).format('YYYY-MM-DD'), 
            moment(this.state.to).format('YYYY-MM-DD')));
        }
      }
      else
      {
        this.handleTicketID(access_token,ticketID);
      }


      this.setState({
        data: this.props.dashboardReducer.data,
        page
      });


    }
    
  }

  async handleTicketID(access_token,ticketID){
    await this.setState({
      ticketID
    })
    await this.props.dispatch(
      actionLoadData(
        access_token, 
        "", 
        ticketID, 
        0, 
        1, 
        "RequestDate desc", 
        "'Open','On-progress','Re-schedule','Re-assigned','Closed','Cancel'", 
        "'1','0'", 
        "", 
        ""
      )
    );
  }
  
}

const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    propertyReducer: state.propertyReducer,
    dashboardReducer: state.dashboardReducer,
    usersReducer: state.usersReducer
  };
};
export default connect(mapStateToProps)(Dashboard);
