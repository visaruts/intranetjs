import React, { Component } from "react";
import { connect } from "react-redux";
import { lang } from "../Lang.jsx";
import { actionGetAccessToken } from "../actions/login";
import {
  actionLoadMyListData
} from "../actions/mylist";
import { ENV, CONFIG } from "../Config_API.jsx";
import qs from "query-string";
import {TinyButton as ScrollUpButton} from "react-scroll-up-button";

/**react-day-picker */
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import MomentLocaleUtils, {
  formatDate,
  parseDate,
} from 'react-day-picker/moment';
import 'moment/locale/it';
import moment from 'moment';
/**react-day-picker */

import PropertySelectBox from "../components/PropertySelectBox.jsx";
import ItemDefect from "../components/ItemDefect.jsx";
import Pagination from "../components/Pagination.jsx";

/** fontawesome */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';


class MyList extends Component {
  state = {
    propertyCode:"",
    search:"",
    defectStatus:{
      Open:true,
      OnProgress:true,
      ReSchedule:true,
      ReAssigned:true,
      Closed:false,
      Cancel:false
    },
    urgent:{
      Yes:true,
      No:true
    },
    order:"RequestDate desc",
    from: new Date(new Date().setMonth(new Date().getMonth() - 3)),
    fromIsEmpty:true,
    fromIsDisabled: false,
    to: new Date(),
    toIsEmpty:true,
    toIsDisabled: false,
    data:[],
    showFilter:true
  };

  handleFromChange = this.handleFromChange.bind(this);
  handleToChange = this.handleToChange.bind(this);
  

  render() {
    //console.log(this.props.mylistReducer);
    // Page _GET
    const queryString = qs.parse(this.props.location.search);
    const page = queryString.p || 1;
    const recordsTotal=this.props.mylistReducer.recordsTotal;
    
    const { from, to } = this.state;
    const modifiers = { start: from, end: to };

    return (
      <div className="notification">
        <ScrollUpButton />
        <div className="has-text-centered">
          <h1 className="title">
            {lang("MD_MYLIST")}{" "}
            {ENV !== "PROD" ? (
              <span className="has-text-danger"> ( {ENV} )</span>
            ) : (
              ""
            )}
          </h1>
        </div>
        <br />
        <div>
          
          <div className="columns">
            <div className="message is-black column is-one-quarter" style={{padding: "0rem"}}>
            
              <div className="has-text-right message-header nav-filter"
                  onClick={(e)=>{
                    e.preventDefault();
                    this.setState({
                      showFilter:!this.state.showFilter
                    })
                  }}>
                <a href="#" aria-label="hide filter"
                  onClick={(e)=>{
                    e.preventDefault();
                    this.setState({
                      showFilter:!this.state.showFilter
                    })
                  }}
                  style={{textDecoration: "none"}}
                >
                  {this.state.showFilter?lang("Hide Filter")+" ":lang("Show Filter")+" "} 
                  <FontAwesomeIcon icon={this.state.showFilter?"angle-up":"angle-down"} />
                </a>
              </div>

              <div className="message-body" style={
                this.state.showFilter?
                  {display: ""}:
                  {display: "none"}
                }>
                <div className="filed">
                  {lang("Hotel")}:
                </div>
                <div className="filed">
                  <div className="select is-info">
                    <PropertySelectBox 
                      showSelectAll="true" 
                      lang={lang("Hotel")} 
                      propertyCode={this.props.mylistReducer.propertyCode}
                      saveState={e => {
                        this.setState({
                          propertyCode: this.props.propertyReducer.propertyCode
                        });
                      }}
                    />
                  </div>
                </div>
                <div className="filed">
                  {lang("Search")}
                </div>
                <div className="field">
                  <div className="control">
                    <input className="input is-info" type="text" placeholder={lang("Search")}
                      onChange={e => {
                        this.setState({search:e.target.value});
                      }} />
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                      {lang("Status")}
                  </div>
                </div>
                <div className="field">
                  <div className="columns is-mobile">
                    <div className="column">
                      <div className="field">
                          <input id="switchOpen" type="checkbox" className="is-checkradio is-info is-circle"
                            defaultChecked={this.state.defectStatus.Open}
                            onClick={e=>{
                              this.setState({
                                defectStatus:{
                                  ...this.state.defectStatus,
                                  Open:!this.state.defectStatus.Open
                                }
                              })
                            }} />
                          <label htmlFor="switchOpen">{lang("Open")}</label>
                      </div>
                    </div>
                    <div className="column">
                      <div className="field">
                          <input id="switchOnProgress" type="checkbox" className="is-checkradio is-info is-circle"
                            defaultChecked={this.state.defectStatus.OnProgress}
                            onClick={e=>{
                              this.setState({
                                defectStatus:{
                                  ...this.state.defectStatus,
                                  OnProgress:!this.state.defectStatus.OnProgress
                                }
                              })
                            }} />
                          <label htmlFor="switchOnProgress">{lang("On-progress")}</label>
                      </div>
                    </div>
                  </div>
                  <div className="columns is-mobile">
                    <div className="column">
                      <div className="field">
                          <input id="switchReSchedule" type="checkbox" className="is-checkradio is-info is-circle"
                            defaultChecked={this.state.defectStatus.ReSchedule}
                            onClick={e=>{
                              this.setState({
                                defectStatus:{
                                  ...this.state.defectStatus,
                                  ReSchedule:!this.state.defectStatus.ReSchedule
                                }
                              })
                            }} />
                          <label htmlFor="switchReSchedule">{lang("Re-schedule")}</label>
                      </div>
                    </div>
                    <div className="column">
                      <div className="field">
                          <input id="switchReAssigned" type="checkbox" className="is-checkradio is-info is-circle"
                            defaultChecked={this.state.defectStatus.ReAssigned}
                            onClick={e=>{
                              this.setState({
                                defectStatus:{
                                  ...this.state.defectStatus,
                                  ReAssigned:!this.state.defectStatus.ReAssigned
                                }
                              })
                            }} />
                          <label htmlFor="switchReAssigned">{lang("Re-assigned")}</label>
                      </div>
                    </div>
                  </div>
                  <div className="columns is-mobile">
                    <div className="column">
                      <div className="field">
                          <input id="switchClosed" type="checkbox" className="is-checkradio is-info is-circle"
                            defaultChecked={this.state.defectStatus.Closed}
                            onClick={e=>{
                              this.setState({
                                defectStatus:{
                                  ...this.state.defectStatus,
                                  Closed:!this.state.defectStatus.Closed
                                }
                              })
                            }} />
                          <label htmlFor="switchClosed">{lang("Closed")}</label>
                      </div>
                    </div>
                    <div className="column">
                      <div className="field">
                          <input id="switchCancel" type="checkbox" className="is-checkradio is-info is-circle"
                            defaultChecked={this.state.defectStatus.Cancel}
                            onClick={e=>{
                              this.setState({
                                defectStatus:{
                                  ...this.state.defectStatus,
                                  Cancel:!this.state.defectStatus.Cancel
                                }
                              })
                            }} />
                          <label htmlFor="switchCancel">{lang("Cancel")}</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                      {lang("Urgent")}
                  </div>
                </div>
                <div className="field">
                  <div className="columns is-mobile">
                    <div className="column">
                      <div className="field">
                          <input id="urgentYes" type="checkbox" className="is-checkradio is-info is-circle"
                            defaultChecked={this.state.urgent.Yes}
                            onClick={e=>{
                              this.setState({
                                urgent:{
                                  ...this.state.urgent,
                                  Yes:!this.state.urgent.Yes
                                }
                              })
                            }} />
                          <label htmlFor="urgentYes">{lang("Yes")}</label>
                      </div>
                    </div>
                    <div className="column">
                      <div className="field">
                      <input id="urgentNo" type="checkbox" className="is-checkradio is-info is-circle"
                            defaultChecked={this.state.urgent.No}
                            onClick={e=>{
                              this.setState({
                                urgent:{
                                  ...this.state.urgent,
                                  No:!this.state.urgent.No
                                }
                              })
                            }} />
                          <label htmlFor="urgentNo">{lang("No")}</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                      {lang("Date Reported")}
                  </div>
                </div>
                <div className="field">
                  <div className="control filter-date">
                    <DayPickerInput className="input is-info"
                      value={from}
                      placeholder={lang("From")}
                      format={"YYYY-MM-DD"}
                      formatDate={formatDate}
                      parseDate={parseDate}
                      dayPickerProps={{
                        selectedDays: [from, { from, to }],
                        disabledDays: { after: to },
                        toMonth: to,
                        modifiers,
                        numberOfMonths: 2,
                        onDayClick: () => this.to.getInput().focus(),
                        locale: lang("code"),
                        localeUtils: MomentLocaleUtils,
                      }}
                      onDayChange={this.handleFromChange}
                    /> 
                    {' '}{lang("To")}{' '}               
                    <DayPickerInput className="input is-info"
                      ref={el => (this.to = el)}
                      value={to}
                      placeholder={lang("To")}
                      format={"YYYY-MM-DD"}
                      formatDate={formatDate}
                      parseDate={parseDate}
                      dayPickerProps={{
                        selectedDays: [from, { from, to }],
                        disabledDays: { before: from },
                        modifiers,
                        month: from,
                        fromMonth: from,
                        numberOfMonths: 2,
                        locale: lang("code"),
                        localeUtils: MomentLocaleUtils,
                      }}
                      onDayChange={this.handleToChange}
                    />
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                      {lang("Sort By")}
                  </div>
                </div>
                <div className="field">
                  <div className="control">
                    <div className="select is-info">
                      <select name="sortBy" value={this.state.order}
                      onChange={e=>{
                        this.setState({
                          order:e.target.value
                        })
                      }}
                      >
                        <option value="RequestDate desc">{lang("New to old")}</option>
                        <option value="RequestDate asc">{lang("Old to new")}</option>
                      </select>
                  </div>
                  </div>
                </div>
                

                <div className="field">
                  <div className="control has-text-centered">
                    <button className="button is-link" onClick={e=>{
                      this.handleFilter()
                    }}>
                      <FontAwesomeIcon icon={"filter"} /> {lang("Filter")}
                    </button>
                  </div>
                </div>
              
              </div>

            </div>  
            <div className="column">
              <Pagination  recordsTotal={recordsTotal} page={page} link='mylist' />
              <div className="columns">
                <div className="column">                  
                  {
                    (this.props.mylistReducer.data.length>0) ?
                      this.props.mylistReducer.data.map((each,key)=>{
                      return <ItemDefect 
                                langCode={lang("code")} 
                                data={{each,key: each.TicketID}} 
                                key={each.TicketID} 
                                propertyCode={this.props.mylistReducer.propertyCode} 
                                showEdit={false}
                                showCancel={true}
                                handleFilter={()=>{this.handleFilter()}}
                              />})
                    :
                      <div className="message">
                        <div className="message-body">
                          {lang("No Data")}
                        </div>
                      </div>
                  }
                </div>
              </div>
              <Pagination  recordsTotal={recordsTotal} page={page} link='mylist' />
            </div>            
          </div>
          
        </div>
        
      </div>
    );
  }
  getDefectStatus(){
    let defectStatus="";
    Object.keys(this.state.defectStatus).map((each, key)=>{
      let s="";
      if(this.state.defectStatus[each]){
        switch(each){
          case "Open": s="Open"; break;
          case "OnProgress": s="On-progress"; break;
          case "ReSchedule": s="Re-schedule"; break;
          case "ReAssigned": s="Re-assigned"; break;
          case "Closed": s="Closed"; break;
          case "Cancel": s="Cancel"; break;
          default: s="Cancel"; break;
        }
        defectStatus+=",'"+s+"'";
      }
    });
    return defectStatus.substring(1, defectStatus.length);
  }
  showFromMonth() {
    const { from, to } = this.state;
    if (!from) {
      return;
    }
    if (moment(to).diff(moment(from), 'months') < 2) {
      this.to.getDayPicker().showMonth(from);
    }
  }
  handleFromChange(from, modifiers, dayPickerInput) {
    // Change the from date and focus the "to" input field
    const input = dayPickerInput.getInput();
    this.setState({ 
      from,
      fromIsEmpty: !input.value.trim(),
      fromIsDisabled: modifiers.disabled === true 
    });
    console.log(from);
  }
  handleToChange(to, modifiers, dayPickerInput) {
    const input = dayPickerInput.getInput();
    this.setState({ 
      to,
      toIsEmpty: !input.value.trim(),
      toIsDisabled: modifiers.disabled === true  }, this.showFromMonth);
      
    console.log(to);
  }

  getDefectUrgent(){
    let urgent="";
    Object.keys(this.state.urgent).map((each, key)=>{
      let s="";
      if(this.state.urgent[each]){
        switch(each){
          case "Yes": s="1"; break;
          case "No": s="0"; break;
          default: s="0"; break;
        }
        urgent+=",'"+s+"'";
      }
    });
    return urgent.substring(1, urgent.length);
  }
  async handleFilter() {
    //this.props.location.search="";
    this.props.history.push({
      search:""
    });
    const { access_token } = this.props.loginReducer;
    const {propertyCode, search, order}=this.state;
    const page=1;
    const defectStatus=this.getDefectStatus();
    const urgent=this.getDefectUrgent();
    //console.log(defectStatus);
    await this.props.dispatch(actionLoadMyListData(access_token, propertyCode
      , search, ((page-1)*CONFIG.page_length), CONFIG.page_length, order, defectStatus, urgent,
      moment(this.state.from).format('YYYY-MM-DD'),
      moment(this.state.to).format('YYYY-MM-DD')));
    this.setState({
      data: this.props.mylistReducer.data 
    })
  }
  async componentDidUpdate(prevProps) {
    const { access_token } = this.props.loginReducer;
    
    const queryString = qs.parse(this.props.location.search);
    const queryString_prev = qs.parse(prevProps.location.search);

    const prevPage = queryString_prev.p || 1; 
    const page = queryString.p || 1;
    const defectStatus=this.getDefectStatus();
    const urgent=this.getDefectUrgent();
    //console.log(page);
    if (prevPage !== page) {
      const {propertyCode, search, page_length, order}=this.props.mylistReducer;
      await this.props.dispatch(actionLoadMyListData(access_token, propertyCode
        , search, ((page-1)*page_length), page_length, order, defectStatus, urgent, 
        moment(this.state.from).format('YYYY-MM-DD'), 
        moment(this.state.to).format('YYYY-MM-DD')));
      this.setState({
        data: this.props.mylistReducer.data 
      })
    }
    
  }
  
  async componentDidMount() {
    let { access_token = "", access_token_exp = 0 } = this.props.loginReducer;
    //console.log(this.props.loginReducer);
    if (access_token === "" || access_token_exp < Date.now()) {
      const request_token = localStorage.getItem("request_token");
      await this.props.dispatch(actionGetAccessToken(request_token));
      //console.log(request_token);
    }
    access_token = this.props.loginReducer.access_token;
    //console.log(access_token);
    if(access_token!=="")
    {
      const queryString = qs.parse(this.props.location.search);
      const page = queryString.p || 1;

      
      const {data, propertyCode, search, page_length, order}=this.props.mylistReducer;
      
      const defectStatus=this.getDefectStatus();
      const urgent=this.getDefectUrgent();
      if(data.length===0 || propertyCode!==this.props.propertyReducer.propertyCode){
        await this.props.dispatch(
          actionLoadMyListData(
            access_token, 
            this.props.propertyReducer.propertyCode, 
            "", 
            ((page-1)*CONFIG.page_length), 
            CONFIG.page_length, 
            this.state.order, 
            defectStatus,
            urgent, 
            moment(this.state.from).format('YYYY-MM-DD'), 
            moment(this.state.to).format('YYYY-MM-DD')
          )
        );
      }
      else
      {
        await this.props.dispatch(actionLoadMyListData(access_token, propertyCode
          , search, ((page-1)*page_length), page_length, order, defectStatus, urgent, 
          moment(this.state.from).format('YYYY-MM-DD'), 
          moment(this.state.to).format('YYYY-MM-DD')));
      }


      this.setState({
        data: this.props.mylistReducer.data 
      })
    }
    
  }
  
}

const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    propertyReducer: state.propertyReducer,
    mylistReducer: state.mylistReducer
  };
};
export default connect(mapStateToProps)(MyList);
