import React, { Component } from "react";
import { Prompt } from "react-router";
import { connect } from "react-redux";
import { lang } from "../Lang.jsx";
import { api, ENV } from "../Config_API.jsx";
import { view } from "../templates/email/insert_ma.jsx";
import {TinyButton as ScrollUpButton} from "react-scroll-up-button";
import axios from "axios";
/** moment datetime */
import moment from 'moment';
import 'moment/min/locales.min';


/**react-day-picker */
import DayPickerInput from 'react-day-picker/DayPickerInput';
import 'react-day-picker/lib/style.css';
import MomentLocaleUtils, {
  formatDate,
  parseDate,
} from 'react-day-picker/moment';
/**react-day-picker */
/**react-day-picker */
import DatePicker from "../components/DatePicker.jsx";
/**react-day-picker */

import { actionGetAccessToken } from "../actions/login";
import { Calendar_HQ, aLeave } from "../db.json"

class Leave extends Component {
  state = {
    thisYear:2019,
    month:["January","February","March","April","May","June","July","August","September","October","November","December"],
    day:[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31],
    leave_color:{
      "leave": "#0C0",
      "sick": "#e01f5f"
    },
    calendar:{},
    from:undefined,
    from_Half:null,
    to:undefined,
    to_Half:null,
    selectedLeaveType:null,
    selectedCalendar:{},
    seletedDayCount:0.0,
    errSelectedDate:"",
  }
  handleFromChange = this.handleFromChange.bind(this);
  handleToChange = this.handleToChange.bind(this);
  render() {
    const {thisYear, month, day, calendar, leave_color, 
      from, to, selectedLeaveType, from_Half, to_Half,
      selectedCalendar, seletedDayCount,
      errSelectedDate
    }=this.state;
    
    const modifiers = { start: from, end: to };

    return (
      <div className="notification">
        <ScrollUpButton />
        <div className="columns is-gapless is-multiline">
          <div className="column box margin5 is-two-thirds" style={{borderRadius: "0px"}}>
            <div className="column">
              {moment(from).format('YYYY-MM-DD')} {moment(to).format('YYYY-MM-DD')} {from_Half} {to_Half} {selectedLeaveType} 
              {seletedDayCount}
              <div className="has-text-danger">{errSelectedDate}</div>
              <DatePicker lang={lang("lang")} selectedDate={from} handleChange={async (from)=>{
                await this.setState({
                  from,
                  from_Half:"2",
                  selectedLeaveType:"leave"
                });
                
                this.handleSelectDate();


                //console.log(from);
                }}/>
                <DatePicker lang={lang("lang")} selectedDate={to} handleChange={async (to)=>{
                await this.setState({
                  to,
                  to_Half:null,
                });
                
                this.handleSelectDate();


                //console.log(from);
                }}/>
              <button type="button" className="button is-info"
              onClick={e=>{
                let leave_fullday="full";
                let leave_type="leave";
                this.setState({
                  calendar:{
                    ...calendar,
                    [from]:{
                      ...calendar[from],
                      leave_fullday,
                      leave_type
                    }
                  }
                });
              }}>Save</button>

              <div>
                <div className="field">
                  <div className="control filter-date">
                    <DayPickerInput className="input is-info"
                      value={from}
                      placeholder={lang("From")}
                      format={"YYYY-MM-DD"}
                      formatDate={formatDate}
                      parseDate={parseDate}
                      dayPickerProps={{
                        selectedDays: [from, { from, to }],
                        disabledDays: { after: to },
                        toMonth: to,
                        modifiers,
                        numberOfMonths: 1,
                        onDayClick: () => this.to.getInput().focus(),
                        locale: lang("code"),
                        localeUtils: MomentLocaleUtils,
                      }}
                      onDayChange={this.handleFromChange}
                    /> 
                    {' '}{lang("To")}{' '}               
                    <DayPickerInput className="input is-info"
                      ref={el => (this.to = el)}
                      value={to}
                      placeholder={lang("To")}
                      format={"YYYY-MM-DD"}
                      formatDate={formatDate}
                      parseDate={parseDate}
                      dayPickerProps={{
                        selectedDays: [from, { from, to }],
                        disabledDays: { before: from },
                        modifiers,
                        month: from,
                        fromMonth: from,
                        numberOfMonths: 1,
                        locale: lang("code"),
                        localeUtils: MomentLocaleUtils,
                      }}
                      onDayChange={this.handleToChange}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="column box margin5 is-one-thirds" style={{borderRadius: "0px"}}>
            <a href="#" className="box-header-icon" aria-label="more options">
              <span className="icon">
                <i className="fas fa-angle-right" aria-hidden="true"></i>
              </span>
            </a>
            <div className="column">
              {"Leave"}
            </div>
          </div>
          <div className="column box margin5 is-full" style={{borderRadius: "0px"}}>
            <div className="columns" style={{margin:"3px 3px"}}>
              <div className="column is-narrow" style={{overflow:"auto"}}>
                
                <table border="1" style={{fontSize:"10px", borderColor: "#000000"}}>
                  <tbody>
                    <tr>
                      <td style={{padding:"2px"}}>{thisYear}</td>
                      {
                        day.map((d,key)=>{
                          return <td key={key} className="has-text-centered" style={{padding:"2px", width:"20px"}}>{d}</td>;
                        })
                      }
                    </tr>
                    {
                      month.map((month_name, month_key)=>{
                        return <tr key={month_key}>
                          <td>{month_name}</td>
                          {
                            day.map((day_number,day_key)=>{
                              const ymd=thisYear.toString() + "-" + ("0" + (month_key+1).toString()).slice(-2) + "-" + ("0" + day_number.toString()).slice(-2);
                              let backgroundColor="#ffffff";
                              let holiday="";
                              let text_half="";
                              let calendar_temp=null;
                              if(selectedCalendar[ymd] && calendar[ymd].IsHoliday_Office===0)
                              {
                                calendar_temp=selectedCalendar[ymd];   
                              }
                              else if(calendar[ymd])
                              {
                                calendar_temp=calendar[ymd];                                
                              }
                              //console.log(selectedCalendar[ymd]);

                              if(calendar_temp!==null){
                                if(calendar_temp.IsHoliday_Office===1)
                                {
                                  if(calendar_temp.Holiday!==null)
                                  {
                                    backgroundColor="#b5b0b0";
                                    holiday=calendar_temp.Holiday;
                                  }
                                  else
                                  {
                                    backgroundColor="#837c7c";
                                  }
                                  
                                }
                                else
                                {
                                  if(calendar_temp.leave_fullday!==null){
                                    backgroundColor=leave_color[calendar_temp.leave_type];
                                    if(calendar_temp.leave_fullday!=="full"){
                                      text_half="0.5";
                                    }
                                    if(selectedCalendar[ymd])
                                    {
                                      if(selectedCalendar[ymd].leave_fullday!=="null"){
                                        text_half="";
                                      }
                                    }
                                  }
                                }
                                
                              }
                              return <td 
                              key={day_key} 
                              className="has-text-centered" 
                              style={{padding:"2px", backgroundColor, color:"#ffffff"}}
                              title={holiday}
                              >
                                {text_half}
                              </td>;
                            })
                          }
                        </tr>;
                      })
                    }
                  </tbody>
                </table>

              </div>
              
              <div className="column">
                {"Leave"}
              </div>
            </div>
          </div>
        </div>
        
      </div>
    );
  }
  
  showFromMonth() {
    const { from, to } = this.state;
    if (!from) {
      return;
    }
    if (moment(to).diff(moment(from), 'months') < 2) {
      this.to.getDayPicker().showMonth(from);
    }
  }
  async handleFromChange(from, modifiers, dayPickerInput) {
    // Change the from date and focus the "to" input field
    const input = dayPickerInput.getInput();
    await this.setState({ 
      from,
      from_Half:"2",
      selectedLeaveType:"leave"
    });
    this.handleSelectDate();
    //console.log(from);
  }
  async handleToChange(to, modifiers, dayPickerInput) {
    const input = dayPickerInput.getInput();
    await this.setState({ to,
      to_Half:null }, this.showFromMonth);
    this.handleSelectDate();  
    //console.log(to);
  }

  handleSelectDate(){
    const {
      calendar, day, thisYear, month, 
      from, to, selectedLeaveType, 
      from_Half, to_Half
    }=this.state;
    if(from!==undefined 
      && to!==undefined 
      && selectedLeaveType!==null)
    {
      console.log(from);
      console.log(to);
      var date1 = new Date(moment(from).format('YYYY-MM-DD'));
      var date2 = new Date(moment(to).format('YYYY-MM-DD'));
      var dateAdd = new Date(moment(from).format('YYYY-MM-DD'));
      //var timeDiff = Math.abs(date2.getTime() - date1.getTime());
      var timeDiff = (date2.getTime() - date1.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
      console.log(diffDays);
      if(diffDays>=0)
      {
        var dateAdd_str=dateAdd.getFullYear()+"-"+("0"+(dateAdd.getMonth()+1).toString()).slice(-2)+"-"+("0"+dateAdd.getDate()).slice(-2);
        let selectedCalendar={};
        let leave_fullday="";
        if(diffDays===0){
          if(from_Half==="1" && to_Half===null){
            leave_fullday="morning";
          }else if(from_Half==="1" && to_Half==="1"){
            leave_fullday="morning";
          }else if(from_Half==="2" && to_Half===null){
            leave_fullday="afternoon";
          }else if(from_Half==="2" && to_Half==="2"){
            leave_fullday="afternoon";
          }else{
            leave_fullday="full";
          }
          console.log(from_Half);
        }
        else
        {
          if(from_Half==="1"){
            leave_fullday="morning";
          }else if(from_Half==="2"){
            leave_fullday="afternoon";
          }else{
            leave_fullday="full";
          }
        }
        
        selectedCalendar = {
          ...selectedCalendar,
          [dateAdd_str]:{
            ...selectedCalendar[dateAdd_str],
            leave_type:selectedLeaveType,
            leave_fullday,
            Calendar_Date: dateAdd_str,
            Calendar_DayOfWeek: "",
            Holiday: null,
            IsHoliday_Office: 0
          }
        };

        //console.log("      >> " + dateAdd_str);
        for(var d=0;d<diffDays;d++){
          dateAdd.setDate(dateAdd.getDate()+1);
          dateAdd_str=dateAdd.getFullYear()+"-"+("0"+(dateAdd.getMonth()+1).toString()).slice(-2)+"-"+("0"+dateAdd.getDate()).slice(-2);
          //console.log("      >> " + dateAdd_str);
          if(to_Half==="1"){
            leave_fullday="morning";
          }else if(to_Half==="2"){
            leave_fullday="afternoon";
          }else{
            leave_fullday="full";
          }
          selectedCalendar = {
            ...selectedCalendar,
            [dateAdd_str]:{
              ...selectedCalendar[dateAdd_str],
              leave_type:selectedLeaveType,
              leave_fullday,
              Calendar_Date: dateAdd_str,
              Calendar_DayOfWeek: "",
              Holiday: null,
              IsHoliday_Office: 0
            }
          };
        }
        let seletedDayCount=0;
        let isDuplicate=false;
        let dateDuplicate=[];
        month.map((month_name, month_key)=>{
          day.map((day_number,day_key)=>{
            const ymd=thisYear.toString() + "-" + ("0" + (month_key+1).toString()).slice(-2) + "-" + ("0" + day_number.toString()).slice(-2);                  
            //console.log(calendar[ymd]);
            if(selectedCalendar[ymd] && calendar[ymd].IsHoliday_Office===0)
            {
              if(selectedCalendar[ymd].leave_fullday==="full")
              {
                seletedDayCount=seletedDayCount+1;
              }
              else
              {
                seletedDayCount=seletedDayCount+0.5;
              }
              //console.log(selectedCalendar[ymd]);
              console.log(calendar[ymd]);
              console.log(selectedCalendar[ymd]);
            }
            if(selectedCalendar[ymd] && 
              calendar[ymd].leave_type!==null && 
              (
                calendar[ymd].leave_fullday===selectedCalendar[ymd].leave_fullday ||
                (
                  calendar[ymd].leave_fullday==="full" || selectedCalendar[ymd].leave_fullday==="full"
                )
              )

            )
            {
              isDuplicate=true;
              dateDuplicate.push(ymd);
              //console.log(calendar[ymd]);
            }

          });
        });
        
        if(!isDuplicate)
        {
          this.setState({
            selectedCalendar,
            seletedDayCount,
            errSelectedDate:""
          });
        }
        else
        {
          this.setState({
            selectedCalendar:{},
            from:undefined,
            to:undefined,
            errSelectedDate:`Sorry, you have duplicate date >> ${dateDuplicate.toString()}`
          });
        }
      }
      else
      {
        if(lang("code")==="th"){
          moment.locale('th');
        }
        else if(lang("code")==="ja"){
          moment.locale('ja');
        }
        else if(lang("code")==="id"){
          moment.locale('id');
        }
        else
        {
          moment.locale('en');
        }
        let startDate=moment(from).format('LL');
        let endDate=moment(to).format('LL');
        this.setState({
          selectedCalendar:{},
          from:undefined,
          to:undefined,
          errSelectedDate:`Sorry, your start date( ${startDate} ) and end date( ${endDate} ) is incorrect. Please kindly check!`
        });
      }

    }
    else{
      this.setState({
        selectedCalendar:{},
        errSelectedDate:""
      });
    }
    
  }

  async componentDidMount() {
    let { access_token = "", access_token_exp = 0 } = this.props.loginReducer; //this.state;
    //console.log(this.props.loginReducer);
    if (access_token === "") {
      const request_token = localStorage.getItem("request_token");
      await this.props.dispatch(actionGetAccessToken(request_token));
    }
    access_token = this.props.loginReducer.access_token;
    //const url="https://intranet.redplanethotels.com/en/leave/leave_api/get_weekend/THA/2019/HQ";
    //const res = await axios.get(url);
    const res=Calendar_HQ;
    let calendar = {};
    res.map(each=>{
      calendar = {
        ...calendar,
        [each.Calendar_Date]:{
          ...each,
          leave_type:null,
          leave_fullday:null
        }
      };
    });

    aLeave.map(each=>{
      var date1 = new Date(each.StartDate);
      var date2 = new Date(each.EndDate);
      var dateAdd = new Date(each.StartDate);
      var timeDiff = Math.abs(date2.getTime() - date1.getTime());
      var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
      console.log("Start: " + each.StartDate + ", End: " + each.EndDate + " >> " + diffDays.toString());
      let dateAdd_str=dateAdd.getFullYear()+"-"+("0"+(dateAdd.getMonth()+1).toString()).slice(-2)+"-"+("0"+dateAdd.getDate()).slice(-2);
      //console.log(dateAdd_str);
      if(calendar[dateAdd_str]){
        
        let {leave_fullday}=calendar[dateAdd_str];
        if(leave_fullday!==null){
          leave_fullday="full";
          
          calendar = {
            ...calendar,
            [dateAdd_str]:{
              ...calendar[dateAdd_str],
              leave_fullday
            }
          };
          //console.log(leave_fullday);
        }
        else
        {
          //if(date1===date2){
          if(diffDays===0){
            if(each.StartDate_half==="1" && each.EndDate_half===null){
              leave_fullday="morning";
            }else if(each.StartDate_half==="1" && each.EndDate_half==="1"){
              leave_fullday="morning";
            }else if(each.StartDate_half==="2" && each.EndDate_half===null){
              leave_fullday="afternoon";
            }else if(each.StartDate_half==="2" && each.EndDate_half==="2"){
              leave_fullday="afternoon";
            }else{
              leave_fullday="full";
            }
            
          }
          else
          {
            if(each.StartDate_half==="1"){
              leave_fullday="morning";
            }else if(each.StartDate_half==="2"){
              leave_fullday="afternoon";
            }else{
              leave_fullday="full";
            }
          }
          
          calendar = {
            ...calendar,
            [dateAdd_str]:{
              ...calendar[dateAdd_str],
              leave_type:each.Type,
              leave_fullday
            }
          };
        }
        //console.log("      >> " + dateAdd_str);
        for(var d=0;d<diffDays;d++){
          dateAdd.setDate(dateAdd.getDate()+1);
          dateAdd_str=dateAdd.getFullYear()+"-"+("0"+(dateAdd.getMonth()+1).toString()).slice(-2)+"-"+("0"+dateAdd.getDate()).slice(-2);
          //console.log("      >> " + dateAdd_str);
          if(each.EndDate_half==="1"){
            leave_fullday="morning";
          }else if(each.EndDate_half==="2"){
            leave_fullday="afternoon";
          }else{
            leave_fullday="full";
          }
          calendar = {
            ...calendar,
            [dateAdd_str]:{
              ...calendar[dateAdd_str],
              leave_type:each.Type,
              leave_fullday
            }
          };
        }
      }
    });
    
    console.log(calendar);
    await this.setState({
      calendar,
      //from: moment().format('YYYY-MM-DD')
    });
    /*var y=2019;
    var m=1;
    var d=2;
    console.log(y.toString() + "-" + ("0" + m.toString()).slice(-2) + "-" + ("0" + d.toString()).slice(-2));*/
  }
}

const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    propertyReducer: state.propertyReducer
  };
};
export default connect(mapStateToProps)(Leave);
