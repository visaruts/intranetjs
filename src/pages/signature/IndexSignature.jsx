
import React, { Component } from "react";
import { connect } from "react-redux";
import "assets/css/contact.scss";
import ReactImageFallback from "react-image-fallback";
import { base_url_webapp,public_url, uniqid, ENV, s3_url_webapp, s3_url_intranet} from "../../Config_API.jsx";
import { actionHiddenNavbar, actionHiddenFooter } from "../../actions/config";
import { actionGetAccessToken } from "actions/login";
import { actionLoadUserCodeData ,actionLoadHotelData} from "actions/contact/actionContact";
class IndexSignature extends Component {
  state = {
    loading: '0',
    data: [],
    dataHotel:[]
  }
  
  render() {
    const {data, dataHotel} = this.state;
    let arrDisplay = [];

    //console.log(dataHotel);
    if(data.length > 0 && dataHotel.length > 0){
        let address1 = '';
        let address2 = '';
        if(data[0].Office == 'Office' || data[0].Office == 'HQ'){
            address1 = dataHotel[0].AddressLine1;
            address2 = dataHotel[0].AddressLine2;
        }
        else{
            address1 = dataHotel[0].PropertyName;
            address2 = dataHotel[0].FullAddress;
        }
        arrDisplay.push(
            <table width='630' key={uniqid()} >
                    <tr key={uniqid()}>
                        <td width='150' style={{width:'150px', verticalAlign:'top', textAlign:'center'}}>
                            <img alt='Red Planet Hotels' style={{'width':'150px', height:'97px'}} width='150' height='97' src='https://cdn.redplanethotels.com/signature/logo-300px.png' border='0'/>
                        </td>
                        <td width='480' style={{width:'480px', verticalAlign:'top', textAlign:'left'}}>
                            <table width='480'>
                                <tr>
                                    <td style={{'width':'480px', textTransform:'uppercase',fontSize:'12px', fontWeight:'bold' ,fontFamily:'arial, sans-serif'}}>
                                        {data[0].UserFName+' '+data[0].UserLName}
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{fontSize:'12px', fontFamily:'arial, sans-serif'}}>
                                    {data[0].JobTitle}
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{color:'#a2a1a0', 'font-size':'12px', fontFamily:'arial, sans-serif'}}>
                                    {address1}
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{color:'#a2a1a0', 'font-size':'12px', fontFamily:'arial, sans-serif'}}>
                                    {address2}
                                    </td>
                                </tr>
                                <tr>
                                    <td style={{fontSize:'12px', fontFamily:'arial, sans-serif'}}>
                                    {(data[0].DID_Phone != ''? <span style={{'color':'#ff2100'}}>T</span>:'')}
                                    {(data[0].DID_Phone != ''? data[0].DID_Phone:'')}
                                    {(data[0].DID_Phone != '' && data[0].Cell_Phone != ''? ' | ':'')}
                                    {(data[0].Cell_Phone != ''? <span style={{color:'#ff2100'}}>M</span>:'')}
                                    {(data[0].Cell_Phone != ''? data[0].Cell_Phone:'N/A')}
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <a className={'web-site'} style={{color:'#3e7afc',fontSize:'12px',fontFamily:'arial, sans-serif'}}
                                        href='https://www.redplanethotels.com'>redplanethotels.com
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
            </table>   
        );   
        arrDisplay.push(
            <img key={uniqid()} alt="Red Planet" src="https://cdn.redplanethotels.com/signature/default.jpg" border="0"/>
        );
        {/* arrDisplay.push( 
            <div  className={"columns"}  key={uniqid()} style={{marginTop:'-30px'}} >        
                <div className={"column"}  >
                    <ReactImageFallback
                    src={"https://cdn.redplanethotels.com/signature/default.jpg"}
                    fallbackImage={public_url + "/images/no.png"}
                    initialImage={public_url + "/images/loader.gif"}
                    alt="Red Planet"
                    />
                </div>
            </div>
        ); */}
    }
    return (
      <div className="notification" style={{marginLeft:'-15px', backgroundColor:'white'}}>
        <div>
            {arrDisplay}   
        </div>                   
      </div>
    );
  }
  async getProfileByUser() {
    //console.log("get_library");
    const userinfo = JSON.parse(localStorage.getItem("userinfo"));
    const usercode = (typeof this.props.location.pathname.split("/")[2] == 'undefined'?userinfo.usercode:this.props.location.pathname.split("/")[2]);
    //console.log(this.props.location.pathname.split("/")[2]);
    const param = {
      usercode: usercode
    }
    let { access_token = "" } = this.props.loginReducer;
    if (access_token === "") {
      const request_token = localStorage.getItem("request_token");
      await this.props.dispatch(actionGetAccessToken(request_token));
    }
    //console.log(param);
    await this.props.dispatch(actionLoadUserCodeData(access_token, 'usercode', param));
    //console.log(this.props.contactReducer.data[0]);
    if (this.props.contactReducer.data.length > 0) {
        const data = this.props.contactReducer.data[0];
       // console.log(data);
        let propertycode = '';
        if(data.EmployerProperty != '' && data.Office == 'Hotel'){
            propertycode = data.EmployerProperty
        }
        else if(data.Office == 'HQ'){
            propertycode = 'HQ01';
        }
        else if(data.Office == 'Office'){
            if(data.Country_of_working=='THA'){
                propertycode = 'HQ01';
            }   
            else
			{
				propertycode='R'+data.Country_of_working;
			}         
        }
        //console.log(propertycode);
        this.setState({
            loading: 1,
            data: this.props.contactReducer.data,
            firstLoadPage:false,
            PropertyCode:propertycode,
            PageTotal:this.props.contactReducer.data.length

        });
    } else {
        this.setState({
            loading: 1,
            data: [],
            firstLoadPage:false,
            PropertyCode:'',
            PageTotal:'0'
        });
    }
  }
  async getHotelByProperty() {
    //console.log("get_library");
    const {PropertyCode} = this.state;
    console.log(PropertyCode);
    const param = {
        propertycode: PropertyCode
    }
    let { access_token = "" } = this.props.loginReducer;
    if (access_token === "") {
      const request_token = localStorage.getItem("request_token");
      await this.props.dispatch(actionGetAccessToken(request_token));
    }
    //console.log(param);
    await this.props.dispatch(actionLoadHotelData(access_token, 'propertycode', param));
    //console.log(this.props.contactReducer.data);
    if (this.props.contactReducer.data.length > 0) {
      this.setState({
        loading: 1,
        dataHotel: this.props.contactReducer.data,
        firstLoadPage:false,
        PageTotal:this.props.contactReducer.data.length

      });
    } else {
      this.setState({
        loading: 1,
        dataHotel: [],
        firstLoadPage:false,
        PageTotal:'0'
      });
    }
  }
  async componentDidUpdate(prevProps) {
    //console.log("componentDidUpdate...");
  }
  async componentDidMount() {
   //console.log(this.props.location.pathname);
   await this.getProfileByUser();
    this.getHotelByProperty();
    await  this.props.dispatch(actionHiddenNavbar());
    await  this.props.dispatch(actionHiddenFooter());
  }
}
const mapStateToProps = state => {
  return {
    loginReducer: state.loginReducer,
    propertyReducer: state.propertyReducer,
    hrReducer: state.hrReducer,
    contactReducer: state.contactReducer,
    s3Reducer: state.s3Reducer
  };
};
export default connect(mapStateToProps)(IndexSignature);
