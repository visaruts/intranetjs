import React, { Component } from "react";
import { Prompt } from "react-router";
import { connect } from "react-redux";
import { lang } from "../Lang.jsx";
import { actionGetAccessToken } from "../actions/login";
import { actionLoadData } from "../actions/dashboard";
import qs from "query-string";
import { api, ENV, CONFIG, base_url } from "../Config_API.jsx";
import {TinyButton as ScrollUpButton} from "react-scroll-up-button";
import axios from "axios";
import { Link } from "react-router-dom";
import Parser from "html-react-parser";
import Select from 'react-select';
import _ from "lodash";
/** fontawesome */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
/** moment datetime */
import moment from 'moment';
import 'moment/min/locales.min';


import Pagination from "../components/Pagination.jsx";
/**
 * Option for page
 */
import Joi from "joi-browser";
import "animate.css/animate.min.css";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const schema = Joi.object().keys({
  From: Joi.string()
    .max(50)
    .required(),
    Subject: Joi.string()
    .max(250)
    .required(),
    To: Joi.string().required(),
    Description: Joi.string()
    .max(500)
    .required(),
    /*TicketID: Joi.string().max(50)*/
});


class EndorsementForm extends Component {
  state = {
    dataForm:{
      From:"",
      Subject:"",
      To:[],
      Description:"",
      TicketID:"",
      TicketData:{code:200,message:""}
    },
    upload: 0,
    userData:[],
    userDataOption:[],
    errors:[]
  };
  notify = (type, message) => {
    const config = {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true
    };
    if (type === "error") {
      toast.error(message, config);
    } else if (type === "success") {
      toast.success(message, config);
    } else if (type === "warn") {
      toast.warn(message, config);
    } else if (type === "info") {
      toast.info(message, config);
    }
  };

  
  //handleTicketID = _.debounce(this.handleTicketID, 600);

  render() {
    
    const { From, Subject, To, Description, TicketID, TicketData } = this.state.dataForm;
    moment.locale(lang("code"));
    //console.log(this.props);
    return <div className="container">
    
    <Prompt
          when={(Subject || "") !== ""}
          message={lang("leavePage")}
        />
    <ScrollUpButton />
    <div className="notification">
      <div className="has-text-right">
        <Link to="/endorsement" className="delete" aria-label="close"
          onClick={e=>{
            this.setState({
              dataForm:{
                Subject:""
              }
            });
          }}
        ></Link>
      </div>
      <div className="has-text-centered">
        <h1 className="title">
        {lang("Send endorsement")}{" "}
          {ENV !== "PROD" ? (
            <span className="has-text-danger"> ( {ENV} )</span>
          ) : (
            ""
          )}
        </h1>
      </div>
      <br />
      <div>
        {/*JSON.stringify(this.state.dataForm.To)*/}
        <div className="columns">
          
          <div className="column">
            <div className="field">
              <div className="control has-text-danger">
                  <strong>{lang("From")}:</strong>
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <input
                    name="From"
                    className="input "
                    type="text"
                    onChange={e => this.handleChangeDataFrom(e, "From")}
                    value={From}
                  />
                </div>
                
                <div className="has-text-danger">
                  {this.displayErrorMessage("From")}
                </div>
              </div>
            
              <div className="field">
                <div className="control has-text-danger">
                  <strong>{lang("Subject")}:</strong>
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <input
                    name="Subject"
                    className="input "
                    type="text"
                    onChange={e => this.handleChangeDataFrom(e, "Subject")}
                    value={Subject}
                  />
                </div>
                
                <div className="has-text-danger">
                  {this.displayErrorMessage("Subject")}
                </div>
              </div>
            
              <div className="field">
                <div className="control has-text-danger">
                  <strong>{lang("Description")}:</strong>
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <textarea
                    name="Description"
                    className="textarea "
                    rows="5"
                    onChange={e => this.handleChangeDataFrom(e, "Description")}
                    value={Description}
                  ></textarea>
                </div>
                
                <div className="has-text-danger">
                  {this.displayErrorMessage("Description")}
                </div>
              </div>


              <div className="field">
                <div className="control has-text-danger">
                  <strong>{lang("To")}:</strong>
                </div>
              </div>
              <div className="field">
                <div className="control">
                <Select
                  className="basic-single "
                  isMulti
                  classNamePrefix="select"
                  value={To}
                  isDisabled={false}
                  isLoading={false}
                  isClearable={true}
                  isRtl={false}
                  isSearchable={true}
                  name="To"
                  options={this.state.userDataOption}
                  onChange={this.handleChangeTo}
                  placeholder={lang("Select")+"..."}
                />
                </div>
                
                <div className="has-text-danger">
                  {this.displayErrorMessage("To")}
                </div>
              </div>

              <div className="field">
                <div className="control has-text-danger">
                  <strong>{lang("Ticket")}#:</strong>
                </div>
              </div>
              <div className="field">
                <div className="control">
                  <input
                    name="TicketID"
                    className="input "
                    type="text"
                    onChange={e => {
                      this.handleTicketID(e.target.value)
                    }}
                    value={TicketID}
                  />

                </div>
                
                <div className={(TicketData.code===200)?"has-text-info":"has-text-danger"}>
                  {
                    (TicketData.message!=="" && TicketData.code===200)?
                      <div>
                        <strong>{lang("Defects")}: </strong>{TicketData.data.Defects} <br />
                        <strong>{lang("Location")}: </strong>{TicketData.data.Location} <br />
                        <strong>{lang("From")}: </strong>{TicketData.data.Name} 
                      </div>
                    :""
                  }
                  {TicketData.code!==200?Parser(TicketData.message):""}
                </div>
              </div>
              
              <hr />
              <div className="filed">
                <div className="control has-text-centered">
                  <button style={{marginRight:"3px"}}
                    className={
                      this.state.upload === 0
                        ? "button is-link"
                        : "button is-link is-loading"
                    }
                    onClick={e => {
                      this.handleSubmit(e);
                    }}
                    disabled={this.state.upload === 0 ? "" : "disabled"}
                  >
                    <span className="ico">
                      <i className="fab fa-telegram-plane" /> {lang("Send")}
                    </span>
                  </button>
                  <Link to="/endorsement" className="button is-black">
                    <span className="ico">
                      <i className="fas fa-times-circle" /> {lang("Cancel")}
                    </span>
                  </Link>
                </div>
              </div>
          </div>            
        </div>
        
      </div>
    </div>
    

  </div>;
  }
  async handleSubmit(e) {
    e.preventDefault();

    let { access_token = "" } = this.props.loginReducer;
    const { From, Subject, To, Description, TicketID } = this.state.dataForm;
    let To_text="";
    if(To.length>0)
    {
      To.map(each=>{
        To_text+=";"+each.value;
      })
    }
    const { error } = Joi.validate(
      { From, Subject, To:To_text, Description },
      schema,
      {
        abortEarly: false
      }
    );
    const errors = error ? error.details : [];
    console.log(errors);
    await this.setState({
      errors
    });

    if(errors.length===0){
      const url = api.MA_API + "endorsement";
      const data = { 
        EnFromName: From,
        EnSubject: Subject,
        EnTo: To_text.substr(1),
        EnDescription: Description,
        RelateTicketID: TicketID
      };
      try {
        const res = await axios.post(url, data, {
          headers: { Authorization: "Bearer " + this.props.loginReducer.access_token }
        });
        if(res.data.code===200)
        {
          this.notify("success", lang("Send Endorsement Success"));
          await this.setState({
            dataForm:{
              From:"",
              Subject:"",
              To:[],
              Description:"",
              TicketID:"",
              TicketData:{code:200,message:""}
            }
          });
          this.props.history.push("/endorsement");
        }
        else{
          console.log(res);
          this.notify("error", lang("Send Endorsement Fail"));
        }
      } catch (err) {
        console.log({ err });
        this.notify("error", lang("Send Endorsement Fail"));
        this.notify("error", err.message);
        //window.location.href = "/connection_error?err="+err.message+"&page=ItemDefect&method=handleSubmit";
      }
    }
  }
  
  async readEndorsement(EnID){
    const {access_token}=this.props.loginReducer;
    const url=api.MA_API +
    "endorsement/message/"+EnID;
    //console.log(url);

    const res_message = await axios.get(
      url,
      {
        headers: {
          Authorization: "Bearer " + access_token,
          "Content-Type": "application/json; charset=utf-8"
        }
      }
    );
    //console.log(res_message);
    return res_message.data.data[0];
  }

  async componentDidMount(){
    console.log("componentDidMount");
    const {Action, EnID}=this.props.match.params;
    const request_token = localStorage.getItem("request_token") || "";
    let access_token="";
    if (request_token !== "") {
      await this.props.dispatch(actionGetAccessToken(request_token));
      access_token = this.props.loginReducer.access_token;
    }
    //console.log(this.props);
    let state={};
    if(Action==="new"){
      state={
        access_token,
        dataForm:{
          ...this.state.dataForm,
          From: this.props.loginReducer.userinfo.firstname + " " + this.props.loginReducer.userinfo.lastname
        }
      }
      await this.setState(state);
    }else if(Action==="reply" || Action==="replyall" || Action==="forward"){
      const endorsement=await this.readEndorsement(EnID);
      console.log(endorsement);
      let To=[];
      let prefix="";
      if(Action==="reply" || Action==="replyall"){
        To=[{
          value: endorsement.EnFrom,
          label: endorsement.EnFromName
        }];
        prefix="RE";
      }
      else if(Action==="forward"){
        prefix="FW";
      }

      if(Action==="replyall"){
        const enTo=endorsement.EnTo.split(";");
        //console.log(enTo);
        const enToName=endorsement.EnToName.split(";");
        //console.log(enToName);
        enTo.map((each,key)=>{
          if(each!==To[0].value){
            To.push({value: each, label: enToName[key]});
          }
        });
      }
      state={
        access_token,
        dataForm:{
          ...this.state.dataForm,
          From: this.props.loginReducer.userinfo.firstname + " " + this.props.loginReducer.userinfo.lastname,
          Subject: prefix+": "+endorsement.EnSubject,
          To,
          Description: 
            "\n\n"+("-".repeat(50))+"\n"+
            lang("From")+": "+endorsement.EnFromName+"\n"+
            lang("To")+": "+endorsement.EnToName+"\n"+
            lang("Time")+": "+((moment(endorsement.CreateDate).format('LLLL')==="Invalid date")?
            "":moment(endorsement.CreateDate).format('LLLL'))+"\n"+
            lang("Subject")+": "+endorsement.EnSubject+"\n"+
            "\n"+
            endorsement.EnDescription.replace(new RegExp("<br />", 'g'),"\n")
        }
      }
      await this.setState(state);
      await this.handleTicketID(endorsement.RelateTicketID);
    }
    
    
    this.getUserEndorsement();
  }
  async componentDidUpdate(prevProps){
    
    console.log("componentDidUpdate");
    const queryString = qs.parse(this.props.location.search);
    const queryString_prev = qs.parse(prevProps.location.search);

    const prevPage = queryString_prev.p || this.state.page;
    const page = queryString.p || this.state.page;
    if (prevPage !== page) {
      //this.getMessage(this.state.selectedFolder, page, "");
    }
  }
  async getUserEndorsement(){
    const {access_token}=this.state;
    if(this.state.userData.length===0){
      const res = await axios.get(
        api.MA_API +
          "user/endorsement",

        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        }
      );
      const {data}=res.data;
      let userDataOption=[];
      data.map(each=>{
        userDataOption.push({value: each.UserCode, label: each.UserFName+" "+each.UserLName})
      });
      //console.log(userDataOption);
      
      this.setState({
        userData: res.data,
        userDataOption
      });
    }
  }
  handleChangeTo = (selectedOption) => {
    //console.log(selectedOption);
    if(selectedOption){
      //const {value=undefined}=selectedOption;
      this.setState({
        dataForm:{
          ...this.state.dataForm,
          To: selectedOption
        }
      })
    }else{
      this.setState({
        dataForm:{
          ...this.state.dataForm,
          To: undefined
        }
      })
    }
  }
  handleChangeDataFrom(e, key) {
    //console.log(e.target.type);
    let val = "";
    val = e.target.value;
    const dataForm = { ...this.state.dataForm, [key]: val };
    this.setState({
      dataForm
    });
  }
  displayErrorMessage(fieldName, fieldType = "text") {
    const { errors } = this.state;
    //console.log(errors);
    let error = {};
    let message = "";
    error = errors.find(each => {
      message = lang(each.context.key + "." + each.type) || "";
      return each.context.key === fieldName;
    });

    return error
      ? 
        message !== ""
        ? message
        : error.message
      : "";
  }

  errorClassName(fieldName, default_class = "input") {
    return this.displayErrorMessage(fieldName) === ""
      ? default_class
      : default_class + " is-danger animated shake";
  }

  async handleTicketID(TicketID){
    if(TicketID.length>=15){
      const res = await axios.get(
        api.MA_API +
          "dashboard/ticket?ticketID="+
          TicketID,

        {
          headers: {
            Authorization: "Bearer " + this.state.access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        }
      );
      const {data}=res.data;
      if(data.length===1)
      {
        await this.setState({
          dataForm:{
            ...this.state.dataForm,
            TicketID,
            TicketData: {
              code:200,
              data:data[0],
              message: "success"
            }
          }
        });
      }
      else{
        await this.setState({
          dataForm:{
            ...this.state.dataForm,
            TicketID,
            TicketData: {
              code:404,
              message:lang("Not found TicketID")
            }
          }
        });
      }
      console.log(this.state.dataForm.TicketData);
    }else{
      if(TicketID.length>=1){
        await this.setState({
          dataForm:{
            ...this.state.dataForm,
            TicketID,
            TicketData: {
              code:404,
              message:lang("Not found TicketID")
            }
          }
        });
      }else{
        await this.setState({
          dataForm:{
            ...this.state.dataForm,
            TicketID,
            TicketData: {
              code:200,
              message:""
            }
          }
        });
      }
    }
  }

}
const mapStateToProps = state => {
  return { 
    landReducer: state.langReducer, 
    loginReducer: state.loginReducer,
    dashboardReducer: state.dashboardReducer 
  };
};
export default connect(mapStateToProps)(EndorsementForm);
