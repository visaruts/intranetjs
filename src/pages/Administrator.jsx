import React, { Component } from "react";
import { connect } from "react-redux";
import { lang } from "../Lang.jsx";
import { actionGetAccessToken } from "../actions/login";
import { actionUsers } from "../actions/users";
import qs from "query-string";
import { api, ENV, CONFIG, base_url } from "../Config_API.jsx";
import {TinyButton as ScrollUpButton} from "react-scroll-up-button";
import axios from "axios";
import { Link } from "react-router-dom";
import Parser from "html-react-parser";
/** fontawesome */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import ItemPermissionModule from "../components/ItemPermissionModule.jsx";

class Administrator extends Component {
  state={
    access_token:"",
    users:[]
  }
  render() {
    //console.log(this.state);
    const {access_token, users}=this.state;
    return <div className="notification">
      <ScrollUpButton />
      {
        access_token!==""?
          <div>
            
            <ItemPermissionModule 
              access_token={access_token}
              modulecode="MANAGEAPPLICATION"
              users={users}
            />
            <ItemPermissionModule 
              access_token={access_token}
              modulecode="MD_DASHBOARD"
              users={users}
            />
            <ItemPermissionModule 
              access_token={access_token}
              modulecode="ADMIN_DASHBOARD"
              users={users}
            />
          </div>:
      ""}

    </div>;
  }
  async componentDidMount(){

    let { access_token = "", access_token_exp = 0 } = this.props.loginReducer; //this.state;
    //console.log(this.props.loginReducer);
    if (access_token === "" || access_token_exp < Date.now()) {
      const request_token = localStorage.getItem("request_token");
      await this.props.dispatch(actionGetAccessToken(request_token));
      //console.log(request_token);
    }
    access_token = this.props.loginReducer.access_token;
    await this.props.dispatch(actionUsers(access_token));
    //access_token=this.props.access_token
    await this.setState({
      access_token,
      users:this.props.usersReducer.users
    });

    
  }
            
  
}
const mapStateToProps = state => {
  return { landReducer: state.langReducer, 
    loginReducer: state.loginReducer, 
    usersReducer: state.usersReducer };
};
export default connect(mapStateToProps)(Administrator);

