/**
 * Require to all page
 */
import React, { Component } from "react";
import { public_url } from "Config_API.jsx";
export default class Error404 extends Component {
  render() {
    return (
      <div>
        <div className="notification">
            <div className="has-text-centered">
            <img src={public_url + "images/error404.png"} srcSet={public_url + " /images/error404.png"} alt="404 Not found"/>
              </div>
          </div>
        </div>
        );
      }
    }
