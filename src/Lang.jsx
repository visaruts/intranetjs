import en from "./lang/en";
import ja from "./lang/ja";
import th from "./lang/th";
const lang = (payload, data=[]) => {
  //const lang = JSON.parse(localStorage.getItem("lang"));
  // console.log("lang");
  const langCode = localStorage.getItem("langCode");
  let lang = {};
  switch (langCode) {
    case "ja":
      lang = ja;
      break;

    case "th":
      lang = th;
      break;

    default:
      lang = en;
      break;
  }
  let str = lang[payload];
  if(data.length > 0){
    data.map((each,key) => str=str.replace("%"+key.toString(),each));
  }
  return str;
};
const langCode = localStorage.getItem("langCode");

export { lang, langCode };
