const HIDDEN_NAVBAR = "HIDDEN_NAVBAR";
const HIDDEN_FOOTER = "HIDDEN_FOOTER";
const actionHiddenNavbar = payload => {
  localStorage.setItem("HIDDEN_NAVBAR", payload);
  return {
    type: HIDDEN_NAVBAR,
    payload
  };
};
const actionHiddenFooter = payload => {
  localStorage.setItem("HIDDEN_FOOTER", payload);
  return {
    type: HIDDEN_FOOTER,
    payload
  };
};

export { HIDDEN_NAVBAR, HIDDEN_FOOTER, actionHiddenNavbar, actionHiddenFooter };
