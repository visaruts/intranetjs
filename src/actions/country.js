const LOAD_COUNTRY = "LOAD_COUNTRY";
const CHANGE_COUNTRY = "CHANGE_COUNTRY";
const actionLoadCountry = () => {
  return {
    type: LOAD_COUNTRY,
    payload: localStorage.getItem("countryCode")
  };
};
const actionChangeCountry = payload => {
  localStorage.setItem("countryCode", payload);
  return {
    type: LOAD_COUNTRY,
    payload
  };
};
export { LOAD_COUNTRY, CHANGE_COUNTRY, actionLoadCountry, actionChangeCountry };
