const LOAD_LANG = "LOAD_LANG";
const CHANGE_LANG = "CHANGE_LANG";
const actionLoadLang = () => {
  return {
    type: LOAD_LANG,
    payload: localStorage.getItem("countryCode")
  };
};
const actionChangeLang = payload => {
  localStorage.setItem("langCode", payload);
  return {
    type: LOAD_LANG,
    payload
  };
};
export { LOAD_LANG, CHANGE_LANG, actionLoadLang, actionChangeLang };
