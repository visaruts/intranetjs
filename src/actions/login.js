import axios from "axios";
import { api } from "../Config_API.jsx";
const AUTH_LOGIN_START = "AUTH_LOGIN_START";
const AUTH_LOGIN_FINISH = "AUTH_LOGIN_FINISH";
const AUTH_FROM_STORAGE = "AUTH_FROM_STORAGE";
const AUTH_LOGOUT = "AUTH_LOGOUT";
const AUTH_USERINFO = "AUTH_USERINFO";
const ACCESS_TOKEN_STORAGE = "ACCESS_TOKEN_STORAGE";

const API_URL = api.LOGIN_API;

const actionAuthFromStorage = request_token => {
  return {
    type: AUTH_FROM_STORAGE,
    payload: request_token
  };
};
const actionAuthLogout = () => {
  localStorage.setItem("request_token", "");
  return {
    type: AUTH_LOGOUT
  };
};

const actionAuthLogin = (username, password) => async dispatch => {
  const url = API_URL + "user/login";
  //console.log(url);
  dispatch({
    type: AUTH_LOGIN_START
  });
  try {
    const data = {
      username,
      password
    };
    let payload = {};
    try {
      const res = await axios.post(url, data);
      //console.log(data);
      //console.log(res);
      if (res.data.code === 200) {
        localStorage.setItem("request_token", res.data.request_token);
      }
      payload = res.data;
    } catch (err) {
      console.log({ err });
      localStorage.setItem("request_token", "");
      payload = { ...err.response.data, request_token: "" };
    }

    dispatch({
      type: AUTH_LOGIN_FINISH,
      payload: payload
    });
  } catch (err) {
    console.log(err);
    dispatch({
      type: AUTH_LOGIN_FINISH
    });
  }
  //return Promise.resolve();
};
const actionGetAccessToken = request_token => async dispatch => {

  const url = API_URL + "user/request_access_token";
  //console.log(url);
  dispatch({
    type: AUTH_LOGIN_START
  });
  try {
    const data = {};
    let payload = {};
    try {
      if (request_token !== "") {
        const res = await axios.post(url, data, {
          headers: { Authorization: "Bearer " + request_token }
        });
        //console.log(data);
        //console.log(res);
        if (res.data.code === 200) {
          localStorage.setItem("access_token", res.data.access_token);
          localStorage.setItem("userinfo", JSON.stringify(res.data.userinfo));
        } else if (res.data.code === 403) {
          localStorage.setItem("access_token", "");
          localStorage.setItem("request_token", "");
        } else {
          localStorage.setItem("request_token", "");
        }
        payload = res.data;
      }
    } catch (err) {
      console.log({ err });
      if (err.message === "Network Error") {
        //window.location.href = "/connection_error";
        console.log({ err });
      } else if (err.response.data.message === "jwt expired") {
        localStorage.setItem("request_token", "");
        window.location.href = "/login";
      } else {
        localStorage.setItem("access_token", "");
        payload = { ...err.response.data, access_token: "" };
      }
    }

    dispatch({
      type: ACCESS_TOKEN_STORAGE,
      payload: payload
    });
  } catch (err) {
    console.log(err);
    dispatch({
      type: AUTH_LOGIN_FINISH
    });
  }
  //return Promise.resolve();
};
export {
  AUTH_LOGIN_START,
  AUTH_LOGIN_FINISH,
  actionAuthLogin,
  actionAuthFromStorage,
  AUTH_FROM_STORAGE,
  AUTH_LOGOUT,
  actionAuthLogout,
  actionGetAccessToken,
  AUTH_USERINFO,
  ACCESS_TOKEN_STORAGE
};
