import axios from "axios";
import { api } from "../Config_API.jsx";
const AUTH_APPLICATION_START = "AUTH_APPLICATION_START";
const AUTH_APPLICATION_FINISH = "AUTH_APPLICATION_FINISH";
const AUTH_APPLICATION_ERROR = "AUTH_APPLICATION_ERROR";
const AUTH_USER_START = "AUTH_USER_START";
const AUTH_USER_FINISH = "AUTH_USER_FINISH";
const AUTH_USER_ERROR = "AUTH_USER_ERROR";

// const API_URL = api.LOGIN_API;

const actionGetApplication = (access_token) => async dispatch => {
  //console.log(url);
  dispatch({
    type: AUTH_APPLICATION_START
  });
  try {
    let payload = {};
    try {
      const res = await axios.get(
        api.INTRANET_API +
          "/user/application",

        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        }
      );
      // const {data}=res.data;
      payload = res.data;
      // console.log(payload);
      dispatch({
        type: AUTH_APPLICATION_FINISH,
        application:payload.data
      });
    } catch (err) {
      console.log({ err });
      localStorage.setItem("request_token", "");
      dispatch({
        type: AUTH_APPLICATION_ERROR
      });
    }

  } catch (err) {
    console.log(err);
    dispatch({
      type: AUTH_APPLICATION_ERROR
    });
  }
  //return Promise.resolve();
};
const actionUsers = (access_token) => async dispatch => {
  //console.log(url);
  dispatch({
    type: AUTH_USER_START
  });
  try {
    let payload = {};
    try {
      const res = await axios.get(
        api.LOGIN_API +
          "user",

        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        }
      );
      const {data}=res.data;
      payload = res.data;
      //console.log(payload);
      dispatch({
        type: AUTH_USER_FINISH,
        users:payload[0]
      });
    } catch (err) {
      console.log({ err });
      localStorage.setItem("request_token", "");
      dispatch({
        type: AUTH_USER_ERROR
      });
    }

  } catch (err) {
    console.log(err);
    dispatch({
      type: AUTH_APPLICATION_ERROR
    });
  }
  //return Promise.resolve();
};
export {
  AUTH_APPLICATION_START,
  AUTH_APPLICATION_ERROR,
  AUTH_APPLICATION_FINISH,
  AUTH_USER_START,
  AUTH_USER_ERROR,
  AUTH_USER_FINISH,
  actionGetApplication,
  actionUsers
};
