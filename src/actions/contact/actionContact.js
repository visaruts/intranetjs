import axios from "axios";
import { api } from "../../Config_API.jsx";

const CONTACT_LOADDATA = "CONTACT_LOADDATA";
const CONTACT_GET_USER = "CONTACT_GET_USER";
const CONTACT_SAVEDATA = "CONTACT_SAVEDATA";

const actionLoadContactData = (access_token,Type) => async dispatch => {
  try {
    let payload = {};
  
    try {
      const res = await axios.get(
        api.INTRANET_API +
        "/contact/"+Type,
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        }
      );
      const {data}=res.data;
      //console.log(data);
      let recordsTotal = 0;
      if(data.length>0){
        recordsTotal=data.length;
      }
      dispatch({
        type: CONTACT_LOADDATA,
        data:data
      });
    } catch (err) {
      console.log({ err });
      if (err.message === "Network Error") {
        window.location.href = "/connection_error?err="+err.message;
      } else {
        payload = { ...err.response.data, message: err.message };
      }
      
      /*dispatch({
        type: MYLIST_LOADDATA
      });*/
      dispatch({
        type: CONTACT_LOADDATA,
        data:[]
      });
    }
  } catch (err) {
    console.log(err);
    /*dispatch({
      type: MYLIST_LOADDATA
    });*/
    dispatch({
      type: CONTACT_SAVEDATA,
      data:[]
    });
  }
};
const actionLoadHotelContactData = (access_token,Type, Pram) => async dispatch => {
  const {
    CountryCode,
    Search
  } =Pram ;
  try {
    let payload = {};
  
    try {
      const res = await axios.get(
        api.INTRANET_API +
        "/contact/"+Type+"?CountryCode="+CountryCode+"&Search="+Search,
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        }
      );
      const {data}=res.data;
      //console.log(data);
      let recordsTotal = 0;
      if(data.length>0){
        recordsTotal=data.length;
      }
      dispatch({
        type: CONTACT_LOADDATA,
        data:data
      });
    } catch (err) {
      console.log({ err });
      if (err.message === "Network Error") {
        window.location.href = "/connection_error?err="+err.message;
      } else {
        payload = { ...err.response.data, message: err.message };
      }
      
      /*dispatch({
        type: MYLIST_LOADDATA
      });*/
      dispatch({
        type: CONTACT_LOADDATA,
        data:[]
      });
    }
  } catch (err) {
    console.log(err);
    /*dispatch({
      type: MYLIST_LOADDATA
    });*/
    dispatch({
      type: CONTACT_SAVEDATA,
      data:[]
    });
  }
};
const actionLoadEmployeeContactData = (access_token,Type, Pram) => async dispatch => {
  const {
    CountryCode,
    Search
  } =Pram ;
  try {
    let payload = {};
  
    try {
      const res = await axios.get(
        api.INTRANET_API +
        "/contact/"+Type+"?CountryCode="+CountryCode+"&Search="+Search,
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        }
      );
      const {data}=res.data;
      //console.log(data);
      let recordsTotal = 0;
      if(data.length>0){
        recordsTotal=data.length;
      }
      dispatch({
        type: CONTACT_LOADDATA,
        data:data
      });
    } catch (err) {
      console.log({ err });
      if (err.message === "Network Error") {
        window.location.href = "/connection_error?err="+err.message;
      } else {
        payload = { ...err.response.data, message: err.message };
      }
      
      /*dispatch({
        type: MYLIST_LOADDATA
      });*/
      dispatch({
        type: CONTACT_LOADDATA,
        data:[]
      });
    }
  } catch (err) {
    console.log(err);
    /*dispatch({
      type: MYLIST_LOADDATA
    });*/
    dispatch({
      type: CONTACT_SAVEDATA,
      data:[]
    });
  }
};
const actionLoadUserCodeData = (access_token,Type, Pram) => async dispatch => {
  const {
    usercode
  } =Pram ;
  try {
    let payload = {};
  
    try {
      const res = await axios.get(
        api.INTRANET_API +
        "/user/"+Type+"?usercode="+usercode,
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        }
      );
      const {data}=res.data;
      //console.log(data);
      let recordsTotal = 0;
      if(data.length>0){
        recordsTotal=data.length;
      }
      dispatch({
        type: CONTACT_LOADDATA,
        data:data
      });
    } catch (err) {
      console.log({ err });
      if (err.message === "Network Error") {
        window.location.href = "/connection_error?err="+err.message;
      } else {
        payload = { ...err.response.data, message: err.message };
      }
      
      /*dispatch({
        type: MYLIST_LOADDATA
      });*/
      dispatch({
        type: CONTACT_LOADDATA,
        data:[]
      });
    }
  } catch (err) {
    console.log(err);
    /*dispatch({
      type: MYLIST_LOADDATA
    });*/
    dispatch({
      type: CONTACT_SAVEDATA,
      data:[]
    });
  }
};
const actionLoadHotelData = (access_token,Type, Pram) => async dispatch => {
  const {
    propertycode
  } =Pram ;
  try {
    let payload = {};
  
    try {
      const res = await axios.get(
        api.INTRANET_API +
        "/user/"+Type+"?propertycode="+propertycode,
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        }
      );
      const {data}=res.data;
      //console.log(data);
      let recordsTotal = 0;
      if(data.length>0){
        recordsTotal=data.length;
      }
      dispatch({
        type: CONTACT_LOADDATA,
        data:data
      });
    } catch (err) {
      console.log({ err });
      if (err.message === "Network Error") {
        window.location.href = "/connection_error?err="+err.message;
      } else {
        payload = { ...err.response.data, message: err.message };
      }
      
      /*dispatch({
        type: MYLIST_LOADDATA
      });*/
      dispatch({
        type: CONTACT_LOADDATA,
        data:[]
      });
    }
  } catch (err) {
    console.log(err);
    /*dispatch({
      type: MYLIST_LOADDATA
    });*/
    dispatch({
      type: CONTACT_SAVEDATA,
      data:[]
    });
  }
};
const actionLoadEntityData = (access_token,Type, Pram) => async dispatch => {
  const {
    isactive
  } =Pram ;
  //console.log(Pram);
  try {
    let payload = {};
  
    try {
      const res = await axios.get(
        api.INTRANET_API +
        "/user/"+Type+"?isactive="+isactive,
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        }
      );
      const {data}=res.data;
      //console.log(data);
      let recordsTotal = 0;
      if(data.length>0){
        recordsTotal=data.length;
      }
      dispatch({
        type: CONTACT_LOADDATA,
        data:data
      });
    } catch (err) {
      console.log({ err });
      if (err.message === "Network Error") {
        window.location.href = "/connection_error?err="+err.message;
      } else {
        payload = { ...err.response.data, message: err.message };
      }
      
      /*dispatch({
        type: MYLIST_LOADDATA
      });*/
      dispatch({
        type: CONTACT_LOADDATA,
        data:[]
      });
    }
  } catch (err) {
    console.log(err);
    /*dispatch({
      type: MYLIST_LOADDATA
    });*/
    dispatch({
      type: CONTACT_SAVEDATA,
      data:[]
    });
  }
};
export { 
  CONTACT_LOADDATA,CONTACT_GET_USER,  
  actionLoadContactData, 
  actionLoadHotelContactData, 
  actionLoadEmployeeContactData, 
  actionLoadUserCodeData, 
  actionLoadHotelData,
  actionLoadEntityData
};
