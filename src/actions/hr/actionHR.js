import axios from "axios";
import { api } from "../../Config_API.jsx";

const HR_LOADDATA = "HR_LOADDATA";
const HR_GET_USER = "HR_GET_USER";
const HR_SAVEDATA = "HR_SAVEDATA";

const actionLoadHRData = (access_token,Type, Pram) => async dispatch => {
  const {
    AnnouncementID,
    Category,
    Department,
    StartDate,
    EndDate,
    Country,
    PinnedCalendar,
    CreateBy,
    AnnouncementStatus,
    PageNum,
    PageSize,
    Search,
    Subtemplate
  } =Pram ;
  try {
    let payload = {};
    // const type="announcement";
    ///console.log("Subtemplate:"+Subtemplate);
    const SubtemplateTmp  = typeof Subtemplate ==="undefined"?"": Subtemplate;
    try {
      const res = await axios.get(
        api.INTRANET_API +
        "/hr/"+Type+"?AnnouncementID="+AnnouncementID+"&Category="+Category+"&Department="+Department
        +"&StartDate="+StartDate+"&EndDate="+EndDate+"&Country="+Country+"&PinnedCalendar="+PinnedCalendar+"&CreateBy="+CreateBy
        +"&AnnouncementStatus="+AnnouncementStatus+"&PageNum="+PageNum+"&PageSize="+PageSize+"&Search="+Search+"&Subtemplate="+ SubtemplateTmp,
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        }
      );
      const {data}=res.data;
      //console.log(data);
      let recordsTotal = 0;
      if(data.length>0){
        recordsTotal=data.length;
      }
      dispatch({
        type: HR_LOADDATA,
        data:data,
        PageSize,PageNum,
        recordsTotal,res
      });
    } catch (err) {
      console.log({ err });
      if (err.message === "Network Error") {
        window.location.href = "/connection_error?err="+err.message;
      } else {
        payload = { ...err.response.data, message: err.message };
      }
      
      /*dispatch({
        type: MYLIST_LOADDATA
      });*/
      dispatch({
        type: HR_LOADDATA,
        data:[],
         PageSize,PageNum,
        recordsTotal:0
      });
    }
  } catch (err) {
    console.log(err);
    /*dispatch({
      type: MYLIST_LOADDATA
    });*/
    dispatch({
      type: HR_SAVEDATA,
      data:[],
      PageSize,PageNum,
      recordsTotal:0
    });
  }
};
const actionGetUserByHR = (access_token,Type, Pram) => async dispatch => {
  const {
    Entity,
    Property,
    Office,
    JobDept,
    JobTitle,
    Status,
    Country,
    PageNum,
    PageSize,
    Search
  } =Pram ;
  try {
    let payload = {};
    try {
      const res = await axios.get(
        api.INTRANET_API +
        "/hr/"+Type+"?Entity="+Entity+"&Property="+Property+"&Office="+Office
        +"&JobDept="+JobDept+"&JobTitle="+JobTitle+"&Status="+Status+"&Country="+Country+"&PageNum="+PageNum+"&PageSize="+PageSize+"&Search="+Search,
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        }
      );
      const {data}=res.data;
      //console.log(data);
      let recordsTotal = 0;
      if(data.length>0){
        recordsTotal=data.length;
      }
      dispatch({
        type: HR_GET_USER,
        data:data,
        PageSize,PageNum,
        recordsTotal,res
      });
    } catch (err) {
      console.log({ err });
      if (err.message === "Network Error") {
        window.location.href = "/connection_error?err="+err.message;
      } else {
        payload = { ...err.response.data, message: err.message };
      }
      dispatch({
        type: HR_GET_USER,
        data:[],
        PageSize,PageNum,
        recordsTotal:0
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({
      type: HR_GET_USER,
      data:[],
      PageSize,PageNum,
      recordsTotal:0
    });
  }
};
const actionSaveHRData = (payload) => {
  return {
    type: HR_SAVEDATA,
    ...payload
  };
};

export { HR_LOADDATA, HR_SAVEDATA,HR_GET_USER, actionLoadHRData, actionSaveHRData,actionGetUserByHR };
