import axios from "axios";
import { api } from "../../Config_API.jsx";

const S3_LOADDATA = "S3_LOADDATA";
const actionLoadS3Data = (access_token, Param) => async dispatch => {
  const {
    AppName,
    Public,
    PathFile
  } =Param ;
  try {
    let payload = {};
    try {
      const res = await axios.get(
        api.INTRANET_API +"/s3?AppName="+AppName+"&Public="+Public+"&PathFile="+PathFile
        ,
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        }
      );
      const {data}=res;
      let recordsTotal = 0;
      // console.log(res);
      if(data.length>0){
        recordsTotal=data.length;
      }
      dispatch({
        type: S3_LOADDATA,
        data:data,
        recordsTotal
      });
    } catch (err) {
      console.log({ err });
      if (err.message === "Network Error") {
        window.location.href = "/connection_error?err="+err.message;
      } else {
        payload = { ...err.response.data, message: err.message };
      }
      dispatch({
        type: S3_LOADDATA,
        data:[],
        recordsTotal:0
      });
    }
  } catch (err) {
    console.log(err);
    dispatch({
      type: S3_LOADDATA,
      data:[],
      recordsTotal:0
    });
  }
};

export { S3_LOADDATA, actionLoadS3Data };
