import axios from "axios";
import { api } from "../Config_API.jsx";

const ENTITY_USER = "ENTITY_USER";

const API_URL = api.LOGIN_API;

const actionGetEntityByUserCode = (access_token) => async dispatch => {
  const url = API_URL + "user/entity";
  try {
    const data = {};
    let payload = {};
    try {
      const res = await axios.post(url, data, {
        headers: { Authorization: "Bearer " + access_token , "Content-Type": "application/json; charset=utf-8"}
      });
      payload = res.data;
    } catch (err) {
      console.log({ err });
      if (err.message === "Network Error") {
        window.location.href = "/connection_error";
      } else {
        payload = { ...err.response.data, message: err.message };
      }
    }
  
    dispatch({
      type: ENTITY_USER,
      payload: payload
    });
  } catch (err) {
    console.log(err);
    dispatch({
      type: ENTITY_USER
    });
  }
};
const actionSaveEntityData = (payload) => {
  return {
    type: ENTITY_USER,
    ...payload
  };
};
export { ENTITY_USER, actionGetEntityByUserCode,actionSaveEntityData};
