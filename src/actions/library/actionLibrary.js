import axios from "axios";
import { api } from "../../Config_API.jsx";

const LIBRARY_LOADDATA = "LIBRARY_LOADDATA";
const LIBRARY_GET_USER = "LIBRARY_GET_USER";
const LIBRARY_SAVEDATA = "LIBRARY_SAVEDATA";

const actionLoadLibraryData = (access_token,Type, Pram) => async dispatch => {
  const {
    DocumentNo,
    Category,
    PageNum,
    PageSize,
    Search,
    CountryCode,
    UserCode
  } =Pram ;
  try {
    let payload = {};
  
    try {
      const res = await axios.get(
        api.INTRANET_API +
        "/library/"+Type+"?DocumentNo="+DocumentNo+"&Category="+Category+"&PageNum="+PageNum+"&PageSize="+PageSize+"&Search="+Search+"&CountryCode="+CountryCode+"&UserCode="+UserCode,
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        }
      );
      const {data}=res.data;
      //console.log(data);
      let recordsTotal = 0;
      if(data.length>0){
        recordsTotal=data.length;
      }
      dispatch({
        type: LIBRARY_LOADDATA,
        data:data
      });
    } catch (err) {
      console.log({ err });
      if (err.message === "Network Error") {
        window.location.href = "/connection_error?err="+err.message;
      } else {
        payload = { ...err.response.data, message: err.message };
      }
      
      /*dispatch({
        type: MYLIST_LOADDATA
      });*/
      dispatch({
        type: LIBRARY_LOADDATA,
        data:[]
      });
    }
  } catch (err) {
    console.log(err);
    /*dispatch({
      type: MYLIST_LOADDATA
    });*/
    dispatch({
      type: LIBRARY_SAVEDATA,
      data:[]
    });
  }
};
const actionLoadUserApplicationData = (access_token,Type, Pram) => async dispatch => {
  const {
    AppCode,
    ModuleCode,
    UserCode
  } =Pram ;
  try {
    let payload = {};
  
    try {
      const res = await axios.get(
        api.INTRANET_API +
        "/library/"+Type+"?AppCode="+AppCode+"&ModuleCode="+ModuleCode+"&UserCode="+UserCode,
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        }
      );
      const {data}=res.data;
      //console.log(data);
      let recordsTotal = 0;
      if(data.length>0){
        recordsTotal=data.length;
      }
      dispatch({
        type: LIBRARY_LOADDATA,
        data:data
      });
    } catch (err) {
      console.log({ err });
      if (err.message === "Network Error") {
        window.location.href = "/connection_error?err="+err.message;
      } else {
        payload = { ...err.response.data, message: err.message };
      }
      
      /*dispatch({
        type: MYLIST_LOADDATA
      });*/
      dispatch({
        type: LIBRARY_LOADDATA,
        data:[]
      });
    }
  } catch (err) {
    console.log(err);
    /*dispatch({
      type: MYLIST_LOADDATA
    });*/
    dispatch({
      type: LIBRARY_SAVEDATA,
      data:[]
    });
  }
};
export { LIBRARY_LOADDATA,LIBRARY_GET_USER,  actionLoadLibraryData, actionLoadUserApplicationData };
