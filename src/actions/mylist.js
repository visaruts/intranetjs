import axios from "axios";
import { api } from "../Config_API.jsx";

const MYLIST_LOADDATA = "MYLIST_LOADDATA";
const MYLIST_SAVEDATA = "MYLIST_SAVEDATA";

const actionLoadMyListData = (access_token, propertyCode, search, page_start, page_length, order, status, urgent, from, to) => async dispatch => {
  try {
    let payload = {};
    const type="MYLIST";
    try {
      const res = await axios.get(
        api.MA_API +
          "dashboard?type="+
          type+
          "&propertycode=" +
          propertyCode +
          "&search=" +
          search +
          "&start=" +
          page_start +
          "&length=" +
          page_length +
          "&status=" +
          status +
          "&urgent=" +
          urgent +
          "&order=" +
          order +
          "&from=" +
          from +
          "&to=" +
          to,

        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        }
      );
      const {data}=res.data;
      let recordsTotal = 0;
      if(data.length>0){
        recordsTotal=data[0].TotalRows;
      }
      
      dispatch({
        type: MYLIST_SAVEDATA,
        data,
        propertyCode, search, page_start, page_length, order, status, urgent, from, to,
        recordsTotal
      });
    } catch (err) {
      console.log({ err });
      if (err.message === "Network Error") {
        window.location.href = "/connection_error?err="+err.message;
      } else {
        payload = { ...err.response.data, message: err.message };
      }
      
      /*dispatch({
        type: MYLIST_LOADDATA
      });*/
      dispatch({
        type: MYLIST_SAVEDATA,
        data:[],
        propertyCode, search, page_start, page_length, order, status, urgent, from, to,
        recordsTotal:0
      });
    }
  } catch (err) {
    console.log(err);
    /*dispatch({
      type: MYLIST_LOADDATA
    });*/
    dispatch({
      type: MYLIST_SAVEDATA,
      data:[],
      propertyCode, search, page_start, page_length, order, status, urgent, from, to,
      recordsTotal:0
    });
  }
};
const actionSaveMyListData = (payload) => {
  return {
    type: MYLIST_SAVEDATA,
    ...payload
  };
};

export { MYLIST_LOADDATA, MYLIST_SAVEDATA, actionLoadMyListData, actionSaveMyListData };
