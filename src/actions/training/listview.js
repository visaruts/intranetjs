import axios from "axios";
import { api } from "../../Config_API.jsx";

const TNLIST_LOADDATA = "TNLIST_LOADDATA";
const TNLIST_SAVEDATA = "TNLIST_SAVEDATA";

const actionLoadTNListData = (access_token,Type, UserCode, PageSize,PageNum,Query) => async dispatch => {
  try {
    let payload = {};
    const type="MYLIST";
    try {
      const res = await axios.get(
        api.INTRANET_API +
        "/training/"+Type+"?UserCode="+UserCode+"&PageSize="+PageSize+"&PageNum="+PageNum+"&Query="+Query
         ,
        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        }
      );
      const {data}=res.data;
      let recordsTotal = 0;
      if(data.length>0){
        recordsTotal=data[0].PageTotal;
      }
      dispatch({
        type: TNLIST_LOADDATA,
        data,
        UserCode, PageSize,PageNum,
        recordsTotal
      });
    } catch (err) {
      console.log({ err });
      if (err.message === "Network Error") {
        window.location.href = "/connection_error?err="+err.message;
      } else {
        payload = { ...err.response.data, message: err.message };
      }
      
      /*dispatch({
        type: MYLIST_LOADDATA
      });*/
      dispatch({
        type: TNLIST_SAVEDATA,
        data:[],
        UserCode, PageSize,PageNum,
        recordsTotal:0
      });
    }
  } catch (err) {
    console.log(err);
    /*dispatch({
      type: MYLIST_LOADDATA
    });*/
    dispatch({
      type: TNLIST_SAVEDATA,
      data:[],
      UserCode, PageSize,PageNum,
      recordsTotal:0
    });
  }
};
const actionSaveTNListData = (payload) => {
  return {
    type: TNLIST_SAVEDATA,
    ...payload
  };
};

export { TNLIST_LOADDATA, TNLIST_SAVEDATA, actionLoadTNListData, actionSaveTNListData };
