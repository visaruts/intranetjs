import axios from "axios";
import { api } from "../Config_API.jsx";
const PROPERTY_USER = "PROPERTY_USER";
const SAVE_PROPERTY = "SAVE_PROPERTY";

const API_URL = api.LOGIN_API;

const actionSavePropertyCode = payload => dispatch => {
  dispatch({
    type: SAVE_PROPERTY,
    payload: payload
  });
};

const actionGetPropertyByUserCode = (
  access_token,
  onlyHotel = 0
) => async dispatch => {
  const url = API_URL + "user/property";
  //console.log("access_token: " + access_token);
  try {
    const data = { onlyHotel };
    let payload = {};
    try {
      if (access_token !== "") {
        const res = await axios.post(url, data, {
          headers: { Authorization: "Bearer " + access_token }
        });
        payload = res.data;
        //console.log({ payload });
      }
    } catch (err) {
      console.log({ err });
      if (err.message === "Network Error") {
        window.location.href = "/connection_error";
      } else {
        payload = { ...err.response.data, message: err.message };
      }
    }

    dispatch({
      type: PROPERTY_USER,
      payload: payload
    });
  } catch (err) {
    console.log(err);
    dispatch({
      type: PROPERTY_USER
    });
  }
};
export {
  PROPERTY_USER,
  actionGetPropertyByUserCode,
  SAVE_PROPERTY,
  actionSavePropertyCode
};
