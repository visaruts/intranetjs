import axios from "axios";
import { api } from "../Config_API.jsx";

const GET_JOBDEPT = "GET_JOBDEPT";
const GET_JOBTITLE = "GET_JOBTITLE";
const GET_COUNTRY = "GET_COUNTRY";
const API_URL = api.INTRANET_API;
const actionGetJobDept = (access_token) => async dispatch => {
  const url = API_URL + "/user/jobdept";
  try {
    const data = {};
    let payload = {};
    try {
      const res = await axios.get(url, data, {
        headers: { Authorization: "Bearer " + access_token , "Content-Type": "application/json; charset=utf-8"}
      });
      payload = res.data.data;
      payload = {code:res.data.code,message:res.data.message,JobDept:payload}
      // console.log(payload);
    } catch (err) {
      console.log({ err });
      if (err.message === "Network Error") {
        window.location.href = "/connection_error";
      } else {
        payload = { ...err.response.data, message: err.message };
      }
    }
    dispatch({
      type: GET_JOBDEPT,
      payload: payload
    });
  } catch (err) {
    console.log(err);
    dispatch({
      type: GET_JOBDEPT
    });
  }
};
const actionGetJobTitle = (access_token) => async dispatch => {
  const url = API_URL + "/user/jobtitle";
  try {
    const data = {};
    let payload = {};
    try {
      const res = await axios.get(url, data, {
        headers: { Authorization: "Bearer " + access_token , "Content-Type": "application/json; charset=utf-8"}
      });
      payload = res.data.data;
      payload = {code:res.data.code,message:res.data.message,JobTitle:payload}
    } catch (err) {
      console.log({ err });
      if (err.message === "Network Error") {
        window.location.href = "/connection_error";
      } else {
        payload = { ...err.response.data, message: err.message };
      }
    }
    dispatch({
      type: GET_JOBTITLE,
      payload: payload
    });
  } catch (err) {
    console.log(err);
    dispatch({
      type: GET_JOBTITLE
    });
  }
};

export { GET_JOBDEPT, GET_JOBTITLE, actionGetJobDept, actionGetJobTitle};
