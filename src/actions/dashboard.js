import axios from "axios";
import { api } from "../Config_API.jsx";

const LOADDATA = "LOADDATA";
const SAVEDATA = "SAVEDATA";

const actionLoadData = (access_token, propertyCode, search, page_start, page_length, order, status, urgent, from, to) => async dispatch => {
  try {
    let payload = {};
    const type="DASHBOARD";
    try {
      /*let txt_order = "";
      order.map((each, id) => {
        txt_order +=
          "&order[" +
          id +
          "][column]=" +
          each.column +
          "&order[" +
          id +
          "][dir]=" +
          each.dir;
      });*/
      const res = await axios.get(
        api.MA_API +
          "dashboard?type="+
          type+
          "&propertycode=" +
          propertyCode +
          "&search=" +
          search +
          "&start=" +
          page_start +
          "&length=" +
          page_length +
          "&status=" +
          status +
          "&urgent=" +
          urgent +
          "&order=" +
          order +
          "&from=" +
          from +
          "&to=" +
          to,

        {
          headers: {
            Authorization: "Bearer " + access_token,
            "Content-Type": "application/json; charset=utf-8"
          }
        }
      );
      const {data}=res.data;
      let recordsTotal = 0;
      if(data.length>0){
        recordsTotal=data[0].TotalRows;
      }
      
      dispatch({
        type: SAVEDATA,
        data,
        propertyCode, search, page_start, page_length, order, status, urgent, from, to,
        recordsTotal
      });
    } catch (err) {
      console.log({ err });
      /*if (err.message === "Network Error") {
        window.location.href = "/connection_error?err="+err.message+"&page=dashboard";
      } else {*/
        payload = { ...err.response.data, message: err.message };
      //}
      
      /*dispatch({
        type: LOADDATA
      });*/
      dispatch({
        type: SAVEDATA,
        data:[],
        propertyCode, search, page_start, page_length, order, status, urgent, from, to,
        recordsTotal:0,
        code: err.response.status,
        err:payload
      });
    }
  } catch (err) {
    console.log(err);
    /*dispatch({
      type: LOADDATA
    });*/
    dispatch({
      type: SAVEDATA,
      data:[],
      propertyCode, search, page_start, page_length, order, status, urgent, from, to,
      recordsTotal:0,
      code: 500,
      err: { ...err }
    });
  }
};
const actionSaveData = (payload) => {
  return {
    type: SAVEDATA,
    ...payload
  };
};

export { LOADDATA, SAVEDATA, actionLoadData, actionSaveData };
